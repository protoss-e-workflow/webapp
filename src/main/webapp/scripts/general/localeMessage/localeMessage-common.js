/**
 * 
 */

var object = $.extend({},UtilPagination);

function isZero(e){
	var check = false;
	if(!$(e).val() || $(e).val() == 0|| $(e).val() == '0'||$(e).val() == 0.0|| $(e).val() == '0.0'){
		check = true;
	}
	return check;
}

function setZeroWhenNull(e){
	if(isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}
}

function nvlNum(e){
	if(e == null || e == undefined || isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}
}

function nvl(e){
	if(e == null){
		return "";
	}else{
		return e;
	}
}

function numberWithCommas(x) {
    var data =  x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    if(data == null || data == undefined || isNaN(data) || !isFinite(data)  || data == "" ){
		return "0";
	}else{
		return data;
	}
}

function checkDupplicate(code,id){
	$.ajax({
        type: "GET",
        url: session['context']+'/localeMessage/findByCode?code='+code,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	if(xhr.responseText){
                		var userObj = JSON.parse(xhr.responseText);
                		if(userObj.id != undefined && id != userObj.id){
                        	$('#warningModal .modal-body').html($MESSAGE_HAS_DUPLICATE);
                			$('#warningModal').modal('show');
                        }else{
                        	save();
                        }
                	}else{
                		save();
                	}
                }
            }
        }
    }).done(function (){
        //close loader
    });
}

function save(){
	var jsonParams = {};
	
	
	jsonParams['parentId']     	= 1;
	jsonParams[csrfParameter]  	= csrfToken;
	jsonParams['id']     		= $('#modal_edit_id').val();
	jsonParams['createdBy'] 	= $('#modal_edit_createdBy').val();
	jsonParams['createdDate'] 	= $('#modal_edit_createdDate').val();
	jsonParams['updateBy'] 		= $('#modal_edit_updateBy').val();
	jsonParams['updateDate'] 	= $('#modal_edit_updateDate').val();

	jsonParams['code']     	    = $('#modal_edit_code').val();
	jsonParams['groupCode']     = $groupCode;
	for(var j=0;j<$PARAMETER_LANGUAGE_JSON.length;j++){
		var index = (j+1)+"";
		jsonParams['variableName'+index]   = $('#modal_edit_variable'+index).val();
	}
	
	var itemData = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/localeMessage',
        data: jsonParams,
        complete: function (xhr) {
        	search();
    		$('#editItemModal').modal('hide');
    		$('#successModal').modal('show');
        	 
        }
    }).done(function (){
        //close loader
    });
	
}

function edit(id){
	$.ajax({
        type: "GET",
        url: session['context']+'/localeMessage/'+id,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	var obj = JSON.parse(xhr.responseText);
                    
                    $('#modal_edit_id').val(obj.id);
            		$('#modal_edit_createdBy').val(obj.createdBy);
            		$('#modal_edit_createdDate').val(obj.createdDate);
            		$('#modal_edit_updateBy').val(obj.updateBy);
            		$('#modal_edit_updateDate').val(obj.updateDate);
            		$('#modal_edit_code').val(obj.code);
            		
            		
            		//Dynamic Field
            		for(var j=0;j<$PARAMETER_LANGUAGE_JSON.length;j++){
            			var index = (j+1)+"";
            			$('#modal_edit_variable'+index).val(obj['variableName'+index]);
            		}
            		
            		$('#editItemModal').modal('show');
            		
                }
            }
        }
    }).done(function (){
        //close loader
    });
	
}

function deleteFunction(id){
	//delete
	var jsonParams = {};
	jsonParams['parentId']     = 1;
	jsonParams[csrfParameter]  = csrfToken;
	
	$.ajax({
        type: "DELETE",
        url: session['context']+'/localeMessage/'+id,
        data: jsonParams,
        complete: function (xhr) {
        	search();
        	$('#deleteItemModal').modal('hide');
        }
    }).done(function (){
        //close loader
    });
}



function search(){
	
	var criteriaObject = {
			groupCode       : $groupCode,
			code            : $("#search_code").val()==""?"":$("#search_code").val(),
			fullText        : $("#search_name").val()==""?"":$("#search_name").val()
	    };
	queryUserByCriteria(criteriaObject);
}

function queryUserByCriteria(criteriaObject){
	object.setId("#paggingSearchMain");                
    object.setUrlData("/localeMessage/findByCriteria"); 
    object.setUrlSize("/localeMessage/findSize"); 
    
    object.loadTable = function(items){
    	var item = items.content;
        // $('.dv-background').show();
        $('#gridMainBody').empty();
        if(item.length > 0){
        	$dataForQuery = item;
            var itemIdTmp ;
            for(var j=0;j<item.length;j++){
            	
                var code       = item[j].code==null?"":item[j].code;
                
                itemIdTmp = item[j].id;
                if(itemIdTmp){

                	var recordsData = ''+
                            '<tr id="'+item[j].id+'" '+
                            ((j%2 == 0) ? ' class="trwhite" ><td class="text-center headcol tdFix"  style="width: 138px;" > ' : ' class="trlightgray" ><td class="text-center headcol tdFixGrey" style="width: 138px;" >')+
                            '	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Edit" data-toggle="modal"  onclick="edit('+item[j].id+')" ><span class="fa fa-pencil-square-o"><jsp:text/></span></button>'+
            				'	<button type="button" class="btn btn-material-red-500  btn-style-small"  title="Delete"  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+item[j].id+'\');$(\'#codeItemDelete\').html(\''+code+'\');"><span class="fa fa-trash"><jsp:text/></span></button>'+
            				'</td>'+
            				
                            ((j%2 == 0) ? '<td class="text-center  headcol tdFix"  style="width: 200px;left: 153px;"> ' : '<td class="text-center  headcol tdFixGrey"  style="width: 200px;left: 153px;">')+
                            code+'</td>';
                	
                	
                	for(var i=0;i<$PARAMETER_LANGUAGE_JSON.length;i++){
                		recordsData += '<td class="text-center">'+nvl(item[j]['variableName'+(i+1)])+'</td>';
                	}
                	
                	recordsData += '</tr>';
                	
                	$('#gridMainBody').append(recordsData);
                }
                
            }
            
            
        }else{
        	//Not Found
        }
        
    };

    object.setDataSearch(criteriaObject); 
    object.search(object); 
    object.loadPage(($CRITERIA_PAGE*1)+1,object);
}




$(document).ready(function () {
	
	$('#search_button').on('click', function () {
		search();
	});
	
	$('#editItemModal').on('hidden.bs.modal', function () {
		$('#modal_edit_id').val('');
		$('#modal_edit_createdBy').val('');
		$('#modal_edit_createdDate').val('');
		$('#modal_edit_updateBy').val('');
		$('#modal_edit_updateDate').val('');
		
		$('#modal_edit_code').val('');
		for(var j=0;j<$PARAMETER_LANGUAGE_JSON.length;j++){
			$('#modal_edit_variable'+(j+1)).val('');
		}
		
	});
	
	$('#editItemModalSave').on('click', function () {
		//validate Mandatory
		var errorMessage = "";
		
		if(!$('#modal_edit_code').val()){
			errorMessage += $MESSAGE_REQUIRE +" " + $LABEL_CODE+"<br/>";
		}
		
		//fetch dynamic require field
		$.each( $( ".requiredField" ), function( i, obj ) {
			if(!obj.value){
				errorMessage += $MESSAGE_REQUIRE +" " +obj.name+"<br/>";
			}
		});
		
		if(errorMessage){
			$('#warningModal .modal-body').html(errorMessage);
			$('#warningModal').modal('show');
		}else{
			//Check Mandatory Field
			checkDupplicate($('#modal_edit_code').val(),$('#modal_edit_id').val());
		}
		
	});
	
	
	
	
	
	search();
	
	$('#successModal').on('shown.bs.modal', function() {
		window.setTimeout(function(){
			$('#successModal').modal('hide');
		},1000);
	});
	

	
	$('.numbersOnly').on("keydown",function(e){
        NumberUtil.numberOnly(e);
    });
	
	$('.amount2digit').on("keydown",function(e){
        NumberUtil.numberOnly(e);
        NumberUtil.numberFixDigit($(this),2);
    });
	
	//Generate Main Table
	for(var j=0;j<$PARAMETER_LANGUAGE_JSON.length;j++){
		if($PARAMETER_LANGUAGE_JSON.length < 5){
			$('#gridHeader').append(''+
	                '<th class="text-center" style="width: 50%;">'+$PARAMETER_LANGUAGE_JSON[j]['variable1']+'</th>'
	        );
		}else{
			$('#gridHeader').append(''+
	                '<th class="text-center" style="width: 170px;">'+$PARAMETER_LANGUAGE_JSON[j]['variable1']+'</th>'
	        );
		}
		
	}
	
	//Generate edit modal
	for(var j=0;j<$PARAMETER_LANGUAGE_JSON.length;j++){
		
		$('#contentEdit').append(''+
                '<div class="form-group">'+
                '	<div class="control-label col-sm-3" style="top: 13px;"> '+
                '		<label style="color: #009688;" >'+$PARAMETER_LANGUAGE_JSON[j]['variable1']+' </label> '+
                '	</div> '+
                ' 	<div class="col-sm-8" style="padding-right: 3px;">'+
                ' 		<input type="text" class="form-control amount2digit"   id="modal_edit_variable'+(j+1)+'" style="color: #2196f3;font-size: 16px;"/> '+
                ' 	</div>'+
                ' 	<div class="col-sm-1" style="font-size: 18px;top: 6px;padding-left: 3px;">'+
                ' 		<label style="color: #ffffff;"><jsp:text/></label>'+
                ' 	</div>'+
                '</div>'
        );
    
	}
});

