/**
 * 
 */

var object = $.extend({},UtilPagination);

function isZero(e){
	var check = false;
	if(!$(e).val() || $(e).val() == 0|| $(e).val() == '0'||$(e).val() == 0.0|| $(e).val() == '0.0'){
		check = true;
	}
	return check;
}

function setZeroWhenNull(e){
	if(isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}
	
}

function checkDupplicate(roleName,id){
	$.ajax({
        type: "GET",
        url: session['context']+'/roles/findByRoleName?roleName='+roleName,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	if(xhr.responseText){
                		var userObj = JSON.parse(xhr.responseText);
                        if(id != userObj.id){
                        	$('#warningModal .modal-body').html($MESSAGE_HAS_DUPLICATE_ROLES);
                			$('#warningModal').modal('show');
                        }else{
                        	save();
                        }
                	}else{
                		save();
                	}
                }
            }
        }
    }).done(function (){
        //close loader
    });
}

function save(){
	var jsonParams = {};
	
	jsonParams['parentId']     	= 1;
	jsonParams[csrfParameter]  	= csrfToken;
	jsonParams['id']     		= $('#modal_edit_id').val();
	jsonParams['createdBy'] 	= $('#modal_edit_createdBy').val();
	jsonParams['createdDate'] 	= $('#modal_edit_createdDate').val();
	jsonParams['updateBy'] 		= $('#modal_edit_updateBy').val();
	jsonParams['updateDate'] 	= $('#modal_edit_updateDate').val();
	jsonParams['roleName']     	= $('#modal_edit_roleName').val();
	jsonParams['flagActive']    = $('#modal_edit_flag_active').prop('checked');
	
	var itemData = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/roles',
        data: jsonParams,
        complete: function (xhr) {
        	var userObj;
        	 if(userObj = JSON.parse(xhr.responseText)){
        		 //if(userObj.id){ }
        		 search();
        		 $('#editItemModal').modal('hide');
        	 }
        	 
        }
    }).done(function (){
        //close loader
    });
	
}

function edit(id){
	$.ajax({
        type: "GET",
        url: session['context']+'/roles/'+id,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	var obj = JSON.parse(xhr.responseText);
                    
                    $('#modal_edit_id').val(obj.id);
            		$('#modal_edit_createdBy').val(obj.createdBy);
            		$('#modal_edit_createdDate').val(obj.createdDate);
            		$('#modal_edit_updateBy').val(obj.updateBy);
            		$('#modal_edit_updateDate').val(obj.updateDate);
            		$('#modal_edit_roleName').val(obj.roleName);
            		
            		
            		$('#modal_edit_flagActive').val(obj.flagActive);
            		if(obj.flagActive){
            			$('#modal_edit_flag_active').prop('checked', true);
            		}else{
            			$('#modal_edit_flag_active').removeAttr( "checked" );
            		}
            		
            		$('#editItemModal').modal('show');
            		
                }
            }
        }
    }).done(function (){
        //close loader
    });
	
}

function deleteFunction(id){
    
	
	$.ajax({
        type: "GET",
        url: session['context']+'/roles/findUserByRole/'+id,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);
                    
                    if(obj){
                    	if(obj.content[0].id){
                    		//change flag
                    		$.ajax({
                    	        type: "GET",
                    	        url: session['context']+'/roles/'+id,
                    	        complete: function (xhr) {
                    	        	if (xhr.readyState == 4) {
                    	                if (xhr.status == 200) {
                    	                    var userObj = JSON.parse(xhr.responseText);
                    	                    
                    	                    $('#modal_edit_id').val(userObj.id);
                    	            		$('#modal_edit_createdBy').val(userObj.createdBy);
                    	            		$('#modal_edit_createdDate').val(userObj.createdDate);
                    	            		$('#modal_edit_updateBy').val(userObj.updateBy);
                    	            		$('#modal_edit_updateDate').val(userObj.updateDate);
                    	            		
                    	            		
                    	            		$('#modal_edit_roleName').val(userObj.roleName);
                    	            		$('#modal_edit_flag_active').removeAttr( "checked" );
                    	            		
                    	            		save();
                    	            		$('#deleteItemModal').modal('hide');
                    	            		$('#warningModal .modal-body').html($MESSAGE_DATA_REFERANCE );
                    	        			$('#warningModal').modal('show');
                    	                }
                    	            }
                    	        }
                    	    }).done(function (){
                    	        //close loader
                    	    });
                    	}else{
                    		//delete
                    		var jsonParams2 = {};
                    		jsonParams2['parentId']     = 1;
                    		jsonParams2[csrfParameter]  = csrfToken;
                    		
                    		$.ajax({
                    	        type: "DELETE",
                    	        url: session['context']+'/roles/'+id,
                    	        data: jsonParams2,
                    	        complete: function (xhr) {
                    	        	search();
                    	        	$('#deleteItemModal').modal('hide');
                    	        }
                    	    }).done(function (){
                    	        //close loader
                    	    });
                    	}
                    }
                    
                }
            }
        }
    }).done(function (){
        //close loader
    });
}



function search(){
	$('#gridMainBody').empty();
	for(var j=0;j<$mapListUser.length;j++){
		$('#gridMainBody').append(''+
                '<tr id="'+$mapListUser[j].sessionId+'" '+
                ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
                '<td class="text-center">'+$mapListUser[j].userName+'</td>'+
                '<td class="text-center">'+$mapListUser[j].sessionId+'</td>'+
                '<td class="text-center">'+
                '	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Detail" data-toggle="modal"  onclick="$(\'#response\').empty();document.getElementById(\'filterbox\').value =\''+$mapListUser[j].sessionId+'\' " ><span class="fa fa-filter"><jsp:text/></span></button>'+
				'	<button type="button" class="btn btn-material-red-500  btn-style-small hide"  title="Kill"  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+$mapListUser[j].sessionId+'\');$(\'#codeItemDelete\').html(\''+$mapListUser[j].userName+'\');"><span class="fa fa-times"><jsp:text/></span></button>'+
				'</td>'+
                '</tr>'
            );
	}
}


$(document).ready(function () {
	
	
	
	$('.numbersOnly').on("keydown",function(e){
        NumberUtil.numberOnly(e);
    });
	
	$('.amount2digit').on("keydown",function(e){
        NumberUtil.numberOnly(e);
        NumberUtil.numberFixDigit($(this),2);
    });
	
	search();
	
	
});

