/**
 * 
 */

var object = $.extend({},UtilPagination);

function isZero(e){
	var check = false;
	if(!$(e).val() || $(e).val() == 0|| $(e).val() == '0'||$(e).val() == 0.0|| $(e).val() == '0.0'){
		check = true;
	}
	return check;
}

function setZeroWhenNull(e){
	if(isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}
	
}

function saveUser(){
	//Define Role
	var rolesString = "";
	$.each( $( ".checkbox_roles:checked" ), function( i, obj ) {
		rolesString += ","+obj.value;
	});
	if(rolesString){
		rolesString = rolesString.substring(1);
		$('#modal_edit_roles').val(rolesString);
	}
	
	//Define Password
	var passwordStr = $('#modal_edit_accessToken_old').val();
	if($('#modal_edit_accessToken').val() != $('#modal_edit_accessToken_old').val()){
		passwordStr = $.md5($("#modal_edit_accessToken").val());
	}
	
	
	
	var jsonParams = {};
	
	jsonParams['parentId']     	= 1;
	jsonParams[csrfParameter]  	= csrfToken;
	jsonParams['id']     		= $('#modal_edit_id').val();
	jsonParams['username']     	= $('#modal_edit_username').val();
	jsonParams['firstName']  	= $('#modal_edit_firstname').val();
	jsonParams['lastName']   	= $('#modal_edit_lastname').val();
	jsonParams['accessToken']  	= passwordStr;
	jsonParams['email'] 		= $('#modal_edit_email').val();
	jsonParams['description'] 	= $('#modal_edit_description').val();
	jsonParams['accountNonExpired'] 	= $('#modal_edit_flag_accountNonExpired').prop('checked');
	jsonParams['credentialsNonExpired'] = $('#modal_edit_flag_credentialsNonExpired').prop('checked');
	jsonParams['accountNonLocked'] 		= $('#modal_edit_flag_accountNonLocked').prop('checked');
	jsonParams['createdBy'] 			= $('#modal_edit_createdBy').val();
	jsonParams['createdDate'] 			= $('#modal_edit_createdDate').val();
	jsonParams['updateBy'] 				= $('#modal_edit_updateBy').val();
	jsonParams['updateDate'] 			= $('#modal_edit_updateDate').val();
	
	var itemData = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/users',
        data: jsonParams,
        complete: function (xhr) {
        	var userObj;
        	 if(userObj = JSON.parse(xhr.responseText)){
        		 if(userObj.id){
        			 var jsonRoleParams = {};
            		 jsonRoleParams['roles']  	= $('#modal_edit_roles').val();
            		 $.ajax({
            		        type: "POST",
            		        headers: {
            		            Accept: 'application/json'
            		        },
            		        url: session['context']+'/users/'+userObj.id+'/roles',
            		        data: jsonRoleParams,
            		        complete: function (xhr) {
            		        	
            		        	searchUser();
            		        	$('#editItemModal').modal('hide');
            		        }
            		    }).done(function (){
            		        //close loader
            		    });
        		 }
        		 
        		 var openFeatureImage = true;
        		 if( openFeatureImage){//$fileUpload && $fileUpload.name
        			 var formData = new FormData();
        			 formData.append("file", document.getElementById('canvas-userImage').toDataURL()); //$fileUpload
        			 formData.append("filename",  $.md5(userObj.id) );
        			 
        			 $.ajax({
        			        type: "POST",
        			        headers: {
        			            Accept: 'application/json'
        			        },
        			        contentType: "application/json; charset=utf-8",
        			        dataType: "json",
        			        url: session['context']+ '/users/uploadUserImage',
        			        processData: false,
        			        contentType: false,
        			        data: formData,
        			        async: false,
        			        complete: function (xhr) {
        			            if (xhr.readyState == 4) {
        			                if (xhr.status == 200) {
        			                    //success
        			                }
        			                else if (xhr.status == 500) {
        			                    //unsuccess
        			                }
        			            } else {
        			                //unsuccess
        			            }
        			        }
        			    });
        		 }
        		 
        	 }
        	 
        }
    }).done(function (){
        //close loader
    });

}

function editUser(user_id){
	$.ajax({
        type: "GET",
        url: session['context']+'/users/'+user_id,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var userObj = JSON.parse(xhr.responseText);
                    
                    $('#modal_edit_id').val(userObj.id);
            		$('#modal_edit_createdBy').val(userObj.createdBy);
            		$('#modal_edit_createdDate').val(userObj.createdDate);
            		$('#modal_edit_updateBy').val(userObj.updateBy);
            		$('#modal_edit_updateDate').val(userObj.updateDate);
            		$('#modal_edit_username').val(userObj.username);
            		$('#modal_edit_firstname').val(userObj.firstName);
            		$('#modal_edit_lastname').val(userObj.lastName);
            		$('#modal_edit_accessToken').val(userObj.accessToken);
            		$('#modal_edit_accessToken_old').val(userObj.accessToken);
            		$('#modal_edit_email').val(userObj.email);
            		$('#modal_edit_description').val(userObj.description);
            		$('#modal_edit_roles').val(userObj.roles);
            		
            		$('#modal_edit_accountNonExpired').val(userObj.accountNonExpired);
            		if(userObj.accountNonExpired){
            			$('#modal_edit_flag_accountNonExpired').prop('checked', true);
            		}else{
            			$('#modal_edit_flag_accountNonExpired').removeAttr( "checked" );
            		}
            		
            		$('#modal_edit_credentialsNonExpired').val(userObj.credentialsNonExpired);
            		if(userObj.credentialsNonExpired){
            			$('#modal_edit_flag_credentialsNonExpired').prop('checked', true);
            		}else{
            			$('#modal_edit_flag_credentialsNonExpired').removeAttr( "checked" );
            		}
            		
            		
            		$('#modal_edit_accountNonLocked').val(userObj.accountNonLocked);
            		if(userObj.accountNonLocked){
            			$('#modal_edit_flag_accountNonLocked').prop('checked', true);
            		}else{
            			$('#modal_edit_flag_accountNonLocked').removeAttr( "checked" );
            		}
            		
            		for (var i = 0; i < userObj.authorities.length; i++) {
                    	$('#checkbox_roles_'+userObj.authorities[i].id).prop('checked', true);
                    }
            		
            		$.ajax({
            	        type: "GET",
            	        url: session['context']+'/users/loadUserImage/'+user_id,
            	        complete: function (xhr2) {
            	        	if (xhr2.readyState == 4) {
            	                if (xhr2.status == 200) {
            	                	if(xhr2.responseText){
            	                		$('#userImage').attr('src', xhr2.responseText);
            	                		var image = new Image();
            	                		image.onload = function() {
            	                			document.getElementById('canvas-userImage').getContext('2d').drawImage(image,0, 0, 300, 300);
            	                		};
            	                		image.src =xhr2.responseText;
            	                		
            	                	}else{
            	                		document.getElementById('canvas-userImage').getContext('2d').drawImage(document.getElementById('originalImage'),0, 0, 300, 300);
            	                	}
            	                }
            	            }
            	        }
            	    }).done(function (){
            	        //close loader
            	    	$('#editItemModal').modal('show');
            	    });
            		
                }
            }
        }
    }).done(function (){
        //close loader
    });
	
}

function deleteUser(user_id){
    
	
	$.ajax({
        type: "GET",
        url: session['context']+'/users/'+user_id,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var userObj = JSON.parse(xhr.responseText);
                    
                   
                    if(userObj){
                    	var rolesString = "";
                    	if(userObj.authorities.length){
                    		for (var i = 0; i < userObj.authorities.length; i++) {
                    			rolesString += ","+userObj.authorities[i].id;
                            }
                    		
                    	}
                    	if(rolesString){
                    		rolesString = rolesString.substring(1);
                    		
                    		var jsonParams = {};
                    		jsonParams['parentId']     = 1;
                    		jsonParams[csrfParameter]  = csrfToken;
                    		//jsonParams['roles']        = rolesString;
                    		
                    		$.ajax({
                    	        type: "POST",
                    	        url: session['context']+'/users/'+user_id+"/roles",
                    	        data: jsonParams,
                    	        complete: function (xhr) {
                    	        	var jsonParams2 = {};
                            		jsonParams2['parentId']     = 1;
                            		jsonParams2[csrfParameter]  = csrfToken;
                            		
                            		$.ajax({
                            	        type: "DELETE",
                            	        url: session['context']+'/users/'+user_id,
                            	        data: jsonParams2,
                            	        complete: function (xhr) {
                            	        	searchUser();
                            	        	$('#deleteItemModal').modal('hide');
                            	        }
                            	    }).done(function (){
                            	        //close loader
                            	    });
                    	        }
                    	    }).done(function (){
                    	        //close loader
                    	    });
                    	}else{
                    		var jsonParams2 = {};
                    		jsonParams2['parentId']     = 1;
                    		jsonParams2[csrfParameter]  = csrfToken;
                    		
                    		$.ajax({
                    	        type: "DELETE",
                    	        url: session['context']+'/users/'+user_id,
                    	        data: jsonParams2,
                    	        complete: function (xhr) {
                    	        	searchUser();
                    	        	$('#deleteItemModal').modal('hide');
                    	        }
                    	    }).done(function (){
                    	        //close loader
                    	    });
                    	}
                    	
                    }
                    
                }
            }
        }
    }).done(function (){
        //close loader
    });
}



function searchUser(){
	var firstname = "";
	var lastname = "";
	if($("#search_fullname").val()){
		if($("#search_fullname").val().split(" ").length > 0){
			firstname = $("#search_fullname").val().split(" ")[0];
		}
		if($("#search_fullname").val().split(" ").length > 1){
			lastname = $("#search_fullname").val().split(" ")[1];
		}
	}
	
	//Define Role
	var rolesString = "";
	$.each( $( ".checkbox_roles_search:checked" ), function( i, obj ) {
		rolesString += ","+obj.value;
	});
	
	if(rolesString){
		rolesString = rolesString.substring(1);
		$('#search_roles').val(rolesString);
	}else{
		$('#search_roles').val($('#search_roles_all').val());
	}
	
	var criteriaObject = {
	        username     : $("#search_username").val()==""?"":$("#search_username").val(),
	        fullName     : $("#search_fullname").val()==""?"":$("#search_fullname").val(),
	        roles        : $("#search_roles").val()==""?"":$("#search_roles").val()
	    };
	queryUserByCriteria(criteriaObject);
}

function queryUserByCriteria(criteriaObject){
	object.setId("#paggingSearchMain");                
    object.setUrlData("/users/findByCriteria"); 
    object.setUrlSize("/users/findSize"); 
    
    object.loadTable = function(items){
    	var item = items.content;
        // $('.dv-background').show();
        $('#gridMainBody').empty();
        if(item.length > 0){
        	$dataForQuery = item;
            var itemIdTmp ;
            for(var j=0;j<item.length;j++){

                var username = item[j].username==null?"":item[j].username;
                var firstName = item[j].firstName==null?"":item[j].firstName;
                var lastName = item[j].lastName==null?"":item[j].lastName;
                var email = item[j].email==null?"":item[j].email;
                var activeFlag = item[j].accountNonLocked==null?"":item[j].accountNonLocked;
                

                var roles = "";
                if(item[j].authorities){
                	 var authoritiesStr = "";
                	 for(var k=0;k<item[j].authorities.length;k++){
                		 authoritiesStr += ","+item[j].authorities[k].roleName;
                	 }
                	 if(authoritiesStr){
                		 roles = authoritiesStr = authoritiesStr.substring(1);
                	 }
                }
                
                
                itemIdTmp = item[j].id;
                if(itemIdTmp){
                	$('#gridMainBody').append(''+
                            '<tr id="'+item[j].id+'" '+
                            ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
                            '<td class="text-center">'+username+'</td>'+
                            '<td class="text-center">'+firstName+' '+lastName+'</td>'+
                            '<td class="text-center">'+email+'</td>'+
                            '<td class="text-center">'+roles+'</td>'+
                            '<td class="text-center">'+
                            ((activeFlag) ? '<img class="img-circle" src="'+$IMAGE_CHECKED+'" style="width: 25px;height: 25px;" />':'<img class="img-circle" src="'+$IMAGE_CANCEL+'" style="width: 25px;height: 25px;" />')+
                            '</td>'+
                            '<td class="text-center">'+
                            '	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Edit" data-toggle="modal"  onclick="editUser('+item[j].id+')" ><span class="fa fa-pencil-square-o"><jsp:text/></span></button>'+
            				'	<button type="button" class="btn btn-material-red-500  btn-style-small"  title="Delete"  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+item[j].id+'\');$(\'#codeItemDelete\').html(\''+username+'\');"><span class="fa fa-trash"><jsp:text/></span></button>'+
            				'</td>'+
                            '</tr>'
                        );
                }
                
            }
            
            
        }else{
        	//Not Found
        }
        
    };

    object.setDataSearch(criteriaObject); 
    object.search(object); 
    object.loadPage(($CRITERIA_PAGE*1)+1,object);
}

function loadAllRole(){
	$('#checkbox_roles_data').empty();
	$('#checkbox_search_roles_data').empty();
	var roleData = $.ajax({
        type: "GET",
        url: session['context']+'/roles/',
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var roles = JSON.parse(xhr.responseText).content;
                    var rolesAll = "";
                    for(var i=0;i<roles.length;i++){
                    	rolesAll += ","+roles[i].id;
                    	$('#checkbox_roles_data').append(''+
                                '<div class="checkbox col-sm-6" style="color: #2196f3;margin-top: 10px;">'+
                                '<label><input type="checkbox" class="checkbox_roles" id="checkbox_roles_'+roles[i].id+'" value="'+roles[i].id+'" /><span class="checkbox-material" style="left: 30px;margin-right: 30px;"><span class="check" style="right: 5px;" ></span></span>'+roles[i].roleName+'</label>'+
                                '</div>'
                            );
                    	$('#checkbox_search_roles_data').append(''+
                                '<div class="checkbox col-sm-3" style="color: #2196f3;margin-top: 10px;">'+
                                '<label><input type="checkbox" class="checkbox_roles_search" id="checkbox_roles_search_'+roles[i].id+'"  value="'+roles[i].id+'" /><span class="checkbox-material" style="left: 30px;margin-right: 30px;"><span class="check" style="right: 5px;" ></span></span>'+roles[i].roleName+'</label>'+
                                '</div>'
                            );
                    }
                    
                    if(rolesAll){
                    	rolesAll = rolesAll.substring(1);
                		$('#search_roles_all').val(rolesAll);
                	}
                    
                    if($CRITERIA_ROLES){
                    	var rolesLs = $CRITERIA_ROLES.split(',');
                    	for(var i=0;i<rolesLs.length;i++){
                    		$('#checkbox_roles_search_'+rolesLs[i]).prop('checked', true);
                    	}
                    }
                    
                   
                    searchUser();
                	
                }
            }
        }
    }).done(function (){
        //close loader
    	
    });
}

function checkUserDupplicate(username,userid){
	$.ajax({
        type: "GET",
        url: session['context']+'/users/findByUsername?username='+username,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	if(xhr.responseText){
                		var userObj = JSON.parse(xhr.responseText);
                        if(userid != userObj.id){
                        	$('#warningModal .modal-body').html($MESSAGE_HAS_DUPLICATE_USERNAME);
                			$('#warningModal').modal('show');
                        }else{
                        	saveUser();
                        }
                	}else{
                		saveUser();
                	}
                }
            }
        }
    }).done(function (){
        //close loader
    });
}

var $fileUpload ;

function readURL(input) {
    if (input.files && input.files[0]) {
    	$fileUpload = input.files[0];
        var reader = new FileReader();

        
        var c = document.getElementById("canvas-userImage");
		var ctx = c.getContext("2d");
		
		var image = new Image();
		
        reader.onload = function (e) {
        	image.src = e.target.result;
        };
        
		image.onload = function(){
			ctx.drawImage(image,0, 0, 300, 300);
		};
		
        reader.readAsDataURL(input.files[0]);
    }
}

$(document).ready(function () {
	
	// Put event listeners into place
	window.addEventListener("DOMContentLoaded", function() {
		// Grab elements, create settings, etc.
        var canvas = document.getElementById('canvas');
        var context = canvas.getContext('2d');
        var video = document.getElementById('video');
        var mediaConfig =  { video: true };
        var errBack = function(e) {
        	console.log('An error has occurred!', e)
        };

		// Put video listeners into place
		
        if(navigator.mediaDevices ) {
        	if(navigator.mediaDevices.getUserMedia){
        		navigator.mediaDevices.getUserMedia(mediaConfig).then(function(stream) {
                    video.src = window.URL.createObjectURL(stream);
                    video.play();
                });
        	}
        }

        /* Legacy code below! */
        else if(navigator.getUserMedia) { // Standard
			navigator.getUserMedia(mediaConfig, function(stream) {
				video.src = stream;
				video.play();
			}, errBack);
		} else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
			navigator.webkitGetUserMedia(mediaConfig, function(stream){
				video.src = window.webkitURL.createObjectURL(stream);
				video.play();
			}, errBack);
		} else if(navigator.mozGetUserMedia) { // Mozilla-prefixed
			navigator.mozGetUserMedia(mediaConfig, function(stream){
				video.src = window.URL.createObjectURL(stream);
				video.play();
			}, errBack);
		}

		// Trigger photo take
		document.getElementById('capture_button').addEventListener('click', function() {
			context.drawImage(video, 0, 0, 400, 300);
			$('#div_capture_user').addClass('hide');
			$('#div_image_user').removeClass('hide');
			
			var c = document.getElementById("canvas-userImage");
			var ctx = c.getContext("2d");
			
			var image = new Image();
			image.onload = function(){
				ctx.drawImage(image,0, 0, 300, 300);
			};
			image.src = canvas.toDataURL();
		});
	}, false);
	
	
	
	$('#userImage').ready(function () {
		var c = document.getElementById("canvas-userImage");
		var ctx = c.getContext("2d");
		
		var image = new Image();
		image.onload = function(){
			ctx.drawImage(image,0, 0, 300, 300);
		};
		image.src = document.getElementById('userImage').src;
		
		//document.getElementById('canvas-userImage').getContext('2d').drawImage(document.getElementById('userImage'),0, 0, 300, 300);
	});
	
	$('#userImage').change(function () {
		var c = document.getElementById("canvas-userImage");
		var ctx = c.getContext("2d");
		
		var image = new Image();
		image.onload = function(){
			ctx.drawImage(image,0, 0, 300, 300);
		};
		image.src = document.getElementById('userImage').src;
		//document.getElementById('canvas-userImage').getContext('2d').drawImage(document.getElementById('userImage'),0, 0, 300, 300);
	});
	
	
	$('.numbersOnly').on("keydown",function(e){
        NumberUtil.numberOnly(e);
    });
	
	$('.amount2digit').on("keydown",function(e){
        NumberUtil.numberOnly(e);
        NumberUtil.numberFixDigit($(this),2);
    });
	
	$('#search_button').on('click', function () {
		searchUser();
	});
	
	$('#editItemModal').on('hidden.bs.modal', function () {
		$('#modal_edit_id').val('');
		$('#modal_edit_createdBy').val('');
		$('#modal_edit_createdDate').val('');
		$('#modal_edit_updateBy').val('');
		$('#modal_edit_updateDate').val('');
		$('#modal_edit_username').val('');
		$('#modal_edit_firstname').val('');
		$('#modal_edit_lastname').val('');
		$('#modal_edit_accessToken').val('');
		$('#modal_edit_email').val('');
		$('#modal_edit_description').val('');
		$('#modal_edit_roles').val('');
		$('#modal_edit_accountNonExpired').val('');
		$('#modal_edit_credentialsNonExpired').val('');
		$('#modal_edit_accountNonLocked').val('');
		$('#modal_edit_flag_accountNonExpired').removeAttr( "checked" );
		$('#modal_edit_flag_credentialsNonExpired').removeAttr( "checked" );
		$('#modal_edit_flag_accountNonLocked').removeAttr( "checked" );
		$('.checkbox_roles').removeAttr( "checked" );
		$('#userImage').attr('src', $CONST_IMAGE_PERSON);
		document.getElementById('canvas-userImage').getContext('2d').drawImage(document.getElementById('originalImage'),0, 0, 300, 300);
		document.getElementById('form_fileupload') .reset();
    	
	});
	
	
	
	$('#search_image_button').on('click', function () {
		$('#userImageFile').click();
	});
	
	$('#camera_button').on('click', function () {
		$('#div_image_user').addClass('hide');
		$('#div_capture_user').removeClass('hide');
	});
	
	
	
	$('#editItemModalSave').on('click', function () {
		//validate Mandatory
		var errorMessage = "";
		
		
		if(!$('#modal_edit_username').val()){
			errorMessage += $MESSAGE_REQUIRE_USERNAME+"<br/>";
		}
		if(!$('#modal_edit_accessToken').val()){
			errorMessage += $MESSAGE_REQUIRE_PASSWORD+"<br/>";
		}
		if($( ".checkbox_roles:checked" ).length==0){
			errorMessage += $MESSAGE_REQUIRE_ROLES+"<br/>";
		}
		
		
		
		
		if(errorMessage){
			$('#warningModal .modal-body').html(errorMessage);
			$('#warningModal').modal('show');
		}else{
			//Check Mandatory Field
			checkUserDupplicate($('#modal_edit_username').val(),$('#modal_edit_id').val());
		}
		
	});
	
	loadAllRole();
	
	
});

