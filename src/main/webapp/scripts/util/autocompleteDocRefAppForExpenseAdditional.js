var listDocRefAppAdditional = [];
var listDocRefAppAdditionalInit = []
var idInputDocAdditional = "";

function AutocompleteDocRefAppForExpenseAdditional() {

}

AutocompleteDocRefAppForExpenseAdditional.init=function (id,userName) {
    var json = {};
    json['userName'] = userName;
    idInputDocAdditional = id;
    var data =$.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },

        url: session['context'] + '/approve/findByDocTypeAndDocStatusCompleteAndAppTypeTravelAndTravelMemberUser',
        data:json,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                data = JSON.parse(xhr.responseText);
                console.info(data);
                if(data != null && data.length > 0){
                    $.each(data,function (index,item) {
                        var data = {
                            label :item.docNumber,
                            doc : item.docNumber
                        };

                        listDocRefAppAdditional.push(data)
                    });

                    $("#"+id).autocomplete({
                        source: listDocRefAppAdditional,
                        minLength: 0,
                        select:function(event,ui) {
                            $("#"+id).val( ui.item.label );
                            checkOnSelectDocRefAppForExpenseAdditional(ui.item.label);
                        }
                    }).focus(function () {
                        $(this).autocomplete('search','')
                    });
                }
            }
        }
    });

}

function checkOnSelectDocRefAppForExpenseAdditional(label) {
    var index = listDocRefAppAdditional.findIndex( listDocRefAppAdditional => listDocRefAppAdditional.label  == label);
    if(index != -1){
        $("#"+idInputDocAdditional).attr("data-doc",listDocRefAppAdditional[index].doc);
    }else{
        $("#"+idInputDocAdditional).val("");
        $("#"+idInputDocAdditional).attr("data-doc","");
    }
}

function checkOnChangeDocRefAppForExpenseAdditional(input) {
    var index = listDocRefAppAdditional.findIndex( listDocRefAppAdditional => listDocRefAppAdditional.label  == input.value);
    if(index != -1){
        $("#"+idInputDocAdditional).attr("data-doc",listDocRefAppAdditional[index].doc);
    }else{
        $("#"+idInputDocAdditional).val("");
        $("#"+idInputDocAdditional).attr("data-doc","");
    }
}

AutocompleteDocRefAppForExpenseAdditional.renderValue = function(docNumber){
    $.each(listDocRefAppAdditional,function (index,item) {

        if(item.doc == docNumber){
            $("#"+idInputDocAdditional).val(item.label );
            checkOnSelectDocRefAppForExpenseAdditional(item.label);
        }
    });
}

function viewDetailDocumentReferenceAdditional(input){
    var docNumber = input.getAttribute('data-attn');
    if(docNumber != ""){
        $.ajax({
            type: "GET",
            headers: {
                Accept: 'application/json'
            },
            url: session['context']+'/approve/findByDocumentNumber/'+docNumber,
            async: false,
            complete: function (xhr) {
                var document = JSON.parse(xhr.responseText);
                if(document != null){
                    if(document.approveType != "0006" && document.approveType != "0007" ){
                        window.open(session.context+'/approve/viewCreateDocDetail?doc='+document.id+"&type=v","_blank");
                    }else{
                        window.open(session.context+'/approve/viewCreateDocSetDetail?doc='+document.id+"&type=v","_blank");
                    }
                }
            }
        });
    }else{
        var errorMessage = MSG_AUTOCOMPLETE_APP_EXPENSE.MESSAGE_REQUIRE_FIELD+" "+LB_AUTOCOMPLETE_APP_EXPENSE.LABEL_DOCUMENT_REFERENCE;

        $('#autoCompleteWarningModal .modal-body').html(errorMessage);
        $('#autoCompleteWarningModal').modal('show');
    }
}

