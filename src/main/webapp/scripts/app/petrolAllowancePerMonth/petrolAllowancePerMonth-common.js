/**
 * 
 */

var object = $.extend({},UtilPagination);
var petrolJson;
// var listEm = listNameEmployee();
var startPageP = 1;
function isZero(e){
	var check = false;
	if(!$(e).val() || $(e).val() == 0|| $(e).val() == '0'||$(e).val() == 0.0|| $(e).val() == '0.0'){
		check = true;
	}
	return check;
}

function setZeroWhenNull(e){
	if(isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}

}

function savePetrolAllowancePerMonth(){
	//Define flagOther
	var flagActive = $('#modal_edit_flag_active').prop('checked')?"Y":"N";
	var empCode,empUserName ;
	if( ($("#modal_edit_empNamePInputEmployeeAll").attr("data-empCode")==""||$("#modal_edit_empNamePInputEmployeeAll").attr("data-empCode")==undefined)&&$("#modal_edit_empNamePInputEmployeeAll").val()!=""){
		empCode = $("#modal_edit_empCode").val();
	}else {
		empCode = $("#modal_edit_empNamePInputEmployeeAll").attr("data-empCode");
	}
	if( ($("#modal_edit_empNamePInputEmployeeAll").attr("data-userName")==""||$("#modal_edit_empNamePInputEmployeeAll").attr("data-userName")==undefined)&&$("#modal_edit_empNamePInputEmployeeAll").val()!=""){
		empUserName = $("#modal_edit_empUserName").val();
	}else {
		empUserName = $("#modal_edit_empNamePInputEmployeeAll").attr("data-userName");
	}



	var jsonParams = {};

	jsonParams['parentId']     	= 1;
	jsonParams[csrfParameter]  	= csrfToken;
	jsonParams['id']     		= $('#modal_edit_id').val();

	jsonParams['empCode']   	= empCode;
	jsonParams['flagActive']  	= flagActive;
	jsonParams['createdBy'] 			= $('#modal_edit_createdBy').val();
	jsonParams['createdDate'] 			= $('#modal_edit_createdDate').val();
	jsonParams['updateBy'] 				= $('#modal_edit_updateBy').val();
	jsonParams['updateDate'] 			= $('#modal_edit_updateDate').val();
	jsonParams['literPerMonth']     	= $('#modal_edit_literPerMonth').val();/*JSON.stringify(jsonPetrolAllowancePerMonthFrom);*/
	jsonParams['empUserName']     	= empUserName;
	// jsonParams['empLastName']     	= empLastName;
	jsonParams['balanceLiterPerMonth']     	= $('#modal_edit_literPerMonth').val();
	var itemData = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/petrolAllowancePerMonths',
        data: jsonParams,
        complete: function (xhr) {
			searchPetrolAllowancePerMonth();
			$('#editItemModal').modal('hide');
			$("#completeModal").modal('show');

        }
    }).done(function (){
        //close loader
    });

}

function editPetrolAllowancePerMonth(id,empCode){
	$.ajax({
        type: "GET",
        url: session['context']+'/petrolAllowancePerMonths/'+id,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var petrolObj = JSON.parse(xhr.responseText);
					var nameEmp = empNameAdd(petrolObj.empUserName);
                    $('#modal_edit_id').val(id);
            		$('#modal_edit_createdBy').val(petrolObj.createdBy);
            		$('#modal_edit_createdDate').val(petrolObj.createdDate);
            		$('#modal_edit_updateBy').val(petrolObj.updateBy);
            		$('#modal_edit_updateDate').val(petrolObj.updateDate);
            		$('#modal_edit_empNamePInputEmployeeAll').val(nameEmp);
            		$('#modal_edit_empCode').val(petrolObj.empCode);
            		$('#modal_edit_empUserName').val(petrolObj.empUserName);
					$('#modal_edit_literPerMonth').val(petrolObj.literPerMonth);
            		if(petrolObj.flagActive=="Y"){
            			$('#modal_edit_flag_active').prop('checked', true);
            		}else{
            			$('#modal_edit_flag_active').removeAttr( "checked" );
            		}

                }
            }
        }
    }).done(function (){
        //close loader
		$('#editItemModal').modal('show');
    });

}

function deletePetrolAllowancePerMonth(id){
	var jsonParams = {};
	jsonParams['parentId']     = 1;
	jsonParams[csrfParameter]  = csrfToken;

	$.ajax({
		type: "DELETE",
		url: session['context']+'/petrolAllowancePerMonths/'+id,
		data: jsonParams,
		complete: function (xhr) {
			searchPetrolAllowancePerMonth();
			$('#deleteItemModal').modal('hide');
			$('#completeModal').modal('show');
		}
	}).done(function (){
		//close loader
	});
}



function searchPetrolAllowancePerMonth(){
	var empCode = "";
	// var empName = "";
	// var empLastName = "";
	// if($("#search_fullname").val()){
	// 	if($("#search_fullname").val().split(" ").length > 0){
	// 		empName = $("#search_fullname").val().split(" ")[0];
	// 	}
	// 	if($("#search_fullname").val().split(" ").length > 1){
	// 		empLastName = $("#search_fullname").val().split(" ")[1];
	// 	}
	// }
	if($("#search_empCode").val().length > 0){
		empCode = $("#search_empCode").val();
	}

	var criteriaObject = {
		empCode     : empCode
	    };
	queryPetrolAllowancePerMonthByCriteria(criteriaObject);
}
function queryPetrolAllowancePerMonthByCriteria(criteriaObject){
	object.setId("#paggingSearchMain");
    object.setUrlData("/petrolAllowancePerMonths/findByCriteria");
    object.setUrlSize("/petrolAllowancePerMonths/findSize");

    object.loadTable = function(items){
    	var item = items.content;
        // $('.dv-background').show();
        $('#gridMainBody').empty();
        if(item.length > 0){
        	$dataForQuery = item;
            var itemIdTmp ;
            for(var j=0;j<item.length;j++){
				var empCode ="";

				empCode = item[j].empCode==null?"":item[j].empCode;
				var empName = item[j].empName==null?"":item[j].empName;


                var literPerMonth = item[j].literPerMonth==null?"":item[j].literPerMonth;
                var balanceLiterPerMonth = item[j].balanceLiterPerMonth==null?"":item[j].balanceLiterPerMonth;
                var flagActive = item[j].flagActive==null?"":item[j].flagActive;


                itemIdTmp = item[j].id==null?"":item[j].id;
                if(itemIdTmp){
                	$('#gridMainBody').append(''+
                            '<tr id="'+itemIdTmp+'" '+
                            ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
							((j%2 == 0) ? '<td class="text-center headcol tdFix"  style="width: 53px; ">' : '<td class="text-center headcol tdFixGrey"  style="width: 53px;">')+
							'<div class="checkbox" style="color: #2f2f2f;  margin-top: 0px;"><label><input type="checkbox" class="checkbox1" value="'+itemIdTmp+'"/><span class="checkbox-material " style="color: :#2f2f2f;"><span class="check " style="color: :#2f2f2f;" /></span></label></div>'+
							'</td>'+
							((j%2 == 0) ? '<td class="text-center headcol tdFix" style="width: 100px; left: 68px;">' : '<td class="text-center headcol tdFixGrey" style="width: 100px; left: 68px;">')+
							'	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Edit" data-toggle="modal"  onclick="editPetrolAllowancePerMonth('+itemIdTmp+','+item[j].empCode+')" ><span class="fa fa-pencil-square-o"><jsp:text/></span></button>'+
							'</td>'+
							((j%2 == 0) ? '<td class="text-center headcol tdFix" style="width: 110px; left: 163px;">' : '<td class="text-center headcol tdFixGrey" style="width: 110px; left: 163px;">')+
							((flagActive)=='Y' ? '<img class="img-circle" src="'+$IMAGE_CHECKED+'" style="width: 25px;height: 25px;" />':'<img class="img-circle" src="'+$IMAGE_CANCEL+'" style="width: 25px;height: 25px;"   />')+
							'</td>'+
                            '<td class="text-center">'+empCode+'</td>'+
                            '<td class="text-center">'+empName+'</td>'+
                            '<td class="text-center">'+literPerMonth+'</td>'+
                            '<td class="text-center">'+balanceLiterPerMonth+'</td>'+
                            '<td class="text-center">'+'<img class="img-circle" src="'+$IMAGE_HISTORY+'"  style="width: 25px;height: 25px; cursor:pointer;" onclick="petrolAllowancePerMonthHistory(\''+empCode+'\')" />'+'</td>'+
                            '</tr>'
                        );
                }

            }

        }else{
        	//Not Found
        }

    };

    object.setDataSearch(criteriaObject);
    object.search(object);
	if(startPageP==1){
		startPageP++;
		object.loadPage(($CRITERIA_PAGE*1)+1,object);
	}else{
		object.loadPage(1,object);
	}
}

function  empNameAdd(userName) {
	var data = $.ajax({
		url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
		headers: {
			Accept : "application/json"
		},
		type: "GET",
		async: false
	}).responseJSON;

	console.info(data);
	return data.FOA+data.FNameTH+" "+data.LNameTH;
}

function checkPetrolAllowancePerMonthDuplicate(empCode,petrolId){
	$.ajax({
        type: "GET",
        url: session['context']+'/petrolAllowancePerMonths/findPetrolAllowancePerMonthByEmpCode?empCode='+empCode,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	if(xhr.responseText){
                		var petrolObj = JSON.parse(xhr.responseText);
						var idPetrol = petrolObj.id;
                        if(petrolId !=  idPetrol) {
                        	$('#warningModal .modal-body').html($MESSAGE_HAS_DUPLICATE_EMPLOYEE);
                			$('#warningModal').modal('show');
                        }else{
                        	savePetrolAllowancePerMonth();
                        }
                	}else{
                		savePetrolAllowancePerMonth();
                	}
                }
            }
        }
    }).done(function (){
        //close loader
    });
}

function checkAllPetrol() {
	$('#checkAll').click(function(event) {
		if(this.checked) {
			$('.checkbox1').each(function() {
				this.checked = true;
			});
		}else{
			$('.checkbox1').each(function() {
				this.checked = false;
			});
		}
	});

	$("#checkAll").prop("checked", false);
	$('.checkbox1').click(function() {
		if($('.checkbox1:checked').length == $('.checkbox1').length){
			$("#checkAll").prop("checked", true);
		}else{
			$("#checkAll").prop("checked", false);
		}
	});
}

function btnDelete(){
	var arrayCheckStatusCheckbox = [] ;
	for (var i = 0; i < $('.checkbox1').length; i++) {
		arrayCheckStatusCheckbox[i] = $('.checkbox1')[i].checked == true;
	}
	var statusCheckboxTrue = arrayCheckStatusCheckbox.indexOf(true);
	if(statusCheckboxTrue >= 0) {
		$('#deleteItemModal').modal('show');

	}else{
		$('#deleteCheckModal').modal('show');

	}
}
function getIdForDel(){
	var lengthOfResponseText;
	// var deleteSuccess = 0;
	// var deleteFail = 0;

	if($('.checkbox1').length != 0) {
		for (var i = 0; i < $('.checkbox1').length; i++) {
			if ($('.checkbox1')[i].checked == true) {
				var id = $('.checkbox1')[i].value;
				lengthOfResponseText = deletePetrolAllowancePerMonth(id);
			}
		}

		searchPetrolAllowancePerMonth();
	}
}


$(document).ready(function () {
	


	$('.numbersOnly').on("keydown",function(e){
        NumberUtil.numberOnly(e);
    });

	$('.amount2digit').on("keydown",function(e){
        NumberUtil.numberOnly(e);
        NumberUtil.numberFixDigit($(this),2);
    });
	
	$('#search_button').on('click', function () {
		searchPetrolAllowancePerMonth();
		checkAllPetrol();
	});
	
	$('#editItemModal').on('hidden.bs.modal', function () {
		$('#modal_edit_id').val('');
		$('#modal_edit_createdBy').val('');
		$('#modal_edit_createdDate').val('');
		$('#modal_edit_updateBy').val('');
		$('#modal_edit_updateDate').val('');
		$('#modal_edit_empNamePInputEmployeeAll').val('');
		$('#modal_edit_literPerMonth').val('');
		$('#modal_edit_empCode').val('');
		$('#modal_edit_flag_active').prop('checked', true);

	});

	$('#completeModal').on('shown.bs.modal', function() {
		window.setTimeout(function(){
			$('#completeModal').modal('hide');
		},1000);
	});
	$("#modal_edit_empNamePInputEmployeeAll").on('keyup',function(){
		AutocompleteEmployeeAll.search($("#modal_edit_empNamePInputEmployeeAll").val());
	});

	$("#modal_edit_empNamePInputEmployeeAll").on('blur',function(){
		$("#modal_edit_empCode").text($("#modal_edit_empNamePInputEmployeeAll").attr("data-empCode"));
	});

	$('#editItemModalSave').on('click', function () {
		//validate Mandatory
		var errorMessage = "";


		if(!$('#modal_edit_empNamePInputEmployeeAll').val()){
			errorMessage += $MESSAGE_REQUIRE_EMPLOYEE+"<br/>";
		}
		if(!$('#modal_edit_literPerMonth').val()){
			errorMessage += $MESSAGE_REQUIRE_LITER_PER_MONTH+"<br/>";
		}



		if(errorMessage){
			$('#warningModal .modal-body').html(errorMessage);
			$('#warningModal').modal('show');
		}else{
			var empCode;
			//Check Mandatory Field
			if( ($("#modal_edit_empNamePInputEmployeeAll").attr("data-empCode")==""||$("#modal_edit_empNamePInputEmployeeAll").attr("data-empCode")==undefined)&&$("#modal_edit_empNamePInputEmployeeAll").val()!=""){
				empCode=$("#modal_edit_empCode").val();

			}
			else {empCode=$("#modal_edit_empNamePInputEmployeeAll").attr("data-empCode");}
			checkPetrolAllowancePerMonthDuplicate(empCode,$('#modal_edit_id').val());
		}

	});
	searchPetrolAllowancePerMonth();
	checkAllPetrol();
});


function petrolAllowancePerMonthHistory(code){
			location.href =  session['context']+'/petrolAllowancePerMonthHistories/listView?empCode='+code;
	// location.href = ?empCode='+empCode.toString();

}