
var object = $.extend({},UtilPagination);

$(document).ready(function () {

    $.material.init();

    search();

    if($CRITERIA_ID){
        searchCriteria_id();
        $("#selector_company").val($CRITERIA_ID);
    }else{
        $CRITERIA_ID = 1;
        searchCriteria_id();
        $("#selector_company").val($CRITERIA_ID);
    }






    $("#add_button").on('click',function () {
        $("#modal_edit_flag_active").prop("checked",true);
        $("#editItemModal").modal('show');
    })


    $("#cancel_btn").on('click',function () {
        clear();
    })

    $("#delete_button").on('click',function () {
        $("#numberOfItem").empty();
        $("#numberOfItem").append($('.checkbox1:checked').size());

        deleteByIdIn();
    })

    $("#all_gl").on('click',function () {
        if($(this).is(':checked')){
            $('.checkbox1').prop('checked',true);
        }else{
            $('.checkbox1').prop('checked',false);
        }
    });
})

function checkDuplicates(){

    var errorMessage = '';

    var jsonParams = {};

    jsonParams['parentId']     	= 1;
    jsonParams[csrfParameter]  	= csrfToken;
    jsonParams['id']     		= $('#modal_edit_id').val();
    jsonParams['createdBy'] 	= $('#modal_edit_createdBy').val();
    jsonParams['createdDate'] 	= $('#modal_edit_createdDate').val();
    jsonParams['updateBy'] 		= $('#modal_edit_updateBy').val();
    jsonParams['updateDate'] 	= $('#modal_edit_updateDate').val();
    jsonParams['glCreate']     	= $('#modal_edit_gl_create').val();
    jsonParams['glSap']     	= $('#modal_edit_gl_sap').val();
    jsonParams['io']     	    = $('#modal_edit_gl_io').val();
    jsonParams['flagActive']    = $('#modal_edit_flag_active').is(':checked');
    jsonParams['company']       = $('#selector_company').val();

    if(!$('#modal_edit_gl_create').val().replace(/ /g,'')){
        console.log("require gl create")
        $('#modal_edit_gl_create').val('');
        errorMessage += $MESSAGE_REQUIRE_GL_CREATE + '<br/>'
    }
    if(!$('#modal_edit_gl_io').val().replace(/ /g,'')) {
        console.log("require io")
        $('#modal_edit_gl_io').val('');
        errorMessage += $MESSAGE_REQUIRE_GL_IO + '<br/>'
    }
    if(!$('#modal_edit_gl_sap').val().replace(/ /g,'')) {
        console.log("require gl sap")
        $('#modal_edit_gl_sap').val('');
        errorMessage += $MESSAGE_REQUIRE_GL_SAP + '<br/>'
    }

    if(errorMessage){
        $("#modalbodyRequire").empty();
        $("#modalbodyRequire").append(errorMessage);
        $("#requireModal").modal('show');
    }else{
        $.ajax({
            type: "GET",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/gl/findByGlCreateeAndIoeAndGlSape',
            data: jsonParams,
            complete: function (xhr) {
                if (xhr.responseText) {
                    var item = JSON.parse(xhr.responseText);
                    console.log('id : ' + item);
                    if (item.id == $('#modal_edit_id').val()) {
                        save(jsonParams);
                    }
                    else{
                        // console.log(item.id);
                        console.log("DUPLICATES FILEDS")
                        $("#itemDuplicates").empty();
                        $("#itemDuplicates").append('\n'+
                            $LABEL_GL_CREATE + ' : ' +$('#modal_edit_gl_create').val() + ' ,\n',
                            $LABEL_GL_IO + ' : ' +$('#modal_edit_gl_sap').val() + ' ,\n',
                            $LABEL_GL_SAP + ' : ' +$('#modal_edit_gl_io').val(),
                            '')
                        $("#warningModal").modal('show');
                        // clear();
                        // search();
                        // }else{

                        // clear();
                    }
                } else {
                    console.log("NOT DUPLICATES XHR");
                    save(jsonParams);
                    // $('#editItemModal').modal('hide');
                    // clear();
                    // search();
                }
            }
        }).done(function () {
            //close loader
        });
    }


}

function save(data) {
    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/gl',
        data: data,
        complete: function (xhr) {
            var item = JSON.parse(xhr.responseText);
            if (item.id) {
                $('#editItemModal').modal('hide');
                clear();
                search();

                modalSuccess($MESSAGE_UPDATE_SUCCESS);
            }else{

            }
        },error : function (xhr) {

            // $("#modalbodyFail").append('Fail to update');
            // $("#failItemModal").modal('show');
            modalFail($MESSAGE_UPDATE_FAIL);
        }
    }).done(function () {
        //close loader
    }).error(function () {
        // $MESSAGE_UPDATE_FAIL
        // $('#editItemModal').modal('hide');
        // $('#editItemModal').modal('hide');
        modalFail($MESSAGE_UPDATE_FAIL);
    });
}

function search(){
    $('#all_gl').prop('checked',false);

    var criteriaObject = {
    };
    queryUserByCriteria(criteriaObject);
}

function searchCriteria_id() {

    var criteriaObject = {
        // id        : $CRITERIA_ID == "" ? "" : $CRITERIA_ID
    };
    queryUserByCriteria(criteriaObject);
}

function queryUserByCriteria(criteriaObject){
    object.setId("#paggingSearchMain");
    object.setUrlData("/gl/findByCriteria");
    object.setUrlSize("/gl/findSize");

    object.loadTable = function(items){
        var item = items.content;
        // $('.dv-background').show();
        $('#tableContent').empty();
        if(item.length > 0){
            $dataForQuery = item;
            var itemIdTmp ;

            for(var j=0;j<item.length;j++){
                if(item[j].id){
                    var  glCreate = item[j].glCreate==null?"":item[j].glCreate;
                    var glSap  = item[j].glSap==null?"":item[j].glSap;
                    var io  = item[j].io==null?"":item[j].io;
                    var flagActive = item[j].flagActive==null?"": item[j].flagActive;

                    itemIdTmp = item[j].id;

                    $('#tableContent').append(''+
                        '<tr id="'+item[j].id+'" '+
                        ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
                        '<td class="text-center">'+
                        // '<div class="checkbox checkbox-info text-center">'+
                        // '<label style="color: black"><input type="checkbox" class="checkbox1" name="selectBox" value="'+ item[j].id +'"/> </label>'+
                        // '</div>'+
                        '<div class="checkbox checkbox-info text-center">'+
                        '   <label style="color: black"><input type="checkbox" class="checkbox1" name="selectBox" value="'+ item[j].id + '"/></label>' +
                        '</div>'+

                        '</td>'+
                        '<td class="text-center">'+
                        '	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Edit" data-toggle="modal"  onclick="edit('+item[j].id+')" ><span class="fa fa-pencil-square-o"><jsp:text/></span></button>'+
                        // '	<button type="button" class="btn btn-material-red-500  btn-style-small"  title="Delete"  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+item[j].id+'\');$(\'#codeItemDelete\').html(\'' +
                        //
                        // '' + $LABEL_GL_CREATE + ' : ' + glCreate +
                        // ', ' + $LABEL_GL_IO + ' : ' + io +
                        // ', ' + $LABEL_GL_SAP + ' : ' + glSap
                        //
                        // +'\');"><span class="fa fa-trash"><jsp:text/></span></button>'+
                        '</td>'+
                        '<td class="text-center">'+
                        ((flagActive) ? '<img class="img-circle" src="'+$IMAGE_CHECKED+'" style="width: 25px;height: 25px;" />':'<img class="img-circle" src="'+$IMAGE_CANCEL+'" style="width: 25px;height: 25px;" />')+
                        '</td>'+
                        '<td class="text-center">'+glCreate+'</td>'+
                        '<td class="text-center">'+io+'</td>'+
                        '<td class="text-center">'+glSap+'</td>'+
                        '</tr>'
                    );

                }
            }
            $.material.init();
        }else{
            //Not Found
        }

    };


    object.setDataSearch(criteriaObject);
    object.search(object);
    object.loadPage(($CRITERIA_PAGE*1)+1,object);
}

function deleteFunction(id) {
    $.ajax({
        type: "DELETE",
        url: session['context']+'/gl/'+id,
        complete: function (xhr) {
            search();
            $('#deleteItemModal').modal('hide');
            modalSuccess($MESSAGE_DELETE_SUCCESS);
        },error : function () {
            // $('#deleteItemModal').modal('hide');
            // $('#deleteItemSetModal').modal('hide');
            // modalFail($MESSAGE_DELETE_FAIL);
        }
    }).done(function (){
        // modalSuccess($MESSAGE_DELETE_SUCCESS);
    }).error(function () {
        $('#deleteItemModal').modal('hide');
        $('#deleteItemSetModal').modal('hide');
        modalFail($MESSAGE_DELETE_FAIL);
    });
}

function edit(id){

    $.ajax({
        type: "GET",
        url: session['context']+'/gl/'+id,
        complete: function (xhr) {
            // search();
            // $('#deleteItemModal').modal('hide');
            var item = JSON.parse(xhr.responseText);
            $('#modal_edit_id').val(item.id);
            $('#modal_edit_createdBy').val(item.createdBy);
            $('#modal_edit_createdDate').val(item.createdDate);
            $('#modal_edit_updateBy').val(item.updateBy);
            $('#modal_edit_updateDate').val(item.updateDate);
            $('#modal_edit_gl_create').val(item.glCreate);
            $('#modal_edit_gl_sap').val(item.glSap);
            $('#modal_edit_gl_io').val(item.io);
            (item.flagActive) ? $('#modal_edit_flag_active').prop('checked',true) : $('#modal_edit_flag_active').prop('checked',false);

            $('#editItemModal').modal('show');

        },
        error : function (xhr) {
            modalFail($MESSAGE_UPDATE_FAIL);
            console.log("error");
        }

    }).done(function (){
        clear();
    })

}

function clear() {
    $('#modal_edit_id').val('');
    $('#modal_edit_createdBy').val('');
    $('#modal_edit_createdDate').val('');
    $('#modal_edit_updateBy').val('');
    $('#modal_edit_updateDate').val('');
    $('#modal_edit_gl_create').val('');
    $('#modal_edit_gl_sap').val('');
    $('#modal_edit_gl_io').val('');
    $('#modal_edit_flag_active').attr('checked', false);
}

function deleteByIdIn() {

    var arrayCheckStatusCheckbox = [] ;
    for (var i = 0; i < $('.checkbox1').length; i++) {
        arrayCheckStatusCheckbox[i] = $('.checkbox1')[i].checked == true;
    }
    var statusCheckboxTrue = arrayCheckStatusCheckbox.indexOf(true);
    if(statusCheckboxTrue >= 0) {
        $('#deleteItemSetModal').modal('show');

    }else{
        $('#deleteCheckModal').modal('show');

    }
}

function getIdForDel(){
    var id = "";
    $.each( $( ".checkbox1:checked" ), function( i, obj ) {
        id += ","+obj.value;
    });

    id = id.substring(1);
    console.log(id);

    var jsonData = {}
    jsonData['ids'] = id;

    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/gl/deleteGroup',
        data: jsonData,
        complete : function (xhr) {
            $("#deleteItemSetModal").modal('hide');
            modalSuccess($MESSAGE_DELETE_SUCCESS);
            search();
        }
    })

}

function modalSuccess(msg) {
    console.log(msg);
    $("#modalbodySuccess").empty();
    $("#modalbodySuccess").append(msg);
    $("#successModal").modal('show');
    setTimeout(function() {$('#successModal').modal('hide');}, 1250);
}


function modalFail(msg) {
    $("#modalbodyFail").empty();
    $("#modalbodyFail").append(msg);
    $("#failItemModal").modal('show');
    // setTimeout(function() {$('#failItemModal').modal('hide');}, 3000);
}