$(document).ready(function () {
    google.charts.load('current', {'packages':['line']});
    google.charts.setOnLoadCallback(companyChart);
})

function companyChart() {
    var data = google.visualization.arrayToDataTable([
        ['Document Type', 'Create', 'Cancel','Complete','Reject','On process'],
        ['1011',            6000,      4000,    1600,   1500,   1400],
        ['2021',            9300,      8100,    7700,   5000,   2100],
        ['3031',            11000,     8500,    6300,   3300,   900],
        ['4041',            8200,      7700,    5100,   2500,   1600],
        ['5051',            7900,      5200,    3800,   2000,   500],
        ['BE83',            7700,      5800,    4800,   3100,   2600],
        ['PNP1',            8100,      6700,    5200,   4800,   2000],
        ['CEC1',            8100,      6400,    6100,   3700,   1000],
        ['9091',            6500,      6200,    3500,   1500,   500]
    ]);

    var options = {
        // title: 'Company Performance',
        curveType: 'function',
        legend: { position: 'right' },
        format: 'decimal',
    };

    var chart = new google.charts.Line(document.getElementById('chart'));
    chart.draw(data, options);
}

function timeChart() {
    var data = google.visualization.arrayToDataTable([
        ['Month', 'Create', 'Cancel','Complete','Reject','On process'],
        ['Jan',       6100,      4000,    1400,   1300,  1200],
        ['Feb',       9300,      8200,    7600,   5000,  2100],
        ['Mar',      12000,      8500,    6000,   3000,   900],
        ['Apr',       8100,      7700,    5200,   2500,  1600],
        ['May',       7900,      5300,    3100,   2000,   300],
        ['Jun',       7800,      5700,    4800,   2900,  2700],
        ['Jul',       8100,      6800,    5200,   4700,  2000],
        ['Aug',       8100,      6400,    6100,   3700,  1300],
        ['Sep',       6700,      6200,    3500,   2400,   600],
        ['Oct',       8100,      7700,    4800,   2500,  1600],
        ['Nov',       7900,      5300,    3000,   2000,   300],
        ['Dec',       7700,      5800,    4800,   3200,  2800]

    ]);

    var options = {
        // title: 'Company Performance',
        curveType: 'function',
        legend: { position: 'right' },
        format: 'decimal',
    };

    var chart = new google.charts.Line(document.getElementById('chart'));
    chart.draw(data, options);
}

function companyReport() {
    $("#panelH").empty();
    $("#chart").empty();
    $("#panelH").append("แสดงแยกตามบริษัท")

    companyChart();
}

function timeReport() {
    $("#panelH").empty();
    $("#chart").empty();
    $("#panelH").append("แสดงแยกตามเวลา")

    timeChart();
}