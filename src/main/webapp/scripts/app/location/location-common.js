/**
 * 
 */

var object = $.extend({},UtilPagination);
var locationType = "D";
var startPage =1;

function isZero(e){
	var check = false;
	if(!$(e).val() || $(e).val() == 0|| $(e).val() == '0'||$(e).val() == 0.0|| $(e).val() == '0.0'){
		check = true;
	}
	return check;
}

function setZeroWhenNull(e){
	if(isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}
	
}

function saveLocation(){
	//Define flagOther
	var flagOther = $('#modal_edit_flag_other').prop('checked')?"Y":"N";
	var flagActive = $('#modal_edit_flag_active').prop('checked')?true:false;


	var jsonParams = {};

	jsonParams['parentId']     	= 1;
	jsonParams[csrfParameter]  	= csrfToken;
	jsonParams['id']     		= $('#modal_edit_id').val();
	jsonParams['code']     	= $('#modal_edit_code').val();
	jsonParams['description'] 	= $('#modal_edit_description').val();
	jsonParams['speacialRate']   	= $('#modal_edit_specialRate').val();
	jsonParams['flagOther'] 	= flagOther;
	jsonParams['locationType']  	= locationType;
	jsonParams['flagActive']  	= flagActive;
	jsonParams['createdBy'] 			= $('#modal_edit_createdBy').val();
	jsonParams['createdDate'] 			= $('#modal_edit_createdDate').val();
	jsonParams['updateBy'] 				= $('#modal_edit_updateBy').val();
	jsonParams['updateDate'] 			= $('#modal_edit_updateDate').val();

	var itemData = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/locations',
        data: jsonParams,
        complete: function (xhr) {
			searchLocation();
			$('#editItemModal').modal('hide');
			$("#completeModal").modal('show');

        }
    }).done(function (){
        //close loader
    });

}

function editLocation(id){
	$.ajax({
        type: "GET",
        url: session['context']+'/locations/'+id,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var locationObj = JSON.parse(xhr.responseText);

                    $('#modal_edit_id').val(locationObj.id);
            		$('#modal_edit_createdBy').val(locationObj.createdBy);
            		$('#modal_edit_createdDate').val(locationObj.createdDate);
            		$('#modal_edit_updateBy').val(locationObj.updateBy);
            		$('#modal_edit_updateDate').val(locationObj.updateDate);
            		$('#modal_edit_code').val(locationObj.code);
					$('#modal_edit_description').val(locationObj.description);
					$('#modal_edit_specialRate').val(locationObj.speacialRate);

            		if(locationObj.flagOther=="Y"){
            			$('#modal_edit_flag_other').prop('checked', true);
            		}else{
            			$('#modal_edit_flag_other').removeAttr( "checked" );
            		}
            		if(locationObj.flagActive){
            			$('#modal_edit_flag_active').prop('checked', true);
            		}else{
            			$('#modal_edit_flag_active').removeAttr( "checked" );
            		}

                }
            }
        }
    }).done(function (){
        //close loader
		$('#editItemModal').modal('show');
    });

}

function deleteLocation(id){
	var jsonParams = {};
	jsonParams['parentId']     = 1;
	jsonParams[csrfParameter]  = csrfToken;

	$.ajax({
		type: "DELETE",
		url: session['context']+'/locations/'+id,
		data: jsonParams,
		complete: function (xhr) {
			searchLocation();
			$('#deleteItemModal').modal('hide');
			$('#completeModal').modal('show');
		}
	}).done(function (){
		//close loader
	});
}



function searchLocation(){
	var code = "";
	var description = "";
		if($("#search_code").val().length > 0){
			code = $("#search_code").val();
		}
		if($("#search_description").val().length > 0){
			description = $("#search_description").val();
		}


	var criteriaObject = {
		code     : code,
		description     : description,
		locationType     : "D"
	    };
	queryLocationByCriteria(criteriaObject);
}

function queryLocationByCriteria(criteriaObject){
	object.setId("#paggingSearchMain");                
    object.setUrlData("/locations/findByCriteria");
    object.setUrlSize("/locations/findSize");
    
    object.loadTable = function(items){
    	var item = items.content;
        // $('.dv-background').show();
        $('#gridMainBody').empty();
        if(item.length > 0){
        	$dataForQuery = item;
            var itemIdTmp ;
            for(var j=0;j<item.length;j++){
            	
                var code = item[j].code==null?"":item[j].code;
                var description = item[j].description==null?"":item[j].description;
                var specialRate = item[j].speacialRate==null?"":item[j].speacialRate;
                // var locationType = item[j].locationType==null?"":item[j].locationType;
                var flagOther = item[j].flagOther==null?"":item[j].flagOther;
                var flagActive = item[j].flagActive==null?"":item[j].flagActive;



                itemIdTmp = item[j].id;
                if(itemIdTmp){
                	$('#gridMainBody').append(''+
                            '<tr id="'+item[j].id+'" '+
                            ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
							((j%2 == 0) ? '<td class="text-center headcol tdFix"  style="width: 53px; ">' : '<td class="text-center headcol tdFixGrey"  style="width: 53px;">')+
							'<div class="checkbox" style="color: #2f2f2f;  margin-top: 0px;"><label><input type="checkbox" class="checkbox1" value="'+item[j].id+'"/><span class="checkbox-material " style="color: :#2f2f2f;"><span class="check " style="color: :#2f2f2f;" /></span></label></div>'+
							'</td>'+
							((j%2 == 0) ? '<td class="text-center headcol tdFix" style="width: 100px; left: 68px;">' : '<td class="text-center headcol tdFixGrey" style="width: 100px; left: 68px;">')+
							'	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Edit" data-toggle="modal"  onclick="editLocation('+item[j].id+')" ><span class="fa fa-pencil-square-o"><jsp:text/></span></button>'+
							// '	<button type="button" class="btn btn-material-red-500  btn-style-small"  title="Delete"  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+item[j].id+'\');$(\'#codeItemDelete\').html(\''+code+'\');"><span class="fa fa-trash"><jsp:text/></span></button>'+
							'</td>'+
							((j%2 == 0) ? '<td class="text-center headcol tdFix" style="width: 110px; left: 163px;">' : '<td class="text-center headcol tdFixGrey" style="width: 110px; left: 163px;">')+
							((flagActive) ? '<img class="img-circle" src="'+$IMAGE_CHECKED+'" style="width: 25px;height: 25px;" />':'<img class="img-circle" src="'+$IMAGE_CANCEL+'" style="width: 25px;height: 25px;" />')+
							'</td>'+
                            '<td class="text-center">'+code+'</td>'+
                            '<td class="text-center">'+description+'</td>'+
                            '<td class="text-center">'+specialRate+'</td>'+
                            '<td class="text-center">'+
                            ((flagOther)=='Y' ? '<img class="img-circle" src="'+$IMAGE_CHECKED+'" style="width: 25px;height: 25px;" />':'<img class="img-circle" src="'+$IMAGE_CANCEL+'" style="width: 25px;height: 25px;" />')+
                            '</td>'+

                            '</tr>'
                        );
                }
                
            }

        }else{
        	//Not Found
        }
        
    };

    object.setDataSearch(criteriaObject); 
    object.search(object);
	if(startPage==1){
		startPage++;
		object.loadPage(($CRITERIA_PAGE*1)+1,object);
	}else{
		object.loadPage(1,object);
	}
}


function checkLocationDuplicate(code,locationId){
	$.ajax({
        type: "GET",
        url: session['context']+'/locations/findByCodeAndLocationType?code='+code+'&locationType='+locationType,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	if(xhr.responseText){
                		var locationObj = JSON.parse(xhr.responseText);
                        if(locationId != locationObj.id){
                        	$('#warningModal .modal-body').html($MESSAGE_HAS_DUPLICATE_CODE);
                			$('#warningModal').modal('show');
                        }else{
                        	saveLocation();
                        }
                	}else{
                		saveLocation();
                	}
                }
            }
        }
    }).done(function (){
        //close loader
    });
}

function checkAllD() {
	$('#checkAll').click(function(event) {
		if(this.checked) {
			$('.checkbox1').each(function() {
				this.checked = true;
			});
		}else{
			$('.checkbox1').each(function() {
				this.checked = false;
			});
		}
	});

	$("#checkAll").prop("checked", false);
	$('.checkbox1').click(function() {
		if($('.checkbox1:checked').length == $('.checkbox1').length){
			$("#checkAll").prop("checked", true);
		}else{
			$("#checkAll").prop("checked", false);
		}
	});
}

function btnDelete(){
	var arrayCheckStatusCheckbox = [] ;
	for (var i = 0; i < $('.checkbox1').length; i++) {
		arrayCheckStatusCheckbox[i] = $('.checkbox1')[i].checked == true;
	}
	var statusCheckboxTrue = arrayCheckStatusCheckbox.indexOf(true);
	if(statusCheckboxTrue >= 0) {
		$('#deleteItemModal').modal('show');

	}else{
		$('#deleteCheckModal').modal('show');

	}
}

function getIdForDel(){
	var lengthOfResponseText;
	// var deleteSuccess = 0;
	// var deleteFail = 0;

	if($('.checkbox1').length != 0) {
		for (var i = 0; i < $('.checkbox1').length; i++) {
			if ($('.checkbox1')[i].checked == true) {
				var id = $('.checkbox1')[i].value;
				lengthOfResponseText = deleteLocation(id);
				// if(lengthOfResponseText == 0){
				// 	deleteSuccess++;
				// }else{
				// 	deleteFail++;
				// }
			}
		}

		searchLocation();
		// checkAlertForDelete(deleteSuccess , deleteFail);
	}
}


$(document).ready(function () {
	

	
	$('.numbersOnly').on("keydown",function(e){
        NumberUtil.numberOnly(e);
    });
	
	$('.amount2digit').on("keydown",function(e){
        NumberUtil.numberOnly(e);
        NumberUtil.numberFixDigit($(this),2);
    });
	
	$('#search_button').on('click', function () {
		searchLocation();
		checkAllD();
	});
	
	$('#editItemModal').on('hidden.bs.modal', function () {
		$('#modal_edit_id').val('');
		$('#modal_edit_createdBy').val('');
		$('#modal_edit_createdDate').val('');
		$('#modal_edit_updateBy').val('');
		$('#modal_edit_updateDate').val('');
		$('#modal_edit_code').val('');
		$('#modal_edit_description').val('');
		$('#modal_edit_specialRate').val('');
		$('#modal_edit_flag_other').removeAttr( 'checked' );
		$('#modal_edit_flag_active').prop('checked', true);

	});

	$('#completeModal').on('shown.bs.modal', function() {
		window.setTimeout(function(){
			$('#completeModal').modal('hide');
		},1000);
	});

	$('#editItemModalSave').on('click', function () {
		//validate Mandatory
		var errorMessage = "";
		
		
		if(!$('#modal_edit_code').val()){
			errorMessage += $MESSAGE_REQUIRE_CODE+"<br/>";
		}
		if(!$('#modal_edit_description').val()){
			errorMessage += $MESSAGE_REQUIRE_DECRIPTION+"<br/>";
		}

		
		
		if(errorMessage){
			$('#warningModal .modal-body').html(errorMessage);
			$('#warningModal').modal('show');
		}else{
			//Check Mandatory Field
			checkLocationDuplicate($('#modal_edit_code').val(),$('#modal_edit_id').val());
		}
		
	});

	searchLocation();
	checkAllD();
});


function btnRoute(){
	location.href = session['context']+'/routeDistances/listView';

}