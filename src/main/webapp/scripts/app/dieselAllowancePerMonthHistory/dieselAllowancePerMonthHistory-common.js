/**
 * 
 */

var object = $.extend({},UtilPagination);
var dieselJson;
// var listEm = listNameEmployee();
function isZero(e){
	var check = false;
	if(!$(e).val() || $(e).val() == 0|| $(e).val() == '0'||$(e).val() == 0.0|| $(e).val() == '0.0'){
		check = true;
	}
	return check;
}

function setZeroWhenNull(e){
	if(isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}
	
}



function searchDieselAllowancePerMonth(){
	var empCode = "";
	var month = "";
	var year = "";

	if($("#search_empCode").val().length > 0){
		empCode = $("#search_empCode").val();
	}
	if($("#search_month").val().length > 0){
		month = $("#search_month").val();
	}
	if($("#search_year").val().length > 0){
		year = $("#search_year").val();
	}

	var criteriaObject = {
		empCode     : empCode,
		month     : month,
		year     : year
	    };
	queryDieselAllowancePerMonthByCriteria(criteriaObject);
}
function queryDieselAllowancePerMonthByCriteria(criteriaObject){
	object.setId("#paggingSearchMain");                
    object.setUrlData("//dieselAllowancePerMonthHistories/findByCriteria");
    object.setUrlSize("//dieselAllowancePerMonthHistories/findSize");

    object.loadTable = function(items){
    	var item = items.content;
        // $('.dv-background').show();
        $('#gridMainBody').empty();
        if(item.length > 0){
        	$dataForQuery = item;
            var itemIdTmp ;
            for(var j=0;j<item.length;j++){


                var month = item[j].month==null?"":item[j].month;
                var year = item[j].year==null?"":item[j].year;
                var detail = item[j].detail==null?"":item[j].detail;
                var literNumber = item[j].literNumber==null?"":item[j].literNumber;
                var birthDate = item[j].birthDate==null?"":item[j].birthDate;


                itemIdTmp = item[j].id==null?"":item[j].id;
                if(itemIdTmp){
                	$('#gridMainBody').append(''+
                            '<tr id="'+itemIdTmp+'" '+
                            ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
                            '<td class="text-center">'+month+'</td>'+
                            '<td class="text-center">'+year+'</td>'+
                            '<td class="text-center">'+detail+'</td>'+
                            '<td class="text-center">'+birthDate+'</td>'+
                            '<td class="text-center">'+literNumber+'</td>'+
                            '</tr>'
                        );
                }
                
            }

        }else{
        	//Not Found
        }
        
    };

    object.setDataSearch(criteriaObject); 
    object.search(object);
    object.loadPage(($CRITERIA_PAGE*1)+1,object);
}
function  empNameAdd(code) {
	var datas = $.ajax({
		url: session.context + "/dieselAllowancePerMonthHistories/findDieselAllowancePerMonthHistoryByEmpCode?userName="+userName,
		headers: {
			Accept : "application/json"
		},
		type: "GET",
		async: false
	}).responseJSON;
	var userName = datas!=null?(datas.userName!=null?datas.userName:""):"";
	if(userName!=""){
		var data = $.ajax({
			url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+datas.empUserName,
			headers: {
				Accept : "application/json"
			},
			type: "GET",
			async: false
		}).responseJSON;

		console.info(data);
		return data.FOA+data.FNameTH+" "+data.LNameTH;
	}
}





$(document).ready(function () {
	

	
	$('.numbersOnly').on("keydown",function(e){
        NumberUtil.numberOnly(e);
    });
	
	$('.amount2digit').on("keydown",function(e){
        NumberUtil.numberOnly(e);
        NumberUtil.numberFixDigit($(this),2);
    });
	
	$('#search_button').on('click', function () {
		searchDieselAllowancePerMonth();
	});
	$('#search_fullName').val(empNameAdd($('#search_empCode').val()));
	// dieselList();
	searchDieselAllowancePerMonth();
});
function dieselAllowancePerMonthPage(){
	location.href =  session['context']+'/dieselAllowancePerMonths/listView';

}