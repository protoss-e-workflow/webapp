var type = null;
var $DATA_TRAVEL_DETAIL;
var $DATA_TRAVEL_MEMBER;
var $DATA_EXTERNAL_MEMBER;
var $DATA_DOCUMENT_ATTACHMENT;
var $DATA_LOCATION_JSON;
var $STATUS;
var $TravelDetailId;
var $TravelMemberId;
var $ExternalMemberId;
var $CarTypeCode;
var $fileUpload="";
var userHead;

var min,max;
var FLAG_CAR = null;
var FLAG_FLIGHT = null;
var FLAG_BED = null;
var FLAG_BED_TWIN = null;
var $DATA_EMPLOYEE;
var listJsonFlowDetails = [];
var DATA_PARAMETER_DUEDATE_ADVANCE;
var HOTEL_TYPE_ACTIVE = false;
var FLIGHT_TYPE_ACTIVE = false;
var ADVANCE_TYPE_ACTIVE = false;

var STATUS_SAVE_DOCUMENT;
var FLOW_CAR_ACTIVE = false;
var checkStatus=0;
var checkStatusReject=0;

var CAR_TYPE_ACTIVE = false;
var $CarTypeCode = null;
var DATA_RESULT_CAR;
var DATA_RESULT_HOTEL;
var DATA_RESULT_FLIGHT;
var $DATA_LINE_APPROVE;
var flagApprove = false;
var SPEACIAL_FLOW;
var flagFinance = false;
var fileUpload;

function moveFixedActionButton() {
    var xPos = $('.container').offset().left;
    for (var i = 0; i < $('.btnActionFixed').size(); i++) {
        $('.btnActionFixed')[i].style.right = (xPos-50) + 'px';
    }
}

function findUserHead() {
    $.ajax({
        url: session.context + '/masterDatas/findByMasterdataInAndMasterDataDetailCodeOrderByCodeList?masterdata=M020&code='+$USERNAME,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false,
        complete: function (xhr) {
            if(xhr.responseJSON.content[0].id){
                userHead = xhr.responseJSON.content
            }
        }
    });


}

$(document).ready(function () {

    $('.dv-background').show();
	/* call master data */
    readMasterDataDocType();
    readMasterDataDocStatus();
    readMasterDataAprType();
    readMasterDataAttachmentType();
    readMasterDataHotel();
    readMasterDataActionReason();
    readMasterDataFlowType();


	$.material.init();
    moveFixedActionButton();
    window.onresize = function (event) {
        moveFixedActionButton()
    };

    $("#222").keypress(function (e) {
        e.preventDefault();
    });

    /* render travel detail */
    window.setTimeout(function(){
        findUserHead();
        validateApproveType();
        $("#advancesMoney").autoNumeric('init',{vMin:0});
        $("#advancesAmount").autoNumeric('init',{vMin:0});
        /* render value document */
        findEmployeeProfileByUserName($DATA_DOCUMENT.requester);
        $("#docNumber").text($DATA_DOCUMENT.docNumber);
        $("#sendTime").text($DATA_DOCUMENT.sendDate == null ? "SEND TIME" : $DATA_DOCUMENT.sendDate);
        $("#travelReason").text($DATA_DOCUMENT.documentApprove.documentApproveItem[0].travelReason);
        if($DATA_EMPLOYEE != null) {
            if(session.lang === 'en'){
                $("#requester").text($DATA_EMPLOYEE.NameEN);
                $("#empCode").text($DATA_EMPLOYEE.Personal_ID);
                $("#position").text($DATA_EMPLOYEE.PositionEN);
                $("#docCreatorName").text($DATA_EMPLOYEE.NameEN);
                $("#costCenter").text($DATA_DOCUMENT.costCenterCode);
            }else{
                $("#requester").text($DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH);
                $("#empCode").text($DATA_EMPLOYEE.Personal_ID);
                $("#position").text($DATA_EMPLOYEE.PositionTH);
                $("#docCreatorName").text($DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH);
                $("#costCenter").text($DATA_DOCUMENT.costCenterCode);
            }
            findRequestByDocument($DATA_DOCUMENT.id);
        }

        if($DATA_DOCUMENT.createdBy != $DATA_DOCUMENT.requester){
            findEmployeeProfileByUserName($DATA_DOCUMENT.createdBy);
            $("#docCreatorName").text($DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH);
        }


        if($DATA_DOCUMENT.companyCode){
            findByPaCode($DATA_DOCUMENT.companyCode);
        }

        if($DATA_DOCUMENT.psa){
            findByPsaCode($DATA_DOCUMENT.psa);
        }

        /* validate BTN QR code */
        if($DATA_DOCUMENT.request != null){
            $("#divBtnQRCode").removeClass('hide');
        }

        // setUpDefaultCarType();
        validateDocStatus($DATA_DOCUMENT.documentStatus);
        validateBTNSet();
        if($DATA_DOCUMENT.documentApprove.documentApproveItem.length > 0){
    	    var dataDocApproveItem = $DATA_DOCUMENT.documentApprove.documentApproveItem;
    	    for(var i=0;i<dataDocApproveItem.length;i++){
    	        if(dataDocApproveItem[i].approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC ||
                    dataDocApproveItem[i].approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
                    findTravelDetailsByDocumentApproveItemId(dataDocApproveItem[i].id);
                    findTravelMembersByDocumentApproveItemId(dataDocApproveItem[i].id);
                    findExternalMembersByDocumentApproveItemId(dataDocApproveItem[i].id);
                    findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);
                }
    	        else if(dataDocApproveItem[i].approveType == MASTER_DATA.APR_TYPE_CAR){
                    renderDataCarBooking(dataDocApproveItem[i].carBookings);
                }else if(dataDocApproveItem[i].approveType == MASTER_DATA.APR_TYPE_HOTEL){
                    renderDataHotelBooking(dataDocApproveItem[i].hotelBooking);
                }else if(dataDocApproveItem[i].approveType == MASTER_DATA.APR_TYPE_FIGHT_BOOKING){
                    renderDataFlightTicket(dataDocApproveItem[i].flightTicket);
                }
            }

            var papsa = $("#company").attr('pa')+"-"+$("#department").attr('psa');

            var data = $.ajax({
                url: session.context + "/intermediaries/findAdminByPapsa/"+papsa+"/"+requester,
                headers: {
                    Accept : "application/json"
                },
                type: "GET",
                async: false
            }).responseJSON;

            if(data) {
                var dataAdminSplit = data.profile.split(':');
                if (dataAdminSplit[1] != "0") {
                    var adminName = dataAdminSplit[1].split("-");

                    var data = $.ajax({
                        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName=" + adminName[0],
                        headers: {
                            Accept: "application/json"
                        },
                        type: "GET",
                        async: false
                    }).responseJSON;

                    if (data) {

                        USERADMIN = data;

                        if ($("#adminName").text() == "") {
                            if(session.lang === 'en'){
                                $("#adminName").text(data.NameEN);
                            }else {
                                $("#adminName").text(data.FOA + data.FNameTH + ' ' + data.LNameTH);
                            }
                        }
                    }
                }
            }

        }

    	if($DATA_DOCUMENT.documentAdvance != null){
    		renderDocumentAdvance($DATA_DOCUMENT.documentAdvance);
    	}else{
            $("#advancesMoney").autoNumeric('set',0);
            $("#advancesAmount").autoNumeric('set',0);
            $("#borrowDate").text("");
            $("#advanceEndDate").text("");
            $("#bankNumber").text("");
            $("#dueDate").text("");
            $("#fiDoc").text("");
    	}

        $("#sumCostEstimate").autoNumeric('init');
        if($DATA_DOCUMENT.documentApprove.documentApproveCostEstimate.length > 0){
            renderCostEstimate($DATA_DOCUMENT.documentApprove.id);
        }
        $('.dv-background').hide();
   },3000);
    

    /* event hide modal complete */
    $('#completeModal').on('shown.bs.modal', function() {
		window.setTimeout(function(){
			$('#completeModal').modal('hide');
		},1000);
	});
    
    /* manage cancel document */
    $("#confirmCancel").on('click',function(){
        validateFlowTypeForCancelRequest();
    });

    $("#hotelADMInputHotel").on('blur',function (){
        if($("#hotelADMInputHotel").attr('data-hotel-code') == "Other"){
            $("#otherHotelADMDiv").removeClass('hide');
        }
    });

    $("#confirmReject").on('click',function(){
        rejectRequestSet();
    });

    $('#btnReport').on('click',function () {


        window.location.href = session['context'] + '/report/printApproveReport?docNum='+$DATA_DOCUMENT.docNumber+"&language="+session.lang;

    });

    let flagTravelPurposeCal = $DATA_DOCUMENT.documentApprove.documentApproveItem.length > 0 ? ($DATA_DOCUMENT.documentApprove.documentApproveItem[0].flagTravelPurposeCal ? $DATA_DOCUMENT.documentApprove.documentApproveItem[0].flagTravelPurposeCal : '') : '';
    flagTravelPurposeCal = flagTravelPurposeCal.split(',');
    if(flagTravelPurposeCal.length > 0){
        for(let x = 0; x < flagTravelPurposeCal.length; x++){
            for(let y = 1; y <= $('input[name="travel_purpose_checkbox"]').size(); y++){
                if(flagTravelPurposeCal[x] === $('#travel_purpose_checkbox' + y).val()){
                    $('#travel_purpose_checkbox' + y).prop('checked',true);
                }
            }
        }
    }

    $('input[name="travel_purpose_checkbox"]').attr('disabled',true);

    $("#textFileNameSapManual").on('click',function () {
        $("#browseFileSapManual").click();
    });

    $("#browseFileSapManual").on('click',function () {
        $("#uploadFileDocumentSapManual").click();
    });

    $("#uploadFileDocumentSapManual").change(function () {
        fileUpload = this.files[0];
        $("#textFileNameSapManual").val(this.files[0].name);
    });

    $("#confirmSapManual").on('click',function () {
        var docClear = $("#docClearingSapManual").val();
        var fileName = $("#textFileNameSapManual").val();
        $('.dv-background').show();
        if(docClear && fileName){
            if(docClear.startsWith('68')){
                var formData = new FormData();
                formData.append("file", fileUpload);
                formData.append("filename", fileUpload.name);
                formData.append("attachmentType", "M152");
                formData.append("document", $DATA_DOCUMENT.id);
                formData.append("docClear", docClear);

                $.ajax({
                    type: "POST",
                    headers: {
                        Accept: 'application/json'
                    },
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    url: session['context'] + '/advance/saveSapManual',
                    processData: false,
                    contentType: false,
                    data: formData,
                    async: false,
                    complete: function (xhr) {
                        if (xhr.readyState == 4) {
                            if (xhr.status == 200) {
                                $("#textFileNameSapManual").val("");
                                $("#docClearingSapManual").val("");
                                $('.dv-background').hide();
                                $("#modalCompleteManual").modal('hide');
                                window.location.href = session.context+'/approve/approveMainMenu'
                            }
                            else if (xhr.status == 500) {
                                //unsuccess
                            }
                        } else {
                            //unsuccess
                        }
                    }
                });
            }else{
                $('.dv-background').hide();
                var errorMessage = "กรุณาระบุเลข 68";
                $('#warningModal .modal-body').html(errorMessage);
                $('#warningModal').modal('show');
            }

        }else{
            var errorMessage = "";
            if(docClear){
                errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_FILE_NAME+"<br/>"
            }else if(fileName){
                errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_CLEARING_DOC_NUMBER+"<br/>"
            }else{
                errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_CLEARING_DOC_NUMBER+" , "+LB.LABEL_FILE_NAME+"<br/>"
            }
            $('.dv-background').hide();
            $('#warningModal .modal-body').html(errorMessage);
            $('#warningModal').modal('show');
        }
    });
});

function renderCostEstimate(documentApproveId) {
    var data = $.ajax({
        url: session.context + "/approveCostEstimates/findByDocumentApprove?documentApprove="+documentApproveId,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $("#gridCostEstimateBody").empty();
    if(data.content[0].id){
        var sumTotal = 0;
        $.each(data.content,function (index,item) {
            var description = (session.lang == "en"?item.descriptionEn:item.descriptionTh);
            $("#gridCostEstimateBody").append('' +
                '<tr>' +
                '<td>'+item.gl +" : "+description+'</td>' +
                '<td>'+item.remark+'</td>' +
                '<td style="text-align: right" name="amountCostEstimate">'+item.amount+'</td>' +
                '</tr>'
            );

            sumTotal += item.amount;
        });

        $("[name=amountCostEstimate]").autoNumeric('init');
        $("#sumCostEstimate").autoNumeric('set',sumTotal)
    }else{
        $("#sumCostEstimate").val('')
    }
}


function findCostCenter(costCenter) {
    $('.dv-background').show();
    setTimeout(function () {

        var data = $.ajax({
            url: session.context + "/intermediaries/findEmployeeProfileByCostCenter?costCenter="+costCenter,
            headers: {
                Accept : "application/json"
            },
            type: "GET",
            async: false
        }).responseJSON;

        if(data){
            var costCenterSplit = data.profile.split('#');
            if(costCenterSplit[2] != "0"){
                var paPsaSplit = costCenterSplit[2].split('-');
                $("#company").text(paPsaSplit[0]+" : "+paPsaSplit[1]);
                $("#company").attr('pa',paPsaSplit[0]);
                $("#department").text(paPsaSplit[2]+" : "+paPsaSplit[3]);
                $("#department").attr('psa',paPsaSplit[2]);
                $('.dv-background').hide();
            }else{
                $('.dv-background').hide();
            }
        }

    },1000);

}


function validateDocStatus(documentStatus){
	/* update by siriradC.  2017.08.04 */
  	 if(documentStatus == MASTER_DATA.DOC_STATUS_DRAFT){
    	$("#ribbon").addClass("ribbon-status-draft");
        $("#ribbon").attr("data-content","DRAFT");
    }if(documentStatus == MASTER_DATA.DOC_STATUS_CANCEL){
    	$("#ribbon").addClass("ribbon-status-cancel");
    	$("#ribbon").attr("data-content","CANCEL");
    }if(documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS){
        $("#ribbon").addClass("ribbon-status-on-process");
        $("#ribbon").attr("data-content","ON PROCESS");
    }if(documentStatus == MASTER_DATA.DOC_STATUS_REJECT){
    	$("#ribbon").addClass("ribbon-status-reject");
    	$("#ribbon").attr("data-content","REJECT");
    }if(documentStatus == MASTER_DATA.DOC_STATUS_COMPLETE){
    	$("#ribbon").addClass("ribbon-status-complete");
    	$("#ribbon").attr("data-content","COMPLETE");
    }
}

function validateBTNSet(){

    if ($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS) {
        if ($USERNAME == $DATA_DOCUMENT.requester) {
            if ($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS) {
                $("#divCancelBtn").removeClass('hide');
                $("#divAttachFileBtn").removeClass('hide');
                $("#divCopyBtn").removeClass('hide');
                $("#divBtnQRCode").removeClass('hide');
                $("#divBtnBack").removeClass('hide');
                $("#divBtnReport").removeClass('hide');

                $("#btnApprove").addClass('hide');

                $("#divCancelBtn").addClass('btn-0');
                $("#divAttachFileBtn").addClass('btn-80');
                $("#divCopyBtn").addClass('btn-160');
                $("#divBtnQRCode").addClass('btn-240');
                $("#divBtnBack").addClass('btn-320');
                $("#divBtnReport").css('margin-top', '400px');

                if (flagApprove) {
                    if ($USERNAME == $DATA_DOCUMENT.requester) {
                        $("#btnCancel").addClass('hide');
                        $("#divCancelBtn").addClass('hide');

                        $("#divAttachFileBtn").removeClass('btn-80');
                        $("#divAttachFileBtn").addClass('btn-0');
                        $("#divCopyBtn").removeClass('btn-160');
                        $("#divCopyBtn").addClass('btn-80');
                        $("#divBtnQRCode").removeClass('btn-240');
                        $("#divBtnQRCode").addClass('btn-160');
                        $("#divBtnBack").removeClass('btn-320');
                        $("#divBtnBack").addClass('btn-240');
                        $("#divBtnReport").css('margin-top', '320px');
                    }
                }

            } else if ($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_CANCEL ||
                $DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_REJECT ||
                $DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_COMPLETE) {

                $("#divAttachFileBtn").removeClass('hide');
                $("#divCopyBtn").removeClass('hide');
                $("#divBtnQRCode").removeClass('hide');
                $('#divBtnReport').removeClass('hide');

                $("#btnApprove").addClass('hide');
                $("#btnCancel").addClass('hide');

                $("#divAttachFileBtn").addClass('btn-0');
                $("#divCopyBtn").addClass('btn-80');
                $("#divBtnQRCode").addClass('btn-160');
                $('#divBtnReport').addClass('btn-240');
                $("#divBtnReport").css('margin-top', '240px');
            }
        } else if (session.roleName.indexOf('ROLE_OFFICE_ADMIN') != -1) {
            $("#divApproveBtn").removeClass('hide');
            $("#actionReasonDiv").removeClass('hide');
            $("#divAttachFileBtn").removeClass('hide');

            $("#divApproveBtn").addClass('btn-0');
            $("#divAttachFileBtn").addClass('btn-80');
        } else if (session.roleName.indexOf('ROLE_ACCOUNT') != -1) {
            // $("#divUnlock").removeClass('hide');
            $("#actionReasonDiv").removeClass('hide');
            $("#divCancelBtn").removeClass('hide');
            $("#divAttachFileBtn").removeClass('hide');
            $("#divBtnQRCode").removeClass('hide');
            $("#divBtnBack").removeClass('hide');
            $("#divBtnReport").removeClass('hide');

            $("#divCancelBtn").addClass('btn-0');
            $("#divAttachFileBtn").addClass('btn-80');
            $("#divBtnQRCode").addClass('btn-160');
            $("#divBtnBack").addClass('btn-240');
            $("#divBtnReport").css('margin-top', '320px');
        } else if (flagFinance) {
            $("#divAttachFileBtn").removeClass('hide');
            $("#divBtnQRCode").removeClass('hide');
            $("#divBtnBack").removeClass('hide');
            $("#divBtnReport").removeClass('hide');

            $("#divAttachFileBtn").addClass('btn-0');
            $("#divBtnQRCode").addClass('btn-80');
            $("#divBtnBack").addClass('btn-160');
            $("#divBtnReport").css('margin-top', '240px');
        } else {
            $("#actionReasonDiv").removeClass('hide');
            $("#divApproveBtn").removeClass('hide');
            $("#divCancelBtn").removeClass('hide');
            $("#divAttachFileBtn").removeClass('hide');
            $('#divBtnReport').removeClass('hide');

            $("#divApproveBtn").addClass('btn-0');
            $("#divCancelBtn").addClass('btn-80');
            $("#divAttachFileBtn").addClass('btn-160');
            $("#divBtnReport").css('margin-top', '240px');
        }
    }else{
        if ($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_CANCEL ||
            $DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_REJECT ) {
            $("#divAttachFileBtn").removeClass('hide');
            $("#divCopyBtn").removeClass('hide');
            $("#divBtnQRCode").removeClass('hide');
            $('#divBtnReport').removeClass('hide');

            $("#btnApprove").addClass('hide');
            $("#btnCancel").addClass('hide');

            $("#divAttachFileBtn").addClass('btn-0');
            $("#divCopyBtn").addClass('btn-80');
            $("#divBtnQRCode").addClass('btn-160');
            $('#divBtnReport').addClass('btn-240');
            $("#divBtnReport").css('margin-top', '240px');
        }

        if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_COMPLETE){
            if($DATA_DOCUMENT.documentAdvance && $DATA_DOCUMENT.documentAdvance.externalClearingDocNumber){
                $("#divAttachFileBtn").removeClass('hide');
                $("#divCopyBtn").removeClass('hide');
                $("#divBtnQRCode").removeClass('hide');
                $('#divBtnReport').removeClass('hide');

                $("#btnApprove").addClass('hide');
                $("#btnCancel").addClass('hide');

                $("#divAttachFileBtn").addClass('btn-0');
                $("#divCopyBtn").addClass('btn-80');
                $("#divBtnQRCode").addClass('btn-160');
                $('#divBtnReport').addClass('btn-240');
                $("#divBtnReport").css('margin-top', '240px');
            }else{
                $("#divAttachFileBtn").removeClass('hide');
                $("#clearManual").removeClass('hide');
                $("#divCopyBtn").removeClass('hide');
                $("#divBtnQRCode").removeClass('hide');
                $('#divBtnReport').removeClass('hide');

                $("#btnApprove").addClass('hide');
                $("#btnCancel").addClass('hide');

                // $("#divClearManual").addClass('btn-0');
                $("#divAttachFileBtn").addClass('btn-80');
                $("#divCopyBtn").addClass('btn-160');
                $("#divBtnQRCode").addClass('btn-240');
                // $('#divBtnReport').addClass('btn-320');
                $("#divBtnReport").css('margin-top', '320px');
            }

        }
    }
}

function validateApproveType(){
    
    if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC){
    	$("#approveTypeLB").text(LB.LABEL_TRAVEL_DEMESTIC);
    	// getMasterDataLocation("D");
        $("#travelReason").val($DATA_DOCUMENT.documentApprove.documentApproveItem[0].travelReason == null ? "" : $DATA_DOCUMENT.documentApprove.documentApproveItem[0].travelReason);
    }else if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
    	$("#approveTypeLB").text(LB.LABEL_TRAVEL_FOREIGN);
    	// getMasterDataLocation("F");
    	$("#travelReason").val($DATA_DOCUMENT.documentApprove.documentApproveItem[0].travelReason == null ? "" : $DATA_DOCUMENT.documentApprove.documentApproveItem[0].travelReason);
    }
    
}

/* PANEL TRAVEL DETAIL */
var remark;
function remarkDetail(obj){
	remark = obj;   
    for(var i = 0; i<$DATA_TRAVEL_DETAIL.length;i++){
    	if($DATA_TRAVEL_DETAIL[i].id == parseInt(obj.id)){
    		$("#alertModal").modal('show');
    	    $("label[id=detailAlert]").text($DATA_TRAVEL_DETAIL[i].remark);
    	}
    }
}

function remarkRequestDetail(txt){

    console.log('>>Message Reason>>>'+txt)

    if(txt != null && txt != '' && txt!= undefined && txt != 'null'){

        $("#alertModal").modal('show');
        $("label[id=detailAlert]").html('Remark : '+txt);

    }

}

function renderTravelDetail(dataTravel){
	$("#collapseHeaderTravel").empty();
	if(dataTravel.length > 0){
		for(var i=0;i<dataTravel.length;i++){
			var startDate = DateUtil.coverDateToString(dataTravel[i].startDate);
			var endDate   = DateUtil.coverDateToString(dataTravel[i].endDate);	
			
			$("#collapseHeaderTravel").append('<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataTravel[i].id+'>'+
                '<div class="panel-heading collapseGray" id='+dataTravel[i].id+' role="button" style="padding-bottom: 0px" >'+
                '<div class="form-group" style="margin-bottom: 0px">'+
                '<div class="col-sm-5" style="padding-left: 0px;padding-right: 0px;" id='+dataTravel[i].id+' >'+
                '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;font-size:18px;"><b>'+(session.lang != 'en' ? dataTravel[i].origin.description : dataTravel[i].origin.descriptionEn)+'</b></label></div>'+
                '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;font-size:16px;"><b>'+startDate+'</b> &#160;&#160;<b>'+dataTravel[i].startTime+'</b></label></div>'+
                '</div>'+
                '<div class="col-sm-4" id='+dataTravel[i].id+' style="padding-left: 0px;padding-right: 0px;" >'+
                '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #04f9fd;font-size:18px;"><b>'+(session.lang != 'en' ? dataTravel[i].destination.description : dataTravel[i].destination.descriptionEn)+'</b></label></div>'+
                '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #04f9fd;font-size:16px;"><b>'+endDate+'</b> &#160;&#160;<b>'+dataTravel[i].endTime+'</b></label></div>'+
                '</div>'+
                '<div class="col-sm-2" id='+dataTravel[i].id+' style="padding-left: 0px;padding-right: 0px;">'+
                '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #b2ff59;font-size:30px"><b>'+dataTravel[i].travelDays+'</b>&#160;&#160;<b>'+LB.LABEL_DAY+'</b></label></div>'+
                '</div>'+
                '<div class="col-sm-1" style="padding-left: 0px;padding-right: 0px;">'+
                '<div class="col-xs-12 col-sm-12" style="padding-left: 0px;padding-right: 0px;"><a href="javascript:void(0)" role="button" id='+dataTravel[i].id+' onclick="remarkDetail(this)"><img src='+IMG.CHAT+' width="50px"/></a></div>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>');
		}
	}
	$("#modalAddDetail").modal('hide');
}

function findTravelDetailsByDocumentApproveItemId(documentAppItem){
	$.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/'+documentAppItem+'/travelDetails',
        complete: function (xhr) {
        	$DATA_TRAVEL_DETAIL = JSON.parse(xhr.responseText);
        	renderTravelDetail($DATA_TRAVEL_DETAIL);
        }
    });  
}


/* PANEL MEMBER PERSON */
function renderDataTravelMember(dataTravelMember){
    $("#collapseHeaderMember").empty();
    $("#personMember").text(dataTravelMember.length);
    if(dataTravelMember.length > 0){
        for(var i=0;i<dataTravelMember.length;i++){
            findEmployeeProfileByUserName(dataTravelMember[i].memberUser);
            var memberName;
            var positionMember;
            if(session.lang === 'en'){
                memberName = $DATA_EMPLOYEE.NameEN;
                positionMember = $DATA_EMPLOYEE.PositionEN;
            }else{
                memberName = $DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH;
                positionMember = $DATA_EMPLOYEE.PositionTH;
            }
            var departmentMember = $DATA_EMPLOYEE.Org_Name_TH_800;

            $("#collapseHeaderMember").append('<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataTravelMember[i].id+'>'+
                '<div class="panel-heading collapseGray" id='+dataTravelMember[i].id+' role="button" style="padding-bottom: 0px" >'+
                '<div class="form-group" style="margin-bottom: 0px">'+
                '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id='+dataTravelMember[i].id+' onclick="editTravelMember(this)" >'+
                '<div class="col-sm-12" style="padding-left: 0px;"><label class="control-label" style="color: white;font-size:16px;"><b>'+memberName+'</b></label></div>'+
                '<div class="col-sm-12" style="padding-left: 0px;"><label class="control-label" style="color: white;font-size:16px;"><b>'+dataTravelMember[i].memberPersonalId+'</b></label></div>'+
                '</div>'+
                '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id='+dataTravelMember[i].id+' onclick="editTravelMember(this)"  >'+
                '<div class="col-sm-12" style="padding-left: 0px;"><label class="control-label" style="color: #04f9fd;font-size:16px;"><b>'+positionMember+'</b></label></div>'+
                '</div>'+
                '<div class="col-sm-4" style="padding-left: 0px;padding-right: 0px;" id='+dataTravelMember[i].id+' onclick="editTravelMember(this)" >'+
                '<div class="col-sm-12" style="padding-left: 0px;"><label class="control-label" style="color: #b2ff59;font-size:16px"><b>'+departmentMember+'</b></label></div>'+
                '</div>'+
                // '<div class="col-sm-3" style="padding-right: 0px;padding-left: 0px;">'+
                // '<a href="javascript:void(0)">'+renderButton("flagFlight",dataTravelMember[i].flagFlight,dataTravelMember[i].id)+'</a>&#160;&#160;&#160;'+
                // '<a href="javascript:void(0)">'+renderButton("flagCar",dataTravelMember[i].flagCar,dataTravelMember[i].id)+'</a>&#160;&#160;&#160;'+
                // '<a href="javascript:void(0)">'+renderButton("flagBed",dataTravelMember[i].flagBed,dataTravelMember[i].id)+'</a>&#160;&#160;&#160;'+
                // '<a href="javascript:void(0)">'+renderButton("flagBedTwin",dataTravelMember[i].flagBedTwin,dataTravelMember[i].id)+'</a>'+
                // '</div >'+
                '</div>'+
                '</div>'+
                '</div>');

        }
    }
	
	
	$("#modalAddPerson").modal('hide');
}

function renderButton(typeBTN,valueType,id){
	if("flagFlight" == typeBTN){
		if(valueType == true){
            return '<img src='+IMG.BTN_PLANE+' id="btn_plane" data-id='+id+' name="vehicleType" width="40px" value="active" />'
        }else{
            return '<img src='+IMG.BTN_PLANE_A+' id="btn_plane"  data-id='+id+' name="vehicleType" width="40px" value="inActive" />'
		}
	}if("flagCar" == typeBTN){
        if(valueType == true){
            return '<img src='+IMG.BTN_CAR+' id="btn_car" data-id='+id+' name="vehicleType" width="40px" value="active" />'
        }else{
            return '<img src='+IMG.BTN_CAR_A+' id="btn_car"  data-id='+id+' name="vehicleType" width="40px" value="inActive" />'
        }
	}if("flagBed" == typeBTN){
        if(valueType == true){
            return '<img src='+IMG.BTN_BED+' id="btn_bed" data-id='+id+' name="bedType" width="40px" value="active" />'
        }else{
            return '<img src='+IMG.BTN_BED_A+' id="btn_bed" data-id='+id+' name="bedType" width="40px" value="inActive"  />'
        }
	}if("flagBedTwin" == typeBTN){
        if(valueType == true){
            return '<img src='+IMG.BTN_BEDS+' id="btn_beds" data-id='+id+' name="bedType" width="40px" value="active" />'
        }else{
            return '<img src='+IMG.BTN_BEDS_A+' id="btn_beds" data-id='+id+' name="bedType" width="40px" value="inActive" />'
        }
	}
}

function findTravelMembersByDocumentApproveItemId(documentAppItem){
	$.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/'+documentAppItem+'/travelMembers',
        complete: function (xhr) {
        	$DATA_TRAVEL_MEMBER = JSON.parse(xhr.responseText);
        	renderDataTravelMember($DATA_TRAVEL_MEMBER);
        	
        }
    });  
}


/* PANEL EXTERNAL MEMBER */
function addExternalMember(btn){
	if(btn == "add"){
		$ExternalMemberId = null;
		$("#externalName").val("");
		$("#externalCompany").val("");
		$("#externalPhoneNumber").val("");
		$("#externalEmail").val("");
		$("#externalRemark").val("");
		$("#modalAddOuterPerson").modal('show');
	}
}

function renderDataExternalMember(dataExternal){
	$("#collapseHeaderExternal").empty();
	$("#personExternal").text(dataExternal.length > 0 ? dataExternal.length : 0);
	if(dataExternal.length > 0){
		for(var i=0;i<dataExternal.length;i++){
			
			$("#collapseHeaderExternal").append('<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px" id='+dataExternal[i].id+'>'+
				'<div class="panel-heading collapseGray" id='+dataExternal[i].id+' role="button" style="padding-bottom: 0px;padding-left: 0px;padding-right: 0px;" >'+
					'<div class="container-fluid">'+
						'<div class="col-sm-5" style="padding-left: 0px;padding-right: 0px;" id='+dataExternal[i].id+' onclick="editExternalMemberl(this)" >'+
							'<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: white;font-size:16px;"><b>'+dataExternal[i].memberName+'</b></label></div>'+
							'<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #08f508;font-size:14px;"><b>'+LB.LABEL_TEL+'</b> &#160;:&#160;<b>'+dataExternal[i].phoneNumber+'</b></label></div>'+
						'</div>'+
						'<div class="col-sm-5" style="padding-left: 0px;padding-right: 0px;" id='+dataExternal[i].id+' onclick="editExternalMemberl(this)"  >'+
							'<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #04f9fd;font-size:16px;"><b>'+dataExternal[i].companyMember+'</b></label></div>'+
							'<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;"><label class="control-label" style="color: #08f508;font-size:14px;"><b>'+dataExternal[i].email+'</b></label></div>'+
						'</div>'+
						// '<div class="col-sm-3" style="padding-left: 0px;padding-right: 0px;">'+
						// 	'<a href="javascript:void(0)">'+renderButtonExternal("flagFlight",dataExternal[i].flagFlight,dataExternal[i].id)+'</a>&#160;&#160;&#160;'+
						// 	'<a href="javascript:void(0)">'+renderButtonExternal("flagCar",dataExternal[i].flagCar,dataExternal[i].id)+'</a>&#160;&#160;&#160;'+
						// 	'<a href="javascript:void(0)">'+renderButtonExternal("flagBed",dataExternal[i].flagBed,dataExternal[i].id)+'</a>&#160;&#160;&#160;'+
						// 	'<a href="javascript:void(0)">'+renderButtonExternal("flagBedTwin",dataExternal[i].flagBedTwin,dataExternal[i].id)+'</a>'+
						// '</div >'+
						'<div class="col-sm-1" style="padding-left: 0px;padding-right: 0px;">'+
							'<div class="col-sm-12" style="padding-left: 0px;padding-right: 0px;"><a href="javascript:void(0)" role="button" id='+dataExternal[i].id+' onclick="remarkExternal(this)"><img src='+IMG.CHAT+' width="40px"/></a></div>'+
						'</div>'+
					'</div>'+
				'</div>'+
			 '</div>');
		}
	}	
	
	$("#modalAddOuterPerson").modal('hide');
}

function remarkExternal(obj){
    for(var i = 0; i<$DATA_EXTERNAL_MEMBER.length;i++){
    	if($DATA_EXTERNAL_MEMBER[i].id == parseInt(obj.id)){
    		$("#alertModal").modal('show');
    	    $("label[id=detailAlert]").text($DATA_EXTERNAL_MEMBER[i].remark);
    	}
    }
}

function insertDataExternalMember(){
	var externalName      		= $("#externalName").val();
	var externalCompany			= $("#externalCompany").val();
	var externalPhoneNumber	    = $("#externalPhoneNumber").val();
	var externalEmail	    	= $("#externalEmail").val();
	var externalRemark	    	= $("#externalRemark").val();

	var errorMessage = "";
	if(externalName == ""){
		errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_FULL_NAME+"<br/>";
		$('#warningModal .modal-body').html(errorMessage);
		$("#warningModal").modal('show');
	}else{
        if(FLAG_CAR == null && FLAG_FLIGHT == null && FLAG_BED == null && FLAG_BED_TWIN == null){
            if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC){
                FLAG_CAR = true;
                FLAG_FLIGHT = false;
                FLAG_BED = true;
                FLAG_BED_TWIN = false;
            }else if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
                FLAG_CAR = false;
                FLAG_FLIGHT = true;
                FLAG_BED = true;
                FLAG_BED_TWIN = false;
            }
        }

		var jsonData = {};
	    jsonData['parentId']     		= 1;
	    jsonData[csrfParameter]  		= csrfToken;
	    jsonData['id']					= $ExternalMemberId;
	    jsonData['memberName']    		= externalName;
	    jsonData['companyMember']  		= externalCompany;
	    jsonData['phoneNumber']      	= externalPhoneNumber;
	    jsonData['email']    			= externalEmail;
	    jsonData['remark']    			= externalRemark;
	    jsonData['flagCar']    			= FLAG_CAR;
	    jsonData['flagFlight']    		= FLAG_FLIGHT;
	    jsonData['flagBed']    			= FLAG_BED;
	    jsonData['flagBedTwin']    		= FLAG_BED_TWIN;
	    jsonData['documentApproveItem']    	= $DATA_DOCUMENT.documentApprove.documentApproveItem[0].id;
	    
	    var dataTravel = $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/saveExternalMember',
            data: jsonData ,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                	findExternalMembersByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItem[0].id);
                }
            }
       });
    }
}

function findExternalMembersByDocumentApproveItemId(documentAppItem){
	$.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/'+documentAppItem+'/externalMembers',
        complete: function (xhr) {
        	$DATA_EXTERNAL_MEMBER = JSON.parse(xhr.responseText);
        	renderDataExternalMember($DATA_EXTERNAL_MEMBER);
        }
    });  
}

function editExternalMemberl(obj){
	$STATUS = "update";
	$ExternalMemberId = obj.id;
	$("#modalAddOuterPerson").modal('show');
	renderDataToModalEditExternalMember(obj.id);
}

function renderDataToModalEditExternalMember(id){
	for(var i = 0; i<$DATA_EXTERNAL_MEMBER.length;i++){
    	if($DATA_EXTERNAL_MEMBER[i].id == parseInt(id)){
    		$("#externalName").val($DATA_EXTERNAL_MEMBER[i].memberName);
    		$("#externalCompany").val($DATA_EXTERNAL_MEMBER[i].companyMember);
    		$("#externalPhoneNumber").val($DATA_EXTERNAL_MEMBER[i].phoneNumber);
    		$("#externalEmail").val($DATA_EXTERNAL_MEMBER[i].email);
    		$("#externalRemark").val($DATA_EXTERNAL_MEMBER[i].remark);
    	}
    }
}

function deleteExternalMember(id){
	
	var jsonParams2 = {};
	jsonParams2['parentId']     = 1;
	jsonParams2[csrfParameter]  = csrfToken;
	
	$.ajax({
        type: "DELETE",
        url: session['context']+'/approve/deleteExternalMember/'+id,
        data: jsonParams2,
        complete: function (xhr) {
        	findExternalMembersByDocumentApproveItemId($DATA_DOCUMENT.documentApprove.documentApproveItem[0].id);
        	$('#deleteItemModal').modal('hide');
        }
    });
}

function changeBtnTypeExternal(btn){

    BTN = btn;
    if(btn.getAttribute('id') == "plane_btn" && btn.getAttribute('value') == "inActive"){
        $("#plane_btn").attr('src',IMG.BTN_PLANE);
        $("#plane_btn").attr('value',"active");
        $("#car_btn").attr('src',IMG.BTN_CAR_A);
        $("#car_btn").attr('value',"inActive");
        FLAG_FLIGHT = true;
        FLAG_CAR = false;
    }else if(btn.getAttribute('id') == "car_btn" && btn.getAttribute('value') == "inActive"){
        $("#car_btn").attr('src',IMG.BTN_CAR);
        $("#car_btn").attr('value',"active");
        $("#plane_btn").attr('src',IMG.BTN_PLANE_A);
        $("#plane_btn").attr('value',"inActive");
        FLAG_CAR = true;
        FLAG_FLIGHT = false;
    }else if(btn.getAttribute('id') == "bed_btn" && btn.getAttribute('value') == "inActive"){
        $("#bed_btn").attr('src',IMG.BTN_BED);
        $("#bed_btn").attr('value',"active");
        $("#beds_btn").attr('src',IMG.BTN_BEDS_A);
        $("#beds_btn").attr('value',"inActive");
        FLAG_BED = true;
        FLAG_BED_TWIN = false;
    }else if(btn.getAttribute('id') == "beds_btn" && btn.getAttribute('value') == "inActive"){
        $("#beds_btn").attr('src',IMG.BTN_BEDS);
        $("#beds_btn").attr('value',"active");
        $("#bed_btn").attr('src',IMG.BTN_BED_A);
        $("#bed_btn").attr('value',"inActive");
        FLAG_BED_TWIN = true;
        FLAG_BED = false;
    }
    updateDataExternalMember(BTN.getAttribute('data-id'));

}

function renderButtonExternal(typeBTN,valueType,id){
    if("flagFlight" == typeBTN){
        if(valueType == true){
            return '<img src='+IMG.BTN_PLANE+' id="plane_btn" data-id='+id+' name="vehicleType" width="40px" value="active" onclick="changeBtnTypeExternal(this)" />'
        }else{
            return '<img src='+IMG.BTN_PLANE_A+' id="plane_btn" data-id='+id+' name="vehicleType" width="40px" value="inActive" onclick="changeBtnTypeExternal(this)" />'
        }
    }if("flagCar" == typeBTN){
        if(valueType == true){
            return '<img src='+IMG.BTN_CAR+' id="car_btn" data-id='+id+'  name="vehicleType" width="40px" value="active" onclick="changeBtnTypeExternal(this)" />'
        }else{
            return '<img src='+IMG.BTN_CAR_A+' id="car_btn" data-id='+id+' name="vehicleType" width="40px" value="inActive" onclick="changeBtnTypeExternal(this)" />'
        }
    }if("flagBed" == typeBTN){
        if(valueType == true){
            return '<img src='+IMG.BTN_BED+' id="bed_btn" data-id='+id+' name="bedType" width="40px" value="active" onclick="changeBtnTypeExternal(this)" />'
        }else{
            return '<img src='+IMG.BTN_BED_A+' id="bed_btn" data-id='+id+' name="bedType" width="40px" value="inActive" onclick="changeBtnTypeExternal(this)" />'
        }
    }if("flagBedTwin" == typeBTN){
        if(valueType == true){
            return '<img src='+IMG.BTN_BEDS+' id="beds_btn" data-id='+id+' name="bedType" width="40px" value="active" onclick="changeBtnTypeExternal(this)" />'
        }else{
            return '<img src='+IMG.BTN_BEDS_A+' id="beds_btn" data-id='+id+' name="bedType" width="40px" value="inActive" onclick="changeBtnTypeExternal(this)" />'
        }
    }
}

function updateDataExternalMember(id){

    var memberName;
    var companyMember;
    var phoneNumber;
    var email;
    var remark;
    var documentApproveItem;

    for(var i=0;i<$DATA_EXTERNAL_MEMBER.length;i++){
        if($DATA_EXTERNAL_MEMBER[i].id == id){
            memberName = $DATA_EXTERNAL_MEMBER[i].memberName;
            companyMember = $DATA_EXTERNAL_MEMBER[i].companyMember;
            phoneNumber = $DATA_EXTERNAL_MEMBER[i].phoneNumber;
            email = $DATA_EXTERNAL_MEMBER[i].email;
            remark = $DATA_EXTERNAL_MEMBER[i].remark;
            documentApproveItem = $DATA_EXTERNAL_MEMBER[i].documentApproveItem.id;
        }
    }


    var jsonData = {};
    jsonData['parentId']     		= 1;
    jsonData[csrfParameter]  		= csrfToken;
    jsonData['id']					= id;
    jsonData['memberName']    		= memberName;
    jsonData['companyMember']  		= companyMember;
    jsonData['phoneNumber']      	= phoneNumber;
    jsonData['email']    			= email;
    jsonData['remark']    			= remark;
    jsonData['flagCar']    			= FLAG_CAR;
    jsonData['flagFlight']    		= FLAG_FLIGHT;
    jsonData['flagBed']    			= FLAG_BED;
    jsonData['flagBedTwin']    		= FLAG_BED_TWIN;
    jsonData['documentApproveItem']    	= documentApproveItem;

    var dataTravel = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/approve/saveExternalMember',
        data: jsonData ,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                findExternalMembersByDocumentApproveItemId(documentApproveItem);
            }
        }
    });
}

function setUpDefaultCarType(str){
    if(session.roleName.indexOf('ROLE_OFFICE_ADMIN') != -1){
        for(var i=0;i<$("[name=iconCarAdm]").length;i++){
            if('van' == $("[name=iconCarAdm]")[i].getAttribute('value')){
                $("[name=iconCarAdm]")[i].setAttribute('src',IMAGE_CAR.VAN_A);
                $("[name=iconCarAdm]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_VAN);
            }else if('sedan' == $("[name=iconCarAdm]")[i].getAttribute('value')){
                $("[name=iconCarAdm]")[i].setAttribute('src',IMAGE_CAR.SEDAN_A);
                $("[name=iconCarAdm]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_SEDAN);
            }else if('tuck' == $("[name=iconCarAdm]")[i].getAttribute('value')){
                $("[name=iconCarAdm]")[i].setAttribute('src',IMAGE_CAR.TUCK_A);
                $("[name=iconCarAdm]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_TUCK);
            }else if('other' == $("[name=iconCarAdm]")[i].getAttribute('value')){
                $("[name=iconCarAdm]")[i].setAttribute('src',IMAGE_CAR.OTHER_A);
                $("[name=iconCarAdm]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_OTHER);
            }
        }
    }else{
        for(var i=0;i<$("[name=iconCar]").length;i++){
            if('van' == $("[name=iconCar]")[i].getAttribute('value')){
                $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.VAN_A);
                $("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_VAN);
            }else if('sedan' == $("[name=iconCar]")[i].getAttribute('value')){
                $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.SEDAN_A);
                $("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_SEDAN);
            }else if('tuck' == $("[name=iconCar]")[i].getAttribute('value')){
                $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.TUCK_A);
                $("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_TUCK);
            }else if('other' == $("[name=iconCar]")[i].getAttribute('value')){
                $("[name=iconCar]")[i].setAttribute('src',IMAGE_CAR.OTHER_A);
                $("[name=iconCar]")[i].setAttribute('id',MASTER_DATA.CAR_TYPE_OTHER);
            }
        }
    }
}

function verifyDocApproveItem(appType) {
    var dataApproveItem = $DATA_DOCUMENT.documentApprove.documentApproveItem;
    for(var i=0;i<dataApproveItem.length;i++){
        if(dataApproveItem[i].approveType == appType){
            return dataApproveItem[i].id;
        }
    }
}


function renderDataCarBooking(dataCarBooking){

    if(dataCarBooking != null){
        $("#carReserve").prop('checked',true);
    }
}


function renderDataHotelBooking(dataHotelBooking){

    if(dataHotelBooking != null){
        $("#hotelReserve").prop('checked',true);
    }

}

/* manage flight */
function renderDataFlightTicket(dataFlightTicket){

    if(dataFlightTicket != null){
        $("#flightReserve").prop('checked',true);

        FLIGHT_TYPE_ACTIVE = true;
    }

}

/* manage advances */
function renderDocumentAdvance(documentAdvance){
    var amount = documentAdvance.amount == null ? 0 : documentAdvance.amount.toFixed(2);
    $("#advancesAmount").text(NumberUtil.formatCurrency(amount));
	$("#borrowDate").text(DateUtil.coverDateToString(documentAdvance.startDate));
	$("#advanceEndDate").text(DateUtil.coverDateToString(documentAdvance.endDate));
	$("#dueDate").text(DateUtil.coverDateToString(documentAdvance.dueDate));
	$("#bankNumber").text(documentAdvance.bankNumber);
    $("#advancesMoney").text(NumberUtil.formatCurrency(amount));
    $("#remark").text(documentAdvance.remark);

    var fiDoc = "";
    if(documentAdvance.externalDocNumber != null){
        fiDoc += documentAdvance.externalDocNumber+",";
    }
    if(documentAdvance.externalPaymentDocNumber != null){
        fiDoc += documentAdvance.externalPaymentDocNumber+",";
    }
    if(documentAdvance.externalClearingDocNumber != null){
        fiDoc += documentAdvance.externalClearingDocNumber+",";
    }

    if(fiDoc != ""){
        $("#fiDoc").text("("+fiDoc.substr(0,fiDoc.lastIndexOf(","))+")");
    }

    if(amount != 0 && documentAdvance.startDate != null && documentAdvance.endDate != null && documentAdvance.dueDate != null){
        ADVANCE_TYPE_ACTIVE = true;
    }
}

/* manage copy document */
function copyDocument(){

	var jsonDocument = {};
	jsonDocument['parentId']     	= 1;
	jsonDocument[csrfParameter]  	= csrfToken;
	jsonDocument['id']   			= $DATA_DOCUMENT.id;

    var dataDocument = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/approve/copyDocument',
        data:jsonDocument,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
            	dataDocument = JSON.parse(xhr.responseText);
            	var appType = dataDocument.documentApprove.documentApproveItem[0].approveType;
            	window.location.href = session.context+'/approve/createDocSetDetail?doc='+dataDocument.id;
            }
        }
    });
}

/* cancel document */
function cancelRequest(listJsonDetails){

    $('.dv-background').show();

    window.setTimeout(function() {

        var jsonData = {};
        jsonData['id'] = $DATA_DOCUMENT.id;
        jsonData['requester'] = $DATA_DOCUMENT.requester;
        jsonData['docNumber'] = $DATA_DOCUMENT.docNumber;
        jsonData['details'] = JSON.stringify(listJsonDetails);


        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/requests/cancelRequest',
            data: jsonData,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    $DATA_REQUEST = JSON.parse(xhr.responseText);
                    if ($DATA_REQUEST != null) {
                        $('.dv-background').hide();
                        cancelDocument($DATA_REQUEST.document.id);
                    }

                }
            }
        });
    },500);

}

function cancelDocument(){
    $('.dv-background').show();
    window.setTimeout(function() {
        var jsonData = {};
        jsonData['parentId'] = 1;
        jsonData[csrfParameter] = csrfToken;
        jsonData['id'] = $DATA_DOCUMENT.id;
        jsonData['documentStatus'] = MASTER_DATA.DOC_STATUS_CANCEL;

        var dataDocument = $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/updateDocumentStatus',
            data: jsonData,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    dataDocument = JSON.parse(xhr.responseText);
                    $('.dv-background').hide();
                    backToMenu();
                }
            }
        });
    },500);
}

/* manage document attachment */
function documentAttachment(){
	getDataAttachmentType();
	$("#modalDocumentAttachment").modal('show');
	$("#attachmentType").val("");
	$("#textFileName").val("");
}

function getDataAttachmentType(){
	$("#attachmentType").empty().append('<option value=""></option>');
    for(var i=0; i<MASTER_DATA.ATTACHMENT_TYPE.length;i++){
        $("#attachmentType").append(
            '<option value='+MASTER_DATA.ATTACHMENT_TYPE[i].code+' extension='+MASTER_DATA.ATTACHMENT_TYPE[i].variable1+'>'+ MASTER_DATA.ATTACHMENT_TYPE[i].description + '</option>');
    }
}

function findDocumentAttachmentByDocumentId(documentId){
	$.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/'+documentId+'/documentAttachment',
        complete: function (xhr) {
        	$DATA_DOCUMENT_ATTACHMENT = JSON.parse(xhr.responseText);
        	renderDocumentAttachment($DATA_DOCUMENT_ATTACHMENT);
        }
    });  
}

function renderDocumentAttachment(dataDocumentAttachment){
	$('#gridDocumentAttachmentBody').empty();
    if(dataDocumentAttachment.length != 0 ){
        for(var i=0; i<dataDocumentAttachment.length;i++){
            $('#gridDocumentAttachmentBody').append('' +
                '<tr id="' + dataDocumentAttachment[i].id + '">' +
                '<td align="center">'+(i+1)+'</td>' +
                '<td align="center">'+getAttachmentType(dataDocumentAttachment[i].attachmentType)+'</td>' +
                '<td align="center">'+dataDocumentAttachment[i].fileName+'</td>' +
                '<td align="center">' +
                '<a ><img style="margin-left: 5px;" src="' + $IMAGE_SEARCH + '" width="30px" id="downloadDocumentExpenseItemAttachment'+i+'" idDocumentExpenseItemAttachmentDownload="'+dataDocumentAttachment[i].id+'" index="' + i + '" onclick="preViewAttachmentFileExpenseItem(\''+dataDocumentAttachment[i].id+'\',\''+dataDocumentAttachment[i].fileName+'\')"/></a>' +
                '<button id='+dataDocumentAttachment[i].id+' fileName="'+dataDocumentAttachment[i].fileName+'" type="button" class="btn btn-material-blue-500 btn-style-small" onclick="downloadDocumentFile($(this)) "><span class="fa fa-cloud-download"/></button>' +
                '</td>' +
                '</tr>'
            );
        }
    }
}

var BTNDownloadFile;
function downloadDocumentFile(btn){
    BTNDownloadFile = btn;
    var id = BTNDownloadFile.attr('id');
    var fileName = BTNDownloadFile.attr('fileName');

    location.href= session.context+'/approve/downloadFileDocumentAttachment?id='+id+'&fileName='+fileName;
}


function getAttachmentType(code){
	for(var i=0;i<MASTER_DATA.ATTACHMENT_TYPE.length;i++){
		if(MASTER_DATA.ATTACHMENT_TYPE[i].code == code){
			return MASTER_DATA.ATTACHMENT_TYPE[i].description;
			break;
		}
	}
}

function callQRCodeDocSet(){
    let user_account;

    $DATA_REQUEST_APPROVER.forEach(function (item) {
        if(item.actionState.indexOf("ACC") >= 0){
            user_account = item.userNameApprover;
        }
    })
    window.location.href = session.context+'/qrcode/generateQrCode?text='+ $URL_EWF +'qrcodescandata/'+user_account+'/'+$DATA_DOCUMENT.docNumber;
}

/* for get employee profile by user name */
function findEmployeeProfileByUserName(userName) {

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName=" + userName,
        headers: {
            Accept: "application/json"
        },
        type: "GET",
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                $DATA_EMPLOYEE = JSON.parse(xhr.responseText);
            }
        }
    });
}

function renderLineApprove(requestApprover){
    findEmployeeProfileByUserName($DATA_DOCUMENT.requester);

    var requesterName;
    var requesterPosition;
    var dataStep;

    if(session.lang === 'en'){
        requesterName = nvl($DATA_EMPLOYEE.NameEN);
        requesterPosition = nvl($DATA_EMPLOYEE.PositionEN);
        dataStep = requestApprover.length + 1;
    }else{
        requesterName = nvl($DATA_EMPLOYEE.FOA) + nvl($DATA_EMPLOYEE.FNameTH) + " " + nvl($DATA_EMPLOYEE.LNameTH);
        requesterPosition = nvl($DATA_EMPLOYEE.PositionTH);
        dataStep = requestApprover.length + 1;
    }


    $("#lineApproverDetail").attr('data-steps',dataStep);
    $("#lineApproverDetail").empty();
    $("#lineApproveMobile").empty();

    $("#lineApproverDetail").append(

        '<li class="idle-complete" style="text-align: center;">'+
        '<span class="step"><span><img src='+IMG.REQUESTER+' style="width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></span>'+
        '<span class="name-idle">'+
        '<div>'+LB.LABEL_REQUESTER+'</div>'+
        '<div style="color: blue;">'+nvl(requesterName)+'</div>'+
        '<div style="color: lightseagreen;">'+nvl(requesterPosition)+'</div>'+
        '</span>'+
        '</li>'
    );

    $("#lineApproveMobile").append('' +
        '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
        '<div class="panel-heading collapseLightGreen" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
        '<div class="container-fluid">'+
        '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
        '<img src="'+IMG.REQUESTER+'" width="45px"/>'+
        '</div>'+
        '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 18px;font-weight: bold"><b>'+LB.LABEL_REQUESTER+'</b></label>'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+ nvl(requesterName) +'</b></label>'+
        '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+ nvl(requesterPosition) +'</b></label>'+
        '</div>'+
        '</div>'+
        '</div>'+
        '</div>'
    );

    requestApprover.sort(sortSequence);

    for(var i=0;i<requestApprover.length;i++){

        findEmployeeProfileByUserName(requestApprover[i].userNameApprover);

        if(session.lang === 'en'){
            requesterName = nvl($DATA_EMPLOYEE.NameEN);
            requesterPosition = nvl($DATA_EMPLOYEE.PositionEN);
            dataStep = requestApprover.length + 1;
        }else{
            requesterName = nvl($DATA_EMPLOYEE.FOA) + nvl($DATA_EMPLOYEE.FNameTH) + " " + nvl($DATA_EMPLOYEE.LNameTH);
            requesterPosition = nvl($DATA_EMPLOYEE.PositionTH);
            dataStep = requestApprover.length + 1;
        }

        var position = $DATA_EMPLOYEE.PositionTH;

        $("#lineApproverDetail").append(
            '<li class='+validateStatus(requestApprover[i].requestStatusCode)+' style="text-align: center;">'+
            '<span class="step"><span>'+validateStatusReject(requestApprover[i].actionState,requestApprover[i].id,requestApprover[i].actionReasonDetail)+'</span></span>'+
            '<span class="name-idle">'+
            '<div>'+validateLanguage(requestApprover[i].actionState)+'</div>'+
            '<div style="color: blue;">'+ requesterName +'</div>'+
            '<div style="color: lightseagreen;">'+requesterPosition +'</div>'+
            '</span>'+
            '</li>'
        );


        if(requestApprover[i].requestStatusCode == null){
            $("#lineApproveMobile").append('' +
                '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
                '<div class="panel-heading collapseLightBlue" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
                '<div class="container-fluid">'+
                '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
                '<img src='+validateIMG(requestApprover[i].actionState)+' width="45px"/>'+
                '</div>'+
                '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 18px;font-weight: bold"><b>'+validateLanguage(requestApprover[i].actionState)+'</b></label>'+
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+ requesterName +'</b></label>'+
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+ requesterPosition +'</b></label>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'
            );
        }else if(requestApprover[i].requestStatusCode == "REJ"){
            $("#lineApproveMobile").append('' +
                '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
                '<div class="panel-heading collapseLightRed" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
                '<div class="container-fluid">'+
                '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
                '<img src='+validateIMG(requestApprover[i].actionState)+' id='+requestApprover[i].id+' onclick="renderRequestMessage(this)" width="45px"/>'+
                '</div>'+
                '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 18px;font-weight: bold"><b>'+validateLanguage(requestApprover[i].actionState)+'</b></label>'+
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+ requesterName +'</b></label>'+
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+ requesterPosition +'</b></label>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'
            );
        } else{
            $("#lineApproveMobile").append('' +
                '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
                '<div class="panel-heading collapseLightGreen" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">'+
                '<div class="container-fluid">'+
                '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">'+
                '<img src='+validateIMG(requestApprover[i].actionState)+' id='+requestApprover[i].id+' onclick="renderRequestMessage(this)" width="45px"/>'+
                '</div>'+
                '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">'+
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 18px;font-weight: bold"><b>'+validateLanguage(requestApprover[i].actionState)+'</b></label>'+
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+ requesterName +'</b></label>'+
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>'+ requesterPosition +'</b></label>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'
            );

            flagApprove = true;
        }

        /** validate butoon for finance */
        if($USERNAME == $DATA_REQUEST.nextApprover && requestApprover[i].userNameApprover == $DATA_REQUEST.nextApprover && requestApprover[i].actionState == "FNC"){
            flagFinance = true;
        }
    }
}

function renderRequestMessage(index){
    console.info(index.id);
}

function validateStatus(status){
    if(status != null && status != "REJ"){
        return "idle-complete";
    }else if(status != null && status == "REJ"){
        checkStatusReject++;
        return "idle";
    }else{
        checkStatus++;
        return "idle";
    }
}

function validateStatusReject(actionState,id,detail){
    if(checkStatus === 1 && checkStatusReject === 0){
        return '<a><img onclick="remarkRequestDetail(\''+nvl(detail)+'\')"   title="View Remark" class="animation-border two"   src='+validateIMG(actionState)+' id='+id+' onclick="renderRequestMessage(this)" style=" width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></a></span>';
    }else if(checkStatusReject === 1 && checkStatus === 0){
        return '<a><img onclick="remarkRequestDetail(\''+nvl(detail)+'\')"   title="View Remark" class="animation-border three"   src='+validateIMG(actionState)+' id='+id+' onclick="renderRequestMessage(this)" style=" width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></a></span>';
    }else{
        return '<a><img onclick="remarkRequestDetail(\''+nvl(detail)+'\')"   title="View Remark" src='+validateIMG(actionState)+' style=" width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></a></span>';
    }
}

function validateIMG(role){
    if(role == "VRF"){ return IMG.VERIFY;}
    if(role == "APR"){ return IMG.APPROVER;}
    if(role == "ACC"){return IMG.ACCOUNT}
    if(role == "FNC"){return IMG.FINANCE}
    if(role == "ADM"){return IMG.ADMIN}
    if(role == "HR"){return IMG.HR}
    if(role == "PAID"){return IMG.PAID}
}

function validateLanguage(role){
    if(role == "VRF"){ return LB.LABEL_VERIFY}
    if(role == "APR"){ return LB.LABEL_APPROVER}
    if(role == "ACC"){return LB.LABEL_ACCOUNT}
    if(role == "FNC"){return LB.LABEL_FINACE}
    if(role == "ADM"){return LB.LABEL_ADMIN}
    if(role == "HR"){return LB.LABEL_HR}
    if(role == "PAID"){return"Paid"}
}


function validateFlowTypeByApproveType(approveType){
    if(approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC){
        return MASTER_DATA.FLOW_TYPE_SET_DOMESTIC;
    }else if(approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
        return MASTER_DATA.FLOW_TYPE_SET_FOREIGN;
    }
}


function findRequestByDocument(id){
    var data = $.ajax({
        url: session.context + "/approve/findRequestByDocument/"+id,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $DATA_REQUEST = data;

    if($DATA_REQUEST != null){
        var data = $.ajax({
            url: session.context + "/approve/findRequestApproverByRequest/"+$DATA_REQUEST.id,
            headers: {
                Accept : "application/json"
            },
            type: "GET",
            async: false
        }).responseJSON;

        $DATA_REQUEST_APPROVER = data;
        renderLineApprove($DATA_REQUEST_APPROVER);
    }else{
        checkLineApproveSet();
    }
}

function findHotelByCode(hotelCode) {
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/findMasterDataHotelByCode',
        complete: function (xhr) {
            var data = JSON.parse(xhr.responseText);
            if(data != null){
                for(var i=0;i<data.length;i++){
                    if(data[i].code == hotelCode){
                        $("#hotel").text(data[i].description);
                        $("#hotelPrice").text(data[i].variable1);
                    }
                }
            }
        }
    });
}

function findAirlineByCode(airlineCode){
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/approve/findMasterDataAirlineByCode',
        complete: function (xhr) {
            var data = JSON.parse(xhr.responseText);
            if(data != null){
                for(var i=0;i<data.length;i++){
                    if(data[i].code == airlineCode){
                        $("#airline").text(data[i].description);
                    }
                }
            }
        }
    });
}

function confirmCancelDocumentSet(){
    if($DATA_DOCUMENT.requester == $USERNAME){
        var warningMessage = MSG.MESSAGE_CONFIRM_CANCEL_DOCUMENT +" "+$DATA_DOCUMENT.docNumber+"<br/>";

        $('#confirmModal .modal-body').html(warningMessage);
        $('#confirmModal').modal('show');
    }else{

        var actionReasonCode = $("#actionReason")[0].selectedOptions[0].value;
        var actionReasonDetail = $("#actionReasonDetail").val();

        if(actionReasonCode == "" || actionReasonDetail == ""){
            $('#warningModal .modal-body').html(MSG.MESSAGE_INPUT_ACTION_REASON);
            $("#warningModal").modal('show');
        }else{
            var warningMessage = MSG.MESSAGE_CONFIRM_REJECT_DOCUMENT +" "+$DATA_DOCUMENT.docNumber+"<br/>";

            $('#confirmRejectModal .modal-body').html(warningMessage);
            $('#confirmRejectModal').modal('show');
        }
    }
}

function sortSequence(a,b) {
    if (a.sequence < b.sequence)
        return -1;
    if (a.sequence > b.sequence)
        return 1;
    return 0;
}

function rejectRequestSet(){

    $('.dv-background').show();

    window.setTimeout(function() {

        /* verify request is active */
        $.ajax({
            type: "GET",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/requests/'+$DATA_REQUEST.id,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    if(xhr.responseText != "Error"){
                        var data_request = JSON.parse(xhr.responseText);
                        if(data_request.requestStatusCode == MASTER_DATA.REQ_STATUS_CXL){
                            $('.dv-background').hide();
                            $('#warningModal .modal-body').html(MSG.MESSAGE_REQUEST_HAS_CANCEL);
                            $("#warningModal").modal('show');
                        }else if(data_request.requestStatusCode == MASTER_DATA.REQ_STATUS_REJ){
                            $('.dv-background').hide();
                            $('#warningModal .modal-body').html(MSG.MESSAGE_REQUEST_HAS_REJECT);
                            $("#warningModal").modal('show');
                        }else if(data_request.requestStatusCode == MASTER_DATA.REQ_STATUS_CMP){
                            $('.dv-background').hide();
                            $('#warningModal .modal-body').html(MSG.MESSAGE_REQUEST_HAS_COMPLETE);
                            $("#warningModal").modal('show');
                        }else{
                            var jsonData = {};

                            if(userHead){
                                if(userHead[0].variable2 == "Y"){
                                    jsonData['userName'] = userHead[0].variable1;
                                    jsonData['actionStatus'] = validateActionState(userHead[0].variable1);
                                }else{
                                    jsonData['userName'] = $USERNAME;
                                    jsonData['actionStatus'] = validateActionState($USERNAME);
                                }
                            }else{
                                jsonData['userName'] = $USERNAME;
                                jsonData['actionStatus'] = validateActionState($USERNAME);
                            }

                            jsonData['documentNumber'] = $DATA_DOCUMENT.docNumber;
                            jsonData['docType'] = $DATA_DOCUMENT.documentType;
                            jsonData['documentFlow'] = $DATA_DOCUMENT.docFlow;
                            jsonData['processId'] = $DATA_DOCUMENT.processId;
                            jsonData['documentId'] = $DATA_DOCUMENT.id;
                            jsonData['actionReasonCode'] = $("#actionReason")[0].selectedOptions[0].value;
                            jsonData['actionReasonDetail'] = $("#actionReasonDetail").val();

                            var url = "";
                            if(session.roleName.indexOf('ROLE_ACCOUNT') == 0){
                                url = session['context'] + '/requests/rejectRequestEmed';
                            }else{
                                url = session['context'] + '/requests/rejectRequest'
                            }

                            $.ajax({
                                type: "POST",
                                headers: {
                                    Accept: 'application/json'
                                },
                                url: url,
                                data:jsonData,
                                async: false,
                                complete: function (xhr) {
                                    if (xhr.readyState == 4) {

                                        $DATA_APPROVE = JSON.parse(xhr.responseText);

                                        if ($DATA_APPROVE != null || $DATA_APPROVE != undefined) {
                                            $('.dv-background').hide();
                                            backToMenu();
                                            // window.location.href = session.context;
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });

    },500);
}

function approveDocumentSet(){

    // if(session.roleName.indexOf('ROLE_OFFICE_ADMIN') != -1){
    //     updateDocumentRoleAdmin();
    // }else{

        $('.dv-background').show();

        window.setTimeout(function() {
            $.ajax({
                type: "GET",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/requests/' + $DATA_REQUEST.id,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        if (xhr.responseText != "Error") {
                            var data_request = JSON.parse(xhr.responseText);
                            if (data_request.nextApprover != $USERNAME) {
                                $('.dv-background').hide();
                                $('#warningModal .modal-body').html(MSG.MESSAGE_REQUEST_IS_APPROVED);
                                $("#warningModal").modal('show');
                            } else {
                                var jsonData = {};
                                jsonData['userName'] = $USERNAME;
                                jsonData['actionStatus'] = validateActionState($USERNAME);
                                jsonData['documentNumber'] = $DATA_DOCUMENT.docNumber;
                                jsonData['docType'] = $DATA_DOCUMENT.documentType;
                                jsonData['documentFlow'] = $DATA_DOCUMENT.docFlow;
                                jsonData['processId'] = $DATA_DOCUMENT.processId;
                                jsonData['documentId'] = $DATA_DOCUMENT.id;
                                jsonData['actionReasonCode'] = $("#actionReason")[0].selectedOptions[0].value;
                                jsonData['actionReasonDetail'] = $("#actionReasonDetail").val();

                                $.ajax({
                                    type: "POST",
                                    headers: {
                                        Accept: 'application/json'
                                    },
                                    url: session['context'] + '/requests/approveRequest',
                                    data: jsonData,
                                    async: false,
                                    complete: function (xhr) {
                                        if (xhr.readyState == 4) {
                                            $DATA_APPROVE = JSON.parse(xhr.responseText);

                                            if ($DATA_APPROVE != null || $DATA_APPROVE != undefined) {

                                                $('.dv-background').hide();

                                                window.location.href = session.context;
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });
        },500);
    // }

}

function validateActionState(userName){
    for(var i=0;i<$DATA_REQUEST_APPROVER.length;i++){
        if(userName == $DATA_REQUEST_APPROVER[i].userNameApprover){
            return $DATA_REQUEST_APPROVER[i].actionState;
        }
    }
}

function backToMenu(){
    window.location.href = session.context+'/approve/approveMainMenu';
}

function validateFlowTypeForCancelRequest(){
    var docAppItem = $DATA_DOCUMENT.documentApprove.documentApproveItem;
    var listJsonDetails = [];
    if(docAppItem.length > 0){
        for(var i=0;i<docAppItem.length;i++){
            if(docAppItem[i].approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC){
                var jsonDetails = {};
                jsonDetails['amount'] = 1;
                jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_DOMESTIC;

                listJsonDetails.push(jsonDetails);
            }else if(docAppItem[i].approveType == MASTER_DATA.APR_TYPE_CAR){
                var jsonDetails = {};
                jsonDetails['amount'] = 1;
                jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_CAR;

                listJsonDetails.push(jsonDetails);
            }else if(docAppItem[i].approveType == MASTER_DATA.APR_TYPE_HOTEL){
                var jsonDetails = {};
                jsonDetails['amount'] = 1;
                jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_HOTEL;

                listJsonDetails.push(jsonDetails);
            }else if(docAppItem[i].approveType == MASTER_DATA.APR_TYPE_ADVANCE){
                var jsonDetails = {};
                jsonDetails['amount'] = $DATA_DOCUMENT.totalAmount;
                jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_ADVANCE;

                listJsonDetails.push(jsonDetails);
            }else{
                var psa_origin = "";
                var psa_destination = "";
                var location_type = "";
                var zone_code = "";

                var data = $.ajax({
                    url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName=" + $DATA_DOCUMENT.requester,
                    headers: {
                        Accept: "application/json"
                    },
                    type: "GET",
                    async: false,
                    complete: function (xhr) {
                        if (xhr.responseText != null) {
                            var dataEmployee = JSON.parse(xhr.responseText);
                            var cLevel = dataEmployee.EESG_ID;
                            var psa_requester = dataEmployee.Personal_PSA_ID;

                            if (docAppItem[i].approveType == MASTER_DATA.APR_TYPE_FIGHT_BOOKING && $DATA_TRAVEL_DETAIL.length > 0) {
                                psa_origin = $DATA_TRAVEL_DETAIL[0].origins.psaCode;
                                psa_destination = $DATA_TRAVEL_DETAIL[0].destination.psaCode;
                                location_type = $DATA_TRAVEL_DETAIL[0].origins.locationType;
                                zone_code = $DATA_TRAVEL_DETAIL[0].destination.zoneCode;

                                var flowType = validateFlowTypeSpecial(cLevel, psa_requester, psa_origin, psa_destination, location_type, zone_code);

                                var jsonDetails = {};
                                jsonDetails['amount'] = 1;
                                jsonDetails['flowType'] = flowType;

                                listJsonDetails.push(jsonDetails);
                            } else if(docAppItem[i].approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
                                psa_origin = $DATA_TRAVEL_DETAIL[0].origins.psaCode;
                                psa_destination = $DATA_TRAVEL_DETAIL[0].destination.psaCode;
                                location_type = $DATA_TRAVEL_DETAIL[0].origins.locationType;
                                zone_code = $DATA_TRAVEL_DETAIL[0].destination.zoneCode;

                                var flowType = validateFlowTypeSpecial(cLevel, psa_requester, psa_origin, psa_destination, location_type, zone_code);

                                var jsonDetails = {};
                                jsonDetails['amount'] = 1;
                                jsonDetails['flowType'] = flowType;

                                listJsonDetails.push(jsonDetails);
                            }
                        }
                    }
                });
            }
        }

        if(listJsonDetails.length > 0){
            cancelRequest(listJsonDetails);
        }
    }

}

function validateFlowTypeSpecial(cLevel,psa_requester,psa_origin,psa_destination,location_type,zone_code) {
    if (psa_origin != "" && psa_destination != "" && location_type != "" && zone_code != "") {
        $.ajax({
            type: "GET",
            headers: {
                Accept: 'application/json'
            },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: session['context'] + '/approve/validateFlowTypeApprove',
            data: {
                cLevel: parseInt(cLevel),
                psa_requester: psa_requester,
                psa_origin: psa_origin,
                psa_destination: psa_destination,
                location_type: location_type,
                zone_code: zone_code
            },
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    if (xhr.responseText != null) {
                        var flowTypeName = xhr.responseText;
                        if (flowTypeName == "H001" || flowTypeName == "H002") {
                            return MASTER_DATA.FLOW_TYPE_H001;
                        } else if (flowTypeName == "H003") {
                            return MASTER_DATA.FLOW_TYPE_H003;
                        } else if (flowTypeName == "H004" || flowTypeName == "H005") {
                            return MASTER_DATA.FLOW_TYPE_H004;
                        }
                    }
                }
            }
        });
    }
}


function nvl(e){
    if(e == null || e == undefined || e.toString() === 'NaN'){
        return "";
    }else{
        return e;
    }
}

function activeCar(btn) {
    if($("[name=iconCarAdm]").hasClass('click') == 'false'){
        $("#"+btn.id).addClass('click');
        var carValue = $("#"+btn.id).attr('value');
        if(carValue == 'van'){
            $("#"+btn.id).attr('src',IMAGE_CAR.VAN);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarADMDiv").addClass('hide');
        }else if(carValue == 'sedan'){
            $("#"+btn.id).attr('src',IMAGE_CAR.SEDAN);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarADMDiv").addClass('hide');
        }else if(carValue == 'tuck'){
            $("#"+btn.id).attr('src',IMAGE_CAR.TUCK);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarADMDiv").addClass('hide');
        }else if(carValue == 'other'){
            $("#"+btn.id).attr('src',IMAGE_CAR.OTHER);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarADMDiv").removeClass('hide');
        }
    }else{
        $("[name=iconCarAdm]").removeClass('click');
        setUpDefaultCarType("ADMIN");
        $("#"+btn.id).addClass('click');
        var carValue = $("#"+btn.id).attr('value');
        if(carValue == 'van'){
            $("#"+btn.id).attr('src',IMAGE_CAR.VAN);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarADMDiv").addClass('hide');
        }else if(carValue == 'sedan'){
            $("#"+btn.id).attr('src',IMAGE_CAR.SEDAN);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarADMDiv").addClass('hide');
        }else if(carValue == 'tuck'){
            $("#"+btn.id).attr('src',IMAGE_CAR.TUCK);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarADMDiv").addClass('hide');
        }else if(carValue == 'other'){
            $("#"+btn.id).attr('src',IMAGE_CAR.OTHER);
            $CarTypeCode = $("#"+btn.id).attr('id');
            $("#otherCarADMDiv").removeClass('hide');
        }
    }
    CAR_TYPE_ACTIVE = true;
}

function setFlowTypeCar(){

    if(CAR_TYPE_ACTIVE == true && $CarTypeCode != null && FLOW_CAR_ACTIVE == false){
        var jsonDetails = {};
        jsonDetails['amount'] = 1;
        jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_CAR;
        jsonDetails['costCenter'] = $DATA_DOCUMENT.costCenterCode;

        for(var i=0;i<listJsonFlowDetails.length;i++){
            if(listJsonFlowDetails[i].flowType == MASTER_DATA.FLOW_TYPE_CAR){
                break;
            }else{
                listJsonFlowDetails.push(jsonDetails);
            }
        }
        FLOW_CAR_ACTIVE = true;
    }else{
        for(var i=0;i<listJsonFlowDetails.length;i++){
            if(listJsonFlowDetails[i].flowType == MASTER_DATA.FLOW_TYPE_CAR){
                listJsonFlowDetails.splice(i,1);
            }
        }
    }
}

function verifyDocApproveItemBySubType(appType){
    var dataApproveItem = $DATA_DOCUMENT.documentApprove.documentApproveItem;
    for(var i=0;i<dataApproveItem.length;i++) {
        if (dataApproveItem[i].approveType == appType) {
            return dataApproveItem[i].id;
        }
    }
}

function validateApproveTypeRoleAdmin(){
    if($DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC ){
        return MASTER_DATA.APR_TYPE_SET_DOMESTIC;
    }else{
        return MASTER_DATA.APR_TYPE_SET_FOREIGN;
    }
}

function updateDocumentRoleAdmin(){
    var reason =  $("#travelReason").val();

    $('.dv-background').show();
    window.setTimeout(function() {

        var jsonDocumentApproveItem = {};
        jsonDocumentApproveItem['parentId'] = 1;
        jsonDocumentApproveItem[csrfParameter] = csrfToken;
        jsonDocumentApproveItem['id'] = $DATA_DOCUMENT.documentApprove.documentApproveItem[0].id;
        jsonDocumentApproveItem['approveType'] = validateApproveTypeRoleAdmin();
        jsonDocumentApproveItem['travelReason'] = reason;
        jsonDocumentApproveItem['document'] = $DATA_DOCUMENT.id;

        var dataDocumentApproveItem = $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/saveDocumentApproveItem',
            data: jsonDocumentApproveItem,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    dataDocumentApproveItem = JSON.parse(xhr.responseText);

                    /* update car */
                    var jsonDocumentApproveItem = {};
                    jsonDocumentApproveItem['parentId'] = 1;
                    jsonDocumentApproveItem[csrfParameter] = csrfToken;
                    jsonDocumentApproveItem['id'] = verifyDocApproveItemBySubType(MASTER_DATA.APR_TYPE_CAR);
                    jsonDocumentApproveItem['approveType'] = MASTER_DATA.APR_TYPE_CAR;
                    jsonDocumentApproveItem['travelReason'] = $("#travelReason").val();
                    jsonDocumentApproveItem['document'] = $DATA_DOCUMENT.id;

                    var dataDocumentApproveItem = $.ajax({
                        type: "POST",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context'] + '/approve/saveDocumentApproveItem',
                        data: jsonDocumentApproveItem,
                        async: false,
                        complete: function (xhr) {
                            if (xhr.readyState == 4) {
                                dataDocumentApproveItem = JSON.parse(xhr.responseText);

                                var carType = ($CarTypeCode == null ? "" : $CarTypeCode);
                                var otherCarType = $("#otherCarTypeADM").val();
                                var carBookingId = dataDocumentApproveItem.carBooking == null ? "" : dataDocumentApproveItem.carBooking.id;

                                var jsonCarBooking = {};
                                jsonCarBooking['parentId'] = 1;
                                jsonCarBooking[csrfParameter] = csrfToken;
                                jsonCarBooking['id'] = carBookingId;
                                jsonCarBooking['carTypeCode'] = carType;
                                jsonCarBooking['otherCarType'] = otherCarType;
                                jsonCarBooking['documentApproveItem'] = dataDocumentApproveItem.id;

                                var dataCarBooking = $.ajax({
                                    type: "POST",
                                    headers: {
                                        Accept: 'application/json'
                                    },
                                    url: session['context'] + '/approve/saveCarBooking',
                                    data: jsonCarBooking,
                                    async: false,
                                    complete: function (xhr) {
                                        if (xhr.readyState == 4) {
                                            DATA_RESULT_CAR = JSON.parse(xhr.responseText);

                                            var jsonDocumentApproveItem = {};
                                            jsonDocumentApproveItem['parentId'] = 1;
                                            jsonDocumentApproveItem[csrfParameter] = csrfToken;
                                            jsonDocumentApproveItem['id'] = verifyDocApproveItemBySubType(MASTER_DATA.APR_TYPE_HOTEL);
                                            jsonDocumentApproveItem['approveType'] = MASTER_DATA.APR_TYPE_HOTEL;
                                            jsonDocumentApproveItem['travelReason'] = $("#travelReason").val();
                                            jsonDocumentApproveItem['otherHotel'] = $("#otherHotel").val();
                                            jsonDocumentApproveItem['document'] = $DATA_DOCUMENT.id;

                                            var dataDocumentApproveItem = $.ajax({
                                                type: "POST",
                                                headers: {
                                                    Accept: 'application/json'
                                                },
                                                url: session['context'] + '/approve/saveDocumentApproveItem',
                                                data: jsonDocumentApproveItem,
                                                async: false,
                                                complete: function (xhr) {
                                                    if (xhr.readyState == 4) {
                                                        dataDocumentApproveItem = JSON.parse(xhr.responseText);

                                                        var singleRoom = $("#singleRoomADM").val();
                                                        var twinRoom = $("#twinRoomADM").val();
                                                        var informationHotel = $("#hotelADMInputHotel").attr('data-hotel-code');
                                                        var hotelBookingId = dataDocumentApproveItem.hotelBooking == null ? "" : dataDocumentApproveItem.hotelBooking.id;

                                                        var jsonHotelBooking = {};
                                                        jsonHotelBooking['parentId'] = 1;
                                                        jsonHotelBooking[csrfParameter] = csrfToken;
                                                        jsonHotelBooking['id'] = hotelBookingId;
                                                        jsonHotelBooking['singleRoom'] = singleRoom;
                                                        jsonHotelBooking['twinRoom'] = twinRoom;
                                                        jsonHotelBooking['informationHotel'] = informationHotel;
                                                        jsonHotelBooking['documentApproveItem'] = dataDocumentApproveItem.id;

                                                        var dataHotelBooking = $.ajax({
                                                            type: "POST",
                                                            headers: {
                                                                Accept: 'application/json'
                                                            },
                                                            url: session['context'] + '/approve/saveHotelBooking',
                                                            data: jsonHotelBooking,
                                                            async: false,
                                                            complete: function (xhr) {
                                                                if (xhr.readyState == 4) {
                                                                    DATA_RESULT_HOTEL = JSON.parse(xhr.responseText);
                                                                    var jsonDocumentApproveItem = {};
                                                                    jsonDocumentApproveItem['parentId'] = 1;
                                                                    jsonDocumentApproveItem[csrfParameter] = csrfToken;
                                                                    jsonDocumentApproveItem['id'] = verifyDocApproveItemBySubType(MASTER_DATA.APR_TYPE_FIGHT_BOOKING);
                                                                    jsonDocumentApproveItem['approveType'] = MASTER_DATA.APR_TYPE_FIGHT_BOOKING;
                                                                    jsonDocumentApproveItem['travelReason'] = $("#travelReason").val();
                                                                    jsonDocumentApproveItem['document'] = $DATA_DOCUMENT.id;

                                                                    var dataDocumentApproveItem = $.ajax({
                                                                        type: "POST",
                                                                        headers: {
                                                                            Accept: 'application/json'
                                                                        },
                                                                        url: session['context'] + '/approve/saveDocumentApproveItem',
                                                                        data: jsonDocumentApproveItem,
                                                                        async: false,
                                                                        complete: function (xhr) {
                                                                            if (xhr.readyState == 4) {
                                                                                dataDocumentApproveItem = JSON.parse(xhr.responseText);

                                                                                var flightTicketId = dataDocumentApproveItem.flightTicket == null ? "" : dataDocumentApproveItem.flightTicket.id;

                                                                                var jsonFlightTicket = {};
                                                                                jsonFlightTicket['parentId'] = 1;
                                                                                jsonFlightTicket[csrfParameter] = csrfToken;
                                                                                jsonFlightTicket['id'] = flightTicketId;
                                                                                jsonFlightTicket['flagOneWay'] = $('input[name="travelTypeADM"]:checked').val();
                                                                                jsonFlightTicket['fixFlight'] = $('input[name="flightTypeADM"]:checked').val();
                                                                                // jsonFlightTicket['airlineCode'] = $("#airlineADM")[0].selectedOptions[0].value;
                                                                                jsonFlightTicket['documentApproveItem'] = dataDocumentApproveItem.id;

                                                                                var dataFlightTicket = $.ajax({
                                                                                    type: "POST",
                                                                                    headers: {
                                                                                        Accept: 'application/json'
                                                                                    },
                                                                                    url: session['context'] + '/approve/saveFlightTicket',
                                                                                    data: jsonFlightTicket,
                                                                                    async: false,
                                                                                    complete: function (xhr) {
                                                                                        if (xhr.readyState == 4) {
                                                                                            DATA_RESULT_FLIGHT = JSON.parse(xhr.responseText);
                                                                                            $('.dv-background').hide();
                                                                                            approveRequestRoleAdmin();
                                                                                        }
                                                                                    }
                                                                                });
                                                                            }
                                                                        }
                                                                    });
                                                                }
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    },500);
}

function approveRequestRoleAdmin(){
    $('.dv-background').show();
    window.setTimeout(function() {
        $.ajax({
            type: "GET",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/requests/' + $DATA_REQUEST.id,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    if (xhr.responseText != "Error") {
                        var data_request = JSON.parse(xhr.responseText);
                        if (data_request.nextApprover != $USERNAME) {
                            $('.dv-background').hide();
                            $('#warningModal .modal-body').html(MSG.MESSAGE_REQUEST_IS_APPROVED);
                            $("#warningModal").modal('show');
                        } else {
                            var jsonData = {};
                            jsonData['userName'] = $USERNAME;
                            jsonData['actionStatus'] = validateActionState($USERNAME);
                            jsonData['documentNumber'] = $DATA_DOCUMENT.docNumber;
                            jsonData['docType'] = $DATA_DOCUMENT.documentType;
                            jsonData['documentFlow'] = $DATA_DOCUMENT.docFlow;
                            jsonData['processId'] = $DATA_DOCUMENT.processId;
                            jsonData['documentId'] = $DATA_DOCUMENT.id;
                            jsonData['actionReasonCode'] = $("#actionReason")[0].selectedOptions[0].value;
                            jsonData['actionReasonDetail'] = $("#actionReasonDetail").val();

                            $.ajax({
                                type: "POST",
                                headers: {
                                    Accept: 'application/json'
                                },
                                url: session['context'] + '/requests/approveRequest',
                                data: jsonData,
                                async: false,
                                complete: function (xhr) {
                                    if (xhr.readyState == 4) {
                                        $DATA_APPROVE = JSON.parse(xhr.responseText);

                                        if ($DATA_APPROVE != null || $DATA_APPROVE != undefined) {

                                            $('.dv-background').hide();
                                            window.location.href = session.context;
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });
    },500);
}

function checkLineApproveSet(){
    setFlowTypeFlight($DATA_DOCUMENT.requester,"requester");
}

function getAuthorizeForLineApprove(userName){

    $('.dv-background').show();

    window.setTimeout(function(){

        if(listJsonFlowDetails.length == 0){
            setFlowTypeDoc();
        }else {
            /** for case modify flow */
            var approveTypeFlow = null;
            if(FLIGHT_TYPE_ACTIVE){
                approveTypeFlow = MASTER_DATA.APR_TYPE_FIGHT_BOOKING;
            }else if(FLIGHT_TYPE_ACTIVE == false && $DATA_DOCUMENT.approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
                approveTypeFlow = MASTER_DATA.APR_TYPE_SET_FOREIGN;
            }else{
                approveTypeFlow = $DATA_DOCUMENT.approveType;
            }
            var jsonData = {};
            jsonData['requester'] = userName;
            jsonData['approveType'] = approveTypeFlow;
            jsonData['papsa']       = $("#company").attr('pa')+"-"+$("#department").attr('psa');
            jsonData['details'] = JSON.stringify(listJsonFlowDetails);

            $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/approve/getAuthorizeForLineApprove',
                data:jsonData,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        $DATA_LINE_APPROVE = JSON.parse(xhr.responseText);
                        $('.dv-background').hide();
                        renderLineApprove($DATA_LINE_APPROVE);
                    }
                }
            });
        }
    },500);
}

function setFlowTypeFlight(userName,person){

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName=" + userName,
        headers: {
            Accept: "application/json"
        },
        type: "GET",
        async: false,
        complete: function (xhr) {
            if (xhr.responseText != null) {
                var dataEmployee = JSON.parse(xhr.responseText);
                var cLevel = dataEmployee.EESG_ID;
                var psa_requester = dataEmployee.Personal_PSA_ID;

                if($DATA_DOCUMENT.documentApprove.documentApproveItem.length > 0){
                    var dataDocApproveItem = $DATA_DOCUMENT.documentApprove.documentApproveItem;
                    for(var i=0;i<dataDocApproveItem.length;i++){
                        if(dataDocApproveItem[i].approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC || dataDocApproveItem[i].approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
                            var approveType = dataDocApproveItem[i].approveType;
                            $.ajax({
                                type: "GET",
                                headers: {
                                    Accept: 'application/json'
                                },
                                url: session['context']+'/approve/'+dataDocApproveItem[i].id+'/travelDetails',
                                complete: function (xhr) {
                                    var dataTravel = JSON.parse(xhr.responseText);
                                    if(dataTravel.length > 0) {
                                        var psa_origin = dataTravel[0].origins.psaCode;
                                        var psa_destination = dataTravel[0].destination.psaCode;
                                        var location_type = dataTravel[0].origins.locationType;
                                        var zone_code = dataTravel[0].destination.zoneCode;

                                        if (FLIGHT_TYPE_ACTIVE == true) {
                                            validateFlowType(cLevel, psa_requester, psa_origin, psa_destination, location_type, zone_code, MASTER_DATA.APR_TYPE_FIGHT_BOOKING, userName);
                                        }else {
                                            validateFlowType(cLevel, psa_requester, psa_origin, psa_destination, location_type, zone_code,approveType, userName);
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            }
        }
    });
}

function validateFlowType(cLevel,psa_requester,psa_origin,psa_destination,location_type,zone_code,approveTypeFlow,userName){

    $('.dv-background').show();
    window.setTimeout(function() {
        var costCenter = $DATA_DOCUMENT.costCenterCode;

        if (psa_origin != "" && psa_destination != "" && location_type != "" && zone_code != "") {

            if (userName != $DATA_DOCUMENT.requester) {
                var data = $.ajax({
                    url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName=" + userName,
                    headers: {
                        Accept: "application/json"
                    },
                    type: "GET",
                    async: false,
                    complete: function (xhr) {
                        if (xhr.responseText != null) {
                            var dataEmployee = JSON.parse(xhr.responseText);
                            costCenter = dataEmployee.Personal_Cost_center;
                        }
                    }
                });
            }

            $.ajax({
                type: "GET",
                headers: {
                    Accept: 'application/json'
                },
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: session['context'] + '/approve/validateFlowTypeApprove',
                data: {
                    cLevel: parseInt(cLevel),
                    psa_requester: psa_requester,
                    psa_origin: psa_origin,
                    psa_destination: psa_destination,
                    location_type: location_type,
                    zone_code: zone_code
                },
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        if (xhr.responseText != null) {
                            var flowTypeName = xhr.responseText;

                            if (flowTypeName == "H001" || flowTypeName == "H002") {
                                SPEACIAL_FLOW = MASTER_DATA.FLOW_TYPE_H001;
                            } else if (flowTypeName == "H003") {
                                SPEACIAL_FLOW = MASTER_DATA.FLOW_TYPE_H003;
                            } else if (flowTypeName == "H004" || flowTypeName == "H005") {
                                SPEACIAL_FLOW = MASTER_DATA.FLOW_TYPE_H004;
                            }

                            if (SPEACIAL_FLOW != "") {

                                var jsonDetails = {};
                                jsonDetails['amount'] = 1;
                                jsonDetails['flowType'] = SPEACIAL_FLOW;
                                jsonDetails['costCenter'] = costCenter;

                                if(listJsonFlowDetails.length > 0){
                                    for(var i=0;i<listJsonFlowDetails.length;i++){
                                        if(listJsonFlowDetails[i].flowType == SPEACIAL_FLOW){
                                            break;
                                        }else{
                                            listJsonFlowDetails.push(jsonDetails);
                                        }
                                    }
                                }else{
                                    listJsonFlowDetails.push(jsonDetails);
                                }

                                if (ADVANCE_TYPE_ACTIVE) {
                                    var jsonDetails = {};
                                    jsonDetails['amount'] = $("#advancesAmount").autoNumeric('get');
                                    jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_ADVANCE;
                                    jsonDetails['costCenter'] = costCenter;

                                    var countAdvance = 0;

                                    for(var i=0;i<listJsonFlowDetails.length;i++){
                                        if(listJsonFlowDetails[i].flowType == MASTER_DATA.FLOW_TYPE_ADVANCE){
                                            countAdvance++;
                                        }
                                    }

                                    if(countAdvance > 1){
                                        for(var i=0;i<listJsonFlowDetails.length;i++){
                                            if(listJsonFlowDetails[i].flowType == MASTER_DATA.FLOW_TYPE_ADVANCE){
                                                listJsonFlowDetails.splice(i,1);
                                            }
                                        }
                                    }else{
                                        listJsonFlowDetails.push(jsonDetails);
                                    }
                                }



                                var jsonData = {};
                                jsonData['requester'] = userName;
                                jsonData['approveType'] = approveTypeFlow;
                                jsonData['papsa']       = $("#company").attr('pa')+"-"+$("#department").attr('psa');
                                jsonData['details'] = JSON.stringify(listJsonFlowDetails);

                                $.ajax({
                                    type: "POST",
                                    headers: {
                                        Accept: 'application/json'
                                    },
                                    url: session['context'] + '/approve/getAuthorizeForLineApprove',
                                    data: jsonData,
                                    async: false,
                                    complete: function (xhr) {
                                        if (xhr.readyState == 4) {
                                            $('.dv-background').hide();
                                            $DATA_LINE_APPROVE = JSON.parse(xhr.responseText);
                                            renderLineApproveCancel($DATA_LINE_APPROVE);
                                        }
                                    }
                                });

                            }
                        }
                    }
                }
            });
        }
    },500);
}

function renderLineApproveCancel(requestApprover){

    $('.dv-background').show();

    window.setTimeout(function() {
        findEmployeeProfileByUserName($DATA_DOCUMENT.requester);

        var requesterName;
        var requesterPosition;
        var dataStep;

        if(session.lang === 'en'){
            requesterName = nvl($DATA_EMPLOYEE.NameEN);
            requesterPosition = nvl($DATA_EMPLOYEE.PositionEN);
            dataStep = requestApprover.length + 1;
        }else{
            requesterName = nvl($DATA_EMPLOYEE.FOA) + nvl($DATA_EMPLOYEE.FNameTH) + " " + nvl($DATA_EMPLOYEE.LNameTH);
            requesterPosition = nvl($DATA_EMPLOYEE.PositionTH);
            dataStep = requestApprover.length + 1;
        }

        $("#lineApproverDetail").attr('data-steps', dataStep);
        $("#lineApproverDetail").empty();
        $("#lineApproveMobile").empty();
        $("#lineApproverDetail").append(
            '<li class="idle" style="text-align: center;">' +
            '<span class="step"><span><img src=' + IMG.REQUESTER + ' style="width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></span>' +
            '<span class="name-idle">' +
            '<div>' + LB.LABEL_REQUESTER + '</div>' +
            '<div style="color: blue;">' + requesterName + '</div>' +
            '<div style="color: lightseagreen;">' + requesterPosition + '</div>' +
            '</span>' +
            '</li>'
        );

        $("#lineApproveMobile").append('' +
            '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">' +
            '<div class="panel-heading collapseLightBlue" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">' +
            '<div class="container-fluid">' +
            '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">' +
            '<img src="' + IMG.REQUESTER + '" width="45px"/>' +
            '</div>' +
            '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">' +
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 18px;font-weight: bold"><b>' + LB.LABEL_REQUESTER + '</b></label>' +
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>' + requesterName + '</b></label>' +
            '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>' + requesterPosition + '</b></label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>'
        );

        for (var i = 0; i < requestApprover.length; i++) {

            findEmployeeProfileByUserName(requestApprover[i].userNameApprover);

            if(session.lang === 'en'){
                requesterName = nvl($DATA_EMPLOYEE.NameEN);
                requesterPosition = nvl($DATA_EMPLOYEE.PositionEN);
                dataStep = requestApprover.length + 1;
            }else{
                requesterName = nvl($DATA_EMPLOYEE.FOA) + nvl($DATA_EMPLOYEE.FNameTH) + " " + nvl($DATA_EMPLOYEE.LNameTH);
                requesterPosition = nvl($DATA_EMPLOYEE.PositionTH);
                dataStep = requestApprover.length + 1;
            }

            $("#lineApproverDetail").append(
                '<li class="idle" style="text-align: center;">' +
                '<span class="step"><span><img src=' + validateIMG(requestApprover[i].actionRoleCode) + ' style="width: 70px; height: 70px; margin-top: -14.5px; margin-left: -25px" alt=""/></span></span>' +
                '<span class="name-idle">' +
                '<div>' + validateLanguage(requestApprover[i].actionState) + '</div>' +
                '<div style="color: blue;">' + requesterName + '</div>' +
                '<div style="color: lightseagreen;">' + requesterPosition + '</div>' +
                '</span>' +
                '</li>'
            );

            $("#lineApproveMobile").append('' +
                '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">' +
                '<div class="panel-heading collapseLightBlue" role="button" style="padding-top: 5px;padding-bottom: 5px;padding-left: 0;padding-right: 0">' +
                '<div class="container-fluid">' +
                '<div class="col-xs-3 text-left" style="padding-left: 0;padding-right: 0">' +
                '<img src=' + validateIMG(requestApprover[i].actionState) + ' width="45px"/>' +
                '</div>' +
                '<div class="col-xs-9" style="padding-left: 0;padding-right: 0">' +
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 18px;font-weight: bold"><b>' + validateLanguage(requestApprover[i].actionState) + '</b></label>' +
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>' + requesterName + '</b></label>' +
                '<label class="col-xs-12" style="color:#fff;padding-left: 0;font-size: 12px"><b>' + requesterPosition + '</b></label>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>'
            );
        }
        $('.dv-background').hide();
    },500);
}

function setFlowTypeDoc(){
    var documentApproveItem = $DATA_DOCUMENT.documentApprove.documentApproveItem;

    for(var i=0;i<documentApproveItem.length;i++){
        var approveType = documentApproveItem[i].approveType;
        if(documentApproveItem[i].approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC){
            var jsonDetails = {};
            jsonDetails['amount'] = 1;
            jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_DOMESTIC;
            jsonDetails['costCenter'] = $DATA_DOCUMENT.costCenterCode;
            listJsonFlowDetails.push(jsonDetails);

            if(ADVANCE_TYPE_ACTIVE){
                setFlowTypeAdvance();
            }

            getAuthorizeForLineApprove($DATA_DOCUMENT.requester);
        }else if(documentApproveItem[i].approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
            var data = $.ajax({
                url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName=" + $DATA_DOCUMENT.requester,
                headers: {
                    Accept: "application/json"
                },
                type: "GET",
                async: false,
                complete: function (xhr) {
                    if (xhr.responseText != null) {
                        var dataEmployee = JSON.parse(xhr.responseText);
                        var cLevel = dataEmployee.EESG_ID;
                        var psa_requester = dataEmployee.Personal_PSA_ID;

                        if(documentApproveItem[i].travelDetail != null && documentApproveItem[i].travelDetail.length > 0){
                            var dataTravelDetail = documentApproveItem[i].travelDetail;

                            var psa_origin = dataTravelDetail[0].origins.psaCode;
                            var psa_destination = dataTravelDetail[0].destinations.psaCode;
                            var location_type = dataTravelDetail[0].origins.locationType;
                            var zone_code = dataTravelDetail[0].destinations.zoneCode;

                            validateFlowType(cLevel, psa_requester, psa_origin, psa_destination, location_type, zone_code, approveType, $DATA_DOCUMENT.requester);
                        }
                        else {

                            var jsonDetails = {};
                            jsonDetails['amount'] = 1;
                            jsonDetails['flowType'] = MASTER_DATA.FLOW_TYPE_FOREIGN;
                            jsonDetails['costCenter'] = $DATA_DOCUMENT.costCenterCode;
                            listJsonFlowDetails.push(jsonDetails);

                            getAuthorizeForLineApprove($DATA_DOCUMENT.requester);
                        }
                    }
                }
            });
        }
    }
}

function findByPaCode(paCode){
    var data = $.ajax({
        url: session.context + "/intermediaries/findByPa/"+paCode,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data != null && data.restBodyList.length > 0){
        var paData = data.restBodyList[0];
        if(session.lang === 'en'){
            $("#company").text(paData.PaCode+" : "+paData.PaNameEn);
        }else{
            $("#company").text(paData.PaCode+" : "+paData.PaNameTh);
        }
        $("#company").attr('pa',paData.PaCode);
    }else{
        $("#company").text("-");
        $("#company").attr('pa',"");
    }
}

function findByPsaCode(psaCode){
    var data = $.ajax({
        url: session.context + "/intermediaries/findByPsa/"+psaCode,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data != null && data.restBodyList.length > 0){
        var psaData = data.restBodyList[0];
        if(session.lang === 'en'){
            $("#department").text(psaData.PsaCode+" : "+psaData.PsaNameEn);
        }else{
            $("#department").text(psaData.PsaCode+" : "+psaData.PsaNameTh);
        }
        $("#department").attr('psa',psaData.PsaCode);
    }else{
        $("#department").text("-");
        $("#department").attr('psa',"");
    }

    var papsa = $("#company").attr('pa')+"-"+$("#department").attr('psa');

    var data = $.ajax({
        url: session.context + "/intermediaries/findAccountByPapsa/"+papsa+"/"+$DATA_DOCUMENT.personalId,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data) {
        var dataAccountSplit = data.profile.split(':');
        if (dataAccountSplit[1] != "0") {
            var accountName = dataAccountSplit[1].split("-");
            console.log("accountName[0]   :   "+accountName[0]);
            var data = $.ajax({
                url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName=" + accountName[0],
                headers: {
                    Accept: "application/json"
                },
                type: "GET",
                async: false
            }).responseJSON;

            if (data) {

                if ($("#accountName").text() == "") {
                    if(session.lang === 'en'){
                        $("#accountName").text(data.NameEN);
                    }else {
                        $("#accountName").text(data.FOA + data.FNameTH + ' ' + data.LNameTH);
                    }
                }
            }
        }
    }
}


function preViewAttachmentFileExpenseItem(id,fileName) {


    var splitTypeFile = fileName.split('.')
    var fileType = splitTypeFile[splitTypeFile.length-1]

    if(fileType == 'pdf' || fileType == 'PDF' || fileType =='txt' || fileType == 'TXT'){
        var url = session['context']+'/approve/preViewPDFDocumentExpItemAttachment?id='+id+'&fileName='+fileName

        var properties;
        var positionX = 0;
        var positionY = 0;
        var width = 0;
        var height = 0;
        properties = "width=" + width + ", height=" + height;
        properties += ", toolbar=0, location=0, directories=0, status=0, menubar=0, scrollbars=1, resizable=1, copyhistory=0";
        properties += ", top=" + positionY + ", left=" + positionX;
        properties += ", fullscreen=1";
        window.open(url, '', properties);
    }

    if(fileType == 'png' || fileType == 'PNG' || fileType =='jpg' || fileType == 'JPG' || fileType == 'jpeg'  || fileType == 'JPEG'){


        var urlIMG = session['context']+'/approve/preViewIMAGEDocumentExpItemAttachment?id='+id+'&fileName='+fileName


        var properties;
        var positionX = 0;
        var positionY = 0;
        var width = 0;
        var height = 0;
        properties = "width=" + width + ", height=" + height;
        properties += ", toolbar=0, location=0, directories=0, status=0, menubar=0, scrollbars=1, resizable=1, copyhistory=0";
        properties += ", top=" + positionY + ", left=" + positionX;
        properties += ", fullscreen=1";
        window.open(urlIMG, '', properties);
    }
}

function unlockAdvance() {

    $('.dv-background').show();
    var jsonDocument = {};
    jsonDocument['parentId']     	= 1;
    jsonDocument[csrfParameter]  	= csrfToken;
    jsonDocument['id']    =  $DATA_DOCUMENT.id;

    setTimeout(function () {
        $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/advance/unlockAdvance',
            data:jsonDocument,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    if(xhr.responseJSON == "Success"){
                        window.location.href = session.context;
                    }else{
                        $('.dv-background').hide();
                        $('#warningModal .modal-body').html(xhr.responseJSON);
                        $("#warningModal").modal('show');
                    }
                }
            }
        });
    },1000);
}

function clearManual() {
    $("#docClearingSapManual").val("");
    $("#textFileNameSapManual").val("");
    $("#modalCompleteManual").modal('show');
}