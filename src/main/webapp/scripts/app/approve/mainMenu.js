

$(document).ready(function () {

    readMasterDataAprType();

    findEmployeeProfileByUserName(session.userName);

    $('#menu_1').on('click',function () {
        gotoDocumentApproveSetDomestic()

    })

    $('#menu_2').on('click',function () {
        gotoDocumentApproveSetForeign()

    })


    $('#menu_3').on('click',function () {
        gotoDocumentApproveCreateDoc()

    })


    $('#menu_4').on('click',function () {
        gotoMyRequestApprove()

    })


    $('#menu_5').on('click',function () {
        gotoIncomingRequestApprove()

    })


    $('#menu_6').on('click',function () {
        gotoHistoryApprove()

    })


    $('#menu_7').on('click',function () {

    })


    $('#menu_8').on('click',function () {
        gotoDocumentSummaryReport();
    })


    $('#menu_9').on('click',function () {

    })







});



function showItem(){
    setTimeout(function(){
        // $('#menu_1').fadeIn('slow');

        $('#menu_1').fadeIn('slow');
        $('#menu_name_1').fadeIn('slow');

    }, 100);

    setTimeout(function(){

        $('#menu_2').fadeIn('slow');
        $('#menu_name_2').fadeIn('slow');
    }, 200);

    setTimeout(function(){

        $('#menu_3').fadeIn('slow');
        $('#menu_name_3').fadeIn('slow');
    }, 300);

    setTimeout(function(){
        $('#menu_4').fadeIn('slow');
        $('#menu_name_4').fadeIn('slow');
    }, 400);

    setTimeout(function(){
        $('#menu_5').fadeIn('slow');
        $('#menu_name_5').fadeIn('slow');
    }, 500);

    setTimeout(function(){
        $('#menu_6').fadeIn('slow');
        $('#menu_name_6').fadeIn('slow');
    }, 600);

    setTimeout(function(){
        $('#menu_7').fadeIn('slow');
        $('#menu_name_7').fadeIn('slow');
    }, 700);

    setTimeout(function(){
        $('#menu_8').fadeIn('slow');
        $('#menu_name_8').fadeIn('slow');
    }, 800);

    setTimeout(function(){
        $('#menu_9').fadeIn('slow');
        $('#menu_name_9').fadeIn('slow');
    }, 900);


    // setClassHover();

}

function setClassHover() {
    $('#img1').addClass( "hvr-float-shadow" );
    $('#img2').addClass( "hvr-float-shadow" );
    $('#img3').addClass( "hvr-float-shadow" );
    $('#img4').addClass( "hvr-float-shadow" );
    $('#img5').addClass( "hvr-float-shadow" );
    $('#img6').addClass( "hvr-float-shadow" );
    $('#img7').addClass( "hvr-float-shadow" );
    $('#img8').addClass( "hvr-float-shadow" );
    $('#img9').addClass( "hvr-float-shadow" );

}

/* event menu */
function gotoDocumentApproveSetDomestic(){
    window.location.href = session.context+'/approve/createDocSet?type=0006'
}

function gotoDocumentApproveSetForeign(){
    window.location.href = session.context+'/approve/createDocSet?type=0007'
}

function gotoDocumentApproveCreateDoc(){
    window.location.href = session.context+'/approve/createDoc';
}

function gotoMyRequestApprove(){
    window.location.href = session.context + '/approve/listView';
}

function gotoIncomingRequestApprove() {
    window.location.href = session.context + '/incomingRequestApprove/listView';
}

function gotoHistoryApprove() {
    window.location.href = session.context + '/historyApprove/listView';
}

function gotoDocumentSummaryReport() {
    window.location.href = session.context + '/documentStatusSummary/listView';
}

function findEmployeeProfileByUserName(userName){

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    if(data){
        if(data.Attorney_ID && parseInt(data.Attorney_ID) <= 207){
            $("#menu8Div").removeClass('hide');
        }
    }
}
