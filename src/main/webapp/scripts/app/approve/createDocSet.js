var $PANEL_APP_TYPE = [];
var FLAG_CAR = null;
var FLAG_FLIGHT = null;
var FLAG_BED = null;
var FLAG_BED_TWIN = null;
var STATUS = null;
var $DATA_COSTCENTER;

$(document).ready(function () {

    $('.dv-background').show();
    /* call master data */
    readMasterDataDocType();
    readMasterDataDocStatus();
    readMasterDataAprType();


    $.material.init();

    window.setTimeout(function(){

        AutocompleteEmployee.renderValue($USERNAME);

        if ($("#requesterInputEmployee").val() != "") {
            $("#requesterInputEmployee").blur();
        }else{
            AutocompleteEmployee.setId('requesterInputEmployee');
            AutocompleteEmployee.search("requesterInputEmployee",$USERNAME);
            AutocompleteEmployee.renderValue($USERNAME);
            $("#requesterInputEmployee").blur();
        }

        // $('.dv-background').hide();
    },2000);


    $('.btn-number').click(function(e){
        e.preventDefault();
        fieldName = $(this).attr('data-field');
        type      = $(this).attr('data-type');
        var input = $("p[name='"+fieldName+"']");
        var currentVal = parseInt(input.text());
        if (!isNaN(currentVal)) {
            if(type == 'minus') {

                if(currentVal > input.attr('min')) {
                    input.text(currentVal - 1);
                    changePerson(input);
                }
                if(parseInt(input.text()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }

            } else if(type == 'plus') {

                if(currentVal < input.attr('max')) {
                    input.text(currentVal + 1);
                    changePerson(input);
                }
                if(parseInt(input.text()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                }
            }
        } else {
            input.text(0);
        }
    });

    $('.input-number').focusin(function(){
        $(this).data('oldValue', $(this).text());
    });

    function changePerson(btn) {
        minValue =  parseInt($(btn).attr('min'));
        maxValue =  parseInt($(btn).attr('max'));
        valueCurrent = parseInt($(btn).text());

        name = $(btn).attr('name');
        if(valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
        }
        if(valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
        }
    }

    $("#select").on('change',function () {
        if($("#select option:selected").val() == "other"){
            $("#otherDiv").removeClass('hide');
        }else{
            $("#otherDiv").addClass('hide');
        }
    });

    $("#requesterInputEmployee").on('keyup',function(){
        if($("#requesterInputEmployee").val() != ""){
            AutocompleteEmployee.setId('requesterInputEmployee');
            AutocompleteEmployee.search("requesterInputEmployee",$("#requesterInputEmployee").val());
        }
    });

    /* travel member panel */
    $("#requesterInputEmployee").on('blur',function (){
        if($("#requesterInputEmployee").val() != ""){
            $("#empCode").text($("#requesterInputEmployee").attr('data-personalId'));
            $("#empCode").attr('data-empCode',$("#requesterInputEmployee").attr('data-empCode'));
            $("#position").text($("#requesterInputEmployee").attr('data-position'));
            // $("#company").text($("#requesterInputEmployee").attr('data-compcode')+" : "+$("#requesterInputEmployee").attr('data-compname'));
            // $("#company").attr('pa',$("#requesterInputEmployee").attr('data-compcode'));
            $("#department").text($("#requesterInputEmployee").attr('data-psa')+" : "+$("#requesterInputEmployee").attr('data-psaName'));
            $("#department").attr('psa',$("#requesterInputEmployee").attr('data-psa'));
            $("#costCenter").val($("#requesterInputEmployee").attr('data-costCenter'));

            findCostCenter($("#requesterInputEmployee").attr('data-costCenter'));
        }else{
            $("#empCode").text("");
            $("#empCode").attr('data-empCode',"");
            $("#position").text("");
            $("#company").text("");
            // $("#department").text("");
            $("#costCenter").val("");
        }
    });

    $("#closeModalCostCenter").on('click',function (){
       $("#warningCostCenterModal").modal('hide');

        if(STATUS == "insert"){
            saveDocument();
        }
    });

    $("#costCenter").on('blur',function () {
        findCostCenter($("#costCenter").val())
    });
});

function findCostCenter(costCenter) {
    $('.dv-background').show();
    setTimeout(function () {

        var data = $.ajax({
            url: session.context + "/intermediaries/findEmployeeProfileByCostCenter?costCenter="+costCenter,
            headers: {
                Accept : "application/json"
            },
            type: "GET",
            async: false
        }).responseJSON;

        if(data){
            var costCenterSplit = data.profile.split('#');
            if(costCenterSplit[2] != "0"){
                var paPsaSplit = costCenterSplit[2].split('-');
                var index1 = paPsaSplit.length-7!=0?paPsaSplit.length-7:0;
                var index2 = paPsaSplit.length-6!=0?paPsaSplit.length-6:0;
                var index3 = paPsaSplit.length-5!=0?paPsaSplit.length-5:0;
                var index4 = paPsaSplit.length-4!=0?paPsaSplit.length-4:0;
                var index5 = paPsaSplit.length-3!=0?paPsaSplit.length-3:0;

                var pa1 = index1 > 0?paPsaSplit[index1]:"";
                var pa2 = index2 > 0?paPsaSplit[index2]:"";
                var pa3 = index3 > 0?paPsaSplit[index3]:"";
                var pa4 = index4 > 0?paPsaSplit[index4]:"";
                var pa5 = index5 > 0?paPsaSplit[index5]:"";

                var text= pa1+pa2+pa3+pa4+pa5;

                $("#company").text(paPsaSplit[0] + " : " + text);
                $("#company").attr('pa', paPsaSplit[0]);
                // $("#department").text(paPsaSplit[paPsaSplit.length -2] + " : " + paPsaSplit[paPsaSplit.length -1]);
                // $("#department").attr('psa', paPsaSplit[paPsaSplit.length -2]);

                $('.dv-background').hide();
            }else{
                $("#company").text("-");
                $("#company").attr('pa',"");
                // $("#department").text("-");
                // $("#department").attr('psa',"");
                $('.dv-background').hide();
            }
        }

    },1000);

}


function varidatePanelFromMenu(){
    // $("div .collapse1").attr('id',MASTER_DATA.APR_TYPE_SET_DOMESTIC);
    // $("div .collapse2").attr('id',MASTER_DATA.APR_TYPE_SET_FOREIGN);
    // if($APP_TYPE == MASTER_DATA.APR_TYPE_SET_DOMESTIC){
    //     $("#"+MASTER_DATA.APR_TYPE_SET_DOMESTIC).removeClass("collapseGray");
    //     $("#"+MASTER_DATA.APR_TYPE_SET_DOMESTIC).addClass("collapseTeal");
    //     $PANEL_APP_TYPE = MASTER_DATA.APR_TYPE_SET_DOMESTIC;

        $("#"+MASTER_DATA.APR_TYPE_SET_FOREIGN).addClass("hide");
    // }else{
    //     $("#"+MASTER_DATA.APR_TYPE_SET_FOREIGN).removeClass("collapseGray");f
    //     $("#"+MASTER_DATA.APR_TYPE_SET_FOREIGN).addClass("collapseTeal");
    //     $PANEL_APP_TYPE = MASTER_DATA.APR_TYPE_SET_FOREIGN;

        $("#"+MASTER_DATA.APR_TYPE_SET_DOMESTIC).addClass("hide");
    // }
}


function checkCollapse(panel) {

    if($(".panel-heading").hasClass("collapseTeal")){
        $(".panel-heading").removeClass("collapseTeal");
        $(".panel-heading").addClass("collapseGray");
        $("#"+panel.id).removeClass("collapseGray");
        $("#"+panel.id).addClass("collapseTeal");
        $PANEL_APP_TYPE = setCollapseIdFollowApproveType($("#"+panel.id));
    }else{
        $(".panel-heading").removeClass("collapseTeal");
        $(".panel-heading").addClass("collapseGray");
        $("#"+panel.id).removeClass("collapseGray");
        $("#"+panel.id).addClass("collapseTeal");
        $PANEL_APP_TYPE = setCollapseIdFollowApproveType($("#"+panel.id));
    }
}

function setCollapseIdFollowApproveType(panel){
    if(panel.hasClass("collapse1")){
        return panel.attr("id",MASTER_DATA.APR_TYPE_SET_DOMESTIC);
    }else if(panel.hasClass("collapse2")){
        return panel.attr("id",MASTER_DATA.APR_TYPE_SET_FOREIGN);
    }
}


function checkDocument() {
    verifyValueIsRequire();
}
function saveDocument(){

    if(STATUS == "insert"){

        var jsonDocumentApproveItem = {};
        jsonDocumentApproveItem['parentId']     	= 1;
        jsonDocumentApproveItem[csrfParameter]  	= csrfToken;
        jsonDocumentApproveItem['approveType']      = validateApproveTypeForSaveApproveItem($APP_TYPE);

        var jsonDocumentApprove = {};
        jsonDocumentApprove['remark']      = "";

        var jsonData = {};
        jsonData['parentId']     	= 1;
        jsonData[csrfParameter]  	= csrfToken;
        jsonData['documentType']    = MASTER_DATA.DOC_TYPE;
        jsonData['documentStatus']  = MASTER_DATA.DOC_STATUS;
        jsonData['requester']       = $("#requesterInputEmployee").attr('data-userName');
        jsonData['companyCode']     = $("#company").attr('pa');
        jsonData['departmentCode']  = $("#requesterInputEmployee").attr('data-deptCode');
        jsonData['personalId']      = $("#requesterInputEmployee").attr('data-personalId');
        jsonData['psa']      = $("#department").attr('psa');
        jsonData['costCenterCode']  = $("#costCenter").val();
        jsonData['approveType']     = $APP_TYPE;
        jsonData['documentApprove']      = JSON.stringify(jsonDocumentApprove);

        $('.dv-background').show();
        var dataDocument = $.ajax({
            type: "POST",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/approve/saveDocument',
            data:jsonData,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    dataDocument = JSON.parse(xhr.responseText);

                    jsonDocumentApproveItem['document'] = dataDocument.id;

                    var dataDocumentApproveItem = $.ajax({
                        type: "POST",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context'] + '/approve/saveDocumentApproveItem',
                        data:jsonDocumentApproveItem,
                        async: false,
                        complete: function (xhr) {
                            if (xhr.readyState == 4) {
                                dataDocumentApproveItem = JSON.parse(xhr.responseText);

                                if(dataDocumentApproveItem.approveType == MASTER_DATA.APR_TYPE_DOMESTIC){
                                    FLAG_CAR = true;
                                    FLAG_FLIGHT = false;
                                    FLAG_BED = true;
                                    FLAG_BED_TWIN = false;
                                }else if(dataDocumentApproveItem.approveType == MASTER_DATA.APR_TYPE_FOREIGN){
                                    FLAG_CAR = false;
                                    FLAG_FLIGHT = true;
                                    FLAG_BED = true;
                                    FLAG_BED_TWIN = false;
                                }


                                var jsonData = {};
                                jsonData['parentId']     		    = 1;
                                jsonData[csrfParameter]  		    = csrfToken;
                                jsonData['memberCode']  		    = $("#requesterInputEmployee").attr('data-empCode');
                                jsonData['memberPersonalId']  		= $("#requesterInputEmployee").attr('data-personalId');
                                jsonData['memberUser']  		    = $("#requesterInputEmployee").attr('data-userName');
                                jsonData['flagCar']    			    = FLAG_CAR;
                                jsonData['flagFlight']    		    = FLAG_FLIGHT;
                                jsonData['flagBed']    			    = FLAG_BED;
                                jsonData['flagBedTwin']    		    = FLAG_BED_TWIN;
                                jsonData['documentApproveItem']    	= dataDocumentApproveItem.id;

                                var dataTravel = $.ajax({
                                    type: "POST",
                                    headers: {
                                        Accept: 'application/json'
                                    },
                                    url: session['context'] + '/approve/saveTravelMember',
                                    data: jsonData ,
                                    async: false,
                                    complete: function (xhr) {
                                        if (xhr.readyState == 4) {
                                            $('.dv-background').hide();
                                            window.location.href = session.context+'/approve/createDocSetDetail?doc='+dataDocument.id;
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }
}

function verifyValueIsRequire(){
    var requester = $("#requester").val();
    var costCenter = $("#costCenter").val();

    var errorMessage = "";
    if(requester == ""){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_EMPLOYEE_NAME+"<br/>";
    }if(costCenter == ""){
        errorMessage += MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_COST_CENTER+"<br/>";
    }

    if(errorMessage){
        $('#warningModal .modal-body').html(errorMessage);
        $("#warningModal").modal('show');
    }else{
        if(costCenter != $("#requesterInputEmployee").attr('data-costCenter')){
            var data = $.ajax({
                url: session.context + "/intermediaries/findEmployeeProfileByCostCenter?costCenter="+costCenter,
                headers: {
                    Accept : "application/json"
                },
                type: "GET",
                async: false
            }).responseJSON;

            $DATA_COSTCENTER = data;
            if($DATA_COSTCENTER ) {
                var costCenterSplit = $DATA_COSTCENTER.profile.split('#');
                if (costCenterSplit[2] != "0") {
                    errorMessage += MSG.MESSAGE_COST_CENTER_NOT_AFFILIATED + "<br/>";

                    $('#warningCostCenterModal .modal-body').html(errorMessage);
                    $("#warningCostCenterModal").modal('show');

                    STATUS = "insert";
                } else {
                    errorMessage += MSG.MESSAGE_COST_CENTER_DOES_NOT_EXIST + "<br/>";

                    $('#warningCostCenterModal .modal-body').html(errorMessage);
                    $("#warningCostCenterModal").modal('show');

                    STATUS = "not insert";
                }
            }else {
                errorMessage += MSG.MESSAGE_COST_CENTER_DOES_NOT_EXIST + "<br/>";

                $('#warningCostCenterModal .modal-body').html(errorMessage);
                $("#warningCostCenterModal").modal('show');

                STATUS = "not insert";
            }
        }else{
            STATUS = "insert";
            saveDocument();
        }
    }
}

function validateApproveTypeForSaveApproveItem(approveType){
    if(approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC){
        return MASTER_DATA.APR_TYPE_SET_DOMESTIC;
    }else{
        return MASTER_DATA.APR_TYPE_SET_FOREIGN;
    }
}


