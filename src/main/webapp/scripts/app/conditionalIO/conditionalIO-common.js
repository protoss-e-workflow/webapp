/**
 * 
 */

var object = $.extend({},UtilPagination);
var companyJson;
var startPageC =1;

function isZero(e){
	var check = false;
	if(!$(e).val() || $(e).val() == 0|| $(e).val() == '0'||$(e).val() == 0.0|| $(e).val() == '0.0'){
		check = true;
	}
	return check;
}

function setZeroWhenNull(e){
	if(isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}
	
}

function saveConditionalIO(){
	//Define flagOther
	var flagActive = $('#modal_edit_flag_active').prop('checked')?true:false;
	var company =  $('#modal_edit_company').val();
	var jsonConditionalIOCompany = {};
	jsonConditionalIOCompany['company']      = company;


	var jsonParams = {};

	jsonParams['parentId']     	= 1;
	jsonParams[csrfParameter]  	= csrfToken;
	jsonParams['id']     		= $('#modal_edit_id').val();

	jsonParams['gl']   	= $('#modal_edit_gl').val();
	jsonParams['io']   	= $('#modal_edit_io').val();
	jsonParams['flagActive']  	= flagActive;
	jsonParams['createdBy'] 			= $('#modal_edit_createdBy').val();
	jsonParams['createdDate'] 			= $('#modal_edit_createdDate').val();
	jsonParams['updateBy'] 				= $('#modal_edit_updateBy').val();
	jsonParams['updateDate'] 			= $('#modal_edit_updateDate').val();
	jsonParams['company']     	= company;/*JSON.stringify(jsonConditionalIOCompany);*/
	var itemData = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/conditionalIOs/saveConditionalIO',
        data: jsonParams,
        complete: function (xhr) {
			$('#editItemModal').modal('hide');
			$("#completeModal").modal('show');
			searchConditionalIO();


        }
    }).done(function (){
        //close loader
    });

}

function editConditionalIO(id,company){
	$.ajax({
        type: "GET",
        url: session['context']+'/conditionalIOs/'+id,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var conditionalIOObj = JSON.parse(xhr.responseText);

                    $('#modal_edit_id').val(id);
            		$('#modal_edit_createdBy').val(conditionalIOObj.createdBy);
            		$('#modal_edit_createdDate').val(conditionalIOObj.createdDate);
            		$('#modal_edit_updateBy').val(conditionalIOObj.updateBy);
            		$('#modal_edit_updateDate').val(conditionalIOObj.updateDate);
            		$('#modal_edit_company').val(company);
					$('#modal_edit_gl').val(conditionalIOObj.gl);
					$('#modal_edit_io').val(conditionalIOObj.io);
            		if(conditionalIOObj.flagActive){
            			$('#modal_edit_flag_active').prop('checked', true);
            		}else{
            			$('#modal_edit_flag_active').removeAttr( "checked" );
            		}

                }
            }
        }
    }).done(function (){
        //close loader
		$('#editItemModal').modal('show');
    });

}

function deleteConditionalIO(id){
	var jsonParams = {};
	jsonParams['parentId']     = 1;
	jsonParams[csrfParameter]  = csrfToken;

	$.ajax({
		type: "DELETE",
		url: session['context']+'/conditionalIOs/'+id,
		data: jsonParams,
		complete: function (xhr) {
			searchConditionalIO();
			$('#deleteItemModal').modal('hide');
			$('#completeModal').modal('show');
		}
	}).done(function (){
		//close loader
	});
}



function searchConditionalIO(){
	var company = "";
		if($("#search_company").val().length > 0){
			company = $("#search_company").val();
		}



	var criteriaObject = {
		company     : company
	    };
	queryConditionalIOByCriteria(criteriaObject);
}

function queryConditionalIOByCriteria(criteriaObject){
	object.setId("#paggingSearchMain");                
    object.setUrlData("/conditionalIOs/findByCriteria");
    object.setUrlSize("/conditionalIOs/findSize");
    
    object.loadTable = function(items){
    	var item = items.content;
        // $('.dv-background').show();
        $('#gridMainBody').empty();
        if(item.length > 0){
        	$dataForQuery = item;
            var itemIdTmp ;
            for(var j=0;j<item.length;j++){
            	
                var gl = item[j].gl==null?"":item[j].gl;
                var io = item[j].io==null?"":item[j].io;
                var company = item[j].company==null?"":item[j].company.id;
                var flagActive = item[j].flagActive==null?"":item[j].flagActive;


                itemIdTmp = item[j].id==null?"":item[j].id;
                if(itemIdTmp){
                	$('#gridMainBody').append(''+
                            '<tr id="'+item[j].id+'" '+
                            ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
							((j%2 == 0) ? '<td class="text-center headcol tdFix"  style="width: 93px; ">' : '<td class="text-center headcol tdFixGrey"  style="width: 93px;">')+
							'<div class="checkbox" style="color: #2f2f2f;  margin-top: 0px;"><label><input type="checkbox" class="checkbox1" value="'+item[j].id+'"/><span class="checkbox-material " style="color: :#2f2f2f;"><span class="check " style="color: :#2f2f2f;" /></span></label></div>'+
							'</td>'+
							((j%2 == 0) ? '<td class="text-center headcol tdFix" style="width: 135px; left: 108px;">' : '<td class="text-center headcol tdFixGrey" style="width: 135px; left: 108px;">')+
							'	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Edit" data-toggle="modal"  onclick="editConditionalIO('+item[j].id+','+company+')" ><span class="fa fa-pencil-square-o"><jsp:text/></span></button>'+
							// '	<button type="button" class="btn btn-material-red-500  btn-style-small"  title="Delete"  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+item[j].id+'\');$(\'#locationFromItemDelete\').html(\''+locationFrom+'\');"><span class="fa fa-trash"><jsp:text/></span></button>'+
							'</td>'+
							((j%2 == 0) ? '<td class="text-center headcol tdFix" style="width: 135px; left: 238px;">' : '<td class="text-center headcol tdFixGrey" style="width: 135px; left: 238px;">')+
							((flagActive) ? '<img class="img-circle" src="'+$IMAGE_CHECKED+'" style="width: 25px;height: 25px;" />':'<img class="img-circle" src="'+$IMAGE_CANCEL+'" style="width: 25px;height: 25px;" />')+
							'</td>'+
                            '<td class="text-center" style="width: 200px;" >'+gl+'</td>'+
                            '<td class="text-center">'+io+'</td>'+


                            '</tr>'
                        );
                }
                
            }

        }else{
        	//Not Found
        }
        
    };

    object.setDataSearch(criteriaObject); 
    object.search(object);
	if(startPageC==1){
		startPageC++;
		object.loadPage(($CRITERIA_PAGE*1)+1,object);
	}else{
		object.loadPage(1,object);
	}
}


function checkConditionalIODuplicate(gl,io,company,conditionalIOId){
	$.ajax({
        type: "GET",
        url: session['context']+'/conditionalIOs/findConditionalIOByGlAndIoAndCompany?gl='+gl+'&io='+io+'&company='+company,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	if(xhr.responseText){
                		var conditionalIOObj = JSON.parse(xhr.responseText);
                        if(conditionalIOId != conditionalIOObj.id){
                        	$('#warningModal .modal-body').html($MESSAGE_HAS_DUPLICATE_CONDITIONAL_IO);
                			$('#warningModal').modal('show');
                        }else{
                        	saveConditionalIO();
                        }
                	}else{
                		saveConditionalIO();
                	}
                }
            }
        }
    }).done(function (){
        //close loader
    });
}

function checkAllIO() {
	$('#checkAll').click(function(event) {
		if(this.checked) {
			$('.checkbox1').each(function() {
				this.checked = true;
			});
		}else{
			$('.checkbox1').each(function() {
				this.checked = false;
			});
		}
	});

	$("#checkAll").prop("checked", false);
	$('.checkbox1').click(function() {
		if($('.checkbox1:checked').length == $('.checkbox1').length){
			$("#checkAll").prop("checked", true);
		}else{
			$("#checkAll").prop("checked", false);
		}
	});
}

function btnDelete(){
	var arrayCheckStatusCheckbox = [] ;
	for (var i = 0; i < $('.checkbox1').length; i++) {
		arrayCheckStatusCheckbox[i] = $('.checkbox1')[i].checked == true;
	}
	var statusCheckboxTrue = arrayCheckStatusCheckbox.indexOf(true);
	if(statusCheckboxTrue >= 0) {
		$('#deleteItemModal').modal('show');

	}else{
		$('#deleteCheckModal').modal('show');

	}
}

function getIdForDel(){
	var lengthOfResponseText;
	// var deleteSuccess = 0;
	// var deleteFail = 0;

	if($('.checkbox1').length != 0) {
		for (var i = 0; i < $('.checkbox1').length; i++) {
			if ($('.checkbox1')[i].checked == true) {
				var id = $('.checkbox1')[i].value;
				lengthOfResponseText = deleteConditionalIO(id);
				// if(lengthOfResponseText == 0){
				// 	deleteSuccess++;
				// }else{
				// 	deleteFail++;
				// }
			}
		}

		searchConditionalIO();
		// checkAlertForDelete(deleteSuccess , deleteFail);
	}
}


$(document).ready(function () {
	

	
	$('.numbersOnly').on("keydown",function(e){
        NumberUtil.numberOnly(e);
    });
	
	$('.amount2digit').on("keydown",function(e){
        NumberUtil.numberOnly(e);
        NumberUtil.numberFixDigit($(this),2);
    });
	
	$('#search_button').on('click', function () {
		searchConditionalIO();
		checkAllIO();
	});
	
	$('#editItemModal').on('hidden.bs.modal', function () {
		$('#modal_edit_id').val('');
		$('#modal_edit_createdBy').val('');
		$('#modal_edit_createdDate').val('');
		$('#modal_edit_updateBy').val('');
		$('#modal_edit_updateDate').val('');
		$('#modal_edit_gl').val('');
		$('#modal_edit_io').val('');
		$('#modal_edit_company').val('');
		$('#modal_edit_flag_active').prop('checked', true);
		// companyList();
	});

	$('#completeModal').on('shown.bs.modal', function() {
		window.setTimeout(function(){
			$('#completeModal').modal('hide');
		},1000);
	});

	$('#editItemModalSave').on('click', function () {
		//validate Mandatory
		var errorMessage = "";
		
		
		if(!$('#modal_edit_gl').val()){
			errorMessage += $MESSAGE_REQUIRE_LABEL_GL+"<br/>";
		}
		if(!$('#modal_edit_io').val()){
			errorMessage += $MESSAGE_REQUIRE_LABEL_IO+"<br/>";
		}

		if(!$('#modal_edit_company').val()){
			errorMessage += $MESSAGE_REQUIRE_LABEL_COMPANY+"<br/>";
		}


		if(errorMessage){
			$('#warningModal .modal-body').html(errorMessage);
			$('#warningModal').modal('show');
		}else{
			//Check Mandatory Field
			checkConditionalIODuplicate($('#modal_edit_gl').val(),$('#modal_edit_io').val(),$('#modal_edit_company').val(),$('#modal_edit_id').val());
		}
		
	});
	companyList();
	searchConditionalIO();
	checkAllIO();

});


function companyList() {
	var companyString = $.ajax({
		url: session['context'] + "/companies/findCompanyAll",
		type: "GET",
		// data:{},
		async: false
	}).responseText;
	companyJson = JSON.parse(companyString);
	$("#search_company").empty();

	$.each(companyJson.content, function (index, item) {
		$("#search_company").append("<option value=" + item.id + ">" + item.code +" "+ item.description + "</option>");
	});
	$("#modal_edit_company").empty();

	$.each(companyJson.content, function (index, item) {
		$("#modal_edit_company").append("<option value=" + item.id + ">" + item.code +" "+ item.description + "</option>");
	});
}