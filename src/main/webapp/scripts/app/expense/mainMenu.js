
var $DATA_EMPLOYEE;

$(document).ready(function () {

    findEmployeeProfileByUserName(session.userName);
    if (session.roleName.indexOf("ROLE_ACCOUNT") != -1) {
        $("#menu10Div").removeClass('hide');
    }else{
        $("#menu10Div").addClass('hide');
    }
    showItem();


    $('#menu_1').on('click',function () {
        gotoDocumentAdvances()

    })

    $('#menu_2').on('click',function () {
        gotoClearExpense()

    })


    $('#menu_3').on('click',function () {
        gotoReimburseExpense()

    })


    $('#menu_4').on('click',function () {
        gotoMyRequestExpense()

    })


    $('#menu_5').on('click',function () {
        gotoIncomingRequestExpense()

    })


    $('#menu_6').on('click',function () {
        gotHistoryExpense()

    })


    $('#menu_7').on('click',function () {

    })


    $('#menu_8').on('click',function () {
        gotoDocumentSummaryReport();
    });


    $('#menu_9').on('click',function () {

    })

    $('#menu_10').on('click',function () {
        genReportWaitPay();
    })





});



function showItem(){

    setTimeout(function(){
        // $('#menu_1').fadeIn('slow');

        $('#menu_1').fadeIn('slow');
        $('#menu_name_1').fadeIn('slow');

    }, 200);

    setTimeout(function(){

        $('#menu_2').fadeIn('slow');
        $('#menu_name_2').fadeIn('slow');
    }, 400);

    setTimeout(function(){

        $('#menu_3').fadeIn('slow');
        $('#menu_name_3').fadeIn('slow');
    }, 600);

    setTimeout(function(){
        $('#menu_4').fadeIn('slow');
        $('#menu_name_4').fadeIn('slow');
    }, 800);

    setTimeout(function(){
        $('#menu_5').fadeIn('slow');
        $('#menu_name_5').fadeIn('slow');
    }, 1000);

    setTimeout(function(){
        $('#menu_6').fadeIn('slow');
        $('#menu_name_6').fadeIn('slow');
    }, 1200);

    setTimeout(function(){
        $('#menu_7').fadeIn('slow');
        $('#menu_name_7').fadeIn('slow');
    }, 1400);

    setTimeout(function(){
        $('#menu_8').fadeIn('slow');
        $('#menu_name_8').fadeIn('slow');
    }, 1600);

    setTimeout(function(){
        $('#menu_9').fadeIn('slow');
        $('#menu_name_9').fadeIn('slow');
    }, 1800);

    setTimeout(function(){
        $('#menu_10').fadeIn('slow');
        $('#menu_name_10').fadeIn('slow');
    }, 1800);



        setClassHover()

}

function setClassHover() {


    $('#img1').addClass( "hvr-float-shadow" );
    $('#img2').addClass( "hvr-float-shadow" );
    $('#img3').addClass( "hvr-float-shadow" );
    $('#img4').addClass( "hvr-float-shadow" );
    $('#img5').addClass( "hvr-float-shadow" );
    $('#img6').addClass( "hvr-float-shadow" );
    $('#img7').addClass( "hvr-float-shadow" );
    $('#img8').addClass( "hvr-float-shadow" );
    $('#img9').addClass( "hvr-float-shadow" );
    $('#img10').addClass( "hvr-float-shadow" );

    

}

/* event menu */
function gotoDocumentAdvances(){
    window.location.href = session.context+'/advance/createDoc';
}

function gotoClearExpense(){
    window.location.href = session.context+'/expense/clearExpense';
}

function gotoReimburseExpense(){
    window.location.href = session.context+'/expense/reimburseExpense';
}

function gotoMyRequestExpense(){
    window.location.href = session.context + '/myExpense/listView';
}

function gotoIncomingRequestExpense() {
    window.location.href = session.context + '/incomingRequestExpense/listView';
}

function  gotHistoryExpense() {
    window.location.href = session.context + '/historyExpense/listView'
}

function  genReportWaitPay() {
    window.location.href = session.context + '/report/gen/REP001?username='+$USERNAME+'&psa='+$DATA_EMPLOYEE.Personal_PSA_ID;
}

function gotoDocumentSummaryReport() {
    window.location.href = session.context + '/documentStatusSummary/listView';
}

function findEmployeeProfileByUserName(userName){

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $DATA_EMPLOYEE = data;

    if(data){
        getOutStandingBalance(data.Personal_ID);

        if(data.Attorney_ID && parseInt(data.Attorney_ID) <= 207){
            $("#menu8Div").removeClass('hide');
        }
    }
}

function getOutStandingBalance(venderNo) {
    var dataAdvanceOutstandings;

    if(venderNo != "0"){
        dataAdvanceOutstandings = $.ajax({
            url: session.context + "/advanceOutstandings/findAdvanceOutstanding?venderNo="+venderNo+"&userName="+session.userName,
            headers: {
                Accept : "application/json"
            },
            type: "GET",
            async: false
        }).responseJSON;
    }

    if(dataAdvanceOutstandings){
        $("#outStanding").autoNumeric('init');
        if(dataAdvanceOutstandings.amount > 0){
            $("#rowOutStanding").removeClass('hide');
            $("#outStanding").autoNumeric('set',dataAdvanceOutstandings.amount);
        }else{
            $("#rowOutStanding").addClass('hide');
        }
    }else{
        $("#rowOutStanding").addClass('hide');
    }
}