var dataAttachmentType = [
    {
        id : 1,
        code : "M151",
        description : "ใบเสร็จรับเงิน",
        extensionFile : "pdf,jpg,JPG,png,PNG"
    },{
        id : 2,
        code : "M152",
        description : "รายละเอียดประกอบ",
        extensionFile : "pdf,jpg,JPG,png,PNG"
    },{
        id : 3,
        code : "M153",
        description : "รายชื่อผู้เข้าร่วมประชุม",
        extensionFile : "pdf,jpg,JPG,png,PNG"
    },{
        id : 4,
        code : "M154",
        description : "รายละเอียดหลักสูตร",
        extensionFile : "pdf,jpg,JPG,png,PNG"
    },{
        id : 5,
        code : "M155",
        description : "Memo อนุมัติโดย HR เห็นชอบ",
        extensionFile : "pdf,jpg,JPG,png,PNG"
    }
];

function moveFixedActionButton() {
    var xPos = $('.container').offset().left;
    for (var i = 0; i < $('.btnActionFixed').size(); i++) {
        $('.btnActionFixed')[i].style.right = (xPos-70) + 'px';
    }
}

function validateDocStatus(){
    /* update by siriradC.  2017.08.04 */
    if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_DRAFT){
        $("#ribbon").addClass("ribbon-status-draft");
        $("#ribbon").attr("data-content","DRAFT");
    }if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_CANCEL){
        $("#ribbon").addClass("ribbon-status-cancel");
        $("#ribbon").attr("data-content","CANCEL");
    }if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS){
        $("#ribbon").addClass("ribbon-status-on-process");
        $("#ribbon").attr("data-content","ON PROCESS");
    }if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_REJECT){
        $("#ribbon").addClass("ribbon-status-reject");
        $("#ribbon").attr("data-content","REJECT");
    }if($DATA_DOCUMENT.documentStatus == MASTER_DATA.DOC_STATUS_COMPLETE){
        $("#ribbon").addClass("ribbon-status-complete");
        $("#ribbon").attr("data-content","COMPLETE");
    }
}

function initClearExpenseDetail() {
    var dataExpenseItem = $DATA_DOCUMENT.documentExpense==null?"":$DATA_DOCUMENT.documentExpense.documentExpenseItems==null?"":$DATA_DOCUMENT.documentExpense.documentExpenseItems;
    $("#accordion1").empty();
    $("#docNumberLabel").text($DATA_DOCUMENT.docNumber);
    $("#docDateLabel").text($DATA_DOCUMENT.createdDate);
    $("#empNameLabel").text($DATA_DOCUMENT.requester);
    $("#creator").text($DATA_DOCUMENT.createdBy);

    if("" != dataExpenseItem){
        $.each(dataExpenseItem,function (index,item) {
            $("#accordion1").append('' +
                '<div class="panel panel-white-perl" style="margin-top: 0px;margin-bottom: 10px">'+
                '<div class="panel-heading collapseGray" id="collapseHeaderDetail" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseExpense'+item.id+'" aria-expanded="false" aria-controls="collapseExpense'+item.id+'" style="">'+
                '<div class="container-fluid" style="padding-right: 0;padding-left: 0">'+
                '<div class="col-sm-1 text-center" style="padding-right: 0;padding-left: 0">'+
                '<img src="'+$IMAGE_EXPENSE+'" width="40px"/>'+
                '</div>'+
                '<div class="col-sm-11" style="padding-right: 0;padding-left: 0">'+
                '<a class="collapsed" style="color:#ffffff;" >'+item.expenseTypeByCompanys.expenseTypes.description+'</a><span class="pull-right"  style="color:#03a9f4;font-size: 20px"><b>0.00</b></span><br/>'+
                '<a class="collapsed" style="color:#ffffff;" >'+item.expenseTypeByCompanys.glCode+'</a>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '<div id="collapseExpense'+item.id+'" class="panel-collapse collapse" role="tabpanel">'+
                '<div class="panel-body panel-white-perl">'+
                '<div class="row">'+
                '<div class="form-horizontal">'+
                '<div class="form-group">'+
                '<div class="container-fluid">'+
                '<a href="javascript:void(0)" data-toggle="modal" data-target="#modalAddDetail"><img src="'+$IMAGE_PLUS+'" width="35"><jsp:text/></img></a>'+
                '<a href="javascript:void(0)"><span class="pull-right" style="color: grey"><img src="'+$IMAGE_LINE+'" width="35"><jsp:text/></img>&#160;&#160;<b>เอกสารแนบนอกจากใบเสร็จ</b></span></a>'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<jsp:text/>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>'
            )
        });
    }

}

function cancelExpense() {
    var warningMessage = MSG.MESSAGE_CONFIRM_CANCEL_DOCUMENT +" "+$DATA_DOCUMENT.docNumber+"<br/>";

    $('#confirmModal .modal-body').html(warningMessage);
    $('#confirmModal').modal('show');
}

function cancelDocument(){
    var jsonData = {}
    jsonData['parentId']     	= 1;
    jsonData[csrfParameter]  	= csrfToken;
    jsonData['id']    			= $DATA_DOCUMENT.id;
    jsonData['documentStatus']  = MASTER_DATA.DOC_STATUS_CANCEL;

    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/expense/updateDocumentStatus',
        data:jsonData,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                $("#ribbon").removeClass("ribbon-status-draft");
                $("#ribbon").removeClass("ribbon-status-on-process");
                $("#ribbon").removeClass("ribbon-status-reject");
                $("#ribbon").removeClass("ribbon-status-complete");

                $("#ribbon").addClass("ribbon-status-cancel");
                $("#ribbon").attr("data-content","CANCEL");

                $('#confirmModal').modal('hide');
                $("#completeModal").modal('show');
            }
        }
    });
}

/* manage document attachment */
function documentAttachment(){
    getDataAttachmentType();
    findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);
    $("#modalDocumentAttachment").modal('show');
    $("#attachmentType").val("");
    $("#textFileName").val("");
}

function getDataAttachmentType(){
    $("#attachmentType").empty().append('<option value=""></option>');
    for(var i=0; i<dataAttachmentType.length;i++){
        $("#attachmentType").append(
            '<option value='+dataAttachmentType[i].code+' extension='+dataAttachmentType[i].extensionFile+'>'+ dataAttachmentType[i].description + '</option>');
    }
}

function findDocumentAttachmentByDocumentId(documentId){
    $.ajax({
        type: "GET",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/expense/'+documentId+'/documentAttachment',
        complete: function (xhr) {
            var $DATA_DOCUMENT_ATTACHMENT = JSON.parse(xhr.responseText);
            renderDocumentAttachment($DATA_DOCUMENT_ATTACHMENT);
        }
    });
}

function renderDocumentAttachment(dataDocumentAttachment){
    $('#gridDocumentAttachmentBody').empty();
    if(dataDocumentAttachment.length != 0 ){
        for(var i=0; i<dataDocumentAttachment.length;i++){
            $('#gridDocumentAttachmentBody').append('' +
                '<tr id="' + dataDocumentAttachment[i].id + '">' +
                '<td align="center">'+(i+1)+'</td>' +
                '<td align="center">'+getAttachmentType(dataDocumentAttachment[i].attachmentType)+'</td>' +
                '<td align="center">'+dataDocumentAttachment[i].fileName+'</td>' +
                '<td align="center"><button id='+dataDocumentAttachment[i].id+' fileName="'+dataDocumentAttachment[i].fileName+'" type="button" class="btn btn-material-blue-500 btn-style-small" onclick="downloadDocumentFile($(this)) "><span class="fa fa-cloud-download"/></button>' +
                '<button type="button" id='+dataDocumentAttachment[i].id+' class="btn btn-material-red-500 btn-style-small"  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+dataDocumentAttachment[i].id+'\');$(\'#typeDataDelete\').val(\'DOCUMENT_ATTACHMENT\');"><span class="fa fa-trash"/></button></td>' +
                '</tr>'
            );
        }
    }
}

function getAttachmentType(code){
    for(var i=0;i<dataAttachmentType.length;i++){
        if(dataAttachmentType[i].code == code){
            return dataAttachmentType[i].description;
            break;
        }
    }
}

function saveDocumentAttachment() {
    var jsonData = {}
    jsonData['parentId'] = 1;
    jsonData[csrfParameter] = csrfToken;
    jsonData['attachmentType'] = $("#attachmentType").val();
    jsonData['document'] = $DATA_DOCUMENT.id;

    var formData = new FormData();
    formData.append("file", $fileUpload);
    formData.append("filename", $fileUpload.name);
    formData.append("attachmentType", $("#attachmentType").val());
    formData.append("document", $DATA_DOCUMENT.id);

    var dataDocumentAttachment = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: session['context'] + '/expense/saveDocumentAttachment',
        processData: false,
        contentType: false,
        data: formData,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    dataDocumentAttachment = JSON.parse(xhr.responseText);
                    $("#uploadDocumentAttachment").removeClass('hide');
                    $('.myProgress').addClass('hide');
                    $("#textFileName").val("");
                    findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);
                    renderDocumentAttachment(dataDocumentAttachment);
                }
                else if (xhr.status == 500) {
                    //unsuccess
                }
            } else {
                //unsuccess
            }
        }
    });
}

function deleteDocumentAttachment(id){

    var jsonParams2 = {};
    jsonParams2['parentId']     = 1;
    jsonParams2[csrfParameter]  = csrfToken;

    $.ajax({
        type: "DELETE",
        url: session['context']+'/expense/deleteDocumentAttachment/'+id,
        data: jsonParams2,
        complete: function (xhr) {
            findDocumentAttachmentByDocumentId($DATA_DOCUMENT.id);
            $('#deleteItemModal').modal('hide');
        }
    });
}

function validateFileExtensions(){
    var extensionFile = $('#attachmentType option:selected').attr('extension');

    var validFileExtensions = extensionFile.split(',');

    var file = $fileUpload.name;
    var ext = file.split('.').pop();
    if (validFileExtensions.indexOf(ext.toLowerCase())==-1){
        $("#labelValidate").empty();
        $("#labelValidate").text(MSG.MESSAGE_FILE_TYPE_INVALID+" "+extensionFile);
        $("#validateFile").removeClass('hide');
    }else{
        $("#labelValidate").empty();
        $("#validateFile").addClass('hide');
    }
}

$(document).ready(function () {
    readMasterDataDocType();
    readMasterDataDocStatusDraft();
    readMasterDataDocStatusOnProcess();
    readMasterDataDocStatusCancel();
    readMasterDataDocStatusReject();
    readMasterDataDocStatusComplete();

    moveFixedActionButton();

    window.setTimeout(function(){
        initClearExpenseDetail();
        validateDocStatus();
    },1000);

    $("#confirmCancel").on('click',function(){
        cancelDocument();
    });

    $("#browseFile").on('click',function(){
        if($("#attachmentType").val() == ""){
            var errorMessage = MSG.MESSAGE_REQUIRE_FIELD +" "+LB.LABEL_ATTACHMENT_TYPE+"<br/>";
            $('#warningModal .modal-body').html(errorMessage);
            $('#warningModal').modal('show');
        }else{
            var extension = $("#attachmentType")[0].selectedOptions[0].getAttribute('extension');
            $("#uploadFileDocument").attr('accept',extension);
            $("#uploadFileDocument").click();
        }
    });

    $("#uploadFileDocument").change(function () {
        $fileUpload = this.files[0];
        $("#textFileName").val(this.files[0].name);
        // validateFileExtensions();
    });

    $("#uploadDocumentAttachment").on('click',function (){
        $("#uploadDocumentAttachment").addClass('hide');
        $('.myProgress').removeClass('hide');
        setTimeout(function (){
            saveDocumentAttachment();
        },1000);
    });

    $("#confirmDelete").on('click',function (){
        deleteDocumentAttachment($("#idItemDelete").val());
    });

    /* upload document expense attachment */
    $("#uploadDocumentExpenseAttachmentTxt").on('click',function () {
       $("#uploadDocumentExpenseAttachmentInput").click(); 
    });

    $("#uploadDocumentExpenseAttachmentBtn").on('click',function () {
        $("#uploadDocumentExpenseAttachmentInput").click();
    });
    
    $("#uploadDocumentExpenseAttachmentInput").change(function () {
        $("#uploadDocumentExpenseAttachmentTxt").val(this.files[0].name)
    });
});

