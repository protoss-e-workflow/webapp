
var object = $.extend({},UtilPagination);
var object_1 = $.extend({},UtilPagination);
var object_2 = $.extend({},UtilPagination);
var object_3 = $.extend({},UtilPagination);
var object_4 = $.extend({},UtilPagination);
var object_5 = $.extend({},UtilPagination);
var expenseTypeId='';
var masterDataDetailApproveType='';
var masterDataDetailApproveType_tmp='';
var masterDataDetailAttchmentType='';
var companyObject = ''
var checkDupExpenseType = false
var checkDupExpenseType_Update = false
var checkDupExpenseTypeCode = ''
var checkDupExpenseTypeFile = false
var checkDupExpenseTypeFile_id = ''
var checkDupExpenseTypeRef = false
var checkDupExpenseTypeCompany = false
var checkDupExpenseTypeReimburse = false
var checkDupExpenseTypeScreen = false
var favorite =''
var startpage =1
var dataStatus;
var tmpCode
var checkDataFile = ''
var checkDataScreen = ''
var checkDataRef = ''
var checkDataCom_pa = ''
var checkDataCom_psa = ''
var checkDataCom_gl = ''
var checkDataReiumbure = ''
var dataHeadOfficeGL= ''
var dataFactoryGL= ''

var imgSrc = ''
$(document).ready(function () {



    findFlowType()
    $.material.init();


$('#addModalReimburse').on('click',function () {
    $('#add_reimburse_role_name').val('')
    checkDataReiumbure=''

})

    $('#openAddExpenseType').on('click',function () {
        $('#add_expenseType_code').val('')
        $('#add_expenseType_description').val('')
        $('#add_expenseType_favorite').prop('checked',false)

    })
$('#updateExpenseType').on('click',function () {

    updateExpenseType()

})


    $('#edit_flowType').on('change',function () {
        var codeFlow = $('#edit_flowType').val()
        findImageSrc_By_FlowType(codeFlow)

        var srcImg = imgSrc

        if(srcImg == 'flow-image_2'){
            $('#flowType').attr("src",$IMG_FLOW_TYPE_2);
        }
        if(srcImg == 'flow-image_3'){
            $('#flowType').attr("src",$IMG_FLOW_TYPE_3);
        }
        if(srcImg == 'flow-image_4'){
            $('#flowType').attr("src",$IMG_FLOW_TYPE_4);
        }
        if(srcImg == 'flow-image_5_HaveAdmin'){
            $('#flowType').attr("src",$IMG_FLOW_TYPE_5_HAVE_ADMIN);
        }
        if(srcImg == 'flow-image_5_NoAdmin'){
            $('#flowType').attr("src",$IMG_FLOW_TYPE_5__NO_ADMIN);
        }
        if(srcImg == 'flow-image_6'){
            $('#flowType').attr("src",$IMG_FLOW_TYPE_6);
        }

            $('#edit_screen_config').on('click',function () {
                $('#edit_screen_fixCode').prop('checked',false)
            })

        $('#edit_screen_fixCode').on('selected',function () {
            $('#edit_screen_config').prop('checked',false)

        })

    })









    findAttachmentTypeAll()
    findAllRole()
    findApprove()
    findApproveSequence()
    searchExpenseType()




    $('#addScreen').on('click',function () {
        checkDataScreen= ''
            $('#add_screen_id').val('')
            $('#add_screen_engName').val('')
            $('#add_screen_thName').val('')
            $('#add_screen_structure_field').val('')
            $('#add_screen_type').val('')
            $('#add_screen_require').prop('checked',false)

    })

    $('#addFile').on('click',function () {
        checkDataFile= ''
        $('#add_file_id').val('')
        $('#add_file_code').val('')
        $('#add_file_require').prop('checked',false)

    })

    $('#addApprove').on('click',function () {
        checkDataRef = ''
        $('#add_approve_id').val('')
        $('#add_approve_type').val('')
        $('#add_approve_ref').val('')
        $('#add_approve_description').val('')

    })


    $('#addGL').on('click',function () {
        checkDataCom_pa=''
        checkDataCom_psa=''
        checkDataCom_gl=''
        $('#add_company_id').val('')
        $('#add_company_pa').val('')
        $('#add_company_psa').val('')
        $('#add_company_glCode').val('')

    })

    $('#openModalExpense').on('click',function () {

        $('#edit_id').val('')
        $('#add_expenseType_code').val('')
        $('#add_expenseType_description').val('')
        $('#add_expenseType_favorite').prop('checked',false)

    })







    $('#search_button').on('click',function () {
        searchExpenseType()

    })
});




// ==================================================== Find Data Function ===================================================

function findApprove(){

    $.ajax({
        type: "GET",
        url: session['context']+'/masterDatas/findByMasterdataInOrderByCode?masterdata='+MASTER_DATA.APPROVE_TYPE_CODE,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);



                    var item = obj.content
                    console.log(item.length)

                    for(var i=0; i< item.length; i++){
                        // console.log(item[i].id)
                        $("#add_approve_type").append('<option value='+item[i].code+'>'+item[i].code+ ': '+item[i].name+ '</option>')

                    }


                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function findApproveSequence(code){



    $("#add_approve_ref").empty()
    $.ajax({
        type: "GET",
        url: session['context']+'/masterDatas/findByMasterdataInOrderByCode?masterdata='+MASTER_DATA.APPROVE_TYPE_REMARK,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);



                    var item = obj.content
                    console.log(item.length)

                    for(var i=0; i< item.length; i++){
                        // console.log(item[i].id)
                        $("#add_approve_ref").append('<option value='+item[i].code+'>'+item[i].description+ '</option>')

                    }


                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function findMasterdataDetailApproveByCode(code){

    $.ajax({
        type: "GET",
        url: session['context']+'/masterDatas/findByMasterdataInAndMasterDataDetailCodeOrderByCode?masterdata='+MASTER_DATA.APPROVE_TYPE_CODE+'&code='+code,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);





                    masterDataDetailApproveType = obj

                    // masterDataDetailApproveType = obj
                    // console.log('MasterDataDetail of Approve Type ======> '+item.code)




                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function findMasterdataDetailApproveByCode_tmp(code){

    $.ajax({
        type: "GET",
        url: session['context']+'/masterDatas/findByMasterdataInAndMasterDataDetailCodeOrderByCode?masterdata='+MASTER_DATA.APPROVE_TYPE_REMARK+'&code='+code,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        var obj = JSON.parse(xhr.responseText);
                        masterDataDetailApproveType_tmp = obj
                    }

                    // masterDataDetailApproveType = obj
                    // console.log('MasterDataDetail of Approve Type ======> '+item.code)




                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function findMasterdataDetailAttchmentTyoeByCode(code){

    $.ajax({
        type: "GET",
        url: session['context']+'/masterDatas/findByMasterdataInAndMasterDataDetailCodeOrderByCode?masterdata='+MASTER_DATA.APPROVE_ATTCHMENT_CODE+'&code='+code,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);





                    masterDataDetailAttchmentType = obj

                    // masterDataDetailApproveType = obj
                    // console.log('MasterDataDetail of Approve Type ======> '+item.code)




                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function showDetail(id,favorite,code,des,flow,fixCode,type,headGL,favGL) {


    dataHeadOfficeGL = headGL == null ? "" : headGL
    dataFactoryGL = favGL == null ? "" : favGL



                console.log('Flow is =====> '+flow)
                console.log('FixCode is =====> '+fixCode)
                console.log('Type is =====> '+type)
    if(favorite.toString() == "true"){
        $('#add_expenseType_favorite_general').prop('checked',true)
    }else{
        $('#add_expenseType_favorite_general').prop('checked',false)
    }

        var isFix =""
        if(fixCode.toString() == 'null'){
            isFix= ""

        }else{
            isFix =fixCode
        }


        if(type == 'CS'){
            $('#edit_screen_config').prop('checked',true);
        }
        if(type == 'FC'){
            $('#edit_screen_fixCode').prop('checked',true);

        }

        $('#expenseType_code_edit').val(code)
        $('#expenseType_des_edit').val(des)
        $('#expenseType_des_edit').val(des)
        $('#edit_screen_fixCode_value').val(isFix)

        tmpCode = $('#expenseType_code_edit').val()



console.log('Favorite is ====> '+favorite)
    $("tr").removeClass("backgroundSelected")
    $("tr#"+id).addClass("backgroundSelected")

       findImageSrc_By_FlowType(flow)
       var detailFlow = imgSrc

    if(detailFlow == 'flow-image_2'){
        $('#flowType').attr("src",$IMG_FLOW_TYPE_2);
        $('#edit_flowType').val(flow)
    }
    if(detailFlow == 'flow-image_3'){
        $('#flowType').attr("src",$IMG_FLOW_TYPE_3);
        $('#edit_flowType').val(flow)
    }
    if(detailFlow == 'flow-image_4'){
        $('#flowType').attr("src",$IMG_FLOW_TYPE_4);
        $('#edit_flowType').val(flow)
    }
    if(detailFlow == 'flow-image_5_HaveAdmin'){
        $('#flowType').attr("src",$IMG_FLOW_TYPE_5_HAVE_ADMIN);
        $('#edit_flowType').val(flow)
    }
    if(detailFlow == 'flow-image_5_NoAdmin'){
        $('#flowType').attr("src",$IMG_FLOW_TYPE_5__NO_ADMIN);
        $('#edit_flowType').val(flow)
    }
    if(detailFlow == 'flow-image_6'){
        $('#flowType').attr("src",$IMG_FLOW_TYPE_6);
        $('#edit_flowType').val(flow)
    }
    dataStatus ='update'
    expenseTypeId = id
    searchExpenseTypeRef()
    searchExpenseTypeCompany()
    searchReimburse()
    searchExpenseTypeScree()
    searchExpenseTypeFile()

    console.log('Expense Type Id => '+expenseTypeId)

    $('#detail').fadeIn('slow')





}
function findAllRole(){

    $('#add_reimburse_role_name').empty()
    $.ajax({
        type: "GET",
        url: session['context']+'/roles/findAllRole',
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);



                    var item = obj.content
                    console.log('Company Object size ===> '+item.length)
                    $("#add_reimburse_role_name").append('<option value='+""+'>'+'</option>')

                    for(var i=0; i< item.length; i++){
                        console.log(item[i].roleName)
                        $("#add_reimburse_role_name").append('<option value='+item[i].roleName+'>'+item[i].roleName+'</option>')

                    }


                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function findAttachmentTypeAll(){

    $('#add_file_code').empty()

    $.ajax({
        type: "GET",
        url: session['context']+'/masterDatas/findByMasterdataInOrderByCode?masterdata='+MASTER_DATA.APPROVE_ATTCHMENT_CODE,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);



                    var item = obj.content
                    console.log(item.length)

                    for(var i=0; i< item.length; i++){
                        console.log(item[i].description)
                        $("#add_file_code").append('<option value='+item[i].code+'>'+item[i].code+ ': '+item[i].description+ '</option>')

                    }


                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function findExpenseTypeByCode(code){
    checkDupExpenseType = false
            console.log('Code find ==> '+code)
    $.ajax({
        type: "GET",
        url: session['context']+'/expenseType/findByCode?code='+code,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        var obj = JSON.parse(xhr.responseText);
                        checkDupExpenseType = true
                       // if(obj.code != code){
                       //
                       //     checkDupExpenseType = true
                       // }

                    }else{
                        console.log('NOT FOUND')
                    }








                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function findExpenseTypeByCode_Update(code){
    checkDupExpenseType_Update = false
    console.log('Code find ==> '+code)
    $.ajax({
        type: "GET",
        url: session['context']+'/expenseType/findByCode?code='+code,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        var obj = JSON.parse(xhr.responseText);
                       console.log(obj.code+ '<<<<<<<<< Item Find For Check Dup');
                       console.log(code+ '<<<<<<<<< Item Compare');

                        checkDupExpenseType_Update = true



                        // if(obj.code != code){
                        //
                        //     checkDupExpenseType_Update = true
                        // }

                    }else{
                        console.log('NOT FOUND')
                    }








                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function findByAttachmentTypeCode(attachmentTypeCode,expenseType){
    checkDupExpenseTypeFile = false
            console.log('Code find ==> '+attachmentTypeCode)
            console.log('Code find ==> '+expenseType)
    $.ajax({
        type: "GET",
        url: session['context']+'/expenseType/findExpenseTypeByIdAndExpenseTypeFileAttachmentCode?attachmentTypeCode='+attachmentTypeCode+'&expenseType='+parseInt(expenseType) ,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        var obj = JSON.parse(xhr.responseText);

                        checkDupExpenseTypeFile = true

                    }else{
                        console.log('NOT FOUND')
                    }








                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function findByCompleteReason(completeReason,expenseType){
    checkDupExpenseTypeRef = false
            console.log('Complete Reason ==> '+completeReason)
            console.log('Expense Type  ==> '+expenseType)
    $.ajax({
        type: "GET",
        url: session['context']+'/expenseType/findExpenseTypeByIdAndExpenseTypeReferencesCompleteReason?completeReason='+completeReason+'&expenseType='+parseInt(expenseType) ,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        var obj = JSON.parse(xhr.responseText);
                        checkDupExpenseTypeRef = true

                    }else{
                        console.log('NOT FOUND')
                    }








                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function findExpenseTypeByIdAndExpenseTypeCompanyPaAndExpenseTypeCompanyPsaAndExpenseTypeCompanyGlCode(pa,psa,glCode,expenseType){
    checkDupExpenseTypeCompany = false
    console.log('PA ==> '+pa)
    console.log('PSA ==> '+psa)
    console.log('GL Code ==> '+glCode)
    console.log('Expense Type  ==> '+expenseType)
    $.ajax({
        type: "GET",
        url: session['context']+'/expenseType/findExpenseTypeByIdAndExpenseTypeCompanyPaAndExpenseTypeCompanyPsaAndExpenseTypeCompanyGlCode?pa='+pa+'&psa='+psa+'&glCode='+glCode+'&expenseType='+parseInt(expenseType) ,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        var obj = JSON.parse(xhr.responseText);
                        checkDupExpenseTypeCompany = true
                    }else{
                        console.log('NOT FOUND')
                    }








                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function findExpenseTypeByIdAndReimburseRoleCode(roleCode,expenseType){
    checkDupExpenseTypeReimburse = false
    console.log('Code find ==> '+roleCode)
    console.log('Code find ==> '+expenseType)
    $.ajax({
        type: "GET",
        url: session['context']+'/expenseType/findExpenseTypeByIdAndReimburseRoleCode?roleCode='+roleCode+'&expenseType='+parseInt(expenseType) ,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        var obj = JSON.parse(xhr.responseText);
                        checkDupExpenseTypeReimburse = true
                    }else{
                        console.log('NOT FOUND')
                    }








                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function changeFavorite(id,favorite,des,code,flowType,fixCode,screenType,headOfficeGL,factoryGL) {



    console.log('Item Id    ===> '+id)
    console.log('Item Favorite    ===> '+favorite)
    console.log('Item Description    ===> '+des)
    console.log('Item Code    ===> '+code)
    console.log('Item flowType    ===> '+flowType)
    console.log('Item fixCode    ===> '+fixCode)
    console.log('Item screenType    ===> '+screenType)
    console.log('Item headOfficeGL    ===> '+headOfficeGL)
    console.log('Item factoryGL    ===> '+factoryGL)

                if(favorite.toString() == "true" ){
                    favorite=false
                }else{
                    favorite=true

                }
    // console.log('Item Favorite    ===> '+item.favorite)

    // var favChange = favorite
    //
    // if(favorite == true){
    //     favChange=false
    // }else{
    //     favChange=true
    // }

    var jsonParams = {};

    jsonParams['parentId']     	= 1;
    jsonParams[csrfParameter]  	= csrfToken;
    jsonParams['id']     		= id
    jsonParams['createdBy'] 	= $('#edit_createdBy').val();
    jsonParams['createdDate'] 	= $('#edit_createdDate').val();
    jsonParams['updateBy'] 		= $('#edit_updateBy').val();
    jsonParams['updateDate'] 	= $('#edit_updateDate').val();
    jsonParams['code']          = code  ;
    jsonParams['description']   = des;
    jsonParams['favorite']      = favorite
    jsonParams['flowType']      = flowType
    jsonParams['fixCode']      = fixCode
    jsonParams['screenType']      = screenType
    jsonParams['headOfficeGL']      = headOfficeGL
    jsonParams['factoryGL']      = factoryGL



                    //
                    // console.log('value change favorite ===> '+jsonParams.favorite)
                    // console.log('value change id ===> '+jsonParams.id)






            var itemData = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context']+'/expenseType',
                data: jsonParams,
                complete: function (xhr) {

                    if( (jsonParams.favorite).toString() == "true"){
                        $('#add_expenseType_favorite_general').prop('checked',true)
                    }else{
                        $('#add_expenseType_favorite_general').prop('checked',false)
                    }



                    searchExpenseType();
                    showDetail(id,favorite)
                    $("tr").removeClass("backgroundSelected")
                    $("tr#"+id).addClass("backgroundSelected")



                }
            }).done(function (){
                //close loader
            });










}
function findExpenseTypeByExpenseTypeScreenStructureField(structureField,expenseType){
    checkDupExpenseTypeScreen = false

    $.ajax({
        type: "GET",
        url: session['context']+'/expenseType/findExpenseTypeByExpenseTypeScreenStructureField?structureField='+structureField+'&expenseType='+parseInt(expenseType) ,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        var obj = JSON.parse(xhr.responseText);
                        checkDupExpenseTypeScreen = true
                    }else{
                        console.log('NOT FOUND')
                    }








                }
            }
        }
    }).done(function (){
        //close loader
    });

}
// ==================================================== Find Data Function ===================================================










// ==================================================== Expense Type ===================================================

function searchExpenseType(){
    $('#detail').hide()
    var criteriaObject = {
        description        : $("#search_description").val()=="" ? "": $("#search_description").val()
    };

    console.log("DATA FOR SEARCR =====> "+criteriaObject.description)
    queryExpenseTypeByCriteria(criteriaObject);

    // $("tr").removeClass("backgroundSelected")
    // $("tr#"+expenseTypeId).addClass("backgroundSelected")


}
function queryExpenseTypeByCriteria(criteriaObject){
    object.setId("#pagingSearchExpenseTypeBody");
    object.setUrlData("/expenseType/findByCriteria");
    object.setUrlSize("/expenseType/findSize");


    object.loadTable = function(items){
        var item = items.content;
        // $('.dv-background').show();
        $('#expenseTypeBody').empty();
        if(item.length > 0){
            $dataForQuery = item;
            var itemIdTmp ;


            for(var j=0;j<item.length;j++){

                var name    = item[j].description==null?"":item[j].description;
                var favorite    = item[j].favorite==null?"":item[j].favorite;
                var itemAll =  item[j]

                itemIdTmp = item[j].id;
                if(itemIdTmp){
                    $('#expenseTypeBody').append(''+
                        // '<td class="text-center">'+''+'</td>'+
                        '<tr id="'+item[j].id+'" '+
                        ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
                        '<td class="text-center">'+''+'</td>'+
                        '<td class="text-center">'+name+'</td>'+
                        '<td class="text-center" style="padding-top: 8px; padding-bottom: 0px;">'+'<a onclick="changeFavorite(\''+item[j].id+'\',\''+item[j].favorite+'\',\''+item[j].description+'\',\''+item[j].code+'\',\''+item[j].flowType+'\',\''+item[j].fixCode+'\',\''+item[j].screenType+'\',\''+item[j].headOfficeGL+'\',\''+item[j].factoryGL+'\')">' + ((favorite===true) ? '<img  class="img-circle" src="' + $IMG_FAVORITE + '" style="width: 35px;height: 35px;" />' : '<img class="img-circle" src="' + $IMG_FAVORITE + '" style="width: 35px;height: 35px; filter: grayscale(100%);" />') +'</a>'+ '</td>' +

                        '<td class="text-center">'+
                        '	<a onclick="$(\'#idItemDelete\').val(\''+item[j].id+'\')" data-target="#deleteExpenseType" data-toggle="modal" "><span><image title="'+$LABEL_DELETE+'" style="width: 35px; height: 35px; border-bottom-left-radius: 40%" src="'+$IMG_DELETE_ITEM+'"  /></span></a>'+
                        '	<a   onclick="showDetail(\''+item[j].id+'\',\''+item[j].favorite+'\',\''+item[j].code+'\',\''+item[j].description+'\',\''+item[j].flowType+'\',\''+item[j].fixCode+'\',\''+item[j].screenType+'\',\''+item[j].headOfficeGL+'\',\''+item[j].factoryGL+'\')"><span><image  title="'+$LABEL_SHOW_DETAIL+'" style="width: 35px; height: 35px; border-bottom-left-radius: 40%" src="'+$IMG_DETAIL_ITEM+'"  /></span></a>'+
                        '</td>'+
                        '</tr>'
                    );
                }

            }


        }else{
            //Not Found
        }

    };

    object.setDataSearch(criteriaObject);
    object.search(object);



    if(startpage==1){
        startpage++;
        object.loadPage(($CRITERIA_PAGE*1)+1,object);
    }else{
        object.loadPage(1,object);
    }


    // object.loadPage(($CRITERIA_PAGE*1)+1,object);


}
function saveExpenseType(){

    var isFavorite =      $("#add_expenseType_favorite").prop('checked') ? true : false

                var code = ($('#add_expenseType_code').val()).trim()
    var jsonParams = {};

    jsonParams['parentId']     	= 1;
    jsonParams[csrfParameter]  	= csrfToken;
    jsonParams['id']     		= $('#edit_id').val();
    jsonParams['createdBy'] 	= $('#edit_createdBy').val();
    jsonParams['createdDate'] 	= $('#edit_createdDate').val();
    jsonParams['updateBy'] 		= $('#edit_updateBy').val();
    jsonParams['updateDate'] 	= $('#edit_updateDate').val();
    jsonParams['code']          =   ($('#add_expenseType_code').val()).trim()  ;
    jsonParams['description']   =($('#add_expenseType_description').val()).trim();
    jsonParams['favorite']      	=isFavorite
    jsonParams['headOfficeGL']      	=''
    jsonParams['factoryGL']      	=''




                    if( ($('#add_expenseType_code').val()).trim()  != "" && ($('#add_expenseType_description').val()).trim() != ""     ){



                        findExpenseTypeByCode(jsonParams.code)

                        if(checkDupExpenseType != true){


                            $('.dv-background').show();


                            var itemData = $.ajax({
                                type: "POST",
                                headers: {
                                    Accept: 'application/json'
                                },
                                url: session['context']+'/expenseType',
                                data: jsonParams,
                                complete: function (xhr) {
                                    var obj = JSON.parse(xhr.responseText);

                                    $('#addExpenseType').modal('hide');

                                    $('#saveSuccess').modal('show');
                                    setTimeout(function () {
                                        $('#saveSuccess').modal('hide');
                                    },1500)



                                    searchExpenseType();

                                    expenseTypeId=obj.id
                                    $("tr#"+obj.id).addClass("backgroundSelected")

                                    // $("div#hideText").empty()
                                }
                            }).done(function (){

                                $('.dv-background').hide();

                            });
                        }else{
                            $('#warningModal .modal-body').html("")
                            $('#warningModal .modal-body').append($MESSAGE_DUPLICATE_CODE_OF_EXPENSE_TYPE+"<br/>");
                            $('#warningModal').modal('show');
                        }



                    }else{
                        $('#warningModal .modal-body').html("")
                        $('#warningModal .modal-body').append($MESSAGE_REQUIRE_FIELD+"<br/>");

                        if(($('#add_expenseType_code').val()).trim() == ""){
                            $('#warningModal .modal-body').append("      - "+$LABEL_CODE+"<br/>");

                        }
                        if(($('#add_expenseType_description').val()).trim() == ""){
                            $('#warningModal .modal-body').append("      - "+$LABEL_DESCRIPTION+"<br/>");
                        }
                        $('#warningModal').modal('show');
                    }





}
function deleteExpenseType(id) {


    console.log('Id Delete  Expense Type =======> '+id)


    var jsonParams = {};
    jsonParams['parentId']     = 1;
    jsonParams[csrfParameter]  = csrfToken;

    $('.dv-background').show();

    $.ajax({
        type: "DELETE",
        url: session['context']+'/expenseType/'+id,
        data: jsonParams,
        complete: function (xhr) {

            $('#deleteExpenseType').modal('hide')
            $('#deleteSuccess').modal('show')
                    setTimeout(function () {
                        $('#deleteSuccess').modal('hide')
                    },1500)




            searchExpenseType();
            ;

        }
    }).done(function (){
        $('.dv-background').hide();
    });

}

// ==================================================== Expense Type Reference ===================================================











// ==================================================== Expense Type Reference ===================================================

function searchExpenseTypeRef(){
    console.log('Find ExpenseTypeRef by id Main = '+expenseTypeId)
    var criteriaObject = {
        expenseType        : expenseTypeId
    };

    console.log("DATA FOR SEARCR =====> "+criteriaObject.description)
    queryExpenseTypeRefByCriteria(criteriaObject);

}
function queryExpenseTypeRefByCriteria(criteriaObject){
    object_1.setId("#pagingSearchExpenseTypeRefBody");
    object_1.setUrlData("/expenseReference/findByCriteria");
    object_1.setUrlSize("/expenseReference/findSize");


    object_1.loadTable = function(items){
        var item = items.content;

        $('#expenseTypeRefBody').empty();
        if(item.length > 0){
            $dataForQuery = item;
            var itemIdTmp ;


            for(var j=0;j<item.length;j++){

                var approveTypeCode    = item[j].approveTypeCode==null?"":item[j].approveTypeCode;
                var completeReason    = item[j].completeReason==null?"":item[j].completeReason;

                var code =''
                var des = ''


                var code_tmp =''
                var des_tmp = ''

                if(approveTypeCode != ""){
                    findMasterdataDetailApproveByCode(approveTypeCode)
                    var code = masterDataDetailApproveType.code
                    var des = masterDataDetailApproveType.description
                }

                //

                if(completeReason != ""){
                    findMasterdataDetailApproveByCode_tmp(completeReason)
                    var code_tmp = masterDataDetailApproveType_tmp.code
                    var des_tmp = masterDataDetailApproveType_tmp.description
                }



                itemIdTmp = item[j].id;
                if(itemIdTmp){
                    $('#expenseTypeRefBody').append(''+
                        '<tr id="'+item[j].id+'" '+
                        ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
                        '<td class="text-center">'+des+'</td>'+
                        '<td class="text-center">'+des_tmp+'</td>'+
                        '<td class="text-center">'+
                        '	<button type="button" class="btn btn-material-red-500  btn-style-small"  title="Delete"  onclick="$(\'#idItemDeleteExpenseRef\').val(\''+item[j].id+'\')" data-target="#deleteExpenseTypeRef" data-toggle="modal""><span class="fa fa-trash"><jsp:text/></span></button>'+
                        '	<button type="button" class="btn btn-material-cyan-500  btn-style-small"  title="Edit" onclick="editExpenseRef(\''+item[j].id+'\')" ><span class="fa fa-pencil-square-o"><jsp:text/></span></button>'+
                        // '	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Up" onclick="" style="border-radius: 30px;width: 30px;padding-left: 8px;padding-top: 4px;" ><span class="fa fa-arrow-up"><jsp:text/></span></button>'+
                        // '	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Down" onclick=""  style="border-radius: 30px;width: 30px;padding-left: 8px;padding-top: 4px;"  ><span class="fa fa-arrow-down"><jsp:text/></span></button>'+

                        '</td>'+
                        '</tr>'
                    );
                }

            }


        }else{
            //Not Found
        }

    };

    object_1.setDataSearch(criteriaObject);
    object_1.search(object_1);
    object_1.loadPage(($CRITERIA_PAGE_1*1)+1,object_1);
}
function saveApproveRef() {



    var jsonExpenseTypeReference = {};

    jsonExpenseTypeReference['parentId']     	           = 1;
    jsonExpenseTypeReference[csrfParameter]  	           = csrfToken;
    jsonExpenseTypeReference['id']     		               = $('#add_approve_id').val();
    jsonExpenseTypeReference['approveTypeCode']            = $('#add_approve_type').val();
    jsonExpenseTypeReference['completeReason']      	   = $('#add_approve_ref').val();
    jsonExpenseTypeReference['expenseType']      	       =   expenseTypeId;



    console.log(' Object Before Save ID = [ '+jsonExpenseTypeReference.id+' ]')


            if(  $('#add_approve_type').val() != "" ){


                findByCompleteReason(jsonExpenseTypeReference.completeReason,expenseTypeId)



                if(   ($('#add_approve_type').val()) != "" ){

                    if(checkDupExpenseTypeRef == false){
                        $('.dv-background').show();
                        var itemData = $.ajax({
                            type: "POST",
                            headers: {
                                Accept: 'application/json'
                            },
                            url: session['context']+'/expenseType/saveExpenseTypeRef',
                            data: jsonExpenseTypeReference,
                            complete: function (xhr) {
                                $('#add_approve_id').val("")
                                $('#addApproveRef').modal('hide');
                                $('#saveSuccess').modal('show');
                                setTimeout(function () {
                                    $('#saveSuccess').modal('hide');
                                },1500)

                                searchExpenseTypeRef()








                            }
                        }).done(function (){
                            $('.dv-background').hide();
                        });
                    }else{


                        if(checkDataRef == jsonExpenseTypeReference.completeReason){
                            $('.dv-background').show();
                            var itemData = $.ajax({
                                type: "POST",
                                headers: {
                                    Accept: 'application/json'
                                },
                                url: session['context']+'/expenseType/saveExpenseTypeRef',
                                data: jsonExpenseTypeReference,
                                complete: function (xhr) {
                                    $('#add_approve_id').val("")
                                    $('#addApproveRef').modal('hide');
                                    $('#saveSuccess').modal('show');
                                    setTimeout(function () {
                                        $('#saveSuccess').modal('hide');
                                    },1500)

                                    searchExpenseTypeRef()








                                }
                            }).done(function (){
                                $('.dv-background').hide();
                            });
                        }else{
                            $('#warningModal .modal-body').html("")
                            $('#warningModal .modal-body').append($MESSAGE_DUPLICATE_COMPLETE_REASON_CODE+"<br/>");
                            $('#warningModal').modal('show');
                        }




                    }



                }else{

                    $('#warningModal .modal-body').html("")
                    $('#warningModal .modal-body').append($MESSAGE_REQUIRE_FIELD+"<br/>");
                    $('#warningModal .modal-body').append("      - "+$LABEL_APPROVE_TYPE_CODE);

                    $('#warningModal').modal('show');

                }
                }else{
                $('#warningModal .modal-body').html("")
                $('#warningModal .modal-body').append($MESSAGE_REQUIRE_FIELD+"<br/>");



                if(($('#add_approve_type').val()) == ""){
                    $('#warningModal .modal-body').append("      - "+$LABEL_APPROVE_REFERENCE_CODE+"<br/>");
                }


                $('#warningModal').modal('show');
            }








}
function deleteExpenseTypeRef(id) {


    console.log('Id DELETE EXPENSE REF ====> '+id)


    var jsonParams = {};
    jsonParams['parentId']     = 1;
    jsonParams[csrfParameter]  = csrfToken;
    $('.dv-background').show();
    $.ajax({
        type: "DELETE",
        url: session['context']+'/expenseReference/'+id,
        data: jsonParams,
        complete: function (xhr) {


            $('#deleteSuccess').modal('show')
            setTimeout(function () {
                $('#deleteSuccess').modal('hide')
            },1500)




            searchExpenseTypeRef();
            $('#deleteExpenseTypeRef').modal('hide');

        }
    }).done(function (){
        $('.dv-background').hide();
    });

}
function editExpenseRef(id) {
            console.log('In Function EditExpenseRef   ID = '+id)
  $('#addApproveRef').modal('show')


    $.ajax({
        type: "GET",
        url: session['context']+'/expenseReference/'+id,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);



                        console.log('id ref ===>'+obj.id)

                    $('#add_approve_id').val(obj.id);
                    $('#add_approve_createdBy').val(obj.createdBy);
                    $('#add_approve_createdDate').val(obj.createdDate);
                    $('#add_approve_updateBy').val(obj.updateBy);
                    $('#add_approve_updateDate').val(obj.updateDate);
                    $('#add_approve_type').val(obj.approveTypeCode);
                    $('#add_approve_ref').val(obj.completeReason);

                    checkDataRef = obj.completeReason





                }
            }
        }
    }).done(function (){
        //close loader
    });

}
// ==================================================== Expense Type Reference ===================================================








// ==================================================== Expense Type Company ===================================================

function saveExpenseCompany() {
console.log(' In Function Save ExpenseCompany ')


    var jsonExpenseTypeReference = {};

    jsonExpenseTypeReference['parentId']     	           = 1;
    jsonExpenseTypeReference[csrfParameter]  	           = csrfToken;
    jsonExpenseTypeReference['id']     		               = $('#add_company_id').val();
    jsonExpenseTypeReference['glCode']     		           = ($('#add_company_glCode').val()).trim();
    jsonExpenseTypeReference['pa']     		               = ($('#add_company_pa').val()).trim();
    jsonExpenseTypeReference['psa']     		           = ($('#add_company_psa').val()).trim();
    jsonExpenseTypeReference['company']      	           = ''
    jsonExpenseTypeReference['expenseType']      	       = expenseTypeId;



    console.log('   ID        =====> '+jsonExpenseTypeReference.id)
    console.log('   GL CODE   =====> '+jsonExpenseTypeReference.glCode)
    console.log('   Company   =====> '+jsonExpenseTypeReference.company)
    console.log('   ExpenseType   =====> '+jsonExpenseTypeReference.expenseType)


    console.log(' Object Before Save ID = [ '+jsonExpenseTypeReference.id+' ]')



    if(   ($('#add_company_pa').val()).trim() != "" && ($('#add_company_psa').val()).trim() != "" && ($('#add_company_glCode').val()).trim() != ""  ){


        findExpenseTypeByIdAndExpenseTypeCompanyPaAndExpenseTypeCompanyPsaAndExpenseTypeCompanyGlCode(jsonExpenseTypeReference.pa,jsonExpenseTypeReference.psa,jsonExpenseTypeReference.glCode,expenseTypeId)


                if(checkDupExpenseTypeCompany ==false){
                    $('.dv-background').show();
                    var itemData = $.ajax({
                        type: "POST",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context']+'/expenseType/saveExpenseTypeCompany',
                        data: jsonExpenseTypeReference,
                        complete: function (xhr) {


                            $('#add_company_id').val("")
                            $('#addGLCompany').modal('hide');


                            $('#saveSuccess').modal('show');
                            setTimeout(function () {
                                $('#saveSuccess').modal('hide');
                            },1500)

                            searchExpenseTypeCompany()



                        }
                    }).done(function (){
                        $('.dv-background').hide();
                    });
                }else{
                    console.log('compare pa ==> '+checkDataCom_pa+' : '+jsonExpenseTypeReference.pa)
                    console.log('compare pa ==> '+checkDataCom_psa+' : '+jsonExpenseTypeReference.psa)
                    console.log('compare pa ==> '+checkDataCom_gl+' : '+jsonExpenseTypeReference.glCode)

                    if(checkDataCom_pa == jsonExpenseTypeReference.pa && checkDataCom_psa == jsonExpenseTypeReference.psa && checkDataCom_gl == jsonExpenseTypeReference.glCode   ){
                        $('.dv-background').show();
                        var itemData = $.ajax({
                            type: "POST",
                            headers: {
                                Accept: 'application/json'
                            },
                            url: session['context']+'/expenseType/saveExpenseTypeCompany',
                            data: jsonExpenseTypeReference,
                            complete: function (xhr) {


                                $('#add_company_id').val("")
                                $('#addGLCompany').modal('hide');


                                $('#saveSuccess').modal('show');
                                setTimeout(function () {
                                    $('#saveSuccess').modal('hide');
                                },1500)

                                searchExpenseTypeCompany()



                            }
                        }).done(function (){
                            $('.dv-background').hide();
                        });
                    }else{

                        $('#warningModal .modal-body').html("")
                        $('#warningModal .modal-body').append($MESSAGE_DUPLICATE_GROUP_CODE_COMPANY+"<br/>");
                        $('#warningModal').modal('show');
                    }



                }



    }else{
        $('#warningModal .modal-body').html("")
        $('#warningModal .modal-body').append($MESSAGE_REQUIRE_FIELD+"<br/>");


        if(($('#add_company_pa').val()).trim() == ""){
            $('#warningModal .modal-body').append("      - "+$LABEL_PERSONAL_AREA+"<br/>");

        }
        if(($('#add_company_psa').val()).trim() == ""){
            $('#warningModal .modal-body').append("      - "+$LABEL_PERSONAL_SUB_AREA+"<br/>");
        }

        if(($('#add_company_glCode').val()).trim() == ""){
            $('#warningModal .modal-body').append("      - "+$LABEL_GL_CODE+"<br/>");
        }
        $('#warningModal').modal('show');
    }






}
function searchExpenseTypeCompany(){
    console.log('Find ExpenseTypeRef by id Main = '+expenseTypeId)
    var criteriaObject = {
        expenseType        : expenseTypeId
    };

    // console.log("DATA FOR SEARCR =====> "+criteriaObject.description)
    queryExpenseTypeCompanyByCriteria(criteriaObject);

}
function queryExpenseTypeCompanyByCriteria(criteriaObject){
    object_2.setId("#pagingSearchExpenseTypeCompanyBody");
    object_2.setUrlData("/expenseByCompanies/findByCriteria");
    object_2.setUrlSize("/expenseByCompanies/findSize");


    object_2.loadTable = function(items){
        var item = items.content;
        // $('.dv-background').show();
        $('#expenseTypeCompanyBody').empty();
        if(item.length > 0){
            $dataForQuery = item;
            var itemIdTmp ;


            for(var j=0;j<item.length;j++){

                var glCode    = item[j].glCode==null?"":item[j].glCode;
                // var company    = item[j].companys==null?"":item[j].companys.code;
                var pa    = item[j].pa==null?"":item[j].pa;
                var psa    = item[j].psa==null?"":item[j].psa;




                itemIdTmp = item[j].id;
                if(itemIdTmp){
                    $('#expenseTypeCompanyBody').append(''+
                        '<tr id="'+item[j].id+'" '+
                        ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
                        '<td class="text-center">'+pa+'</td>'+
                        '<td class="text-center">'+psa+'</td>'+
                        '<td class="text-center">'+glCode+'</td>'+
                        // '<td class="text-center">'+company+'</td>'+
                        '<td class="text-center">'+
                        '	<button type="button" class="btn btn-material-red-500  btn-style-small"  title="Delete"  onclick="$(\'#idItemDeleteExpenseCompany\').val(\''+item[j].id+'\')" data-target="#deleteExpenseTypeCompany" data-toggle="modal""><span class="fa fa-trash"><jsp:text/></span></button>'+
                        '	<button type="button" class="btn btn-material-cyan-500  btn-style-small"  title="Edit" onclick="editExpenseTypeCompany(\''+item[j].id+'\')" ><span class="fa fa-pencil-square-o"><jsp:text/></span></button>'+
                        // '	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Up" onclick="" style="border-radius: 30px;width: 30px;padding-left: 8px;padding-top: 4px;" ><span class="fa fa-arrow-up"><jsp:text/></span></button>'+
                        // '	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Down" onclick=""  style="border-radius: 30px;width: 30px;padding-left: 8px;padding-top: 4px;"  ><span class="fa fa-arrow-down"><jsp:text/></span></button>'+

                        '</td>'+
                        '</tr>'
                    );
                }

            }


        }else{
            //Not Found
        }

    };

    object_2.setDataSearch(criteriaObject);
    object_2.search(object_2);
    object_2.loadPage(($CRITERIA_PAGE_2*1)+1,object_2);
}
function deleteExpenseCompny(id) {




    console.log('Id Delete  Expense Type =======> '+id)


    var jsonParams = {};
    jsonParams['parentId']     = 1;
    jsonParams[csrfParameter]  = csrfToken;
    $('.dv-background').show();
    $.ajax({
        type: "DELETE",
        url: session['context']+'/expenseByCompanies/'+id,
        data: jsonParams,
        complete: function (xhr) {


            $('#deleteSuccess').modal('show')
            setTimeout(function () {
                $('#deleteSuccess').modal('hide')
            },1500)




            searchExpenseTypeCompany();
            $('#deleteExpenseTypeCompany').modal('hide');

        }
    }).done(function (){
        $('.dv-background').hide();
    });

}
function editExpenseTypeCompany(id) {
    console.log('In Function EditExpenseRef   ID = '+id)
    $('#addGLCompany').modal('show')


    $.ajax({
        type: "GET",
        url: session['context']+'/expenseByCompanies/'+id,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);



                    console.log('id ref ===>'+obj.id)

                    $('#add_company_id').val(obj.id);
                    $('#add_company_createdBy').val(obj.createdBy);
                    $('#add_company_createdDate').val(obj.createdDate);
                    $('#add_company_updateBy').val(obj.updateBy);
                    $('#add_company_updateDate').val(obj.updateDate);
                    $('#add_company_glCode').val(obj.glCode);
                    $('#add_company_pa').val(obj.pa);
                    $('#add_company_psa').val(obj.psa);


                    checkDataCom_pa = obj.pa
                    checkDataCom_psa = obj.psa
                    checkDataCom_gl = obj.glCode






                }
            }
        }
    }).done(function (){
        //close loader
    });

}

// ==================================================== Expense Type Company ===================================================







// ==================================================== Reimburse  ===================================================

function saveReimburse() {
    console.log(' In Function Save ExpenseCompany ')


    var jsonExpenseTypeReference = {};

    jsonExpenseTypeReference['parentId']     	           = 1;
    jsonExpenseTypeReference[csrfParameter]  	           = csrfToken;
    jsonExpenseTypeReference['id']     		               = $('#add_reimburse_id').val();
    jsonExpenseTypeReference['roleCode']     		       = $('#add_reimburse_role_name').val();
    jsonExpenseTypeReference['expenseType']      	       = expenseTypeId;



    console.log('   ID   =====> '+jsonExpenseTypeReference.id)
    console.log('   ExpenseType   =====> '+jsonExpenseTypeReference.expenseType)


    console.log(' Object Before Save ID = [ '+jsonExpenseTypeReference.id+' ]')



    findExpenseTypeByIdAndReimburseRoleCode(jsonExpenseTypeReference.roleCode,expenseTypeId)


                    if($('#add_reimburse_role_name').val() != "" ){

                        if(checkDupExpenseTypeReimburse ==false){
                            $('.dv-background').show();
                            var itemData = $.ajax({
                                type: "POST",
                                headers: {
                                    Accept: 'application/json'
                                },
                                url: session['context']+'/expenseType/saveReimburse',
                                data: jsonExpenseTypeReference,
                                complete: function (xhr) {


                                    $('#add_reimburse_id').val("")
                                    $('#addUserBudget').modal('hide');


                                    $('#saveSuccess').modal('show');
                                    setTimeout(function () {
                                        $('#saveSuccess').modal('hide');
                                    },1500)

                                    searchReimburse()



                                }
                            }).done(function (){
                                $('.dv-background').hide();
                            });
                        }else{

                            if(checkDataReiumbure == jsonExpenseTypeReference.roleCode){
                                $('.dv-background').show();
                                var itemData = $.ajax({
                                    type: "POST",
                                    headers: {
                                        Accept: 'application/json'
                                    },
                                    url: session['context']+'/expenseType/saveReimburse',
                                    data: jsonExpenseTypeReference,
                                    complete: function (xhr) {


                                        $('#add_reimburse_id').val("")
                                        $('#addUserBudget').modal('hide');


                                        $('#saveSuccess').modal('show');
                                        setTimeout(function () {
                                            $('#saveSuccess').modal('hide');
                                        },1500)

                                        searchReimburse()



                                    }
                                }).done(function (){
                                    $('.dv-background').hide();
                                });
                            }else{
                                $('#warningModal .modal-body').html("")
                                $('#warningModal .modal-body').append($MESSAGE_DUPLICATE_ROLE_CODE+"<br/>");
                                $('#warningModal').modal('show');
                            }


                        }
                    }else{
                        $('#warningModal .modal-body').html("")
                        $('#warningModal .modal-body').append($MESSAGE_REQUIRE_FIELD+"<br/>");

                        if($('#add_reimburse_role_name').val() == ""){
                            $('#warningModal .modal-body').append("      - "+$LABEL_USER_AUTHORIZATION_DATA+"<br/>");
                        }
                        $('#warningModal').modal('show');
                    }








}
function searchReimburse(){
    console.log('Find ExpenseTypeRef by id Main = '+expenseTypeId)
    var criteriaObject = {
        expenseType        : expenseTypeId
    };

    // console.log("DATA FOR SEARCR =====> "+criteriaObject.description)
    queryReimburseByCriteria(criteriaObject);

}
function queryReimburseByCriteria(criteriaObject){
    object_3.setId("#pagingSearchReimburseBody");
    object_3.setUrlData("/reimburse/findByCriteria");
    object_3.setUrlSize("/reimburse/findSize");


    object_3.loadTable = function(items){
        var item = items.content;
        // $('.dv-background').show();
        $('#reimburseBody').empty();
        if(item.length > 0){
            $dataForQuery = item;
            var itemIdTmp ;


            for(var j=0;j<item.length;j++){

                var roleCode    = item[j].roleCode==null?"":item[j].roleCode;


                itemIdTmp = item[j].id;
                if(itemIdTmp){
                    $('#reimburseBody').append(''+
                        '<tr id="'+item[j].id+'" '+
                        ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
                        '<td class="text-center">'+roleCode+'</td>'+
                        '<td class="text-center">'+
                        '	<button type="button" class="btn btn-material-red-500  btn-style-small"  title="Delete"  onclick="$(\'#idItemDeleteReimburse\').val(\''+item[j].id+'\')" data-target="#deleteReimburse" data-toggle="modal""><span class="fa fa-trash"><jsp:text/></span></button>'+
                        '	<button type="button" class="btn btn-material-cyan-500  btn-style-small"  title="Edit" onclick="editReimburse(\''+item[j].id+'\')" ><span class="fa fa-pencil-square-o"><jsp:text/></span></button>'+
                        // '	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Up" onclick="" style="border-radius: 30px;width: 30px;padding-left: 8px;padding-top: 4px;" ><span class="fa fa-arrow-up"><jsp:text/></span></button>'+
                        // '	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Down" onclick=""  style="border-radius: 30px;width: 30px;padding-left: 8px;padding-top: 4px;"  ><span class="fa fa-arrow-down"><jsp:text/></span></button>'+

                        '</td>'+
                        '</tr>'
                    );
                }

            }


        }else{
            //Not Found
        }

    };

    object_3.setDataSearch(criteriaObject);
    object_3.search(object_3);
    object_3.loadPage(($CRITERIA_PAGE_3*1)+1,object_3);
}
function deleteReimburse(id) {




    console.log('Id Delete  Expense Type =======> '+id)


    var jsonParams = {};
    jsonParams['parentId']     = 1;
    jsonParams[csrfParameter]  = csrfToken;
    $('.dv-background').show();
    $.ajax({
        type: "DELETE",
        url: session['context']+'/reimburse/'+id,
        data: jsonParams,
        complete: function (xhr) {


            $('#deleteSuccess').modal('show')
            setTimeout(function () {
                $('#deleteSuccess').modal('hide')
            },1500)




            searchReimburse();
            $('#deleteReimburse').modal('hide');

        }
    }).done(function (){
        $('.dv-background').hide();
    });

}
function editReimburse(id) {
    console.log('In Function EditExpenseRef   ID = '+id)
    $('#addUserBudget').modal('show')


    $.ajax({
        type: "GET",
        url: session['context']+'/reimburse/'+id,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);



                    console.log('id ref ===>'+obj.id)

                    $('#add_reimburse_id').val(obj.id);
                    $('#add_reimburse_createdBy').val(obj.createdBy);
                    $('#add_reimburse_createdDate').val(obj.createdDate);
                    $('#add_reimburse_updateBy').val(obj.updateBy);
                    $('#add_reimburse_updateDate').val(obj.updateDate);
                    $('#add_reimburse_role_name').val(obj.roleCode);
                    checkDataReiumbure = obj.roleCode



                }
            }
        }
    }).done(function (){
        //close loader
    });

}



// ==================================================== Reimburse  ===================================================




// ==================================================== Expense Type Screen  ===================================================

function saveExpenseTypeScreen() {
    console.log(' In Function Save ExpenseCompany ')

    var isRequireScreen =      $("#add_screen_require").prop('checked') ? true : false

    var jsonExpenseTypeReference = {};

    jsonExpenseTypeReference['parentId']     	           = 1;
    jsonExpenseTypeReference[csrfParameter]  	           = csrfToken;
    jsonExpenseTypeReference['id']     		               = $('#add_screen_id').val();
    jsonExpenseTypeReference['nameTH']     		           = ($('#add_screen_thName').val()).trim();
    jsonExpenseTypeReference['nameENG']     		       = ($('#add_screen_engName').val()).trim();
    jsonExpenseTypeReference['type']     		           = $('#add_screen_type').val();
    jsonExpenseTypeReference['structureField']     		   = $('#add_screen_structure_field').val();
    jsonExpenseTypeReference['require']     		       = isRequireScreen
    jsonExpenseTypeReference['expenseType']      	       = expenseTypeId;



    console.log('   ID   =====> '+jsonExpenseTypeReference.id)
    console.log('   ExpenseType   =====> '+jsonExpenseTypeReference.expenseType)


    console.log(' Object Before Save ID = [ '+jsonExpenseTypeReference.id+' ]')



    if(  ($('#add_screen_thName').val()).trim() != "" && ($('#add_screen_engName').val()).trim() != "" && $('#add_screen_type').val() != "" && $('#add_screen_structure_field').val() != ""   ){

        findExpenseTypeByExpenseTypeScreenStructureField(jsonExpenseTypeReference.structureField,expenseTypeId)

        if(checkDupExpenseTypeScreen != true){
            $('.dv-background').show();
            var itemData = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context']+'/expenseType/saveExpenseTypeScreen',
                data: jsonExpenseTypeReference,
                complete: function (xhr) {


                    $('#add_screen_id').val("")
                    $('#addScreenFixDetail').modal('hide');


                    $('#saveSuccess').modal('show');
                    setTimeout(function () {
                        $('#saveSuccess').modal('hide');
                    },1500)

                    searchExpenseTypeScree()



                }
            }).done(function (){
                $('.dv-background').hide();
            });
        }else{
            if(checkDataScreen == jsonExpenseTypeReference.structureField ){
                $('.dv-background').show();
                var itemData = $.ajax({
                    type: "POST",
                    headers: {
                        Accept: 'application/json'
                    },
                    url: session['context']+'/expenseType/saveExpenseTypeScreen',
                    data: jsonExpenseTypeReference,
                    complete: function (xhr) {


                        $('#add_screen_id').val("")
                        $('#addScreenFixDetail').modal('hide');


                        $('#saveSuccess').modal('show');
                        setTimeout(function () {
                            $('#saveSuccess').modal('hide');
                        },1500)

                        searchExpenseTypeScree()



                    }
                }).done(function (){
                    $('.dv-background').hide();
                });
            }else{

                $('#warningModal .modal-body').html("")
                $('#warningModal .modal-body').append($MESSAGE_DUPLICATE_STRUCTURE_FIELD+"<br/>");
                $('#warningModal').modal('show');
            }





        }



    }else{


        $('#warningModal .modal-body').html("")
        $('#warningModal .modal-body').append($MESSAGE_REQUIRE_FIELD+"<br/>");


        if(($('#add_screen_thName').val()).trim() == ""){
            $('#warningModal .modal-body').append("      - "+$LABEL_TH_NAME+"<br/>");

        }
        if(($('#add_screen_engName').val()).trim() == ""){
            $('#warningModal .modal-body').append("      - "+$LABEL_ENG_NAME+"<br/>");
        }
        if($('#add_screen_type').val() == ""){
            $('#warningModal .modal-body').append("      - "+$LABEL_DATA_TYPE+"<br/>");
        }
        if($('#add_screen_structure_field').val() == ""){
            $('#warningModal .modal-body').append("      - "+$LABEL_STRUCTURE_FIELD+"<br/>");
        }
        $('#warningModal').modal('show');

    }






}
function searchExpenseTypeScree(){
    console.log('Find ExpenseTypeRef by id Main = '+expenseTypeId)
    var criteriaObject = {
        expenseType        : expenseTypeId
    };

    // console.log("DATA FOR SEARCR =====> "+criteriaObject.description)
    queryExpenseTypeScreenByCriteria(criteriaObject);

}
function queryExpenseTypeScreenByCriteria(criteriaObject){
    object_4.setId("#pagingSearchExpenseTypeScreenBody");
    object_4.setUrlData("/expenseTypeScreen/findByCriteria");
    object_4.setUrlSize("/expenseTypeScreen/findSize");


    object_4.loadTable = function(items){
        var item = items.content;
        // $('.dv-background').show();
        $('#expenseTypeScreenBody').empty();
        if(item.length > 0){
            $dataForQuery = item;
            var itemIdTmp ;


            for(var j=0;j<item.length;j++){

                var enName    = item[j].nameENG==null?"":item[j].nameENG;
                var type    = item[j].type==null?"":item[j].type;
                var require    = item[j].require==null?"":item[j].require;


                itemIdTmp = item[j].id;
                if(itemIdTmp){
                    $('#expenseTypeScreenBody').append(''+
                        '<tr id="'+item[j].id+'" '+
                        ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
                        '<td class="text-center">'+enName+'</td>'+
                        '<td class="text-center">'+type+'</td>'+
                        '<td class="text-center" style="padding-top: 5px; padding-bottom: 0px;">' + ((require===true) ? '<img  class="img-circle" src="' + $IMAGE_CHECKED + '" style="width: 30px;height: 30px;" />' : '<img class="img-circle" src="' + $IMAGE_CANCEL + '" style="width: 30px;height: 30px;" />') + '</td>' +

                        '<td class="text-center">'+
                        '	<button type="button" class="btn btn-material-red-500  btn-style-small"  title="Delete"  onclick="$(\'#idItemDeleteExpenseTypeScreen\').val(\''+item[j].id+'\')" data-target="#deleteExpenseTypeScreen" data-toggle="modal""><span class="fa fa-trash"><jsp:text/></span></button>'+
                        '	<button type="button" class="btn btn-material-cyan-500  btn-style-small"  title="Edit" onclick="editExpenseTypeScreen(\''+item[j].id+'\')" ><span class="fa fa-pencil-square-o"><jsp:text/></span></button>'+
                        // '	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Up" onclick="" style="border-radius: 30px;width: 30px;padding-left: 8px;padding-top: 4px;" ><span class="fa fa-arrow-up"><jsp:text/></span></button>'+
                        // '	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Down" onclick=""  style="border-radius: 30px;width: 30px;padding-left: 8px;padding-top: 4px;"  ><span class="fa fa-arrow-down"><jsp:text/></span></button>'+

                        '</td>'+
                        '</tr>'
                    );
                }

            }


        }else{
            //Not Found
        }

    };

    object_4.setDataSearch(criteriaObject);
    object_4.search(object_4);
    object_4.loadPage(($CRITERIA_PAGE_4*1)+1,object_4);
}
function deleteExpenseTypeScreen(id) {




    console.log('Id Delete  Expense Type =======> '+id)


    var jsonParams = {};
    jsonParams['parentId']     = 1;
    jsonParams[csrfParameter]  = csrfToken;
    $('.dv-background').show();
    $.ajax({
        type: "DELETE",
        url: session['context']+'/expenseTypeScreen/'+id,
        data: jsonParams,
        complete: function (xhr) {


            $('#deleteSuccess').modal('show')
            setTimeout(function () {
                $('#deleteSuccess').modal('hide')
            },1500)




            searchExpenseTypeScree();
            $('#deleteExpenseTypeScreen').modal('hide');

        }
    }).done(function (){
        $('.dv-background').hide();
    });

}
function editExpenseTypeScreen(id) {
    console.log('In Function EditExpenseTypeScreen ID = '+id)
    $('#addScreenFixDetail').modal('show')


    $.ajax({
        type: "GET",
        url: session['context']+'/expenseTypeScreen/'+id,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);



                    console.log('id ref ===>'+obj.id)

                    $('#add_screen_id').val(obj.id);
                    $('#add_screen_createdBy').val(obj.createdBy);
                    $('#add_screen_createdDate').val(obj.createdDate);
                    $('#add_screen_updateBy').val(obj.updateBy);
                    $('#add_screen_updateDate').val(obj.updateDate);
                    $('#add_screen_engName').val(obj.nameENG);
                    $('#add_screen_thName').val(obj.nameTH);
                    $('#add_screen_structure_field').val(obj.structureField);
                    $('#add_screen_type').val(obj.type);


                    checkDataScreen = obj.structureField


                    if(obj.require ==true){
                        $('#add_screen_require').prop('checked',true)
                    }else{
                        $('#add_screen_require').prop('checked',false)
                    }



                }
            }
        }
    }).done(function (){
        //close loader
    });

}


// ==================================================== Expense Type Screen  ===================================================



// ==================================================== Expense Type File  ===================================================


function saveExpenseTypeFile() {
    console.log(' In Function Save ExpenseFile ')

    var isRequireFile =      $("#add_file_require").prop('checked') ? true : false

    var jsonExpenseTypeReference = {};

    jsonExpenseTypeReference['parentId']     	           = 1;
    jsonExpenseTypeReference[csrfParameter]  	           = csrfToken;
    jsonExpenseTypeReference['id']     		               = $('#add_file_id').val();
    jsonExpenseTypeReference['attachmentTypeCode']     	   = $('#add_file_code').val();
    jsonExpenseTypeReference['require']     		       = isRequireFile
    jsonExpenseTypeReference['expenseType']      	       = expenseTypeId;



    console.log('   ID   =====> '+jsonExpenseTypeReference.id)
    console.log('   ExpenseType   =====> '+jsonExpenseTypeReference.expenseType)


    console.log(' Object Before Save ID = [ '+jsonExpenseTypeReference.id+' ]')



    if( $('#add_file_code').val() != "" && $('#add_file_code').val() != null  ){


        findByAttachmentTypeCode(jsonExpenseTypeReference.attachmentTypeCode,expenseTypeId)
                console.log(' CHECK ID BEFORE SAVE ==> '+checkDupExpenseTypeFile_id+' : '+jsonExpenseTypeReference.id)



                if(  checkDupExpenseTypeFile == false ){
                    $('.dv-background').show();
                    var itemData = $.ajax({
                        type: "POST",
                        headers: {
                            Accept: 'application/json'
                        },
                        url: session['context']+'/expenseType/saveExpenseTypeFile',
                        data: jsonExpenseTypeReference,
                        complete: function (xhr) {


                            $('#add_file_id').val("")
                            $('#addFileAttractDetail').modal('hide');


                            $('#saveSuccess').modal('show');
                            setTimeout(function () {
                                $('#saveSuccess').modal('hide');
                            },1500)

                            searchExpenseTypeFile()



                        }
                    }).done(function (){
                        $('.dv-background').hide();
                    });
                }else{
                        // console.log('check data ====> '+checkDataFile+' : '+jsonExpenseTypeReference.attachmentTypeCode)
                    if(checkDataFile == jsonExpenseTypeReference.attachmentTypeCode ){
                        $('.dv-background').show();
                        var itemData = $.ajax({
                            type: "POST",
                            headers: {
                                Accept: 'application/json'
                            },
                            url: session['context']+'/expenseType/saveExpenseTypeFile',
                            data: jsonExpenseTypeReference,
                            complete: function (xhr) {


                                $('#add_file_id').val("")
                                $('#addFileAttractDetail').modal('hide');


                                $('#saveSuccess').modal('show');
                                setTimeout(function () {
                                    $('#saveSuccess').modal('hide');
                                },1500)

                                searchExpenseTypeFile()



                            }
                        }).done(function (){
                            $('.dv-background').hide();
                        });
                    }else{

                        $('#warningModal .modal-body').html("")
                        $('#warningModal .modal-body').append($MESSAGE_DUPLICATE_ATTCHMENT_TYPE_CODE+"<br/>");
                        $('#warningModal').modal('show');

                    }

                }


    }else{

        $('#warningModal .modal-body').html("")
        $('#warningModal .modal-body').append($MESSAGE_REQUIRE_FIELD+"<br/>");




        $('#warningModal .modal-body').append("      - "+$LABEL_APPROVE_TYPE_CODE+"<br/>");

        $('#warningModal').modal('show');

    }





}
function searchExpenseTypeFile(){
    console.log('Find ExpenseTypeRef by id Main = '+expenseTypeId)
    var criteriaObject = {
        expenseType        : expenseTypeId
    };

    // console.log("DATA FOR SEARCR =====> "+criteriaObject.description)
    queryExpenseTypeFileByCriteria(criteriaObject);

}
function queryExpenseTypeFileByCriteria(criteriaObject){
    object_5.setId("#pagingSearchExpenseFileBody");
    object_5.setUrlData("/expenseTypeFile/findByCriteria");
    object_5.setUrlSize("/expenseTypeFile/findSize");


    object_5.loadTable = function(items){
        var item = items.content;
        // $('.dv-background').show();
        $('#expenseTypeFileBody').empty();
        if(item.length > 0){
            $dataForQuery = item;
            var itemIdTmp ;


            for(var j=0;j<item.length;j++){

                var attachmentTypeCode    = item[j].attachmentTypeCode==null?"":item[j].attachmentTypeCode;
                var require    = item[j].require==null?"":item[j].require;

                if(attachmentTypeCode != ""){
                    findMasterdataDetailAttchmentTyoeByCode(attachmentTypeCode)

                    var attachmentTypeCode = masterDataDetailAttchmentType.description
                }


                itemIdTmp = item[j].id;
                if(itemIdTmp){
                    $('#expenseTypeFileBody').append(''+
                        '<tr id="'+item[j].id+'" '+
                        ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
                        '<td class="text-center">'+attachmentTypeCode+'</td>'+
                        '<td class="text-center" style="padding-top: 5px; padding-bottom: 0px;">' + ((require===true) ? '<img  class="img-circle" src="' + $IMAGE_CHECKED + '" style="width: 30px;height: 30px;" />' : '<img class="img-circle" src="' + $IMAGE_CANCEL + '" style="width: 30px;height: 30px;" />') + '</td>' +
                        '<td class="text-center">'+
                        '	<button type="button" class="btn btn-material-red-500  btn-style-small"  title="Delete"  onclick="$(\'#idItemDeleteExpenseTypeFile\').val(\''+item[j].id+'\')" data-target="#deleteExpenseTypeFile" data-toggle="modal""><span class="fa fa-trash"><jsp:text/></span></button>'+
                        '	<button type="button" class="btn btn-material-cyan-500  btn-style-small"  title="Edit" onclick="editExpenseTypeFile(\''+item[j].id+'\')" ><span class="fa fa-pencil-square-o"><jsp:text/></span></button>'+
                        // '	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Up" onclick="" style="border-radius: 30px;width: 30px;padding-left: 8px;padding-top: 4px;" ><span class="fa fa-arrow-up"><jsp:text/></span></button>'+
                        // '	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Down" onclick=""  style="border-radius: 30px;width: 30px;padding-left: 8px;padding-top: 4px;"  ><span class="fa fa-arrow-down"><jsp:text/></span></button>'+

                        '</td>'+
                        '</tr>'
                    );
                }

            }


        }else{
            //Not Found
        }

    };

    object_5.setDataSearch(criteriaObject);
    object_5.search(object_5);
    object_5.loadPage(($CRITERIA_PAGE_5*1)+1,object_5);
}
function deleteExpenseTypeFile(id) {




    console.log('Id Delete  Expense Type =======> '+id)


    var jsonParams = {};
    jsonParams['parentId']     = 1;
    jsonParams[csrfParameter]  = csrfToken;

    $('.dv-background').show();
    $.ajax({
        type: "DELETE",
        url: session['context']+'/expenseTypeFile/'+id,
        data: jsonParams,
        complete: function (xhr) {


            $('#deleteSuccess').modal('show')
            setTimeout(function () {
                $('#deleteSuccess').modal('hide')
            },1500)




            searchExpenseTypeFile();
            $('#deleteExpenseTypeFile').modal('hide');

        }
    }).done(function (){
        $('.dv-background').hide();
    });

}
function editExpenseTypeFile(id) {


    console.log('In Function EditExpenseTypeScreen ID = '+id)
    $('#addFileAttractDetail').modal('show')


    $.ajax({
        type: "GET",
        url: session['context']+'/expenseTypeFile/'+id,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);



                    console.log('id ref ===>'+obj.id)

                    $('#add_file_id').val(obj.id);
                    $('#add_file_createdBy').val(obj.createdBy);
                    $('#add_file_createdDate').val(obj.createdDate);
                    $('#add_file_updateBy').val(obj.updateBy);
                    $('#add_file_updateDate').val(obj.updateDate);
                    $('#add_file_code').val(obj.attachmentTypeCode);


                    checkDataFile = obj.attachmentTypeCode


                    if(obj.require ==true){
                        $('#add_file_require').prop('checked',true)
                    }else{
                        $('#add_file_require').prop('checked',false)
                    }



                }
            }
        }
    }).done(function (){
        //close loader
    });

}


// ==================================================== Expense Type File  ===================================================







function updateExpenseType(){



    var code = ($('#add_expenseType_code').val()).trim()
    var jsonParams = {};

    jsonParams['parentId']     	    = 1;
    jsonParams[csrfParameter]  	    = csrfToken;
    jsonParams['id']     		    = expenseTypeId
    jsonParams['updateBy'] 		    = $('#edit_updateBy').val();
    jsonParams['updateDate'] 	    = $('#edit_updateDate').val();
    jsonParams['code']              = ($('#expenseType_code_edit').val()).trim()  ;
    jsonParams['description']       = ($('#expenseType_des_edit').val()).trim();
    jsonParams['favorite']      	= $('#add_expenseType_favorite_general').prop('checked') == true ? true : false
    jsonParams['flowType']      	= $('#edit_flowType').val()
    jsonParams['screenType']      	= $('#edit_screen_config').prop('checked') == true ? "CS" : "FC"
    jsonParams['fixCode']      	    = $('#edit_screen_fixCode_value').val()
    jsonParams['headOfficeGL']      = dataHeadOfficeGL
    jsonParams['factoryGL']         = dataFactoryGL






    if( ($('#expenseType_code_edit').val()).trim()  != "" && ($('#expenseType_des_edit').val()).trim() != ""     ) {

// console.log('After Compare '+tmpCode+' = '+jsonParams.code)

        if(tmpCode != jsonParams.code){
            findExpenseTypeByCode_Update(jsonParams.code)
        }else{
            checkDupExpenseType_Update=false
        }



        if (checkDupExpenseType_Update == false) {
            $('.dv-background').show();
            var itemData = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/expenseType',
                data: jsonParams,
                complete: function (xhr) {

                    $('#addExpenseType').modal('hide');

                    $('#saveSuccess').modal('show');
                    setTimeout(function () {
                        $('#saveSuccess').modal('hide');
                    }, 1500)


                    searchExpenseType();
                    console.log(' TR ID =============> '+expenseTypeId)


                }

            }).done(function () {
                $('.dv-background').hide();
            });

        } else {
            $('#warningModal .modal-body').html("")
            $('#warningModal .modal-body').append($MESSAGE_DUPLICATE_CODE_OF_EXPENSE_TYPE + "<br/>");
            $('#warningModal').modal('show');
        }

    }else{
        $('#warningModal .modal-body').html("")
        $('#warningModal .modal-body').append($MESSAGE_REQUIRE_FIELD+"<br/>");

        if(($('#expenseType_code_edit').val()).trim() == ""){
            $('#warningModal .modal-body').append("      - "+$LABEL_CODE+"<br/>");

        }
        if(($('#expenseType_des_edit').val()).trim() == ""){
            $('#warningModal .modal-body').append("      - "+$LABEL_DESCRIPTION+"<br/>");
        }
        $('#warningModal').modal('show');
    }












}
function findFlowType(){


    $("#edit_flowType").empty()

    $.ajax({
        type: "GET",
        url: session['context']+'/masterDatas/findByMasterdataInOrderByCode?masterdata='+MASTER_DATA.FLOW_TYPE_CODE,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);



                    var item = obj.content
                    console.log(item.length)
                    $("#add_assigner_flowType").append('<option value='+""+'>'+''+ '</option>')
                    $("#search_flowType").append('<option value='+""+'>'+''+ '</option>')


                    for(var i=0; i< item.length; i++){
                        // console.log(item[i].id)
                        $("#edit_flowType").append('<option value='+item[i].code+'>'+item[i].description+ '</option>')

                    }


                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function findImageSrc_By_FlowType(code){




    $.ajax({
        type: "GET",
        url: session['context']+'/masterDatas/findByMasterdataInAndMasterDataDetailCodeLikeOrderByCode?masterdata='+MASTER_DATA.FLOW_TYPE_CODE+'&code='+code,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        var obj = JSON.parse(xhr.responseText);



                        var item = obj


                        imgSrc = obj.variable1
                    }


                }
            }
        }
    }).done(function (){
        //close loader
    });

}


