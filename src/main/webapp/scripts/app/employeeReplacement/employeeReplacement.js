/**
 * Created by ratthanan-w on 15/9/2560.
 */

var object = $.extend({},UtilPagination);
var object_tmp = $.extend({},UtilPagination);
var assigneeObject = ''
var assigneeObject_tmp = ''
var assignerObject = ''
var expenseTypeObject = ''
var empCodeReplace = ''
var empUserReplace = ''
var checkEmpCodeAssignee = ''
var $DATA_EMPLOYEE_FOA='';
var $DATA_EMPLOYEE_FNameTH='';
var $DATA_EMPLOYEE_LNameTH=''

var $DATA_EMPLOYEE_Assigner_FOA='';
var $DATA_EMPLOYEE_Assigner_FNameTH='';
var $DATA_EMPLOYEE_Assigner_LNameTH='';
var checkDupAssigner
var checkDupAssignee
var checkDupAssigner_edit
var keepNameUserAgree
var keepCodeUserAgree
var checkDuplicate_EmpCodeAssignee
var lastRow=0
var checkUpdate =0
$(document).ready(function () {










    initUtilAssignee()
    initUtilAssigner()






    $('#addModalAssignee').on('click',function () {


        initUtilAssignee()
        $('#add_assignee_codeInputEmployeeAll').val('')
        $('#add_assignee_id').val('')

    })






    $('#addUserAgree').on('click',function () {
        initUtilAssigner()
        $('#add_assigner_codeInputEmployeeAll_tmp').val('')
        $('#add_assigner_flowType').val('')
        $('#add_assigner_amount').val('')
        $('#startDateEndDate').val('')
        $('#add_assigner_id').val('')

        checkUpdate++;

    })


    searchUserReplacement()

    $('#search_button_userReplace').on('click',function () {
        searchUserReplacement()


    })

    $('#search_userAgree').on('click',function () {
        searchUserAgree()

    })


});




function searchUserReplacement(){

    var criteriaObject = {
        empCodeReplace        :  $('#search_userReplace').val() == "" ? "" : $('#search_userReplace').val()

    };

    console.log("DATA FOR SEARCR =====> "+criteriaObject.empCodeReplace)
    queryUserReplacementByCriteria(criteriaObject);

}
function queryUserReplacementByCriteria(criteriaObject){
    object.setId("#pagingSearchReplaceUserBody");
    object.setUrlData("/employeeReplacement/findByCriteria");
    object.setUrlSize("/employeeReplacement/findSize");




    object.loadTable = function(items){
        var item = items.content;
        // $('.dv-background').show();


        $('#employeeReplaceBody').empty();
        if(item.length > 0){
            $dataForQuery = item;
            var itemIdTmp ;

            var checkDupAssignee = []

            console.log('SIZE ASSIGNEE =====>'+item.length)

            var checkSize = item.length;
            var checkDupCode = '';
            var itemArray = []



            lastRow=0

            for(var j=0;j<item.length;j++){

                console.log("Query Item empCodeAssignee ===> "+item[j].empCodeReplace)
                console.log("Query Item empUserNameAssignee ===> "+item[j].empUserNameAssignee)

                empCodeReplace = item[j].empCodeReplace == null ? "" : item[j].empCodeReplace
                empUserNameReplace = item[j].empUserNameReplace == null ? "" : item[j].empUserNameReplace

                if(empCodeReplace != "" && empCodeReplace != null){
                    findEmployeeReplacement_By_EmpCodeReplace(empCodeReplace)
                    findEmployeeProfileByUserName(assigneeObject_tmp.empUserNameReplace)
                }





                $('#employeeReplaceBody').append(''+
                    '<tr id="'+empCodeReplace+'" '+
                    ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
                    '<td class="text-center">'+'<image class="img-circle" width="45px;" src="'+$IMG_ASSIGNEE+'"  />'+'</td>'+
                    '<td class="text-center" style="padding-top: 25px; color: #8bd5d2;  font-weight: 800">'+empCodeReplace+'</td>'+
                    '<td class="text-center" style="padding-top: 25px;">'+$DATA_EMPLOYEE_FOA+' '+$DATA_EMPLOYEE_FNameTH+' '+$DATA_EMPLOYEE_LNameTH+'</td>'+

                    '<td class="text-center"  style="padding-top: 15px;">'+

                    '	<a onclick="$(\'#idItemDelete\').val(\''+item[j].empCodeReplace+'\')" data-target="#deleteExpenseType" data-toggle="modal" "><span><image style="width: 35px; height: 35px; border-bottom-left-radius: 40%" src="'+$IMG_DELETE_ITEM+'"  /></span></a>'+
                    '	<a   onclick="showDetail(\''+empCodeReplace+'\',\''+item[j].empCodeReplace+'\',\''+assigneeObject_tmp.empUserNameReplace+'\')"><span><image style="width: 35px; height: 35px; border-bottom-left-radius: 40%" src="'+$IMG_DETAIL_ITEM+'"  /></span></a>'+

                    '</td>'+
                    '</tr>'
                );



                lastRow = j

            }


        }else{


        }

    };

    object.setDataSearch(criteriaObject);
    object.search(object);
    object.loadPage(($CRITERIA_PAGE*1)+1,object);


   var checkZero = $('#pagingSearchReplaceUserBodyToDisplayRecords').text()

    if(checkZero == '0'){
       $('#pagingSearchReplaceUserBodyStartDisplayRecords').empty()
       $('#pagingSearchReplaceUserBodyStartDisplayRecords').append('0')
       $('#pagingSearchReplaceUserBodyDisplayTotalRecord').empty()
       $('#pagingSearchReplaceUserBodyDisplayTotalRecord').append('0')
    }else{
        var totalResult =$('#pagingSearchReplaceUserBodyToDisplayRecords').text()
        $('#pagingSearchReplaceUserBodyDisplayTotalRecord').empty()
        $('#pagingSearchReplaceUserBodyDisplayTotalRecord').append(totalResult)
    }

    $DATA_EMPLOYEE_FOA=''
    $DATA_EMPLOYEE_FNameTH=''
    $DATA_EMPLOYEE_LNameTH=''

}
function findAssigneeByEmpCode(empCode){

    $.ajax({
        type: "GET",
        url: session['context']+'/employees/findByEmpCode?empCode='+empCode ,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        var obj = JSON.parse(xhr.responseText);
                        assigneeObject = obj
                    }else{
                        console.log('NOT FOUND')
                    }








                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function findAssignerByEmpCode(empCode){

    $.ajax({
        type: "GET",
        url: session['context']+'/employees/findByEmpCode?empCode='+empCode ,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        var obj = JSON.parse(xhr.responseText);
                        assignerObject = obj
                    }else{
                        console.log('NOT FOUND')
                    }








                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function saveAssignee(){




    var jsonParams = {};

    jsonParams['parentId']     	     = 1;
    jsonParams[csrfParameter]  	     = csrfToken;
    jsonParams['id']     		     = $('#add_assignee_id').val();
    jsonParams['createdBy'] 	     = $('#add_assignee_createdBy').val();
    jsonParams['createdDate'] 	     = $('#add_assigneet_createdDate').val();
    jsonParams['updateBy'] 		     = $('#add_assignee_updateBy').val();
    jsonParams['updateDate'] 	     = $('#add_assigneet_updateDate').val();
    jsonParams['empCodeReplace']    = $("#add_assignee_codeInputEmployeeAll").attr("data-empCode")  ;
    jsonParams['empUserNameReplace']= $("#add_assignee_codeInputEmployeeAll").attr("data-username")  ;







    if($("#add_assignee_codeInputEmployeeAll").val() != ""){


        searchAllAssignee(jsonParams.empCodeReplace)

        if(checkDuplicate_EmpCodeAssignee == false){
            $('.dv-background').show();
            var itemData = $.ajax({
                async:false,
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context']+'/employeeReplacement',
                data: jsonParams,
                complete: function (xhr) {
                    if(obj = JSON.parse(xhr.responseText)){

                        $('#addExpenseType').modal('hide');

                        $('#saveSuccess').modal('show');
                        setTimeout(function () {
                            $('#saveSuccess').modal('hide');
                        },1500)

                        // console.log('empCodeReplace after save'+obj.empCodeAssignee)
                        // console.log('empUserNameAssignee after save'+obj.empUserNameAssignee)
                        //
                        empCodeReplace = obj.empCodeReplace
                        empUserReplace = obj.empUserNameReplace
                        //
                        searchUserReplacement()
                        searchUserAgree()
                        $('#search_button_assigner').hide()
                        $('#search_button_assigner_head').hide()
                        showDetail_AfterSave(obj.empCodeReplace,obj.empCodeReplace,obj.empUserNameReplace)
                        // // $("tr#"+this).addClass("backgroundSelected")

                    }




                }
            }).done(function (){
                $('.dv-background').hide();
            });
        }else{
            searchUserReplacement()
            $('#warningModal .modal-body').html("")
            $('#warningModal .modal-body').append($MESSAGE_DUPLICATE_EMPLOYEE_REPLACE+"<br/>");
            $('#warningModal').modal('show');
        }


    }else{

        $('#warningModal .modal-body').html("")
        $('#warningModal .modal-body').append($MESSAGE_REQUIRE_FIELD+"<br/>");
        $('#warningModal .modal-body').append("      - "+$LABEL_EMPLOYEE_REPLACE);

        $('#warningModal').modal('show');


    }




    // }else{
    //     $('#warningModal .modal-body').html("")
    //     $('#warningModal .modal-body').append("   Dupplicate Assignee"+"<br/>");
    //     $('#warningModal').modal('show');
    // }













}
function deleteAssignee(id) {

            // alert('delete UserAgree ID ===> '+id)


    console.log('Id Delete  Expense Type =======> '+id)


    var jsonParams = {};
    jsonParams['parentId']     = 1;
    jsonParams[csrfParameter]  = csrfToken;
    $('.dv-background').show();
    $.ajax({
        type: "DELETE",
        url: session['context']+'/employeeReplacement/'+id,
        data: jsonParams,
        complete: function (xhr) {


            $('#deleteSuccess').modal('show')
            setTimeout(function () {
                $('#deleteSuccess').modal('hide')
            },1500)




            searchUserAgree();


            $('#deleteAssigner').modal('hide');

        }
    }).done(function (){
        $('.dv-background').hide();
    });

}
function showDetail(id,empCodeReplaces,empUserReplaces) {





    $('#addUserAgree').fadeIn('slow');
    $('#search_userAgree').fadeIn('slow');

    empUserReplace = empUserReplaces
    empCodeReplace = empCodeReplaces



    // alert('empUserReplace = '+empUserReplace)
    // alert('empCodeReplace = '+empCodeReplace)



    $("tr").removeClass("backgroundSelected")
    $("tr#"+id).addClass("backgroundSelected")
    searchUserAgree()





}
function searchUserAgree(){

    var criteriaObject = {

        empCodeReplace        : empCodeReplace,
        empCodeAgree        : $("#input_userAgree").val()== "" ? "": $("#input_userAgree").val()

    };

    console.log("DATA FOR SEARCR empCodeReplace =====> "+criteriaObject.empCodeReplace)
    console.log("DATA FOR SEARCR empCodeAgree =====> "+criteriaObject.empCodeAgree)
    queryUserAgreeByCriteria(criteriaObject);

}
function queryUserAgreeByCriteria(criteriaObject){
    object.setId("#pagingSearchUserAgreeBody");
    object.setUrlData("/employeeReplacement/findUserAgreeByCriteria");
    object.setUrlSize("/employeeReplacement/findUserAgreeSize");


    object.loadTable = function(items){
        var item = items.content;
        // $('.dv-background').show();


        $('#employeeAgreeBody').empty();
        if(item.length > 0){
            $dataForQuery = item;
            var itemIdTmp ;

            var checkDupAssignee = []


            for(var j=0;j<item.length;j++){



                var empCodeAgree    = item[j].empCodeAgree==null?"":item[j].empCodeAgree;
                var empUserNameAgree    = item[j].empUserNameAgree==null?"":item[j].empUserNameAgree;


                if( empUserNameAgree != "" && empUserNameAgree != null){
                    findEmployeeProfileByUserName_Assigner(empUserNameAgree)

                }






                itemIdTmp = item[j].id;
                if(itemIdTmp){
                    $('#employeeAgreeBody').append(''+
                        '<tr style="width: 100%" id="'+item[j].id+'" '+
                        ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
                        '<td style="width: 10%" class="text-center">'+'<a onclick="editAssigner(\''+item[j].id+'\')" data-target="#addAssigner" data-toggle="modal"><image width="45px;"  class="img-circle" src="'+$IMG_ASSIGNER+'"  /></a>'+'</td>'+

                        '<td class="text-center" style="padding-top: 25px; color: #8bd5d2;  font-weight: 800">'+empCodeAgree+'</td>'+
                        '<td class="text-center" style="padding-top: 25px;">'+$DATA_EMPLOYEE_Assigner_FOA+' '+$DATA_EMPLOYEE_Assigner_FNameTH+' '+$DATA_EMPLOYEE_Assigner_LNameTH+'</td>'+

                        // '<td style="width: 5%" class="text-center" style="padding-top: 25px;">'+flowType+'</td>'+
                        // '<td style="width: 10%" class="text-center" style="padding-top: 25px;">'+startDate+' - '+endDate+'</td>'+
                        // '<td style="width: 5%" class="text-center" style="padding-top: 25px;">'+(amount)+'</td>'+
                        '<td style="width: 10%"    class="text-center"  style="padding-top: 15px;">'+

                        '	<a onclick="$(\'#idDeleteAssigner\').val(\''+item[j].id+'\')" data-target="#deleteAssigner" data-toggle="modal" "><span><image class="img-circle" width="35;" src="'+$IMG_DELETE_ITEM+'"  /></span></a>'+

                        '</td>'+
                        '</tr>'
                    );
                }

            }


        }else{
            //Not Found
        }

    };

    object.setDataSearch(criteriaObject);
    object.search(object);
    object.loadPage(($CRITERIA_PAGE_ASSIGNER*1)+1,object);
}
function saveUserAgree(){





    var jsonSaveAssigner = {};

    jsonSaveAssigner['parentId']     	     = 1;
    jsonSaveAssigner[csrfParameter]  	     = csrfToken;
    jsonSaveAssigner['id']     		         = $('#add_assigner_id').val();
    jsonSaveAssigner['createdBy'] 	         = $('#add_assignee_createdBy').val();
    jsonSaveAssigner['createdDate'] 	     = $('#add_assigneet_createdDate').val();
    jsonSaveAssigner['updateBy'] 		     = $('#add_assignee_updateBy').val();
    jsonSaveAssigner['updateDate'] 	         = $('#add_assigneet_updateDate').val();
    jsonSaveAssigner['empCodeReplace']      = empCodeReplace  ;
    jsonSaveAssigner['empUserNameReplace']  = empUserReplace  ;
    jsonSaveAssigner['empCodeAgree']      = $('#add_assigner_codeInputEmployeeAll_tmp').attr("data-empCode")
    jsonSaveAssigner['empUserNameAgree']  = $('#add_assigner_codeInputEmployeeAll_tmp').attr("data-username")






                // alert('check dup  '+jsonSaveAssigner.empCodeAgree+' : '+jsonSaveAssigner.empCodeAgree)


if(jsonSaveAssigner.empCodeReplace != jsonSaveAssigner.empCodeAgree){
    if(   $('#add_assigner_codeInputEmployeeAll_tmp').val() != "" ){





        findByEmpCodeReplaceAndEmpCodeAgree(jsonSaveAssigner.empCodeReplace,jsonSaveAssigner.empCodeAgree)




        if(checkDupAssigner == false){



            console.log('CASE SAVE !!!')
            console.log('item for check dup empCodeReplace  : '+jsonSaveAssigner.empCodeReplace)
            console.log('item for check dup empUserNameReplace  : '+jsonSaveAssigner.empUserNameReplace)
            console.log('item for check dup empCodeAgree  : '+jsonSaveAssigner.empCodeAgree)
            console.log('item for check dup empUserNameAgree  : '+jsonSaveAssigner.empUserNameAgree)



            $('.dv-background').show();
            var itemDatas = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context']+'/employeeReplacement',
                data: jsonSaveAssigner,
                complete: function (xhr) {
                    searchUserAgree();
                    $('#add_assigner_name').val('')

                    $('#addAssigner').modal('hide');

                    $('#saveSuccess').modal('show');
                    setTimeout(function () {
                        $('#saveSuccess').modal('hide');
                    },1500)




                }
            }).done(function (){
                $('.dv-background').hide();
            });
        }else{

            if(checkDupAssigner_edit == $('#add_assigner_codeInputEmployeeAll_tmp').val()){


                // alert('case Update !!')

                jsonSaveAssigner['empCodeAgree']      = keepCodeUserAgree
                jsonSaveAssigner['empUserNameAgree']  = keepNameUserAgree





                $('.dv-background').show();

                var itemDatas = $.ajax({
                    type: "POST",
                    headers: {
                        Accept: 'application/json'
                    },
                    url: session['context']+'/employeeReplacement',
                    data: jsonSaveAssigner,
                    complete: function (xhr) {
                        searchUserAgree();
                        $('#add_assigner_name').val('')


                        $('#addAssigner').modal('hide');

                        $('#saveSuccess').modal('show');
                        setTimeout(function () {
                            $('#saveSuccess').modal('hide');
                        },1500)




                    }
                }).done(function (){
                    $('.dv-background').hide();
                });
            }else{
                $('#warningModal .modal-body').html("")
                $('#warningModal .modal-body').append($MESSAGE_DUPLICATE_EMPLOYEE_AGREE+"<br/>");
                $('#warningModal').modal('show');
            }




        }



    }else{
        $('#warningModal .modal-body').html("")
        $('#warningModal .modal-body').append($MESSAGE_REQUIRE_FIELD+"<br/>");

        if(($('#add_assigner_CodeInputEmployeeAll').val()) == ""){
            $('#warningModal .modal-body').append("      - "+$LABEL_EMPLOYEE_AGREE+"<br />");
        }





        $('#warningModal').modal('show');
    }
}else{
    $('#warningModal .modal-body').html("")
    $('#warningModal .modal-body').append($MESSAGE_EMPLOYEE_REPLACE_EQUAL_EMPLOYEE_AGREE+"<br/>");
    $('#warningModal').modal('show');
}













}
function showDetail_AfterSave(name,empCodeAssignee,empUserAssign) {





    $('#addUserAgree').fadeIn('slow');
    $('#search_userAgree').fadeIn('slow');

    empUserReplace = empUserAssign
    empCodeReplace = empCodeAssignee

    $("tr").removeClass("backgroundSelected")
    $("tr#"+name).addClass("backgroundSelected")
    searchUserAgree()

    console.log('empCodeAssignee ===> '+empCodeAssignee)



}
function deleteAllEmpCodeAssignee(empCodeReplace){
    $('.dv-background').show();
    $.ajax({
        type: "GET",
        url: session['context']+'/employeeReplacement/findByEmpCodeReplace?empCodeReplace='+empCodeReplace,
        async:false,
        complete: function (xhr) {


            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);



                    var item = obj.content
                    for(var i=0; i< item.length; i++){

                        var idForDelete = item[i].id
                        var jsonParams = {};
                        jsonParams['parentId']     = 1;
                        jsonParams[csrfParameter]  = csrfToken;
                                console.log('Item Id For Delete ====> '+idForDelete)
                        $.ajax({
                            type: "DELETE",
                            url: session['context']+'/employeeReplacement/'+idForDelete,
                            data: jsonParams,
                            complete: function (xhr) {

                                // searchAssignee()
                                $('#deleteSuccess').modal('show')
                                setTimeout(function () {
                                    $('#deleteSuccess').modal('hide')
                                },1500)



                            }
                        }).done(function (){

                        });




                    }
                    // searchUserAgree();

                }
            }


            searchUserReplacement()
            searchUserAgree();
            $('#deleteExpenseType').modal('hide');

        }
        // searchUserAgree();
    }).done(function (){
        $('.dv-background').hide();
    });

    searchUserAgree();

}
function findEmployeeReplacement_By_EmpCodeReplace(empCodeReplace){
    checkEmpCodeAssignee = false

    $.ajax({
        type: "GET",
        url: session['context']+'/employeeReplacement/findByEmpCodeReplace?empCodeReplace='+empCodeReplace,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        // alert('found User')
                        var obj = JSON.parse(xhr.responseText);
                        var item = obj.content
                        checkEmpCodeAssignee = true
                        assigneeObject_tmp = item[0]
                    }else{
                        console.log('NOT FOUND')
                    }








                }
            }
        }
    }).done(function (){
        //close loader
    });

}
function editAssigner(id){



    $('#add_assigner_codeInputEmployeeAll_tmp').val('');



    initUtilAssigner()




    $.ajax({
        type: "GET",
        url: session['context']+'/employeeReplacement/'+id,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var obj = JSON.parse(xhr.responseText);



                    console.log('id ref ===>'+obj.id)
                    console.log('ID IS------> '+obj.id)
                    $('#add_assigner_id').val(obj.id);
                    $('#add_assigner_createdBy').val(obj.createdBy);
                    $('#add_assigner_createdDate').val(obj.createdDate);
                    $('#add_assigner_updateBy').val(obj.updateBy);
                    $('#add_assigner_updateDate').val(obj.updateDate);
                    $('#add_assigner_codeInputEmployeeAll_tmp').val(obj.empCodeAgree);
                    // alert('user for edit====> '+obj.empUserNameAgree)
                    findEmployeeProfileByUserName_Assigner(obj.empUserNameAgree)
                    $('#add_assigner_codeInputEmployeeAll_tmp').val($DATA_EMPLOYEE_Assigner_FOA+''+$DATA_EMPLOYEE_Assigner_FNameTH+' '+$DATA_EMPLOYEE_Assigner_LNameTH);

                    checkDupAssigner_edit =  $DATA_EMPLOYEE_Assigner_FOA+''+$DATA_EMPLOYEE_Assigner_FNameTH+' '+$DATA_EMPLOYEE_Assigner_LNameTH
                    keepCodeUserAgree = obj.empCodeAgree
                    keepNameUserAgree = obj.empUserNameAgree

                    console.log('Keep Emp Code is ===> '+keepCodeUserAgree)
                    // alert('name user==> '+checkDupAssigner_edit)
                    AutocompleteEmployeeAll_tmp.search(obj.empUserNameAgree,'add_assigner_codeInputEmployeeAll_tmp')
                    AutocompleteEmployeeAll_tmp.renderValue(obj.empUserNameAgree)






                }
            }
        }
    }).done(function (){
        //close loader
    });


}
function findEmployeeProfileByUserName(userName){

    console.log('FIND BY UserName ====> '+userName)

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    console.info(data);
    // console.info('DATA USER IS-----------------> '+data.FOA);
    // $DATA_EMPLOYEE = data;
    $DATA_EMPLOYEE_FOA = data.FOA;
    $DATA_EMPLOYEE_FNameTH = data.FNameTH;
    $DATA_EMPLOYEE_LNameTH = data.LNameTH;
}
function findEmployeeProfileByUserName_Assigner(userName){

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    console.info(data);
    // console.info(data.FOA);
    // $DATA_EMPLOYEE = data;
    $DATA_EMPLOYEE_Assigner_FOA = data.FOA;
    $DATA_EMPLOYEE_Assigner_FNameTH = data.FNameTH;
    $DATA_EMPLOYEE_Assigner_LNameTH = data.LNameTH;
}
function searchAllAssignee(empCodeReplace){


    var criteriaObject = {
        empCodeReplace        :  ''
    };

    queryAllAssignee(criteriaObject,empCodeReplace);

}
function queryAllAssignee(criteriaObject,empCodeReplace){
    // alert('empCodeReplace check Dup===> '+empCodeReplace)
    // object_tmp.setId("#pagingSearchDelegateBody");
    object_tmp.setUrlData("/employeeReplacement/findByCriteria");
    object_tmp.setUrlSize("/employeeReplacement/findSize");

    checkDuplicate_EmpCodeAssignee = false


    object_tmp.loadTable = function(items){
        var item = items.content;
        $('.dv-background').show();


        $('#delegateBody').empty();
        if(item.length > 0){








            for(var j=0;j<item.length;j++){



                if(item[j].empCodeReplace == empCodeReplace){
                    // alert('found Duplicate')
                    checkDuplicate_EmpCodeAssignee =true
                    break;
                }




            }


        }else{
            //Not Found
        }

    };

    object_tmp.setDataSearch(criteriaObject);
    object_tmp.search(object_tmp);
    object_tmp.loadPage(($CRITERIA_PAGE*1)+1,object_tmp);



}
function initUtilAssignee(){



    $('#add_assignee_codeInputEmployeeAll').on('keyup',function () {

        AutocompleteEmployeeAll.setId('add_assignee_codeInputEmployeeAll')
        AutocompleteEmployeeAll.search($('#add_assignee_codeInputEmployeeAll').val(),'add_assignee_codeInputEmployeeAll')

    })

}
function initUtilAssigner(){



    $('#add_assigner_codeInputEmployeeAll_tmp').on('keyup',function () {


        AutocompleteEmployeeAll_tmp.search($('#add_assigner_codeInputEmployeeAll_tmp').val(),'add_assigner_codeInputEmployeeAll_tmp')

    })


}



function findByEmpCodeReplaceAndEmpCodeAgree(empCodeReplace,empCodeAgree){

    // alert('empCodeReplace ===> '+empCodeReplace)
    // alert('empCodeAgree ===> '+empCodeAgree)

    checkDupAssigner = false

    $.ajax({
        type: "GET",
        url: session['context']+'/employeeReplacement/findByEmpCodeReplaceAndEmpCodeAgree?empCodeReplace='+empCodeReplace+'&empCodeAgree='+empCodeAgree,
        async:false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    if(xhr.responseText){
                        // alert('found User Dup')
                        var obj = JSON.parse(xhr.responseText);

                        checkDupAssigner = true



                    }else{
                        console.log('NOT FOUND')
                    }








                }
            }
        }
    }).done(function (){
        //close loader
    });

}
