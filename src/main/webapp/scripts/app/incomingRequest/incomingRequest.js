var object = $.extend({},UtilPagination);
var code;
var TEMP;
$(document).ready(function () {

    readDataZip(function () {
        readMasterDataDocAppType();
        readMasterDataDocExpType();
        readMasterDataDocAdvType();
        readMasterDataDocStatusDraft();
        readMasterDataDocStatusOnProcess();
        readMasterDataDocStatusCancel();
        readMasterDataDocStatusReject();
        readMasterDataDocStatusComplete();
        readMasterDataAprTypeSetDomestic();
        readMasterDataAprTypeSetForeign();
        $('.dv-background').show();
    });

    window.setTimeout(function () {

        $('#doc_status_row').empty();
        $('#doc_status_row').append(
            '<div class="form-horizontal">' +
            '<div class="col-sm-12" style="padding-right: 0px;">' +
            '<input type="hidden" id="search_doc_status_all" />' +
            '<input type="hidden" class="form-control" style="color: #ff0000;"  id="search_doc_status" value=""/>' +
            '</div>' +
            '<div class="col-sm-offset-1 col-sm-2">' +
            '<div class="checkbox checkbox-info">' +
            '<label style="color: black"><input type="checkbox" class="checkbox_status1" id="checkbox_status_' + MASTER_DATA.DOC_STATUS_DRAFT + '" value="' + MASTER_DATA.DOC_STATUS_DRAFT + '"/> ' + $LABEL_DRAFT_STATUS + '</label>' +
            '</div>' +
            '</div>' +
            '<div class="col-sm-2">' +
            '<div class="checkbox checkbox-info">' +
            '<label style="color: black"><input type="checkbox" class="checkbox_status1" id="checkbox_status_' + MASTER_DATA.DOC_STATUS_ON_PROCESS + '" value="' + MASTER_DATA.DOC_STATUS_ON_PROCESS + '"/> ' + $LABEL_ON_PROCESS_STATUS + '</label>' +
            '</div>' +
            '</div>' +
            '<div class="col-sm-2">' +
            '<div class="checkbox checkbox-info">' +
            '<label style="color: black"><input type="checkbox" class="checkbox_status1" id="checkbox_status_' + MASTER_DATA.DOC_STATUS_CANCEL + '" value="' + MASTER_DATA.DOC_STATUS_CANCEL + '"/> ' + $LABEL_CANCEL_STATUS + '</label>' +
            '</div>' +
            '</div>' +
            '<div class="col-sm-2">' +
            '<div class="checkbox checkbox-info">' +
            '<label style="color: black"><input type="checkbox" class="checkbox_status1" id="checkbox_status_' + MASTER_DATA.DOC_STATUS_REJECT + '" value="' + MASTER_DATA.DOC_STATUS_REJECT + '"/> ' + $LABEL_REJECT_STATUS + '</label>' +
            '</div>' +
            '</div>' +
            '<div class="col-sm-2">' +
            '<div class="checkbox checkbox-info">' +
            '<label style="color: black"><input type="checkbox" class="checkbox_status1" id="checkbox_status_' + MASTER_DATA.DOC_STATUS_COMPLETE + '" value="' + MASTER_DATA.DOC_STATUS_COMPLETE + '"/> ' + $LABEL_COMPLETE_STATUS + '</label>' +
            '</div>' +
            '</div>' +
            '<a title="' + $MESSAGE_SEARCH + '" onclick="searchIncomingMyRequest()"><img src="' + $SEARCH + '" width="70px"/>&#160;</a>' +
            '</div>'
        );

        $('#checkbox_modal').append(
            '<div class="col-xs-12">' +
            '<div class="checkbox checkbox-info">' +
            '<label style="color: black"><input type="checkbox" class="checkbox_status" id="checkbox_status_mobile_' + MASTER_DATA.DOC_STATUS_DRAFT + '" value="' + MASTER_DATA.DOC_STATUS_DRAFT + '"/> ' + $LABEL_DRAFT_STATUS + '</label>' +
            '</div>' +
            '</div>' +
            '<div class="col-xs-12">' +
            '<div class="checkbox checkbox-info">' +
            '<label style="color: black"><input type="checkbox" class="checkbox_status" id="checkbox_status_mobile_' + MASTER_DATA.DOC_STATUS_ON_PROCESS + '" value="' + MASTER_DATA.DOC_STATUS_ON_PROCESS + '"/> ' + $LABEL_ON_PROCESS_STATUS + '</label>' +
            '</div>' +
            '</div>' +
            '<div class="col-xs-12">' +
            '<div class="checkbox checkbox-info">' +
            '<label style="color: black"><input type="checkbox" class="checkbox_status" id="checkbox_status_mobile_' + MASTER_DATA.DOC_STATUS_CANCEL + '" value="' + MASTER_DATA.DOC_STATUS_CANCEL + '"/> ' + $LABEL_CANCEL_STATUS + '</label>' +
            '</div>' +
            '</div>' +
            '<div class="col-xs-12">' +
            '<div class="checkbox checkbox-info">' +
            '<label style="color: black"><input type="checkbox" class="checkbox_status" id="checkbox_status_mobile_' + MASTER_DATA.DOC_STATUS_REJECT + '" value="' + MASTER_DATA.DOC_STATUS_REJECT + '"/> ' + $LABEL_REJECT_STATUS + '</label>' +
            '</div>' +
            '</div>' +
            '<div class="col-xs-12">' +
            '<div class="checkbox checkbox-info">' +
            '<label style="color: black"><input type="checkbox" class="checkbox_status" id="checkbox_status_mobile_' + MASTER_DATA.DOC_STATUS_COMPLETE + '" value="' + MASTER_DATA.DOC_STATUS_COMPLETE + '"/> ' + $LABEL_COMPLETE_STATUS + '</label>' +
            '</div>' +
            '</div>'
        )

        $(".checkbox_status1").prop('checked', true);
        $(".checkbox_status").prop('checked', true);


        $.material.init();

        if($CODE == 'APP'){
            code = MASTER_DATA.DOC_APP_TYPE;
        }else{
            code = MASTER_DATA.DOC_ADV_TYPE;
        }

        searchIncomingMyRequest();
        $('.dv-background').hide();

    },2000);


    if($CODE == "APP"){
        $("#buttonBar").empty();
        $("#advance").addClass("hide");
        $("#expense").addClass("hide");
        $("#approve").removeClass("hide");

        $('#approve_mobile').removeClass('hide');
        $('#expense_mobile').addClass('hide');
        $('#advance_mobile').addClass('hide');

        $('#expense_mobile_button').addClass('hide');
        $('#advance_mobile_button').addClass('hide');

        code = MASTER_DATA.DOC_APP_TYPE;

        searchIncomingMyRequest();

    }else {

        $("#buttonBar").empty();

        $("#buttonBar").append(''+
            '<div title="' + $LABEL_DOCUMENT_ADVANCE + '" id="divADV" class="btn btnActionFixed" style="float:left; left:30px; margin-top: 0px; padding:6px; position: fixed; border-radius:8px;">' +
            '   <img src="/GWF/resources/images/icon-approve/t1.png" id="btnADV" style="  height: 70px ; width: 70px;"><jsp:text /></img>' +
            '</div>' +
            '<div title="' + $LABEL_DOCUMENT_EXPENSE + '" id="divEXP" class="btn btnActionFixed" style="float:left; left:30px; margin-top: 80px; padding:6px; position: fixed; border-radius:8px;">' +
            '   <img src="/GWF/resources/images/icon-approve/t3.png" id="btnEXP" style="  height: 70px ; width: 70px;"><jsp:text /></img>' +
            '</div>');
        moveFixedActionButton();

        if($CODE == "ADV" ){

            $("#divADV").attr('disabled',true);
            $('#btnADV').css('filter','grayscale(0%)');

            $("#divEXP").attr('disabled',false);
            $('#btnEXP').css('filter','grayscale(100%)');

            $("#advance").removeClass("hide");
            $("#expense").addClass("hide");
            $("#myRequest").addClass("hide");

            code = MASTER_DATA.DOC_ADV_TYPE;

            $('#advance_mobile').removeClass('hide');
            $('#expense_mobile').addClass('hide');
            $('#approve_mobile').addClass('hide');

            searchIncomingMyRequest()
        }else{
            $("#divEXP").attr('disabled',true);
            $('#btnEXP').css('filter','grayscale(0%)');

            $("#divADV").attr('disabled',false);
            $('#btnADV').css('filter','grayscale(100%)');

            $("#advance").addClass("hide");
            $("#expense").removeClass("hide");
            $("#myRequest").addClass("hide");

            code = MASTER_DATA.DOC_EXP_TYPE;

            $('#expense_mobile').removeClass('hide');
            $('#advance_mobile').addClass('hide');
            $('#approve_mobile').addClass('hide');

            searchIncomingMyRequest()
        }
    }

    $("#btnADV").on('click',function () {
        $("#divADV").attr('disabled',true);
        $("#divEXP").attr('disabled',false);

        $('#btnADV').css('filter','grayscale(0%)');
        $('#btnEXP').css('filter','grayscale(100%)');

        code = MASTER_DATA.DOC_ADV_TYPE;

        $("#advance").removeClass("hide");
        $("#expense").addClass("hide");
        $("#myRequest").addClass("hide");

        searchIncomingMyRequest()
    });

    $("#btnEXP").on('click',function () {
        $("#divADV").attr('disabled',false);
        $("#divEXP").attr('disabled',true);

        $('#btnEXP').css('filter','grayscale(0%)');
        $('#btnADV').css('filter','grayscale(100%)');


        code = MASTER_DATA.DOC_EXP_TYPE;

        $("#advance").addClass("hide");
        $("#expense").removeClass("hide");
        $("#myRequest").addClass("hide");

        searchIncomingMyRequest()
    });

    $('#expense_mobile_button').on('click',function () {
        code = MASTER_DATA.DOC_EXP_TYPE;

        $("#approve_mobile").addClass("hide");
        $("#advance_mobile").addClass("hide");
        $("#expense_mobile").removeClass("hide");

        searchIncomingRequestMobile();
    });
    $('#advance_mobile_button').on('click',function () {
        code = MASTER_DATA.DOC_ADV_TYPE;

        $("#approve_mobile").addClass("hide");
        $("#advance_mobile").removeClass("hide");
        $("#expense_mobile").addClass("hide");

        searchIncomingRequestMobile();
    });

    $(window).resize(function () {
        moveFixedActionButton();
    })



});

function moveFixedActionButton() {
    var xPos = $('.container').offset().left;
    for (var i = 0; i < $('.btnActionFixed').size(); i++) {
        $('.btnActionFixed')[i].style.left = (xPos-75) + 'px';
    }
}

function searchIncomingMyRequest(){
    $('.dv-background').show();

    window.setTimeout(function () {
        var documentStatus = "";
        $.each( $( ".checkbox_status1:checked" ), function( i, obj ) {
            documentStatus += ","+obj.value;
        });

        if(documentStatus){
            documentStatus = documentStatus.substring(1);
            $('#search_doc_status').val(documentStatus);
        }else{
            $('#search_doc_status').val($('#search_doc_status_all').val());
        }

        var criteriaObject = {
            nextApprover : $USERNAME,
            documentStatus        : $("#search_doc_status").val()==""?"":$("#search_doc_status").val(),
            documentType        :  code,
            docNumber           : $("#input_docNumber").val()==""?"":$("#input_docNumber").val(),
            titleDescription    :$("#input_titleDescription").val()==""?"":$("#input_titleDescription").val(),
            projection : "haveDoc"
        };

        queryIncomingMyRequestByCriteria(criteriaObject);
    },500);


}
function searchIncomingRequestMobile() {
    $('.dv-background').show();
    window.setTimeout(function () {
        let documentStatus = "";
        $.each( $( ".checkbox_status:checked" ), function( i, obj ) {
            documentStatus += ","+obj.value ;
        });

        if(documentStatus){
            documentStatus = documentStatus.substring(1);
            $('#search_doc_status_mobile').val(documentStatus);
        }else{
            $('#search_doc_status_mobile').val($('#search_doc_status_all_mobile').val());
        }

        var criteriaObject = {
            nextApprover : $USERNAME,
            documentStatus        : $("#search_doc_status_mobile").val()==""?"":$("#search_doc_status_mobile").val(),
            documentType        :  code,
            docNumber           : $("#input_docNumber_mobile").val()==""?"":$("#input_docNumber_mobile").val(),
            titleDescription    :$("#input_titleDescription_mobile").val()==""?"":$("#input_titleDescription_mobile").val(),
            projection : "haveDoc"
        };

        queryIncomingMyRequestByCriteria(criteriaObject);
    },500)

}

function queryIncomingMyRequestByCriteria(criteriaObject) {

    let urlSearch;
    if(code == "APP"){
        urlSearch = '/incomingRequestApprove/findByCriteria';
    }else{
        urlSearch = '/incomingRequestExpense/findByCriteria'
    }

    $.ajax({
        url : session.context + urlSearch,
        data : criteriaObject,
        complete : function(xhr){
            if(xhr.status == 200){
                if(xhr.responseText){
                    let item = JSON.parse(xhr.responseText);
                    item = item.content;

                    let record = "";
                    let record_mobile = '';

                    console.log("length -> " + item.length);
                    if (item.length > 0) {
                        $('.dv-background').show();
                        $("#bodyGrid").empty();
                        $('#mobile_content').empty();

                        for (let j = 0; j < item.length; j++) {
                            if (item[j].id) {
                                // if(item[i].document.documentStatus == "ONP") {

                                let to_day = new Date();
                                let days = to_day.getDate();
                                let month = to_day.getMonth()+1; //January is 0!
                                let year = to_day.getFullYear();
                                let hours = to_day.getHours();
                                let minutes = to_day.getMinutes();

                                if(days<10)
                                    days = '0'+days;
                                if(month<10)
                                    month = '0'+month;
                                if(hours<10)
                                    hours = '0'+hours;
                                if(minutes<10)
                                    minutes = '0'+minutes;

                                to_day = year+'-'+month+'-'+days+' '+hours+':'+minutes;

                                let start_time
                                if(item[j].lastActionTime)
                                    start_time = moment(item[j].lastActionTime,moment.ISO_8601);
                                else
                                    start_time = moment(item[j].document.sendDate,moment.ISO_8601);
                                let end_time = moment(to_day,moment.ISO_8601);

                                record += '' +
                                    '<div class="row">' +
                                    '<div class="col-sm-12">' +
                                    '<div class="col-sm-3" style="padding-top: 10px; padding-left: 0; padding-right: 0; text-align: center">' +
                                    '<div class="col-sm-6" style="text-align: right;">' +
                                    '<a title="' + $MESSAGE_APPROVE_REQUEST_TOOLTIP +'" onclick="findRequestByDocument(' + item[j].document.id + ',\'' + item[j].document.docNumber + '\',' + item[j].document.processId + ',\'' + item[j].document.documentType + '\')">' +
                                    '<img src=' + $tick + ' width="50px"/>&#160;' +
                                    '</a>' +
                                    '</div>';

                                record_mobile += '' +
                                    '<div style="height: 60px; width: 100%; background-color: #858585; padding-right: 0px; padding-left: 0px; margin-top: 10px;">'+
                                    '<div class="col-xs-1 col-xs-1" style="padding-top: 5px; padding-left: 5px; width: 55px; display: inline-block">'+
                                    '<a onclick="findRequestByDocument(' + item[j].document.id + ',\'' + item[j].document.docNumber + '\',' + item[j].document.processId + ',\'' + item[j].document.documentType + '\')">' +
                                    '<img src='+$tick +' width="50px;"/>' +
                                    '</a>'+
                                    '</div>'+
                                    '<a data-toggle="collapse" data-target="#detail' + j +'">'+
                                    '<div class="col-xs-10" style="display: inline-block; vertical-align: middle; padding-left: 0px; padding-right: 0px;">'+
                                    '<div   class="col-xs-12" style="text-align:left; font-size: 17px; color: white; display: inline-block; white-space: nowrap; width: 300px; overflow: hidden;text-overflow: ellipsis;">'+
                                    '<div style="display: inline-block" title="' + item[j].document.docNumber + '">' + item[j].document.docNumber +'</div>'+
                                    '</div>'+
                                    '<div class="col-xs-8" style="font-size: 17px; color: #52f7fc; display: inline-block; text-align: left; padding-right: 0px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">'+
                                    '<div style="display: inline-block;" title="' + (item[j].document.titleDescription ? item[j].document.titleDescription : '-') +'">'+
                                    (item[j].document.titleDescription ? item[j].document.titleDescription : '-') +
                                    '</div>'+
                                    '</div>'+
                                    '<div class="col-xs-4" style="font-size: 17px; color: #1efc06; display: inline-block; text-align: right; padding-right: 0px; padding-left: 0px;">'+
                                    end_time.diff(start_time,'days') +' Day'+
                                    '</div>'+
                                    '</div>'+
                                    '</a>'+
                                    '</div>'+
                                    '<div id="detail' + j + '" class="panel-collapse collapse" role="detail' + j + '">' +
                                    '<div class="panel-body panel-white-perl" style="padding-right: 5px; padding-left: 5px; padding-bottom: 4px; background-color: white; text-align: center;">' +
                                    '<div class="col-xs-12" style="font-size: 16px; text-align: left">' +
                                    $LABEL_REQUESTER +
                                    '</div>' +
                                    '<div class="col-xs-12" style="font-size: 16px; color:deepskyblue; text-align: right; white-space: nowrap; overflow: hidden;text-overflow: ellipsis; direction:rtl;">' +
                                    ((item[j].document.requester) ? findEmployeeProfileByUserName(item[j].document.requester) : '-') +
                                    '</div>' +
                                    '<div class="col-xs-12" style="font-size: 4em; font-weight: bold; color: white; display: inline-block; text-align: center; vertical-align: middle;">' +
                                    '<a onclick="incomingDoc(' + item[j].document.id + ',\'' + item[j].document.documentType + '\',\'' + item[j].document.approveType  +'\',\'' + item[j].document.createdBy +'\')">' +
                                    '<input class="btn btn-primary" type="button" value="' + $BUTTON_MORE_DETAIL + '">' +
                                    '</a>'+
                                    '</div>' +
                                    '</div>'+
                                    '</div>';

                                // if (code == "APP") {
                                record += '' +
                                    '<div class="col-sm-6" style="text-align: left;">' +
                                    '<a title="' + $MESSAGE_VIEW_DOC_TOOLTIP + '" onclick="incomingDoc(' + item[j].document.id + ',\'' + item[j].document.documentType + '\',\'' + item[j].document.approveType + '\',\'' + item[j].document.createdBy + '\')">' +
                                    '<img src=' + $menu + ' width="50px"/>&#160;' +
                                    '</a>' +
                                    '</div>';

                                record += '' +
                                    '</div>' +
                                    '<div class="col-sm-1" style="text-align: center; padding-top: 0px; color: orange"> ' +
                                    '<div class="col-sm-12" style="font-size: 40px; font-weight: bold">' + end_time.diff(start_time,'days') + '</div>' +
                                    '<div class="col-sm-12"> DAY </div>' +
                                    '</div>' +
                                    '<div class="col-sm-3" style="text-align: left; padding-left: 0">' +
                                    '<div class="col-sm-12" style="white-space: nowrap; overflow: hidden;text-overflow: ellipsis; direction: rtl;" title="' + item[j].document.docNumber + '">' +
                                    item[j].document.docNumber +
                                    '</div>' +
                                    '<div class="col-sm-12" style="color: green; white-space: nowrap;overflow: hidden;text-overflow: ellipsis;" title="' + (item[j].document.titleDescription ? item[j].document.titleDescription : '-') + '">' +
                                    (item[j].document.titleDescription ? item[j].document.titleDescription : '-') +
                                    '</div>' +
                                    '</div>' +
                                    '<div class="col-sm-3" style="text-align: left; padding-top: 0px; padding-left: 0px; padding-right: 0px;">' +
                                    '<div class="col-sm-12">' + ((item[j].document.requester) ? findEmployeeProfileByUserName(item[j].document.requester) : '-') + '</div>' +
                                    '<div class="col-sm-12" style="font-size: 12px; color: deepskyblue;">' + new Date(item[j].document.sendDate).format("yyyy-mm-dd HH:mm") + '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '<hr style="margin-top: 10px; margin-bottom: 10px;"/>';
                                // }
                            }
                        }
                        $("#bodyGrid").append(record);
                        $("#mobile_content").append(record_mobile);
                        $('.dv-background').hide();
                    }
                }
            }
        }
    });
    $('.dv-background').hide();
}

function findEmployeeProfileByUserName(userName){
    let data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;
    return data.FOA+data.FNameTH+" "+data.LNameTH;
}

function findRequestByDocument(id,docNumber,processId,docType){
    $('.dv-background').show();
    window.setTimeout(function () {
        var data1 = $.ajax({
            url: session.context + "/approve/findRequestByDocument/"+ id,
            headers: {
                Accept : "application/json"
            },
            type: "GET",
            async: false
        }).responseJSON;

        console.info(data1);
        $DATA_REQUEST = data1;

        var data2 = $.ajax({
            url: session.context + "/approve/findRequestApproverByRequest/"+$DATA_REQUEST.id,
            headers: {
                Accept : "application/json"
            },
            type: "GET",
            async: false
        }).responseJSON;

        $DATA_REQUEST_APPROVER = data2;

        let actionState
        let approver
        let documentId
        let rejectReasonCode
        let requestStatus
        let userNameApprover

        for(let i=0;i<$DATA_REQUEST_APPROVER.length; i++){
            if($DATA_REQUEST_APPROVER[i].userNameApprover == $USERNAME && $DATA_REQUEST_APPROVER[i].requestStatusCode === null){
                console.log("have request");
                actionState = $DATA_REQUEST_APPROVER[i].actionState;
                approver = $DATA_REQUEST_APPROVER[i].approver;
                documentId = $DATA_REQUEST_APPROVER[i].id;
                rejectReasonCode = $DATA_REQUEST_APPROVER[i].requestStatusCode;
                requestStatus = $DATA_REQUEST_APPROVER[i].requestStatusCode;
                userNameApprover = $DATA_REQUEST_APPROVER[i].userNameApprover;
                break;
            }
        }

        var jsonData = {};
        jsonData['userName'] = $USERNAME;
        jsonData['actionStatus'] = validateActionState($USERNAME);
        jsonData['documentNumber'] = docNumber;
        jsonData['docType'] = docType;
        jsonData['documentFlow'] = $DATA_REQUEST.docFlow;
        jsonData['processId'] = processId;
        jsonData['documentId'] = id;
        jsonData['actionReasonCode'] = '';
        jsonData['actionReasonDetail'] = '';

        $.ajax({
            type : "POST",
            url: session.context + "/requests/approveRequest",
            data:jsonData,
            async: false,
            complete: function (xhr) {
                $('.dv-background').hide();
                modalSuccess($MESSAGE_APPROVE_SUCCESS);
                searchIncomingMyRequest();
            }
        })
    },500);
}

function validateActionState(userName){
    for(var i=0;i<$DATA_REQUEST_APPROVER.length;i++){
        if(userName == $DATA_REQUEST_APPROVER[i].userNameApprover){
            return $DATA_REQUEST_APPROVER[i].actionState;
        }
    }
}

function modalSuccess(msg) {
    console.log(msg);
    $("#modalbodySuccess").empty();
    $("#modalbodySuccess").append(msg);
    $("#successModal").modal('show');
    setTimeout(function() {$('#successModal').modal('hide');}, 1250);
}


function incomingDoc(id,documentType,approveType,createdBy){
    if(documentType == MASTER_DATA.DOC_APP_TYPE){
        if(approveType == MASTER_DATA.APR_TYPE_SET_DOMESTIC || approveType == MASTER_DATA.APR_TYPE_SET_FOREIGN){
            window.location.href = session.context + '/approve/viewCreateDocSetDetail?doc=' + id;
        }else{
            window.location.href = session.context + '/approve/viewCreateDocDetail?doc=' + id;
        }
    }else if(documentType == MASTER_DATA.DOC_EXP_TYPE){
        if(session.roleName.indexOf("ROLE_ACCOUNT")){
            window.location.href = session.context + '/expense/clearExpenseDetail?doc=' + id;
        }else{
            window.location.href = session.context + '/expense/expenseDetail?doc=' + id;
        }
    }else {
        window.location.href = session.context + '/advance/advanceDetail?doc=' + id;
    }
}
