var object_approve = $.extend({},UtilPagination);
var object_expense = $.extend({},UtilPagination);
var $DATA_EMPLOYEE;
var dataApproves = [];
var dataExpense = [];
let EXP_COLOR = [];
let APP_COLOR = [];
let ADV_COLOR = [];
var $DocumentTYPE = 'APP';

$(document).ready(function () {

    // $('#createDate_picker').val(new Date().format('dd/mm/yyyy') + ' to ' + new Date().format('dd/mm/yyyy'));

    readDataZip(function () {
        readMasterDataDocAppType()
        readMasterDataDocExpType()
        readMasterDataDocAdvType()
        readMasterDataDocStatusDraft()
        readMasterDataDocStatusOnProcess()
        readMasterDataDocStatusCancel();
        readMasterDataDocStatusReject();
        readMasterDataDocStatusComplete();
        readMasterDataDocStatusLost();
        readMasterDataAprTypeDomestic();
        readMasterDataAprTypeForeign();
        readMasterDataAprTypeCar();
        readMasterDataAprTypeHotel();
        readMasterDataAprTypeFightBooking();
        readMasterDataAprTypeSetDomestic();
        readMasterDataAprTypeSetForeign();
    });

    // $('.dv-background').show();
    $.material.init();

    window.setTimeout(function () {
        // searchApprove();
        // searchExpense();

        $('#doc_status_row').empty();
        $('#doc_status_row').append(
            '<div class="form-horizontal">' +
            '<div class="col-sm-12" style="padding-right: 0px;">'+
            '<input type="hidden" id="search_doc_status_all" />'+
            '<input type="hidden" class="form-control" style="color: #ff0000;"  id="search_doc_status" value="${criteria_documentStatus}"/>'+
            '</div>'+
            // '<div class="col-sm-2" style="width: 15%; margin-left: 5%;">'+
            // '<div class="checkbox checkbox-info">'+
            // '<label style="font-size: 15px; color: white"><input type="checkbox" class="checkbox_status" id="checkbox_status_' + MASTER_DATA.DOC_STATUS_DRAFT + '" value="' + MASTER_DATA.DOC_STATUS_DRAFT  + '"/> '+ $LABEL_DRAFT_STATUS + '</label>'+
            // '</div>'+
            // '</div>'+
            '<div class="col-sm-2" style="width: 25%">'+
            '<div class="checkbox checkbox-info">'+
            '<label style="font-size: 15px; color: white"><input type="checkbox" class="checkbox_status" id="checkbox_status_' + MASTER_DATA.DOC_STATUS_ON_PROCESS+ '" value="' + MASTER_DATA.DOC_STATUS_ON_PROCESS + '"/> '+ $LABEL_ON_PROCESS_STATUS + '</label>'+
            '</div>'+
            '</div>'+
            '<div class="col-sm-3" style="width: 25%">'+
            '<div class="checkbox checkbox-info">'+
            '<label style="font-size: 15px; color: white"><input type="checkbox" class="checkbox_status" id="checkbox_status_' + MASTER_DATA.DOC_STATUS_CANCEL + '" value="' + MASTER_DATA.DOC_STATUS_CANCEL  + '"/> '+ $LABEL_CANCEL_STATUS + '</label>'+
            '</div>'+
            '</div>'+
            '<div class="col-sm-2" style="width: 25%">'+
            '<div class="checkbox checkbox-info">'+
            '<label style="font-size: 15px; color: white"><input type="checkbox" class="checkbox_status" id="checkbox_status_' + MASTER_DATA.DOC_STATUS_REJECT + '" value="' + MASTER_DATA.DOC_STATUS_REJECT  + '"/> '+ $LABEL_REJECT_STATUS + '</label>'+
            '</div>'+
            '</div>'+
            '<div class="col-sm-2" style="width: 25%">'+
            '<div class="checkbox checkbox-info">'+
            '<label style="font-size: 15px; color: white"><input type="checkbox" class="checkbox_status" id="checkbox_status_' + MASTER_DATA.DOC_STATUS_COMPLETE + '" value="' + MASTER_DATA.DOC_STATUS_COMPLETE  + '"/> ' + $LABEL_COMPLETE_STATUS +'</label>'+
            '</div>'+
            '</div>'+
            // '<a title="' + $MESSAGE_SEARCH + '" onclick="searchMyApproveRequest()"><img src="' + $SEARCH + '" width="70px"/>&#160;</a>'+
            '</div>'
        );

        $('#doc_type_row').empty();
        $('#doc_type_row').append(
            '<div class="form-horizontal">' +
                '<div class="col-sm-12" style="padding-right: 0px;">'+
                    '<input type="hidden" id="search_doc_type_all" />'+
                    '<input type="hidden" class="form-control" style="color: #ff0000;"  id="search_doc_type">'+
                '</div>'+
                // '<div class="col-sm-offset-3 col-sm-3" style="width: 20%">'+
                '<div class="col-sm-3" style="width: 23%">'+
                    '<div class="checkbox checkbox-info">'+
                        '<label style="font-size: 15px; color: white"><input type="checkbox" class="checkbox_status1" id="checkbox_status_' + MASTER_DATA.DOC_APP_TYPE + '" value="' + MASTER_DATA.DOC_APP_TYPE   + '"/> '+ $LABEL_APPROVE_DASHBOARD + '</label>'+
                    '</div>'+
                '</div>'+
                '<div class="col-sm-3" style="width: 20%">'+
                    '<div class="checkbox checkbox-info">'+
                        '<label style="font-size: 15px; color: white"><input type="checkbox" class="checkbox_status1" id="checkbox_status_' + MASTER_DATA.DOC_EXP_TYPE + '" value="' + MASTER_DATA.DOC_EXP_TYPE   + '"/> '+ $LABEL_EXPENSE + '</label>'+
                    '</div>'+
                '</div>'+
                '<div class="col-sm-3" style="width: 20%">'+
                    '<div class="checkbox checkbox-info">'+
                        '<label style="font-size: 15px; color: white"><input type="checkbox" class="checkbox_status1" id="checkbox_status_' + MASTER_DATA.DOC_ADV_TYPE + '" value="' + MASTER_DATA.DOC_ADV_TYPE + '"/> ' + $LABEL_ADVANCE +'</label>'+
                    '</div>'+
                '</div>'+
            // '<a title="' + $MESSAGE_SEARCH + '" onclick="searchMyApproveRequest()"><img src="' + $SEARCH + '" width="70px"/>&#160;</a>'+
            '</div>'
        );

        $.material.init();

        $(".checkbox_status").prop('checked', true);
        $(".checkbox_status1").prop('checked', true);

        findUserPA();
        loadColor();

        if(userPA.length){
            $COLOR = JSON.parse($COLOR.responseText);
            $COLOR = $COLOR.content.sort(function(a,b){
                return a.id > b.id;
            });
            $COLOR.forEach(function(item){
                if(item.code == MASTER_DATA.DOC_APP_TYPE + '_' + userPA[0].companyCode){
                    APP_COLOR.push({'code':item.code,'minDay':item.variable1,'maxDay':item.variable2,'color':item.variable3})
                }else if(item.code == MASTER_DATA.DOC_EXP_TYPE + '_' + userPA[0].companyCode){
                    EXP_COLOR.push({'code':item.code,'minDay':item.variable1,'maxDay':item.variable2,'color':item.variable3})
                }else if(item.code == MASTER_DATA.DOC_ADV_TYPE + '_' + userPA[0].companyCode){
                    ADV_COLOR.push({'code':item.code,'minDay':item.variable1,'maxDay':item.variable2,'color':item.variable3})
                }
            })
        }


        // searchAllDocument();

        $('.dv-background').hide();

        $('#search_doc_status_all').val(MASTER_DATA.DOC_STATUS_ON_PROCESS + ',' + MASTER_DATA.DOC_STATUS_CANCEL + ',' + MASTER_DATA.DOC_STATUS_REJECT + ',' + MASTER_DATA.DOC_STATUS_COMPLETE );

        $('#search_doc_type_all').val(MASTER_DATA.DOC_APP_TYPE + ',' + MASTER_DATA.DOC_EXP_TYPE + ',' + MASTER_DATA.DOC_ADV_TYPE );


    },2000);

    $('#requestExpense').on('click',function () {
        $('.dv-background').show();
        $DocumentTYPE = MASTER_DATA.DOC_EXP_TYPE;

        window.setTimeout(function () {
            searchAllDocument();
            $('.dv-background').hide();
        },500);

    })

    $('#requestApprove').addClass('bgblue');

    $('#requestApprove').on('click',function () {
        $('.dv-background').show();
        $DocumentTYPE = MASTER_DATA.DOC_APP_TYPE;

        window.setTimeout(function(){
            searchAllDocument();
            $('.dv-background').hide();
        },500);

    })

    $('#requestADV').on('click',function () {
        $('.dv-background').show();
        $DocumentTYPE = MASTER_DATA.DOC_ADV_TYPE;

        window.setTimeout(function(){
            searchAllDocument();
            $('.dv-background').hide();
        },500);

    })


    $(".rectangle-tab").click(function () {
        $(".rectangle-tab").removeClass('bgblue')
        $(this).addClass('bgblue')
    })


    $('#search_btn').click(function () {
        $('.dv-background').show();
        $.material.init();

        window.setTimeout(function () {
            // searchApprove();
            // searchExpense();
            searchAllDocument();

            $('.dv-background').hide();
        },2000);
    })

    $("#createDate_picker").flatpickr({
        mode: "range",
        dateFormat: "d/m/Y",
        locale:"en"
    });

    $('#employeeInputEmployeeAllInputEmployeeAll').css('color','white');

    $('#employeeInputEmployeeAllInputEmployeeAll').keypress(function () {
        AutocompleteEmployeeAll.search($('#employeeInputEmployeeAllInputEmployeeAll').val());
    })

    // $('#app_selected_btn').on('click',function () {
    //     alert('test approve');
    //     // approveSelectedApprove(dataApproves);
    //     $('#app_selected_btn').attr('disabled',true);
    //     $('#app_selected').css("filter","grayscale(100%)");
    //     dataApproves.length = 0;
    //
    //     $('.dv-background').show();
    //     window.setTimeout(function () {
    //         searchApprove();
    //         $('.dv-background').hide();
    //     },1000);
    //
    // });
    //
    // $('#exp_selected_btn').on('click',function () {
    //     alert('test expense');
    //     // approveSelectedExpense(dataExpense);
    //     $('#exp_selected_btn').attr('disabled',true);
    //     $('#exp_selected').css("filter","grayscale(100%)");
    //     dataExpense.length = 0;
    //
    //     $('.dv-background').show();
    //     window.setTimeout(function () {
    //         searchExpense();
    //         $('.dv-background').hide();
    //     },1000);
    //
    // });


    $("#incomingFilter").click(function () {
        $(this).find('img').toggle();
    })

})


function searchAllDocument() {

    let documentStatus = "";
    $.each( $( ".checkbox_status:checked" ), function( i, obj ) {
        documentStatus += ","+obj.value;
    });

    if(documentStatus){
        documentStatus = documentStatus.substring(1);
        $('#search_doc_status').val(documentStatus);
    }else{
        $(".checkbox_status").prop('checked', true);
        documentStatus = $('#search_doc_status_all').val();
    }

    let documentType = "";
    $.each( $( ".checkbox_status1:checked" ), function( i, obj ) {
        documentType += ","+obj.value;
    });

    if(documentType){
        documentType = documentType.substring(1);
        $('#search_doc_type').val(documentType);
    }else{
        $(".checkbox_status1").prop('checked', true);
        documentType = $('#search_doc_type_all').val();
    }

    // var today = new Date().format("dd/mm/yyyy");

    var temp = $('#createDate_picker').val();
    var startDate = (temp == '' || temp == null || temp == undefined) ? '' : temp.substring(0,temp.indexOf(' to'));
    var endDate = (temp == '' || temp == null || temp == undefined) ? '' : temp.substring(temp.indexOf('to ') + 3);

    var requester = '' ;
    if ($('#employeeInputEmployeeAllInputEmpAll').attr('data-username') !== undefined){
        requester = $('#employeeInputEmployeeAllInputEmployeeAll').attr('data-username');
    }

    var criteriaObject = {
        createdDateStart  : startDate,
        createdDateEnd : endDate,
        creator : requester,
        documentNumber : $('#documentNumber_input').val() == "" ? '' : $('#documentNumber_input').val(),
        documentType : documentType,//MASTER_DATA.DOC_APP_TYPE + ',' + MASTER_DATA.DOC_EXP_TYPE + ',' + MASTER_DATA.DOC_ADV_TYPE ,
        documentStatus : documentStatus
    };


    try {
        findAllDocument(criteriaObject);
    }catch (e){
        console.error('searchAllDocument');
        console.error(e);
    }
}

function findAllDocument(criteriaObject){

    object_expense.setId("#paggingAllDoc");
    object_expense.setUrlData("/viewDoc/viewAllDoc");
    object_expense.setUrlSize("/viewDoc/viewAllDocSize");

    object_expense.loadTable = function(item) {

        $('#panel_content').empty();

        let nextApprover;
        let sumDateProcess = 0;
        let record;
        let isPaid;
        let DocumentTYPE;

        if(item.length > 0){
            for(let i = 0 ; i < item.length; i++){
                sumDateProcess = 0;
                record = '';
                isPaid = false;
                if(item[i].id){

                    DocumentTYPE = item[i].documentType;

                    let $APPROVERS = $.ajax({
                        url: session.context + '/myApprove/getApprover/' + item[i].requests.id,
                        headers: {
                            Accept : "application/json"
                        },
                        type: "GET",
                        async: false
                    }).responseJSON;

                    $APPROVERS = $APPROVERS.requestApprovers;

                    $APPROVERS.sort(function (a, b) {
                        return a.sequence > b.sequence;
                    });

                    let $APPROVER_CANCEL;
                    let $APPROVER_ACTIONSTATE;
                    for(let j = 0 ; j < $APPROVERS.length ; j++) {
                        if($APPROVERS[j].actionTime == null) {
                            $APPROVER_CANCEL = $APPROVERS[j].userNameApprover;
                            $APPROVER_ACTIONSTATE = $APPROVERS[j].actionState;
                            break;
                        }
                    }

                    // console.log('$APPROVER_CANCEL -> ' + $APPROVER_CANCEL);

                    record += '' +
                        '<div class="row">' +
                        '<div class="col-sm-10">' +
                        '<div class="col-sm-1" style="padding-top: 10px; padding-left: 0; padding-right: 0; text-align: right">';

                    // if (item[i].documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS || item[i].documentStatus == MASTER_DATA.DOC_STATUS_DRAFT) {
                    if (item[i].documentStatus == MASTER_DATA.DOC_STATUS_ON_PROCESS) {
                        if (item[i].requests) {
                            nextApprover = findEmployeeProfileByUserName(item[i].requests.nextApprover);
                            record += '' +
                                // '<a title="' + $MESSAGE_CANCEL_REQUEST_TOOLTIP + '"  onclick="$(\'#id_haveRequest\').val(' + item[i].id + '),$(\'#docNumber_haveRequest\').val(\'' + item[i].docNumber + '\'),$(\'#type_haveRequest\').val(\'' + item[i].documentType + '\'),$(\'#approveType_haveRequest\').val(\'' + item[i].approveType + '\'),$(\'#approver_haveRequest\').val(\'' + $APPROVER_CANCEL + '\')" data-target="#haveRequestModal" data-toggle="modal">' +
                                '<a title="' + $MESSAGE_CANCEL_REQUEST_TOOLTIP + '"  onclick="$(\'#approver_haveRequest\').val(\'' + $APPROVER_CANCEL + '\'),$(\'#docNumber_haveRequest\').val(\'' + item[i].docNumber + '\'),$(\'#type_haveRequest\').val(\'' + item[i].documentType + '\')' +
                                ',$(\'#processId_haveRequest\').val(\'' + item[i].processId + '\'),$(\'#id_haveRequest\').val(' + item[i].id + '),$(\'#docFlow_haveRequest\').val(\'' + item[i].docFlow + '\'),$(\'#actionState_haveRequest\').val(\'' + $APPROVER_ACTIONSTATE + '\'),$(\'#requestId_haveRequest\').val(' + item[i].requests.id + ')" data-target="#haveRequestModal" data-toggle="modal">' +
                                '<img src="' + $IMAGE_CANCEL + '" width="40px"/>&#160;' +
                                '</a>' +
                                '<a title="' + $MESSAGE_VIEW_DOC_TOOLTIP + '" onclick="ViewDoc(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')">' +
                                '<img src=' + $IMAGE_MENU + ' width="35px"/>&#160;' +
                                '</a>';
                        } else {
                            record += '' +
                                '<a title="' + $MESSAGE_CANCEL_DOC_TOOLTIP + '"  onclick="$(\'#idDoc_noQuest\').val(\'' + item[i].id + '\'),$(\'#typeDoc_noQuest\').val(\'' + item[i].documentType + '\'),$(\'#docnumber_noRequest\').empty(),$(\'#docnumber_noRequest\').append(\'' + item[i].docNumber + '\')"  data-target="#noRequestModal" data-toggle="modal">' +
                                '<img src="' + $IMAGE_CANCEL + '" width="40px"/>&#160;' +
                                '</a>' +
                                '<a title="' + $MESSAGE_VIEW_DRAFT_DOC_TOOLTIP + '" onclick="viewDraft(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')">' +
                                '<img src=' + $IMG_PENCIL + ' width="35px"/>&#160;' +
                                '</a>';
                        }
                    }else{
                        record += '' +
                            '<a title="' + $MESSAGE_VIEW_DOC_TOOLTIP + '" onclick="ViewDoc(' + item[i].id + ',\'' + item[i].documentType + '\',\'' + item[i].approveType + '\',\'' + item[i].createdBy + '\')">' +
                            '<img src=' + $IMAGE_MENU + ' width="35px"/>&#160;' +
                            '</a>';
                    }



                    let dateCreated;

                    if(item[i].createdDate){
                        let createdDate = new Date(item[i].createdDate);
                        let dateCreate = createdDate.getDate();
                        let monthCreate = createdDate.getMonth() + 1;
                        let yearCreate = createdDate.getFullYear();
                        let hourCreate = createdDate.getHours();
                        let minutesCreate = createdDate.getMinutes();

                        if(dateCreate < 10) dateCreate = '0' + dateCreate;
                        if(monthCreate < 10) monthCreate = '0' + monthCreate;
                        if(minutesCreate < 10) minutesCreate = '0' + minutesCreate;
                        if(hourCreate < 10) hourCreate = '0' + hourCreate;

                        dateCreated = yearCreate + '-' + monthCreate + '-' + dateCreate + ' ' + hourCreate + ':' + minutesCreate;
                    }else{
                        dateCreated = '-';
                    }



                    record += '</div>' +
                        '<div class="col-sm-2" style="text-align: left; padding-right: 0px;">' +
                        '<div class="col-sm-12" style="padding-right: 0px; white-space: nowrap; overflow: hidden;text-overflow: ellipsis; direction: rtl;" title="' + item[i].docNumber + '">' +
                        item[i].docNumber +
                        '</div>' +
                        '<div class="col-sm-12" style="color: orange; white-space: nowrap; overflow: hidden;text-overflow: ellipsis;" title="' + (item[i].titleDescription ? item[i].titleDescription : '-') + '">' +
                        (item[i].titleDescription ? item[i].titleDescription : '-') +
                        '</div>' +
                        '<div class="col-sm-12" style="color: deepskyblue; font-size: 12px;"> ' +
                        // ((item[i].createdDate) ? item[i].createdDate.substring(0, item[i].createdDate.lastIndexOf(':')) : '-') +
                        dateCreated +
                        '</div>' +
                        '<div class="col-sm-12">' +
                        ((item[i].documentStatus == MASTER_DATA.DOC_STATUS_DRAFT) ? '<span style="background-color: red; color: white; font-weight: 900;">DRAFT</span>' : '') +
                        '</div>' +
                        '</div>';

                    if (item[i].requests) {
                        // $.ajax({
                        //     url: session.context + '/myApprove/getApprover/' + item[i].requests.id,
                        //     type: "GET",
                        //     async: false,
                        //     complete: function (xhr) {

                                let docRemark = $.ajax({
                                    url : session.context + '/documentAccountRemarks/findByDocumentId?docId=' + item[i].id,
                                    headers: {
                                        Accept : "application/json"
                                    },
                                    type: "GET",
                                    async: false
                                }).responseJSON;

                                let itemRemark,accountDays;
                                if(docRemark.content.length > 0){
                                    itemRemark = docRemark.content[0];
                                    if(itemRemark.startDate){
                                        accountDays = accountProcessDays(itemRemark.startDate,itemRemark.endDate)
                                    }
                                }


                                // $APPROVERS = JSON.parse(xhr.responseText);


                                // $APPROVERS = $APPROVERS.requestApprovers;
                                //
                                // $APPROVERS.sort(function (a, b) {
                                //     return a.sequence > b.sequence;
                                // });

                                record += '' +
                                    '<div class="col-sm-5">' +
                                    '<ol class="progress" data-steps="' + $APPROVERS.length + '" style="height: 100%;">';

                                let checkActive = 0;
                                let oneDay = 24 * 60 * 60 * 1000;
                                let diffDays;
                                let nameLast;
                                let dateLast;
                                let start_time;
                                let end_time;

                                let to_day = new Date();
                                let days = to_day.getDate();
                                let month = to_day.getMonth() + 1; //January is 0!
                                let year = to_day.getFullYear();
                                let hours = to_day.getHours();
                                let minutes = to_day.getMinutes();

                                if (days < 10)
                                    days = '0' + days;
                                if (month < 10)
                                    month = '0' + month;
                                if (hours < 10)
                                    hours = '0' + hours;
                                if (minutes < 10)
                                    minutes = '0' + minutes;


                                to_day = year+'-'+month+'-'+days+' '+hours+':'+minutes;

                                sumDateProcess = 0;

                                // if($APPROVERS[0].requestStatusCode == MASTER_DATA.DOC_STATUS_REJECT){
                                //
                                //     let todaySend = new Date(item[0].sendDate);
                                //     let dateSend = todaySend.getDate() < 10 ? '0' + todaySend.getDate() : todaySend.getDate();
                                //     let monthSend = (todaySend.getMonth() + 1) < 10 ? '0' + (todaySend.getMonth() + 1) : todaySend.getMonth() + 1;
                                //     let yearSend = todaySend.getFullYear();
                                //     let hourSend = todaySend.getHours() < 10 ? '0' + todaySend.getHours() : todaySend.getHours();
                                //     let minuteSend = todaySend.getMinutes() < 10 ? '0' + todaySend.getMinutes() : todaySend.getMinutes();
                                //     let TempSendDate = yearSend + '-' + monthSend + '-' + dateSend +' ' + hourSend + ':' + minuteSend;
                                //
                                //     let actionDate = new Date($APPROVERS[0].actionTime);
                                //     let actionDay = actionDate.getDate() < 10 ? '0' + actionDate.getDate() : actionDate.getDate();
                                //     let actionoMonth = (actionDate.getMonth() + 1) < 10 ? '0' + (actionDate.getMonth() + 1) : actionDate.getMonth() + 1;
                                //     let actionYear = actionDate.getFullYear();
                                //     let actionHour = actionDate.getHours() < 10 ? '0' + actionDate.getHours() : actionDate.getHours();
                                //     let actionMinute = actionDate.getMinutes() < 10 ? '0' + actionDate.getMinutes() : actionDate.getMinutes();
                                //
                                //     let actionTemp  = actionYear + '-' + actionoMonth + '-' + actionDay + ' ' + actionHour + ':' + actionMinute;
                                //
                                //
                                //     start_time = moment(TempSendDate,moment.ISO_8601);
                                //     end_time = moment(actionTemp,moment.ISO_8601);
                                //
                                //     diffDays = end_time.diff(start_time,'days');
                                //
                                //     sumDateProcess += diffDays;
                                //
                                //     record+= '' +
                                //         '<li class="reject" style="text-align: left;">'+
                                //             '<div style="text-align: center">'+
                                //                 '<span class="step">'+
                                //                     '<span>'+
                                //                         $APPROVERS[0].actionState+
                                //                     '</span>'+
                                //                 '</span>'+
                                //                 '<span class="name">'+diffDays+'</span>'+
                                //             '</div>'+
                                //         '</li>';
                                //
                                // }else if ($APPROVERS[0].actionTime) {
                                //
                                //     let actionDate = new Date($APPROVERS[0].actionTime);
                                //     let actionDay = actionDate.getDate() < 10 ? '0' + actionDate.getDate() : actionDate.getDate();
                                //     let actionoMonth = (actionDate.getMonth() + 1) < 10 ? '0' + (actionDate.getMonth() + 1) : actionDate.getMonth() + 1;
                                //     let actionYear = actionDate.getFullYear();
                                //     let actionHour = actionDate.getHours() < 10 ? '0' + actionDate.getHours() : actionDate.getHours();
                                //     let actionMinute = actionDate.getMinutes() < 10 ? '0' + actionDate.getMinutes() : actionDate.getMinutes();
                                //
                                //     let actionTemp  = actionYear + '-' + actionoMonth + '-' + actionDay + ' ' + actionHour + ':' + actionMinute;
                                //
                                //     let todaySend = new Date(item[i].sendDate);
                                //     let dateSend = todaySend.getDate() < 10 ? '0' + todaySend.getDate() : todaySend.getDate();
                                //     let monthSend = (todaySend.getMonth() + 1) < 10 ? '0' + (todaySend.getMonth() + 1) : todaySend.getMonth() + 1;
                                //     let yearSend = todaySend.getFullYear();
                                //     let hourSend = todaySend.getHours() < 10 ? '0' + todaySend.getHours() : todaySend.getHours();
                                //     let minuteSend = todaySend.getMinutes() < 10 ? '0' + todaySend.getMinutes() : todaySend.getMinutes();
                                //
                                //     let TempSendDate = yearSend + '-' + monthSend + '-' + dateSend +' ' + hourSend + ':' + minuteSend;
                                //
                                //     start_time = moment(TempSendDate, moment.ISO_8601);
                                //     end_time = moment(actionTemp, moment.ISO_8601);
                                //
                                //     diffDays = end_time.diff(start_time, 'days');
                                //
                                //     sumDateProcess += diffDays;
                                //     record += '' +
                                //         '<li class="done" style="text-align: center;">' +
                                //             '<div style="text-align: center">' +
                                //                 '<span class="step">' +
                                //                     '<span>' +
                                //                         $APPROVERS[0].actionState +
                                //                     '</span>' +
                                //                 '</span>' +
                                //                 '<span class="name">' +
                                //                     diffDays +
                                //                 '</span>' +
                                //             '</div>' +
                                //         '</li>';
                                //
                                // } else if ($APPROVERS[0].userNameApprover == item[i].requests.nextApprover) {
                                //
                                //     let todaySend = new Date(item[i].sendDate);
                                //     let dateSend = todaySend.getDate() < 10 ? '0' + todaySend.getDate() : todaySend.getDate();
                                //     let monthSend = (todaySend.getMonth() + 1) < 10 ? '0' + (todaySend.getMonth() + 1) : todaySend.getMonth() + 1;
                                //     let yearSend = todaySend.getFullYear();
                                //     let hourSend = todaySend.getHours() < 10 ? '0' + todaySend.getHours() : todaySend.getHours();
                                //     let minuteSend = todaySend.getMinutes() < 10 ? '0' + todaySend.getMinutes() : todaySend.getMinutes();
                                //
                                //     let TempSendDate = yearSend + '-' + monthSend + '-' + dateSend +' ' + hourSend + ':' + minuteSend;
                                //
                                //     start_time = moment(TempSendDate, moment.ISO_8601);
                                //     end_time = moment(to_day, moment.ISO_8601);
                                //
                                //     diffDays = end_time.diff(start_time,'days');
                                //
                                //     sumDateProcess += diffDays;
                                //
                                //     record += '' +
                                //         '<li class="active" style="text-align: left;">' +
                                //             '<div style="text-align: center">' +
                                //                 '<span class="step">' +
                                //                     '<span>'
                                //                         + $APPROVERS[0].actionState +
                                //                     '</span>' +
                                //                 '</span>' +
                                //                 '<span class="name">' + diffDays + '</span>' +
                                //             '</div>' +
                                //         '</li>';
                                //
                                // }
                                //
                                // for (let j = 1; j < $APPROVERS.length; j++) {
                                //     let actionState = $APPROVERS[j].actionState;
                                //
                                //     if($APPROVERS[j].requestStatusCode == MASTER_DATA.DOC_STATUS_REJECT){
                                //         start_time = moment($APPROVERS[j-1].actionTime,moment.ISO_8601);
                                //         end_time = moment($APPROVERS[j].actionTime,moment.ISO_8601);
                                //
                                //         diffDays = end_time.diff(start_time,'days');
                                //
                                //         sumDateProcess = sumDateProcess + diffDays;
                                //
                                //         record +=
                                //             '<li class="reject" style="text-align: left;">'+
                                //                 '<div style="text-align: center">'+
                                //                     '<span class="step">'+
                                //                         '<span>'+
                                //                             $APPROVERS[j].actionState+
                                //                         '</span>'+
                                //                     '</span>'+
                                //                     '<span class="name">'+diffDays+'</span>'+
                                //                 '</div>'+
                                //             '</li>';
                                //
                                //     }else if ($APPROVERS[j].actionState != null && $APPROVERS[j].requestStatusCode != null) {
                                //
                                //         if (checkActive == 0) {
                                //
                                //             start_time = moment($APPROVERS[j-1].actionTime, moment.ISO_8601);
                                //             end_time = moment($APPROVERS[j].actionTime, moment.ISO_8601);
                                //
                                //             diffDays = end_time.diff(start_time, 'days');
                                //
                                //             if(actionState.toUpperCase().indexOf('ACC') >= 0){
                                //                 console.log('diff for acc done : ' + diffDays);
                                //             }
                                //
                                //             sumDateProcess = sumDateProcess + diffDays;
                                //             record +=
                                //                 '<li class="done" style="text-align: center;">' +
                                //                     '<div style="text-align: center">' +
                                //                         '<span class="step">' +
                                //                             '<span>' +
                                //                                 $APPROVERS[j].actionState +
                                //                             '</span>' +
                                //                         '</span>' +
                                //                         '<span class="name">' +
                                //                             (actionState.toUpperCase().indexOf('ACC') >= 0 ? (accountDays ? diffDays - accountDays : diffDays) : diffDays) +
                                //                         '</span>' +
                                //                     '</div>' +
                                //                 '</li>';
                                //
                                //         } else {
                                //
                                //             record +=
                                //                 '<li  style="text-align: center;">' +
                                //                     '<div style="text-align: center">' +
                                //                         '<span class="step">' +
                                //                             '<span>'
                                //                                 + $APPROVERS[j].actionState +
                                //                             '</span>' +
                                //                         '</span>' +
                                //                         '<span class="name" style="color: transparent;">diffDays</span>' +
                                //                     '</div>' +
                                //                 '</li>';
                                //
                                //         }
                                //     } else {
                                //         checkActive++
                                //         if (checkActive > 1) {
                                //             record +=
                                //                 '<li>' +
                                //                     '<span class="step">' +
                                //                         '<span>'
                                //                             + $APPROVERS[j].actionState +
                                //                         '</span>' +
                                //                     '</span>' +
                                //                     '<span class="name" style="color: transparent">' +
                                //                         'Waiting' +
                                //                     '</span>' +
                                //                 '</li>';
                                //
                                //         } else {
                                //
                                //             nameLast = $APPROVERS[j].approver;
                                //             dateLast = $APPROVERS[j].receiveTime;
                                //
                                //             if ($APPROVERS[j].userNameApprover != $APPROVERS[j-1].userNameApprover) {
                                //                 if($APPROVERS[j-1].actionTime) {
                                //
                                //                     if(($APPROVERS[j].actionState).toUpperCase().indexOf("PAID") >= 0){
                                //                         isPaid = true;
                                //                     }
                                //
                                //                     start_time = moment($APPROVERS[j].createdDate, moment.ISO_8601);
                                //                     end_time = moment(to_day, moment.ISO_8601);
                                //
                                //                     diffDays = end_time.diff(start_time, 'days');
                                //                     sumDateProcess = sumDateProcess + diffDays;
                                //
                                //                     if(actionState.toUpperCase().indexOf('ACC') >= 0){
                                //                         console.log('diff for acc active : ' + diffDays);
                                //                     }
                                //
                                //                     record +=
                                //                         '<li class="active" style="text-align: left;">' +
                                //                             '<div style="text-align: center">' +
                                //                                 '<span class="step">' +
                                //                                     '<span>' +
                                //                                         $APPROVERS[j].actionState +
                                //                                     '</span>' +
                                //                                 '</span>' +
                                //                                 '<span class="name">' +
                                //                                     (actionState.toUpperCase().indexOf('ACC') >= 0 ? (accountDays ? diffDays - accountDays : diffDays) : diffDays) +
                                //                                 '</span>' +
                                //                             '</div>' +
                                //                         '</li>';
                                //
                                //                 }else{
                                //                     record +=
                                //                         '<li>' +
                                //                             '<span class="step">' +
                                //                                 '<span>'
                                //                                     + $APPROVERS[j].actionState +
                                //                                 '</span>' +
                                //                             '</span>' +
                                //                             '<span class="name" style="color: transparent">' +
                                //                                 'Waiting' +
                                //                             '</span>' +
                                //                         '</li>';
                                //
                                //                 }
                                //
                                //             } else {
                                //                 record += '' +
                                //                     '<li>' +
                                //                         '<span class="step">' +
                                //                             '<span>'
                                //                                 + $APPROVERS[j].actionState +
                                //                             '</span>' +
                                //                         '</span>' +
                                //                         '<span class="name" style="color: transparent">a</span>' +
                                //                     '</li>';
                                //
                                //             }
                                //         }
                                //     }
                                // }


                                if($APPROVERS[0].requestStatusCode == MASTER_DATA.DOC_STATUS_REJECT){

                                    let todaySend = new Date(item[0].sendDate);
                                    let dateSend = todaySend.getDate() < 10 ? '0' + todaySend.getDate() : todaySend.getDate();
                                    let monthSend = (todaySend.getMonth() + 1) < 10 ? '0' + (todaySend.getMonth() + 1) : todaySend.getMonth() + 1;
                                    let yearSend = todaySend.getFullYear();
                                    let hourSend = todaySend.getHours() < 10 ? '0' + todaySend.getHours() : todaySend.getHours();
                                    let minuteSend = todaySend.getMinutes() < 10 ? '0' + todaySend.getMinutes() : todaySend.getMinutes();
                                    let TempSendDate = yearSend + '-' + monthSend + '-' + dateSend +' ' + hourSend + ':' + minuteSend;

                                    start_time = moment(TempSendDate,moment.ISO_8601);
                                    end_time = moment($APPROVERS[0].actionTime,moment.ISO_8601);

                                    diffDays = end_time.diff(start_time,'days');

                                    sumDateProcess += diffDays;

                                    record+= '' +
                                        '<li class="reject" style="text-align: left;">'+
                                        '<div style="text-align: center">'+
                                        '<span class="step">'+
                                        '<span>'+
                                        $APPROVERS[0].actionState+
                                        '</span>'+
                                        '</span>'+
                                        '<span class="name">'+diffDays+'</span>'+
                                        '</div>'+
                                        '</li>';

                                }else if($APPROVERS[0].actionTime){

                                    let todaySend = new Date(item[i].sendDate);
                                    let dateSend = todaySend.getDate() < 10 ? '0' + todaySend.getDate() : todaySend.getDate();
                                    let monthSend = (todaySend.getMonth() + 1) < 10 ? '0' + (todaySend.getMonth() + 1) : todaySend.getMonth() + 1;
                                    let yearSend = todaySend.getFullYear();
                                    let hourSend = todaySend.getHours() < 10 ? '0' + todaySend.getHours() : todaySend.getHours();
                                    let minuteSend = todaySend.getMinutes() < 10 ? '0' + todaySend.getMinutes() : todaySend.getMinutes();
                                    let TempSendDate = yearSend + '-' + monthSend + '-' + dateSend +' ' + hourSend + ':' + minuteSend;

                                    start_time = moment(TempSendDate,moment.ISO_8601);
                                    end_time = moment($APPROVERS[0].actionTime,moment.ISO_8601);

                                    diffDays = end_time.diff(start_time,'days');

                                    sumDateProcess = sumDateProcess + diffDays;
                                    record += '' +
                                        '<li class="done" style="text-align: center;">'+
                                        '<div style="text-align: center">'+
                                        '<span class="step">'+
                                        '<span>'+
                                        $APPROVERS[0].actionState +
                                        '</span>'+
                                        '</span>'+
                                        '<span class="name">'+
                                        diffDays+
                                        '</span>'+
                                        '</div>'+
                                        '</li>';

                                    // checkFirst = 1;
                                }
                                else if($APPROVERS[0].userNameApprover === item[i].requests.nextApprover){

                                    let todaySend = new Date(item[i].sendDate);
                                    let dateSend = todaySend.getDate() < 10 ? '0' + todaySend.getDate() : todaySend.getDate();
                                    let monthSend = (todaySend.getMonth() + 1) < 10 ? '0' + (todaySend.getMonth() + 1) : todaySend.getMonth() + 1;
                                    let yearSend = todaySend.getFullYear();
                                    let hourSend = todaySend.getHours() < 10 ? '0' + todaySend.getHours() : todaySend.getHours();
                                    let minuteSend = todaySend.getMinutes() < 10 ? '0' + todaySend.getMinutes() : todaySend.getMinutes();
                                    let TempSendDate = yearSend + '-' + monthSend + '-' + dateSend +' ' + hourSend + ':' + minuteSend;


                                    end_time = moment(to_day,moment.ISO_8601);
                                    start_time = moment(TempSendDate,moment.ISO_8601);
                                    diffDays = end_time.diff(start_time,'days');

                                    sumDateProcess = sumDateProcess+diffDays;

                                    nameLast =  $APPROVERS[0].approver;
                                    dateLast =  $APPROVERS[0].receiveTime;
                                    record += ''+
                                        '<li class="active" style="text-align: left;">'+
                                        '<div style="text-align: center">'+
                                        '<span class="step">'+
                                        '<span>'
                                        +$APPROVERS[0].actionState+
                                        '</span>'+
                                        '</span>'+
                                        '<span class="name">'+diffDays+'</span>'+
                                        '</div>'+
                                        '</li>';

                                    // }else if(item[i].documentStatus == MASTER_DATA.DOC_STATUS_CANCEL){
                                    //     record += '' +
                                    //         '<div class="col-sm-4">' +
                                    //             '<ol class="progress" data-steps="3" style="height: 100%;">' +
                                    //                 '<li class="cancel">' +
                                    //                     '<span class="step"><span>VRF</span></span>' +
                                    //                     '<span class="name" style="color: transparent">1 Day</span>' +
                                    //                 '</li>' +
                                    //                 '<li class="cancel">' +
                                    //                     '<span class="step"><span>APR</span></span>' +
                                    //                     '<span  class="name" style="color: transparent" >5 Days</span>' +
                                    //                 '</li>' +
                                    //                 '<li class="cancel">' +
                                    //                     '<span class="step"><span >ADM</span></span>' +
                                    //                     '<span class="name" style="color: transparent" >5 Days</span>' +
                                    //                 '</li>' +
                                    //             '</ol>' +
                                    //         '</div>';
                                }else{
                                    record += ''+
                                        '<li>'+
                                        '<span class="step">'+
                                        '<span>'
                                        +$APPROVERS[0].actionState+
                                        '</span>'+
                                        '</span>'+
                                        '<span class="name" style="color: transparent">'+
                                        'Waiting'+
                                        '</span>'+
                                        '</li>';

                                }

                                for(let j =  1; j <$APPROVERS.length ; j++){

                                    if($APPROVERS[j].requestStatusCode == MASTER_DATA.DOC_STATUS_REJECT){
                                        start_time = moment($APPROVERS[j-1].actionTime,moment.ISO_8601);
                                        end_time = moment($APPROVERS[j].actionTime,moment.ISO_8601);

                                        diffDays = end_time.diff(start_time,'days');

                                        sumDateProcess = sumDateProcess + diffDays;

                                        record +=
                                            '<li class="reject" style="text-align: left;">'+
                                            '<div style="text-align: center">'+
                                            '<span class="step">'+
                                            '<span>'+
                                            $APPROVERS[j].actionState+
                                            '</span>'+
                                            '</span>'+
                                            '<span class="name">'+
                                            // ($APPROVERS[j].actionState.toUpperCase().indexOf('ACC') >= 0 ? (accountDays ? diffDays - accountDays : diffDays) : diffDays) +
                                            diffDays +
                                            '</span>'+
                                            '</div>'+
                                            '</li>';


                                    }else if($APPROVERS[j].actionTime){
                                        start_time = moment($APPROVERS[j-1].actionTime,moment.ISO_8601);
                                        end_time = moment($APPROVERS[j].actionTime,moment.ISO_8601);
                                        diffDays = end_time.diff(start_time,'days');

                                        sumDateProcess = sumDateProcess + diffDays;
                                        record += '' +
                                            '<li class="done" style="text-align: center;">'+
                                            ' <div style="text-align: center">'+
                                            ' <span class="step">'+
                                            ' <span>'+
                                            $APPROVERS[j].actionState +
                                            '</span>'+
                                            ' </span>'+
                                            ' <span class="name">'+
                                            // ($APPROVERS[j].actionState.toUpperCase().indexOf('ACC') >= 0 ? (accountDays ? diffDays - accountDays : diffDays) : diffDays) +
                                            diffDays +
                                            '</span>'+
                                            '</div>'+
                                            '</li>';

                                    }else if(($APPROVERS[j-1].actionTime && $APPROVERS[j-1].requestStatusCode != MASTER_DATA.DOC_STATUS_REJECT) && !$APPROVERS[j].actionTime){

                                        start_time = moment($APPROVERS[j-1].actionTime,moment.ISO_8601);
                                        end_time = moment(to_day,moment.ISO_8601);
                                        diffDays = end_time.diff(start_time,'days');

                                        sumDateProcess = sumDateProcess + diffDays;

                                        record +=
                                            '<li class="active" style="text-align: left;">'+
                                            '<div style="text-align: center">'+
                                            '<span class="step">'+
                                            '<span>'+
                                            $APPROVERS[j].actionState+
                                            '</span>'+
                                            '</span>'+
                                            '<span class="name">'+
                                            // ($APPROVERS[j].actionState.toUpperCase().indexOf('ACC') >= 0 ? (accountDays ? diffDays - accountDays : diffDays) : diffDays) +
                                            diffDays +
                                            '</span>'+
                                            '</div>'+
                                            '</li>';

                                    }else{
                                        record += ''+
                                            '<li>'+
                                            '<span class="step">'+
                                            '<span>'
                                            +$APPROVERS[j].actionState+
                                            '</span>'+
                                            '</span>'+
                                            '<span class="name" style="color: transparent">'+
                                            'Waiting'+
                                            '</span>'+
                                            '</li>';

                                    }
                                }

                                record +=
                                    '</ol></div>';
                            // }
                        // });

                        // }else if(item[i].documentStatus == MASTER_DATA.DOC_STATUS_CANCEL){
                        //     record += '' +
                        //         '<div class="col-sm-4">' +
                        //             '<ol class="progress" data-steps="3" style="height: 100%;">' +
                        //                 '<li class="cancel">' +
                        //                     '<span class="step"><span>VRF</span></span>' +
                        //                     '<span class="name" style="color: transparent">1 Day</span>' +
                        //                 '</li>' +
                        //                 '<li class="cancel">' +
                        //                     '<span class="step"><span>APR</span></span>' +
                        //                     '<span  class="name" style="color: transparent" >5 Days</span>' +
                        //                 '</li>' +
                        //                 '<li class="cancel">' +
                        //                     '<span class="step"><span >ADM</span></span>' +
                        //                     '<span class="name" style="color: transparent" >5 Days</span>' +
                        //                 '</li>' +
                        //             '</ol>' +
                        //         '</div>';
                    }else {
                        record += '' +
                            '<div class="col-sm-5">' +
                            '<ol class="progress" data-steps="3" style="height: 100%;">' +
                            '<li>' +
                            '<span class="step"><span>VRF</span></span>' +
                            '<span class="name" style="color: transparent">1 Day</span>' +
                            '</li>' +
                            '<li>' +
                            '<span class="step"><span>APR</span></span>' +
                            '<span  class="name" style="color: transparent" >5 Days</span>' +
                            '</li>' +
                            '<li>' +
                            '<span class="step"><span >ADM</span></span>' +
                            '<span class="name" style="color: transparent" >5 Days</span>' +
                            '</li>' +
                            '</ol>' +
                            '</div>';
                    }

                    let rgbCode;

                    if (DocumentTYPE == MASTER_DATA.DOC_APP_TYPE) {
                        APP_COLOR.forEach(function (item) {
                            if (sumDateProcess >= item.minDay && sumDateProcess <= item.maxDay) {
                                rgbCode = item.color;
                            }
                        })
                    } else if (DocumentTYPE == MASTER_DATA.DOC_EXP_TYPE) {
                        EXP_COLOR.forEach(function (item) {
                            if (sumDateProcess >= item.minDay && sumDateProcess <= item.maxDay) {
                                rgbCode = item.color;
                            }
                        })
                    } else {
                        ADV_COLOR.forEach(function (item) {
                            if (sumDateProcess >= item.minDay && sumDateProcess <= item.maxDay) {
                                rgbCode = item.color;
                            }
                        })
                    }

                    let sendDate;
                    if(item[i].sendDate){
                        let todaySend = new Date(item[i].sendDate);
                        let dateSend = todaySend.getDate() < 10 ? '0' + todaySend.getDate() : todaySend.getDate();
                        let monthSend = (todaySend.getMonth() + 1) < 10 ? '0' + (todaySend.getMonth() + 1) : todaySend.getMonth() + 1;
                        let yearSend = todaySend.getFullYear();
                        let hourSend = todaySend.getHours() < 10 ? '0' + todaySend.getHours() : todaySend.getHours();
                        let minuteSend = todaySend.getMinutes() < 10 ? '0' + todaySend.getMinutes() : todaySend.getMinutes();

                        sendDate = yearSend + '-' + monthSend + '-' + dateSend +' ' + hourSend + ':' + minuteSend;
                    }else {
                        sendDate = '-';
                    }

                    let lastActionTime;
                    if(item[i].requests){
                        if(item[i].requests.nextApprover){
                            if(item[i].requests.lastActionTime){
                                let lastActionDate = new Date(item[i].requests.lastActionTime);
                                let lastDate = lastActionDate.getDate() < 10 ? '0' + lastActionDate.getDate() : lastActionDate.getDate();
                                let lastMonth = (lastActionDate.getMonth() + 1) < 10 ? '0' + (lastActionDate.getMonth() + 1) : lastActionDate.getMonth() + 1;
                                let lastYear = lastActionDate.getFullYear();
                                let lastHour = lastActionDate.getHours() < 10 ? '0' + lastActionDate.getHours() : lastActionDate.getHours();
                                let lastMinute = lastActionDate.getMinutes() < 10 ? '0' + lastActionDate.getMinutes() : lastActionDate.getMinutes();

                                lastActionTime = lastYear + '-' + lastMonth + '-' + lastDate + ' ' + lastHour + ':' + lastMinute;
                            }else{
                                lastActionTime = sendDate;
                            }
                        }
                        else {
                            lastActionTime = sendDate;
                        }
                    }else{
                        lastActionTime = '-';
                    }

                    record += '' +
                        '<div class="col-sm-1" style="text-align: center; padding-top: 0px; font-size: 40px; color:' + rgbCode + '">' + sumDateProcess + '</div>' +
                        '<div class="col-sm-2" style="text-align: center; padding-top: 0px; padding-left: 0px; padding-right: 0px; text-align: left;">' +
                        '<div class="col-sm-12">' +
                        (isPaid ? $LABEL_BANK_TRANSFER : (((nextApprover != 'NaN undefined' && nextApprover)) ? nextApprover : '-')) +
                        '</div>' +
                        '<div class="col-sm-12" style="font-size: 12px; color: deepskyblue;">' +
                        // ((item[i].requests) ? (((item[i].requests.nextApprover) ? ((item[i].requests.lastActionTime) ? item[i].requests.lastActionTime.substring(0, item[i].requests.lastActionTime.lastIndexOf(':')) : sendDate ) : '-')) : '-') +
                        lastActionTime+
                        '</div>' +
                        '</div>' +
                        '<div class="col-sm-1" style="padding-top: 0px; padding-right: 0px; color: limegreen; text-align: right;">' +
                        '<b style="font-size: 15px;">' + ((item[i].totalAmount) ? item[i].totalAmount.toLocaleString(undefined, {minimumFractionDigits: 2}) : (0).toLocaleString(undefined, {minimumFractionDigits: 2}) ) + ' ฿</b>' +
                        '</div> ' +
                        '</div>';

                    if(MASTER_DATA.DOC_STATUS_ON_PROCESS == item[i].documentStatus){
                        record += '<div class="col-sm-1" style="display : inline-block;">' + $LABEL_ON_PROCESS_STATUS  + '</div>';
                    }else if(MASTER_DATA.DOC_STATUS_CANCEL  == item[i].documentStatus){
                        record += '<div class="col-sm-1" style="display : inline-block;">' + $LABEL_CANCEL_STATUS  + '</div>';
                    }else if(MASTER_DATA.DOC_STATUS_REJECT  == item[i].documentStatus){
                        record += '<div class="col-sm-1" style="display : inline-block;">' + $LABEL_REJECT_STATUS  + '</div>';
                    }else if(MASTER_DATA.DOC_STATUS_COMPLETE  == item[i].documentStatus){
                        record += '<div class="col-sm-1" style="display : inline-block;">' + $LABEL_COMPLETE_STATUS + '</div>';
                    }


                    if(item[i].documentType == MASTER_DATA.DOC_APP_TYPE){
                        record += '<div class="col-sm-1" style="display: inline-block"><img src="' + $IMG_APP + '" width="40px"/>&#160;</div></div>';
                    }else if(item[i].documentType == MASTER_DATA.DOC_EXP_TYPE){
                        record += '<div class="col-sm-1" style="display: inline-block"><img src="' + $IMG_EXP + '" width="40px"/>&#160;</div></div>';
                    }else{
                        record += '<div class="col-sm-1" style="display: inline-block"><img src="' + $IMG_ADV + '" width="40px"/>&#160;</div></div>';
                    }

                    record += '<hr style="margin-top: 10px; margin-bottom: 10px;"/>';


                    $('#panel_content').append(record);

                }
            }


        }else{
            console.log('data not found');
        }

    }

    $('.dv-background').hide();

    object_expense.setDataSearch(criteriaObject);
    object_expense.search(object_expense);
}


function cancelRequest(id,docNumber,type,approveType,createdBy,approveCancel){

    var listJsonDetails = [];

    var jsonDetails = {};
    jsonDetails['amount'] = 1;
    jsonDetails['flowType'] = validateFlowTypeByApproveType(approveType);

    listJsonDetails.push(jsonDetails);

    var jsonData = {};
    jsonData['id'] = id;
    jsonData['requester'] = createdBy;
    jsonData['docNumber'] = docNumber;
    jsonData['details'] = JSON.stringify(listJsonDetails);


    $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context'] + '/requests/cancelRequest',
        data:jsonData,
        async: false,
        complete: function (xhr) {
            if (xhr.readyState == 4) {
                $DATA_REQUEST = JSON.parse(xhr.responseText);
                if($DATA_REQUEST != null){
                    cancelDocument(id,type,approveType);
                }

            }
            $('#noRequestModal').modal('hide')
            $('#cancleDocMobile').modal('hide');
            $('#haveRequestModal').modal('hide');
            $('#cancleRequest_mobile').modal('hide');
        }
    });



}

function cancelDocument(id,type,approveType){

    $('.dv-background').show();
    window.setTimeout(function () {
        if(type == 'APP') {


            var jsonData = {};
            jsonData['parentId']     	= 1;
            jsonData[csrfParameter]  	= csrfToken;
            jsonData['id']    			= id
            jsonData['documentStatus']  = MASTER_DATA.DOC_STATUS_CANCEL

            var dataDocument = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/approve/updateDocumentStatus',
                data:jsonData,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        // $('#noRequestModal').modal('hide')
                        // $('#cancleDocMobile').modal('hide');
                        // $('#haveRequestModal').modal('hide');
                        // $('#cancleRequest_mobile').modal('hide');
                        // findByRequester_Moblie_Mod();
                        // searchApprove();
                        // searchExpense();
                        searchAllDocument();
                        // $('.dv-background').hide();
                    }

                }
            });

        }
        if(type == MASTER_DATA.DOC_EXP_TYPE) {


            var jsonData = {};
            jsonData['parentId']     	= 1;
            jsonData[csrfParameter]  	= csrfToken;
            jsonData['id']    			= id
            jsonData['documentStatus']  = MASTER_DATA.DOC_STATUS_CANCEL

            var dataDocument = $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/expense/updateDocumentStatus',
                data:jsonData,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {

                        // $('#noRequestModal').modal('hide')
                        // $('#cancleDocMobile').modal('hide');
                        // $('#haveRequestModal').modal('hide');
                        // $('#cancleRequest_mobile').modal('hide');
                        // // findByRequester_Moblie_Mod();
                        // findByRequesterAndDocumentType();
                        // searchApprove();
                        // searchExpense();
                        searchAllDocument();
                        $('.dv-background').hide();
                    }

                }
            });

        }

        if(type == MASTER_DATA.DOC_ADV_TYPE) {


            var jsonData = {}
            jsonData['parentId']     	= 1;
            jsonData[csrfParameter]  	= csrfToken;
            jsonData['id']    			= id;
            jsonData['documentStatus']  = MASTER_DATA.DOC_STATUS_CANCEL;

            $.ajax({
                type: "POST",
                headers: {
                    Accept: 'application/json'
                },
                url: session['context'] + '/advance/updateDocumentStatus',
                data:jsonData,
                async: false,
                complete: function (xhr) {
                    if (xhr.readyState == 4) {
                        $('#noRequestModal').modal('hide')
                        $('#cancleDocMobile').modal('hide');
                        $('#haveRequestModal').modal('hide');
                        $('#cancleRequest_mobile').modal('hide');
                        // findByRequester_Moblie_Mod();
                        findByRequesterAndDocumentType();
                        $('.dv-background').hide();
                    }

                }

            });

        }

    },500);

}


function validateFlowTypeByApproveType(approveType){
    if(approveType == MASTER_DATA.APR_TYPE_DOMESTIC){
        return MASTER_DATA.FLOW_TYPE_DOMESTIC;
    }else if(approveType == MASTER_DATA.APR_TYPE_FOREIGN){
        return MASTER_DATA.FLOW_TYPE_FOREIGN;
    }else if(approveType == MASTER_DATA.APR_TYPE_CAR){
        return MASTER_DATA.FLOW_TYPE_CAR;
    }else if(approveType == MASTER_DATA.APR_TYPE_HOTEL){
        return MASTER_DATA.FLOW_TYPE_HOTEL;
    }else if(approveType == MASTER_DATA.APR_TYPE_FIGHT_BOOKING){
        return MASTER_DATA.FLOW_TYPE_FIGHT_BOOKING;
    }
}

function findEmployeeProfileByUserName(userName){

    var data = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByUserName?userName="+userName,
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;

    $DATA_EMPLOYEE = data;

    return $DATA_EMPLOYEE.FOA+$DATA_EMPLOYEE.FNameTH+" "+$DATA_EMPLOYEE.LNameTH;
}

function ViewDoc(id,documentType,approveType,createdBy) {

    if(documentType == MASTER_DATA.DOC_APP_TYPE){
        window.location.href = session.context + '/approve/viewCreateDocSetDetail?doc=' + id;
    }else if(documentType == MASTER_DATA.DOC_EXP_TYPE){
        window.location.href = session.context + '/expense/expenseDetail?doc=' + id;
    }else {
        window.location.href = session.context + '/advance/advanceDetail?doc=' + id;
    }
}

function viewDraft(id,documentType,approveType,createdBy) {

    if(documentType == MASTER_DATA.DOC_APP_TYPE){
        window.location.href = session.context + '/approve/createDocSetDetail?doc=' + id;
    }else if(documentType == MASTER_DATA.DOC_EXP_TYPE){
        window.location.href = session.context + '/expense/clearExpenseDetail?doc=' + id;
    }else {
        window.location.href = session.context + '/advance/createDoc?doc=' + id;
    }
}

function findUserPA(){
    userPA = $.ajax({
        url: session.context + "/intermediaries/findAutoCompleteEmployeeByKeySearchAndUserName?userName=" + $USER_NAME + '&keySearch=%',
        headers: {
            Accept : "application/json"
        },
        type: "GET",
        async: false
    }).responseJSON;
}

function loadColor() {
    if(userPA[0]){
        $COLOR = $.ajax({
            url : session.context + '/parameters/findByParameterDetailCode?code='+ userPA[0].companyCode,
            type : "GET",
            async : false ,
            complete : function (xhr) {
                if(xhr.readyState == 4){
                    if(xhr.statusCode() == 200){
                        if(xhr.responseText){

                        }
                    }
                }
            }
        })
    }
}

function accountProcessDays(startedDate,endedDate) {
    let to_day = new Date();
    let days = to_day.getDate();
    let month = to_day.getMonth()+1; //January is 0!
    let year = to_day.getFullYear();
    let hours = to_day.getHours();
    let minutes = to_day.getMinutes();

    if(days<10)
        days = '0'+days;
    if(month<10)
        month = '0'+month;
    if(hours<10)
        hours = '0'+hours;
    if(minutes<10)
        minutes = '0' + minutes;

    to_day = year+'-'+month+'-'+days+' '+hours+':'+minutes;


    let endDate; //= moment(to_day,moment.ISO_8601);
    let startDate;
    if(endDate){
        startDate = moment(startedDate,moment.ISO_8601);
        endDate = moment(endedDate,moment.ISO_8601);
        return endDate.diff(startDate,'days');
    }else{
        startDate = moment(startedDate,moment.ISO_8601);
        endDate = moment(to_day,moment.ISO_8601);
        return endDate.diff(startDate,'days');
    }
}

function validateActionState(userName){
    for(var i=0;i<$DATA_REQUEST_APPROVER.length;i++){
        if(userName == $DATA_REQUEST_APPROVER[i].userNameApprover){
            return $DATA_REQUEST_APPROVER[i].actionState;
        }
    }
}

function rejectRequest(approveCancel,docNumber,docType,processId,docId,docFlow,actionState,requestId){
    $('.dv-background').show();
    var jsonData = {};
    jsonData['userName'] = approveCancel;
    jsonData['actionStatus'] = actionState;//validateActionState(approveCancel);
    jsonData['documentNumber'] = docNumber;
    jsonData['docType'] = docType;
    jsonData['documentFlow'] = docFlow;
    jsonData['processId'] = processId;
    jsonData['documentId'] = docId;
    jsonData['actionReasonCode'] = 'REJECT BY ADMIN';
    jsonData['actionReasonDetail'] = 'REJECT BY ADMIN';

    setTimeout(function () {
        $.ajax({
            type: "GET",
            headers: {
                Accept: 'application/json'
            },
            url: session['context'] + '/requests/'+ requestId,
            async: false,
            complete: function (xhr) {
                if (xhr.readyState == 4) {
                    if (xhr.responseText != "Error") {
                        var data_request = JSON.parse(xhr.responseText);
                        if (data_request.requestStatusCode == MASTER_DATA.REQ_STATUS_CXL) {
                            $('.dv-background').hide();
                            $('#warningModal .modal-body').html(MSG.MESSAGE_REQUEST_HAS_CANCEL);
                            $("#warningModal").modal('show');
                        } else if (data_request.requestStatusCode == MASTER_DATA.REQ_STATUS_REJ) {
                            $('.dv-background').hide();
                            $('#warningModal .modal-body').html(MSG.MESSAGE_REQUEST_HAS_REJECT);
                            $("#warningModal").modal('show');
                        } else if (data_request.requestStatusCode == MASTER_DATA.REQ_STATUS_CMP) {
                            $('.dv-background').hide();
                            $('#warningModal .modal-body').html(MSG.MESSAGE_REQUEST_HAS_COMPLETE);
                            $("#warningModal").modal('show');
                        } else {
                            $.ajax({
                                type: "POST",
                                headers: {
                                    Accept: 'application/json'
                                },
                                url: session['context'] + '/requests/rejectRequest',
                                data:jsonData,
                                async: false,
                                complete: function (xhr) {
                                    if (xhr.readyState == 4) {
                                        let $DATA_APPROVE = JSON.parse(xhr.responseText);

                                        if ($DATA_APPROVE != null || $DATA_APPROVE != undefined) {
                                            // window.location.href = session.context;
                                            $('#haveRequestModal').modal('hide');
                                            searchAllDocument();
                                            $('.dv-background').hide()
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });
    },1000);
}

function findRequestByDocument(id,docNumber,processId,docType){
    $('.dv-background').show();
    window.setTimeout(function(){
        var data1 = $.ajax({
            url: session.context + "/approve/findRequestByDocument/"+ id,
            headers: {
                Accept : "application/json"
            },
            type: "GET",
            async: false
        }).responseJSON;

        $DATA_REQUEST = data1;

        var data2 = $.ajax({
            url: session.context + "/approve/findRequestApproverByRequest/"+$DATA_REQUEST.id,
            headers: {
                Accept : "application/json"
            },
            type: "GET",
            async: false
        }).responseJSON;

        $DATA_REQUEST_APPROVER = data2;

        let actionState
        let approver
        let documentId
        let rejectReasonCode
        let requestStatus
        let userNameApprover

        for(let i=0;i<$DATA_REQUEST_APPROVER.length; i++){
            if($DATA_REQUEST_APPROVER[i].userNameApprover == $USER_NAME && $DATA_REQUEST_APPROVER[i].requestStatusCode === null){

                actionState = $DATA_REQUEST_APPROVER[i].actionState;
                approver = $DATA_REQUEST_APPROVER[i].approver;
                documentId = $DATA_REQUEST_APPROVER[i].id;
                rejectReasonCode = $DATA_REQUEST_APPROVER[i].requestStatusCode;
                requestStatus = $DATA_REQUEST_APPROVER[i].requestStatusCode;
                userNameApprover = $DATA_REQUEST_APPROVER[i].userNameApprover;
                break;
            }
        }

        var jsonData = {};
        jsonData['userName'] = $USERNAME;
        jsonData['actionStatus'] = validateActionState($USERNAME);
        jsonData['documentNumber'] = docNumber;
        jsonData['docType'] = docType;
        jsonData['documentFlow'] = $DATA_REQUEST.docFlow;
        jsonData['processId'] = processId;
        jsonData['documentId'] = id;
        jsonData['actionReasonCode'] = '';
        jsonData['actionReasonDetail'] = '';


        $.ajax({
            type : "POST",
            url: session.context + "/requests/approveRequest",
            data:jsonData,
            async: false,
            complete: function (xhr) {
                $('.dv-background').hide();
                modalSuccess($MESSAGE_APPROVE_SUCCESS);

                searchByNextApproverAndRequesterAndTitleDescription();
                // $.material.init()


            }
        });
    },500);


}