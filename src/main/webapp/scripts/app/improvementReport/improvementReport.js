$(document).ready(function () {
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(createChart);
})

function createChart() {
    var data = google.visualization.arrayToDataTable([
        ['สาเหตุความผิดพลาด', 'สัดส่วน'],
        ['เอกสารไม่ครบ',        60],
        ['เอกสารไม่ถูกต้อง',       18],
        ['กรอกรายละเอียดไม่ครบ',   15],
        ['อื่นๆ',              7]
    ]);

    var options = {
        // title: 'My Daily Activities',
        pieHole: 0.4,
        legend: { position: 'top' }
    };

    var chart = new google.visualization.PieChart(document.getElementById('chart'));
    chart.draw(data, options);
}