/**
 * 
 */

var object = $.extend({},UtilPagination);
var locationType = "D";
var locationJson;
var startPageR =1;
function isZero(e){
	var check = false;
	if(!$(e).val() || $(e).val() == 0|| $(e).val() == '0'||$(e).val() == 0.0|| $(e).val() == '0.0'){
		check = true;
	}
	return check;
}

function setZeroWhenNull(e){
	if(isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}
	
}

function saveRouteDistance(){
	//Define flagOther
	var flagActive = $('#modal_edit_flag_active').prop('checked')?true:false;
	var locationFromId =  $('#modal_edit_locationFrom').val();
	var locationToId =  $('#modal_edit_locationTo').val();
	var jsonLocationFrom = {};
	var jsonLocationTo = {};
	jsonLocationFrom['locationFrom']      = locationFromId;
	jsonLocationTo['locationTo']      = locationToId;


	var jsonParams = {};

	jsonParams['parentId']     	= 1;
	jsonParams[csrfParameter]  	= csrfToken;
	jsonParams['id']     		= $('#modal_edit_id').val();

	jsonParams['distance']   	= $('#modal_edit_distance').val();
	jsonParams['flagActive']  	= flagActive;
	jsonParams['createdBy'] 			= $('#modal_edit_createdBy').val();
	jsonParams['createdDate'] 			= $('#modal_edit_createdDate').val();
	jsonParams['updateBy'] 				= $('#modal_edit_updateBy').val();
	jsonParams['updateDate'] 			= $('#modal_edit_updateDate').val();
	jsonParams['locationFrom']     	= locationFromId;/*JSON.stringify(jsonLocationFrom);*/
	jsonParams['locationTo']     	= locationToId;
	var itemData = $.ajax({
        type: "POST",
        headers: {
            Accept: 'application/json'
        },
        url: session['context']+'/routeDistances/saveRouteDistance',
        data: jsonParams,
        complete: function (xhr) {
			searchRouteDistance();
			$('#editItemModal').modal('hide');
			$("#completeModal").modal('show');

        }
    }).done(function (){
        //close loader
    });

}

function editRouteDistance(id,fromId,toId){
	$.ajax({
        type: "GET",
        url: session['context']+'/routeDistances/'+id,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var locationObj = JSON.parse(xhr.responseText);

                    $('#modal_edit_id').val(locationObj.id);
            		$('#modal_edit_createdBy').val(locationObj.createdBy);
            		$('#modal_edit_createdDate').val(locationObj.createdDate);
            		$('#modal_edit_updateBy').val(locationObj.updateBy);
            		$('#modal_edit_updateDate').val(locationObj.updateDate);
            		$('#modal_edit_locationFrom').val(fromId);
					$('#modal_edit_locationTo').val(toId);
					$('#modal_edit_distance').val(locationObj.distance);
            		if(locationObj.flagActive){
            			$('#modal_edit_flag_active').prop('checked', true);
            		}else{
            			$('#modal_edit_flag_active').removeAttr( "checked" );
            		}

                }
            }
        }
    }).done(function (){
        //close loader
		$('#editItemModal').modal('show');
    });

}

function deleteRouteDistance(id){
	var jsonParams = {};
	jsonParams['parentId']     = 1;
	jsonParams[csrfParameter]  = csrfToken;

	$.ajax({
		type: "DELETE",
		url: session['context']+'/routeDistances/'+id,
		data: jsonParams,
		complete: function (xhr) {
			searchRouteDistance();
			$('#deleteItemModal').modal('hide');
			$('#completeModal').modal('show');
		}
	}).done(function (){
		//close loader
	});
}



function searchRouteDistance(){
	var locationFrom = "";
	var locationTo = "";
		if($("#search_locationFrom").val().length > 0){
			locationFrom = $("#search_locationFrom").val();
		}
		if($("#search_locationTo").val().length > 0){
			locationTo = $("#search_locationTo").val();
		}


	var criteriaObject = {
		locationFromDescription     : locationFrom,
		locationToDescription     : locationTo
	    };
	queryLocationByCriteria(criteriaObject);
}

function queryLocationByCriteria(criteriaObject){
	object.setId("#paggingSearchMain");                
    object.setUrlData("/routeDistances/findByCriteria");
    object.setUrlSize("/routeDistances/findSize");
    
    object.loadTable = function(items){
    	var item = items.content;
        // $('.dv-background').show();
        $('#gridMainBody').empty();
        if(item.length > 0){
        	$dataForQuery = item;
            var itemIdTmp ;
            for(var j=0;j<item.length;j++){
            	
                var locationFrom = item[j].locationFrom==null?"":item[j].locationFrom.description;
                var locationTo = item[j].locationTo==null?"":item[j].locationTo.description;
                var distance = item[j].distance==null?"":item[j].distance;
                var flagActive = item[j].flagActive==null?"":item[j].flagActive;

				var locationFromId = item[j].locationFrom==null?"":item[j].locationFrom.id;
				var locationToId = item[j].locationTo==null?"":item[j].locationTo.id;

                itemIdTmp = item[j].id;
                if(itemIdTmp){
                	$('#gridMainBody').append(''+
                            '<tr id="'+item[j].id+'" '+
                            ((j%2 == 0) ? ' class="trwhite" ' : ' class="trlightgray"')+'>'+
							((j%2 == 0) ? '<td class="text-center headcol tdFix"  style="width: 53px; ">' : '<td class="text-center headcol tdFixGrey"  style="width: 53px;">')+
							'<div class="checkbox" style="color: #2f2f2f;  margin-top: 0px;"><label><input type="checkbox" class="checkbox1" value="'+item[j].id+'"/><span class="checkbox-material " style="color: :#2f2f2f;"><span class="check " style="color: :#2f2f2f;" /></span></label></div>'+
							'</td>'+
							((j%2 == 0) ? '<td class="text-center headcol tdFix" style="width: 100px; left: 68px;">' : '<td class="text-center headcol tdFixGrey" style="width: 100px; left: 68px;">')+
							'	<button type="button" class="btn btn-material-blue-500  btn-style-small"  title="Edit" data-toggle="modal"  onclick="editRouteDistance('+item[j].id+','+locationFromId+','+locationToId+')" ><span class="fa fa-pencil-square-o"><jsp:text/></span></button>'+
							// '	<button type="button" class="btn btn-material-red-500  btn-style-small"  title="Delete"  data-toggle="modal" data-target="#deleteItemModal" onclick="$(\'#idItemDelete\').val(\''+item[j].id+'\');$(\'#locationFromItemDelete\').html(\''+locationFrom+'\');"><span class="fa fa-trash"><jsp:text/></span></button>'+
							'</td>'+
							((j%2 == 0) ? '<td class="text-center headcol tdFix" style="width: 110px; left: 163px;">' : '<td class="text-center headcol tdFixGrey" style="width: 110px; left: 163px;">')+
							((flagActive) ? '<img class="img-circle" src="'+$IMAGE_CHECKED+'" style="width: 25px;height: 25px;" />':'<img class="img-circle" src="'+$IMAGE_CANCEL+'" style="width: 25px;height: 25px;" />')+
							'</td>'+
                            '<td class="text-center">'+locationFrom+'</td>'+
                            '<td class="text-center">'+locationTo+'</td>'+
                            '<td class="text-center">'+distance+'</td>'+


                            '</tr>'
                        );
                }
                
            }

        }else{
        	//Not Found
        }
        
    };

    object.setDataSearch(criteriaObject); 
    object.search(object);
	if(startPageR==1){
		startPageR++;
		object.loadPage(($CRITERIA_PAGE*1)+1,object);
	}else{
		object.loadPage(1,object);
	}
}


function checkLocationDuplicate(locationFromId,locationToId,locationId){
	if(locationFromId!=locationToId){
	$.ajax({
        type: "GET",
        url: session['context']+'/routeDistances/findByLocationFromAndLocationTo?locationFrom='+locationFromId+'&locationTo='+locationToId,
        complete: function (xhr) {
        	if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                	if(xhr.responseText){
                		var locationObj = JSON.parse(xhr.responseText);
                        if(locationId != locationObj.id){
                        	$('#warningModal .modal-body').html($MESSAGE_HAS_DUPLICATE_LOCATION);
                			$('#warningModal').modal('show');
                        }else{
                        	saveRouteDistance();
                        }
                	}else{
                		saveRouteDistance();
                	}
                }
            }
        }
    }).done(function (){
        //close loader
    });
	} else {
		$('#warningModal .modal-body').html($MESSAGE_HAS_DUPLICATE_LOCATION);
		$('#warningModal').modal('show');
	}
}

function checkAllRoute() {
	$('#checkAll').click(function(event) {
		if(this.checked) {
			$('.checkbox1').each(function() {
				this.checked = true;
			});
		}else{
			$('.checkbox1').each(function() {
				this.checked = false;
			});
		}
	});

	$("#checkAll").prop("checked", false);
	$('.checkbox1').click(function() {
		if($('.checkbox1:checked').length == $('.checkbox1').length){
			$("#checkAll").prop("checked", true);
		}else{
			$("#checkAll").prop("checked", false);
		}
	});
}

function btnDelete(){
	var arrayCheckStatusCheckbox = [] ;
	for (var i = 0; i < $('.checkbox1').length; i++) {
		arrayCheckStatusCheckbox[i] = $('.checkbox1')[i].checked == true;
	}
	var statusCheckboxTrue = arrayCheckStatusCheckbox.indexOf(true);
	if(statusCheckboxTrue >= 0) {
		$('#deleteItemModal').modal('show');

	}else{
		$('#deleteCheckModal').modal('show');

	}
}

function getIdForDel(){
	var lengthOfResponseText;
	// var deleteSuccess = 0;
	// var deleteFail = 0;

	if($('.checkbox1').length != 0) {
		for (var i = 0; i < $('.checkbox1').length; i++) {
			if ($('.checkbox1')[i].checked == true) {
				var id = $('.checkbox1')[i].value;
				lengthOfResponseText = deleteRouteDistance(id);
				// if(lengthOfResponseText == 0){
				// 	deleteSuccess++;
				// }else{
				// 	deleteFail++;
				// }
			}
		}

		searchRouteDistance();
		// checkAlertForDelete(deleteSuccess , deleteFail);
	}
}


$(document).ready(function () {
	

	
	$('.numbersOnly').on("keydown",function(e){
        NumberUtil.numberOnly(e);
    });
	
	$('.amount2digit').on("keydown",function(e){
        NumberUtil.numberOnly(e);
        NumberUtil.numberFixDigit($(this),2);
    });
	
	$('#search_button').on('click', function () {
		searchRouteDistance();
		checkAllRoute();

	});
	
	$('#editItemModal').on('hidden.bs.modal', function () {
		$('#modal_edit_id').val('');
		$('#modal_edit_createdBy').val('');
		$('#modal_edit_createdDate').val('');
		$('#modal_edit_updateBy').val('');
		$('#modal_edit_updateDate').val('');
		$('#modal_edit_locationFrom').val('');
		$('#modal_edit_locationTo').val('');
		$('#modal_edit_distance').val('');
		$('#modal_edit_flag_active').prop('checked', true);

	});

	$('#completeModal').on('shown.bs.modal', function() {
		window.setTimeout(function(){
			$('#completeModal').modal('hide');
		},1000);
	});

	$('#editItemModalSave').on('click', function () {
		//validate Mandatory
		var errorMessage = "";
		
		
		if(!$('#modal_edit_locationFrom').val()){
			errorMessage += $MESSAGE_REQUIRE_locationFrom+"<br/>";
		}
		if(!$('#modal_edit_locationTo').val()){
			errorMessage += $MESSAGE_REQUIRE_DECRIPTION+"<br/>";
		}

		
		
		if(errorMessage){
			$('#warningModal .modal-body').html(errorMessage);
			$('#warningModal').modal('show');
		}else{
			//Check Mandatory Field
			checkLocationDuplicate($('#modal_edit_locationFrom').val(),$('#modal_edit_locationTo').val(),$('#modal_edit_id').val());
		}
		
	});
	locationList();
	searchRouteDistance();
	checkAllRoute();
});


function locationList() {
	var locationString = $.ajax({
		url: session['context'] + "/locations/findByLocationType",
		type: "GET",
		data:{locationType:locationType},
		async: false
	}).responseText;
	locationJson = JSON.parse(locationString);
	// $("#modal_edit_zoneCode").empty();
	$("#modal_edit_locationFrom").append("<option value="+''+"></option>");

	$.each(locationJson, function (index, item) {
		$("#modal_edit_locationFrom").append("<option value=" + item.id + ">" + item.description + "</option>");
	});

	$("#modal_edit_locationTo").append("<option value="+''+"></option>");
	$.each(locationJson, function (index, item) {
		$("#modal_edit_locationTo").append("<option value=" + item.id + ">" + item.description + "</option>");
	});
}