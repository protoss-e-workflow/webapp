package com.spt.app;

import com.spt.app.spring.configuration.ApplicationConfiguration;
import com.spt.app.spring.security.JwtFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import com.spt.app.spring.configuration.ApplicationInitializer;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.context.ConfigurableApplicationContext;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.TimeZone;

@SpringBootApplication
@ComponentScan(basePackages = "com.spt.app")
@EnableAutoConfiguration
@Import({ApplicationConfiguration.class})
public class GWFApplication extends SpringBootServletInitializer implements WebApplicationInitializer{

    static ConfigurableApplicationContext ctx;
    static ApplicationInitializer bean;

    @Value("${services.auth}")
    private String authService;

    @Value("${ewfApplicationKey}")
    private String ewfApplicationKey;

    @Value("${validateTokenURI}")
    private String validateEngineServer;

    @Value("${jwt.token.name}")
    private String jwtTokenName;

    @Value("${init.token.name}")
    private String initToken;

    @Bean
    public FilterRegistrationBean jwtFilter() {
        final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new JwtFilter());
        registrationBean.setInitParameters(Collections.singletonMap("services.auth", authService));
        registrationBean.addInitParameter("ewfApplicationKey", ewfApplicationKey);
        registrationBean.addInitParameter("validateTokenURI", validateEngineServer);
        registrationBean.addInitParameter("jwtTokenName", jwtTokenName);
        registrationBean.addInitParameter("initToken", initToken);
        registrationBean.addUrlPatterns("/");
        return registrationBean;
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(GWFApplication.class);
    }

    /*Set Time Zone */

//    @PostConstruct
//    void started() {
//
//        TimeZone.setDefault(TimeZone.getTimeZone("GMT+7"));
//
//    }

    public static void main(String[] args) {
        ctx = SpringApplication.run(GWFApplication.class, args);
        bean = ctx.getBean(ApplicationInitializer.class);
//        TimeZone.setDefault(TimeZone.getTimeZone("GMT+7"));
    }
}
