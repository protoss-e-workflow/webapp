package com.spt.app.controller.general;

import java.lang.reflect.Type;
import java.net.HttpCookie;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.app.aop.annotation.Loggable;
import com.spt.app.service.BaseCodeService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.MenuService;
import com.spt.app.service.UserAuthorizationService;
import com.spt.app.service.UserService;
import com.spt.app.spring.security.GrantedAuthorityTypeAdaptor;
import com.spt.app.util.SortMapUtil;
import flexjson.JSONSerializer;

@Controller
@RequestMapping("/menus")
public class MenuController extends BaseCommonController{

	static final Logger LOGGER = LoggerFactory.getLogger(MenuController.class);
	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };
	protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

	
	@Autowired
	UserAuthorizationService userAuthorizationService;
	
	@Override
	@GetMapping(value="/findByCriteria",produces="application/json", headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<String>  findByCriteria(HttpServletRequest request) {
        Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("queryString", request.getQueryString());
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  service.findByCriteria(objectMap);
      

        String contents = responseEntity.getBody();

        Gson gson = new Gson();
        Map map = gson.fromJson(contents, Map.class);
        List<Map> contentList = (List<Map>) map.get("content"); 
   
        contentList = SortMapUtil.editList(contentList);
        contentList = SortMapUtil.sortData(contentList);
 

        return new ResponseEntity<String>((new JSONSerializer().deepSerialize(contentList)),headers, HttpStatus.OK);
	}

	@Override
	@Resource(name="MenuService")
	public void setService(BaseCommonService service) {

		super.service = service;
	}
	
	@Loggable
	@GetMapping("/listView")
	public String showPrivilege() {
		return "menus";
	}
	
	@Loggable
	@GetMapping("/listViewByRole")
	public String showPrivilegeByRole() {
		return "menusByRole";
	}
	
	
	@GetMapping("/reloadMenu")
	public ResponseEntity<String> reloadMenu(HttpServletRequest request) {
		HttpHeaders headers = new HttpHeaders();
		userAuthorizationService.getUserDetail(String.valueOf(request.getSession(true).getAttribute("userName")));
        headers.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>("", headers, HttpStatus.OK);
	}
	
	@GetMapping("/addPrivilegeAndReloadMenu/{id}")
	public ResponseEntity<String> addPrivilegeAndReloadMenu(@PathVariable Long id,HttpServletRequest request) {
		String roles = (String) request.getSession(true).getAttribute("ROLES");
		((MenuService)service).putRole(id, Arrays.asList(roles.split(",")) );
		
		HttpHeaders headers = new HttpHeaders();
		userAuthorizationService.getUserDetail(String.valueOf(request.getSession(true).getAttribute("userName")));
        headers.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>("", headers, HttpStatus.OK);
	}
	

	@GetMapping("/getMaxSequence/{id}")
	public ResponseEntity<String> getMaxSequence(@PathVariable Integer id,HttpServletRequest request) {
		HttpHeaders headers = new HttpHeaders();
		
		Map<Integer,Long> maxSequenceMap = (Map<Integer, Long>) request.getSession(true).getAttribute("MENU_MAX_SEQUENCE");
		
	    Integer newSequence = 1;
	    try {
			newSequence = maxSequenceMap.get(id).intValue() + 1;
		} catch (Exception e) { }
		headers.add("Content-Type", "application/json; charset=utf-8");
		return new ResponseEntity<String>(""+newSequence, headers, HttpStatus.OK);
	}
	
	
	@Loggable
	@GetMapping("/findByCode")
	public ResponseEntity<String> findByCode( ModelMap map,
			@RequestParam(name="code",required=false) String code) {
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((BaseCodeService)service).findByCode(code);
			
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}
	
	@PostMapping(value="/{id}/roles",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8") 
	public ResponseEntity putRole(@PathVariable Long id,@RequestParam("roles") String roles,@RequestParam("rolesBefore") String rolesBefore) {
		LOGGER.info("roles="+roles);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
		
		ResponseEntity<String> responseEntity;
		try {
			Map<String,String> rolesBeforeMap = gson.fromJson(rolesBefore, Map.class);
			Map<String,String> roleMap = gson.fromJson(roles, Map.class);
			for(String key:roleMap.keySet()){
				if("".equals(roleMap.get(key))){
					responseEntity = ((MenuService)service).deleteRole(Long.valueOf(key),Arrays.asList(rolesBeforeMap.get(key).split(","))  );
				}else{
					responseEntity = ((MenuService)service).putRole(Long.valueOf(key), Arrays.asList(roleMap.get(key).split(",")) );
				}
				
			}
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}
	
	@PostMapping(value="/{id}/roles") 
	public ResponseEntity deleteRole(@PathVariable Long id,@RequestParam(name="roles",required=false) String roles) {
		LOGGER.info("roles="+roles);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
		
		ResponseEntity<String> responseEntity;
		try {
			if(roles!=null){
				responseEntity = ((MenuService)service).deleteRole(id, Arrays.asList(roles.split(",")) );
			}else{
				responseEntity = ((MenuService)service).deleteAllRole(id);
			}
			
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}
}
