package com.spt.app.controller.general;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.RoleService;
import com.spt.app.service.UserService;

@Controller
@RequestMapping("/roles")
public class RoleController extends BaseCommonController{

	static final Logger LOGGER = LoggerFactory.getLogger(RoleController.class);
	
	@Override
	@Resource(name="RoleService")
	public void setService(BaseCommonService service) {

		super.service = service;
	}
	
	@Loggable
	@GetMapping("/listView")
	public String getRoleList() {
		return "roles";
	}
	
	@GetMapping("/findByRoleName")
	public ResponseEntity<String> findByRoleName(@RequestParam("roleName") String roleName) {
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((RoleService)service).findByRoleName(roleName);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
		
		
	}
	
	@GetMapping("/findUserByRole/{id}")
	public ResponseEntity<String> findUserByRole(@PathVariable Long id) {
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((RoleService)service).findUserByRole(id);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
		
		
	}


	@Loggable
	@GetMapping("/findAllRole")
	public ResponseEntity<String> findAllRole() {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((RoleService)service).findAllRole();

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

}
