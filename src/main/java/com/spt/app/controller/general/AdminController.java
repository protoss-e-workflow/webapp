package com.spt.app.controller.general;

import java.net.HttpCookie;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.constant.ApplicationConstant;

@RestController
public class AdminController {

	static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);
	
	SimpleDateFormat sd = new SimpleDateFormat("yyyyMMddhhmm");
	
	@GetMapping("/admin/fetchparameter")
	public String getParameter() {
		ApplicationConstant.FETCH_FRONTEND_PARAMETER_TIME = sd.format(new Date());
		return "";
	}
	

}
