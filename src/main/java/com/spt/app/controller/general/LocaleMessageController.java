package com.spt.app.controller.general;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.service.BaseCodeService;
import com.spt.app.service.BaseCommonService;

@Controller
@RequestMapping("/localeMessage")
public class LocaleMessageController extends BaseCommonController{

	static final Logger LOGGER = LoggerFactory.getLogger(LocaleMessageController.class);
	
	@Override
	@Resource(name="LocaleMessageService")
	public void setService(BaseCommonService service) {

		super.service = service;
	}
	
	@Loggable
	@GetMapping("/listView")
	public String masterDataDetail( ModelMap map,@RequestParam(name="code",required=false) String code) {

		if( "BT".equals(String.valueOf(code) )){
			map.addAttribute("headerName"  , "LABEL_BUTTON");
			map.addAttribute("buttonLB"  , "btn-nonactive");
			map.addAttribute("buttonMS"  , "btn-nonactive");
			map.addAttribute("buttonBT"  , "btn-active");
			map.addAttribute("groupCode"  , "BT");
		}else if( "MS".equals(String.valueOf(code) )){
			map.addAttribute("headerName"  , "LABEL_MESSAGE");
			map.addAttribute("buttonLB"  , "btn-nonactive");
			map.addAttribute("buttonMS"  , "btn-active");
			map.addAttribute("buttonBT"  , "btn-nonactive");
			map.addAttribute("groupCode"  , "MS");
		}else {
			map.addAttribute("headerName"  , "LABEL_LABEL");
			map.addAttribute("buttonLB"  , "btn-active");
			map.addAttribute("buttonMS"  , "btn-nonactive");
			map.addAttribute("buttonBT"  , "btn-nonactive");
			map.addAttribute("groupCode"  , "LB");
		}

		
		return "localeMessage";
	}

	@Loggable
	@GetMapping("/findByCode")
	public ResponseEntity<String> findByCode( ModelMap map,
			@RequestParam(name="code",required=false) String code) {
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((BaseCodeService)service).findByCode(code);
			
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}
	
	
}
