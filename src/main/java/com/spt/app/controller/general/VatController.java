package com.spt.app.controller.general;

import com.spt.app.service.BaseCommonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/vats")
public class VatController extends BaseCommonController{

    static final Logger LOGGER = LoggerFactory.getLogger(VatController.class);

    @Override
    @Resource(name="VatService")
    public void setService(BaseCommonService service) {
        super.service = service;
    }

    @GetMapping("/findAllVat")
    public ResponseEntity findAllVat(HttpServletRequest request){
        LOGGER.info("--- Find All Vat ---");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = (service).load();
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }
}
