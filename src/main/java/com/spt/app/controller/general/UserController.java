package com.spt.app.controller.general;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.core.io.ByteArrayResource;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.UserService;

@Controller
@RequestMapping("/users")
public class UserController extends BaseCommonController{

	static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	@Override
	@Resource(name="UserService")
	public void setService(BaseCommonService service) {

		super.service = service;
	}
	
	@Loggable
	@GetMapping("/listView")
	public String getUsersList() {
		return "users";
	}
	
	@GetMapping("/findByUsername")
	public ResponseEntity<String> findByUsername(@RequestParam("username") String username) {
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((UserService)service).findByUsername(username);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}


	}
	
	@PostMapping(value="/{id}/roles",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8") 
	public ResponseEntity putRole(@PathVariable Long id,@RequestParam("roles") String roles) {
		LOGGER.info("roles="+roles);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
		
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((UserService)service).putRole(id, Arrays.asList(roles.split(",")) );
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}
	
	@PostMapping("/uploadUserImage")
    public ResponseEntity<String>  handleFileUpload(//@RequestParam("file") MultipartFile file,
                                   //RedirectAttributes redirectAttributes
    		MultipartHttpServletRequest multipartHttpServletRequest) {
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((UserService)service).uploadFile(multipartHttpServletRequest.getParameter("file"),
					multipartHttpServletRequest.getParameter("filename"));
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
    }
	
	@GetMapping("/loadUserImage/{id}")
	public ResponseEntity<String> loadUserImage(@PathVariable Long id) {
		ResponseEntity<org.springframework.core.io.Resource> responseEntity =  ((UserService)service).loadImage(id);
		    String base64="";
			try {
				base64 = Base64.getEncoder().encodeToString(IOUtils.toByteArray(responseEntity.getBody().getInputStream()));
				base64 = "data:image/*;base64," + base64;
				return ResponseEntity.ok().body(base64);
			} catch (Exception e) {

				//e.printStackTrace();
				return ResponseEntity.ok().body("");
			}
	}
	

	@PostMapping(value="/{id}/roles") 
	public ResponseEntity deleteRole(@PathVariable Long id,@RequestParam(name="roles",required=false) String roles) {
		LOGGER.info("roles="+roles);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
		
		ResponseEntity<String> responseEntity;
		try {
			if(roles!=null){
				responseEntity = ((UserService)service).deleteRole(id, Arrays.asList(roles.split(",")) );
			}else{
				responseEntity = ((UserService)service).deleteAllRole(id);
			}
			
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}
	

}
