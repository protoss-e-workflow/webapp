package com.spt.app.controller.general;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.spt.app.aop.annotation.Loggable;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.BaseParentService;
import com.spt.app.service.MasterDataService;
import com.spt.app.service.BaseCodeService;
import com.spt.app.service.RoleService;
import com.spt.app.service.UserService;

@Controller
@RequestMapping("/masterDatas")
public class MasterDataController extends BaseCommonController{

	static final Logger LOGGER = LoggerFactory.getLogger(MasterDataController.class);
	
	@Override
	@Resource(name="MasterDataService")
	public void setService(BaseCommonService service) {

		super.service = service;
	}
	
	@Loggable
	@GetMapping("/listView")
	public String masterDataDetail( ModelMap map,@RequestParam(name="code",required=false) String code) {
		
		
		if(code!=null){
			JsonParser parser = new JsonParser();
			
			ResponseEntity<String> masterDataResponse = ((BaseCodeService)service).findByCode(code);
			JsonElement obj = parser.parse(masterDataResponse.getBody());
			map.addAttribute("masterDataEntity", obj);
			map.addAttribute("masterDataName"  , obj.getAsJsonObject().get("nameMessageCode").getAsString());
			
			
			try {
				if(obj.getAsJsonObject().get("numberOfColumn").getAsBigInteger().intValue() == 0){
					map.addAttribute("widthDescription"  , "100%");
				}else{
					map.addAttribute("widthDescription"  , "300px");
				}
			} catch (Exception e) { }
			
			boolean flagPage = false;
			try {
				flagPage = obj.getAsJsonObject().get("flagOrgCode").getAsBoolean();
			} catch (Exception e) { }
			
			if(flagPage){
				return "masterDatas";
			}else{
				return "masterDatasWithoutOrg";
			}
			
		}else{
			map.addAttribute("masterDataEntity", "");
			map.addAttribute("masterDataName"  , "LABEL_MASTER_DATA");
			map.addAttribute("widthDescription"  , "300px");
			return "masterDatas";
		}
		
		
	}
	
	@Loggable
	@GetMapping("/findByMasterDataByCodeByOrgCode")
	public ResponseEntity<String> findByMasterDataByCode( ModelMap map,
			@RequestParam("masterdata") Long id,
			@RequestParam(name="code",required=false) String code,
			@RequestParam(name="orgCode",required=false) String orgCode) {
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((MasterDataService)service).findByMasterDataByCodeByOrgCode(id,code,orgCode);
			
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}
	
	@PostMapping(value="/{id}/masterdata",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8") 
	public ResponseEntity putMasterData(@PathVariable Long id,@RequestParam("masterdata") String masterdata) {
		LOGGER.info("masterdata="+masterdata);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
		
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((BaseParentService)service).putParent(id, Arrays.asList(masterdata.split(",")) );
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}


	@Loggable
	@GetMapping("/findByMasterCodeIn")
	public ResponseEntity<String> findByMasterCodeIn( @RequestParam(name="code",required=false) String code) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		List<Map> responseEntity ;
		try {
			responseEntity = ((MasterDataService)service).findByMasterCodeIn(code);

			return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").deepSerialize(responseEntity)), headers, HttpStatus.OK);

		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}


	@Loggable
	@GetMapping("/findMasterDataDetailByMasterdataCode")
	public ResponseEntity<String> findMasterDataDetailByMasterdataCode() {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((MasterDataService)service).findMasterDataDetailByMasterdataCode();

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}



	@Loggable
	@GetMapping("/findSequenceNumberApproveTypeInMasterDataDetail")
	public ResponseEntity<String> findSequenceNumberApproveTypeInMasterDataDetail(@RequestParam(name="detailCode",required=false)
																						  String detailCode) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((MasterDataService)service).findSequenceNumberApproveTypeInMasterDataDetail(detailCode);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}



	@Loggable
	@GetMapping("/findMasterDataDetailApproveTypeByCode")
	public ResponseEntity<String> findMasterDataDetailApproveTypeByCode(@RequestParam(name="code",required=false)
																				String code) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((MasterDataService)service).findMasterDataDetailApproveTypeByCode(code);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}


	@Loggable
	@GetMapping("/findMasterDataDetailOfAttachmentTypeAll")
	public ResponseEntity<String> findMasterDataDetailOfAttachmentTypeAll() {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((MasterDataService)service).findMasterDataDetailOfAttachmentTypeAll();

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}


	@Loggable
	@GetMapping("/findMasterDataDetailAttachmentTypeByCode")
	public ResponseEntity<String> findMasterDataDetailAttachmentTypeByCode(@RequestParam(name="code",required=false)
																				   String code) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((MasterDataService)service).findMasterDataDetailAttachmentTypeByCode(code);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}


	@Loggable
	@GetMapping("/findByMasterdataInOrderByCode")
	public ResponseEntity<String> findByMasterdataInOrderByCode(@RequestParam(name="masterdata",required=false)
																				   String masterdata) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((MasterDataService)service).findByMasterdataInOrderByCode(masterdata);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}



	@Loggable
	@GetMapping("/findByMasterdataInAndMasterDataDetailCodeOrderByCode")
	public ResponseEntity<String> findByMasterdataInAndMasterDataDetailCodeOrderByCode(@RequestParam(name="masterdata",required=false) String masterdata,
																@RequestParam(name="code",required=false)String code) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((MasterDataService) service).findByMasterdataInAndMasterDataDetailCodeOrderByCode(masterdata, code);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);

		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

	@Loggable
	@GetMapping("/findByMasterdataInAndMasterDataDetailCodeOrderByCodeList")
	public ResponseEntity<String> findByMasterdataInAndMasterDataDetailCodeOrderByCodeList(@RequestParam(name="masterdata",required=false) String masterdata,
																						   @RequestParam(name="code",required=false)String code) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((MasterDataService) service).findByMasterdataInAndMasterDataDetailCodeOrderByCodeList(masterdata, code);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);

		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

	/* update by iMilkii 2017.09.18 for auto complete hotel */
	@Loggable
	@GetMapping("/findAutoCompleteHotelByKeySearch")
	public ResponseEntity<String> findAutoCompleteHotelByKeySearch(@RequestParam(name="code",required=false) String code) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		List<Map> responseEntity;
		try {
			LOGGER.info("><><>< findAutoCompleteHotelByKeySearch ><><>< ");
			LOGGER.info("><><>< code : {} ",code);
			responseEntity = ((MasterDataService)service).findAutoCompleteHotelByKeySearch(code);
			LOGGER.info("><><>< findAutoCompleteHotelByKeySearch : {} ",responseEntity.size());

			return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").deepSerialize(responseEntity)), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}



	@Loggable
	@GetMapping("/findByMasterdataInAndMasterDataDetailCodeLikeOrderByCode")
	public ResponseEntity<String> findByMasterdataInAndMasterDataDetailCodeLikeOrderByCode(@RequestParam(name="masterdata",required=false) String masterdata,
																					       @RequestParam(name="code",required=false)String code) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((MasterDataService)service).findByMasterdataInAndMasterDataDetailCodeLikeOrderByCode(masterdata,code);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

	@GetMapping("/findUserMember")
	@Loggable
	public ResponseEntity<String> findUserMember(@RequestParam("userHead") String userHead){
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type","application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try{
			responseEntity = ((MasterDataService)service).findByVariable1(userHead);
			return new ResponseEntity<String>(responseEntity.getBody(),headers,HttpStatus.OK);
		}catch (Exception e){
			e.printStackTrace();
			return new ResponseEntity<String>("",headers,HttpStatus.OK);
		}
	}

}
