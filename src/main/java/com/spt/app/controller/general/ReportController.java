package com.spt.app.controller.general;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.spt.app.aop.annotation.Loggable;
import com.spt.app.service.BaseCodeService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.MasterDataService;
import com.spt.app.service.ReportService;
import com.spt.app.service.RoleService;
import com.spt.app.service.UserService;

@Controller
@RequestMapping("/report")
public class ReportController  extends BaseCommonController{

	static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);
	

	@Override
	@Resource(name="ReportService")
	public void setService(BaseCommonService service) {

		super.service = service;
	}
	
	@Loggable
	@GetMapping("/criteria/{reportCode}")
	public String viewCriteria(ModelMap map,@PathVariable String reportCode) {
		LOGGER.info("reportCode={}",reportCode);
		JsonParser parser = new JsonParser();
		ResponseEntity<String> reportConfigurationResponse = ((BaseCodeService)service).findByCode(reportCode);
		JsonElement config = parser.parse(reportConfigurationResponse.getBody());
		map.addAttribute("headerReport"  , config.getAsJsonObject().get("nameMessageCode").getAsString());
		map.addAttribute("reportType"    , config.getAsJsonObject().get("reportType").getAsString());
		
		ResponseEntity<String> reportCriteriaResponse = ((ReportService)service).getCriteriaByCode(reportCode);
		JsonElement criteria = parser.parse(reportCriteriaResponse.getBody());
		map.addAttribute("criteriaObject", criteria.getAsJsonObject().get("content"));
		map.addAttribute("reportCode"    , reportCode);
		
		return "reportCriteria";
	}
	

	@GetMapping("/gen/{reportCode}")
	public ResponseEntity<org.springframework.core.io.Resource> loadUserImage(HttpServletRequest request,@PathVariable String reportCode) {
		
		Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("queryString", request.getQueryString());
        objectMap.put("reportCode", reportCode);
        
		ResponseEntity<org.springframework.core.io.Resource> responseEntity =  ((ReportService)service).genReport(objectMap);
		return responseEntity;
	}
}
