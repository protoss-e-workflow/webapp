package com.spt.app.controller.general;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.spt.app.service.UserAuthorizationService;
import com.spt.app.spring.security.CustomUser;
import com.spt.app.spring.security.JwtFilter;
import com.spt.app.util.CookieUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.spt.app.spring.security.CustomUserDetailsService;
import com.spt.app.spring.security.CustomUserModel;

import java.util.ArrayList;
import java.util.Map;

@Controller
public class SecurityController {

    static final Logger LOGGER = LoggerFactory.getLogger(SecurityController.class);

    private RequestCache requestCache = new HttpSessionRequestCache();

    @Autowired
    CustomUserModel customUserModel;

    @Autowired
    UserAuthorizationService userAuthorizationService;

    @Value("${services.home}")
    private String authServiceHome;

    @Value("${validateTokenURI}")
    private String validateTokenURI;

    @Value("${ewfApplicationKey}")
    private String ewfApplicationKey;

    @Value("${DefaultPassword}")
    private String DefaultPassword;

    @Value("${jwt.token.name}")
    private String jwtTokenName;

    @Value("${init.token.name}")
    private String initToken;

    @Value("${init.username}")
    private String initUsername;

    @RequestMapping(value = { "/", "/home" }, method = RequestMethod.GET)
    public String homePage(ModelMap model) {
        return "index";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String adminPage(ModelMap model) {
        return "admin";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, ModelMap model) {
        String token = CookieUtil.getValue(httpServletRequest, jwtTokenName);
        String username = CookieUtil.getValue(httpServletRequest, initUsername);
        if(!StringUtils.isEmpty(token)){
            Map<String, Object> map = new JwtFilter().authenticateValidate(token, ewfApplicationKey, validateTokenURI);
            if(map.size() > 0 || initToken.equals(token)){
                Authentication auth = new UsernamePasswordAuthenticationToken(username/*map.get("username").toString()*/, DefaultPassword, new ArrayList<>());
                SecurityContextHolder.getContext().setAuthentication(auth);
                CustomUser user = (CustomUser) userAuthorizationService.getUserDetail(username/*map.get("username").toString()*/);
                httpServletRequest.getSession(true).setAttribute("userName",username/*map.get("username").toString()*/);
                httpServletRequest.getSession(true).setAttribute("userAgent",httpServletRequest.getHeader("User-Agent"));
                httpServletRequest.getSession(true).setAttribute("screen_width",String.valueOf(httpServletRequest.getParameter("screen_width")));
                httpServletRequest.getSession(true).setAttribute("screen_height",String.valueOf(httpServletRequest.getParameter("screen_height")));
                httpServletRequest.getSession(true).setAttribute("menu",customUserModel.getValue("MENU"));
                httpServletRequest.getSession(true).setAttribute("menu_authorize",customUserModel.getValue("MENU_AUTHORIZE"));
                httpServletRequest.getSession(true).setAttribute("img_src",customUserModel.getValue("IMG_SRC"));
                httpServletRequest.getSession(true).setAttribute("PARAMETER_LANGUAGE",customUserModel.getValue("PARAMETER_LANGUAGE"));
                httpServletRequest.getSession(true).setAttribute("PARAMETER_LANGUAGE_JSON",customUserModel.getValue("PARAMETER_LANGUAGE_JSON"));
                httpServletRequest.getSession(true).setAttribute("first_name",customUserModel.getValue("FIRST_NAME"));
                httpServletRequest.getSession(true).setAttribute("last_name",customUserModel.getValue("LAST_NAME"));
                httpServletRequest.getSession(true).setAttribute("companyCode",customUserModel.getValue("COMPANY_CODE"));

                /* Add to Bean SESSION SCOPE */
                customUserModel.addValue(CustomUserModel.ATTR_CUSTOM_USER, user);
                customUserModel.addValue(CustomUserModel.ATTR_CUSTOM_USER_NAME, username/*map.get("username").toString()*/);

                SavedRequest savedRequest = requestCache.getRequest(httpServletRequest, httpServletResponse);
                String targetUrl = savedRequest.getRedirectUrl();
                try {
                    if(null!=targetUrl){
                        httpServletResponse.sendRedirect(targetUrl);
                    }else{
                        httpServletResponse.sendRedirect(authServiceHome);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return "login";
    }



    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "login";
    }

    @RequestMapping(value = "/Doc_Access_Denied", method = RequestMethod.GET)
    public String accessDeniedDocPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "docAccessDenied";
    }


    @RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap model) {
        model.addAttribute("user", getPrincipal());
        return "accessDenied";
    }

    private String getPrincipal(){
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }
}
