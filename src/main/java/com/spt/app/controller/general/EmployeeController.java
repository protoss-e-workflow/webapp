package com.spt.app.controller.general;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.CompanyService;
import com.spt.app.service.EmployeeService;
import com.spt.app.service.ExpenseTypeFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/employees")
public class EmployeeController extends BaseCommonController{

    static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

    @Override
    @Resource(name="EmployeeService")
    public void setService(BaseCommonService service) {
        super.service = service;
    }

    @GetMapping("/findAllEmployee")
    public ResponseEntity findAllEmployee(HttpServletRequest request){
        LOGGER.info("--- Find All Employee ---");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = (service).load();
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }



    @GetMapping("/findByEmpCode")
    public ResponseEntity<String> findByCode(@RequestParam("empCode") String empCode) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((EmployeeService)service).findByEmpCode(empCode);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }



    @Loggable
    @GetMapping("/findAllEmployees")
    public ResponseEntity<String> findAllEmployee() {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((EmployeeService)service).findAllEmployee();

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    @Loggable
    @GetMapping("/findByEmpUsername")
    public ResponseEntity<String> findByEmpUsername(@RequestParam("empUsername") String empUsername) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((EmployeeService)service).findByEmpUsername(empUsername);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }



}
