package com.spt.app.controller.general;

import java.util.Arrays;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.spt.app.aop.annotation.Loggable;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.MasterDataService;
import com.spt.app.service.BaseCodeService;
import com.spt.app.service.RoleService;
import com.spt.app.service.UserService;

@Controller
@RequestMapping("/masterDataConfig")
public class MasterDataConfigController extends BaseCommonController{

	static final Logger LOGGER = LoggerFactory.getLogger(MasterDataConfigController.class);
	
	@Override
	@Resource(name="MasterDataConfigService")
	public void setService(BaseCommonService service) {

		super.service = service;
	}
	
	@Loggable
	@GetMapping("/listView")
	public String masterDataConfig() {
		return "masterDataConfig";
	}
	
	@Loggable
	@GetMapping("/findByMasterDataByCode")
	public ResponseEntity<String> findByMasterDataByCode( ModelMap map,
			@RequestParam(name="code",required=false) String code) {
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((BaseCodeService)service).findByCode(code);
			
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}


}
