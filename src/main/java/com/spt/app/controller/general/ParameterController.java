package com.spt.app.controller.general;

import java.util.Arrays;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.spt.app.aop.annotation.Loggable;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.BaseParentService;
import com.spt.app.service.MasterDataService;
import com.spt.app.service.ParameterService;
import com.spt.app.service.BaseCodeService;
import com.spt.app.service.RoleService;
import com.spt.app.service.UserService;

@Controller
@RequestMapping("/parameters")
public class ParameterController extends BaseCommonController{

	static final Logger LOGGER = LoggerFactory.getLogger(ParameterController.class);
	
	@Override
	@Resource(name="ParameterService")
	public void setService(BaseCommonService service) {

		super.service = service;
	}
	
	@Loggable
	@GetMapping("/listView")
	public String parameterDetail() {
		return "parameters";
	}
	

	@PostMapping(value="/{id}/parameter",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8") 
	public ResponseEntity putMasterData(@PathVariable Long id,@RequestParam("parameter") String parameter) {
		LOGGER.info("parameter="+parameter);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
		
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((BaseParentService)service).putParent(id, Arrays.asList(parameter.split(",")) );
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}
	
	
	@GetMapping(value="/{id}/details",produces="application/json", headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<String>  getDetail(@PathVariable Long id) {
		LOGGER.info("id="+id);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
		
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((ParameterService)service).findParameterDetailsByParameterId(id);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
        
	}

	@GetMapping(value = "/findByParameterDetailCode",produces = "application/json",headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<String> getParameterDetail(@RequestParam("code") String code){
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type","application/json; charset=utf-8");

		ResponseEntity<String> responseEntity;
		try{
			responseEntity = ((ParameterService)service).findByParameterDetailCode(code);
			return new ResponseEntity<String>(responseEntity.getBody(),headers,HttpStatus.OK);
		}catch (Exception e){
			return new ResponseEntity<String>("Error",headers,HttpStatus.OK);
		}
	}

	
	
}
