package com.spt.app.controller.master;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.LocationService;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/locations")
public class LocationController extends BaseCommonController {

	static final Logger LOGGER = LoggerFactory.getLogger(LocationController.class);

	@Override
	@Resource(name="LocationService")
	public void setService(BaseCommonService service) {
		super.service = service;
	}

	@Loggable
	@GetMapping("/listView")
	public String locationList(@RequestParam(name="code",required=false) String locationType) {
		String locations = "";


		if(locationType.equals("D")){
			locations =  "locations";
		}else if(locationType.equals("F")){
			locations = "locationForeigns";
		}
		return locations;
	}
	
	@GetMapping("/findByCode")
	public ResponseEntity<String> findByCode(@RequestParam("code") String code) {
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((LocationService)service).findByCode(code);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
		
		
	}
	@GetMapping("/findByCodeAndLocationType")
	public ResponseEntity<String> findByCodeAndLocationType(@RequestParam("code") String code, @RequestParam("locationType") String locationType) {
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((LocationService)service).findByCodeAndLocationType(code,locationType);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}


	}


	@Loggable
	@GetMapping("/findByLocationType")
	public ResponseEntity<String> findByLocationType( @RequestParam(name="locationType",required=false) String locationType) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		List<Map> responseEntity ;
		try {
			responseEntity = ((LocationService)service).findByLocationType(locationType);

			return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").deepSerialize(responseEntity)), headers, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

	@Loggable
	@GetMapping("/findByLocationTypeAndDescription")
	public ResponseEntity<String> findByLocationTypeAndDescription(@RequestParam(name="locationType",required=false) String locationType,
																   @RequestParam(name="description",required=false) String description) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		try {
			ResponseEntity<String> result = ((LocationService)service).findByLocationTypeAndDescription(locationType, description);
			return new ResponseEntity<String>(result.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}



}
