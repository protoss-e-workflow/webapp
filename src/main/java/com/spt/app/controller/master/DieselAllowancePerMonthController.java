package com.spt.app.controller.master;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.DieselAllowancePerMonthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

@Controller
@RequestMapping("/dieselAllowancePerMonths")
public class DieselAllowancePerMonthController extends BaseCommonController {

	static final Logger LOGGER = LoggerFactory.getLogger(DieselAllowancePerMonthController.class);

	@Override
	@Resource(name="DieselAllowancePerMonthService")
	public void setService(BaseCommonService service) {

		super.service = service;
	}

	@Loggable
	@GetMapping("/listView")
	public String dieselAllowancePerMonthList() {


		return "dieselAllowancePerMonths";
	}



	@GetMapping("/findDieselAllowancePerMonthByEmpCode")
	public ResponseEntity<String> findDieselAllowancePerMonthByEmpCode(@RequestParam("empCode") String empCode) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DieselAllowancePerMonthService)service).findByEmpCode(empCode);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}


	}

	@GetMapping("/findDieselAllowancePerMonthByEmpUserName")
	public ResponseEntity<String> findDieselAllowancePerMonthByEmpUserName(@RequestParam("empUserName") String empUserName) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DieselAllowancePerMonthService)service).findByEmpUserName(empUserName);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}
}
