package com.spt.app.controller.master;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.LocationService;
import com.spt.app.service.RouteDistanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/routeDistances")
public class RouteDistanceController extends BaseCommonController {

	static final Logger LOGGER = LoggerFactory.getLogger(RouteDistanceController.class);

	@Override
	@Resource(name="RouteDistanceService")
	public void setService(BaseCommonService service) {

		super.service = service;
	}

	@Loggable
	@GetMapping("/listView")
	public String routeDistanceList() {


		return "routeDistances";
	}

	@PostMapping(value="/saveRouteDistance",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity saveRouteDistance(HttpServletRequest request) {
		LOGGER.info("-= SAVE ROUTE DISTANCE =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((RouteDistanceService)service).saveRouteDistance(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	@GetMapping("/findByLocationFromAndLocationTo")
	public ResponseEntity<String> findByLocationFromAndLocationTo(@RequestParam("locationFrom") Long locationFrom, @RequestParam("locationTo") Long locationTo) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((RouteDistanceService)service).findByLocationFromAndLocationTo(locationFrom,locationTo);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}


	}

	@GetMapping("/calculateRouteDistanceFromLocation")
	public ResponseEntity<String> calculateRouteDistanceFromLocation(@RequestParam("locationFrom") String locationFrom,@RequestParam("locationTo") String locationTo,@RequestParam("companyCode") String companyCode) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((RouteDistanceService)service).calculateRouteDistanceFromLocation(locationFrom,locationTo,companyCode);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}


	}

	@GetMapping("/calculateRouteDistanceFromDistance")
	public ResponseEntity<String> calculateRouteDistanceFromDistance(@RequestParam("distance") String distance,@RequestParam("companyCode") String companyCode) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((RouteDistanceService)service).calculateRouteDistanceFromDistance(distance,companyCode);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}


	}

}
