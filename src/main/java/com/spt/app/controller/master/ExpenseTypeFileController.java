package com.spt.app.controller.master;

import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.BaseParentService;
import com.spt.app.service.ExpenseTypeFileService;
import com.spt.app.service.ExpenseTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;

@Controller
@RequestMapping("/expenseTypeFile")
public class ExpenseTypeFileController extends BaseCommonController {

	static final Logger LOGGER = LoggerFactory.getLogger(ExpenseTypeFileController.class);


	@Override
	@Resource(name="ExpenseTypeFileService")
	public void setService(BaseCommonService service) {

		super.service = service;
	}




	@GetMapping("/findByAttachmentTypeCode")
	public ResponseEntity<String> findByCode(@RequestParam("code") String code) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((ExpenseTypeFileService)service).findByAttachmentTypeCode(code);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}



	@PostMapping(value="/{id}/expenseType",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity putMasterData(@PathVariable Long id, @RequestParam("expenseType") String expenseType) {
		LOGGER.info("==========================  ID PUT PARENT EXPENSETYPE  ========================{} "+expenseType);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((BaseParentService)service).putParent(id, Arrays.asList(expenseType.split(",")) );
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

}
