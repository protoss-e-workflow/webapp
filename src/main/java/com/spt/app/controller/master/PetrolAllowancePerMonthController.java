package com.spt.app.controller.master;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.PetrolAllowancePerMonthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/petrolAllowancePerMonths")
public class PetrolAllowancePerMonthController extends BaseCommonController {

	static final Logger LOGGER = LoggerFactory.getLogger(PetrolAllowancePerMonthController.class);

	@Override
	@Resource(name="PetrolAllowancePerMonthService")
	public void setService(BaseCommonService service) {

		super.service = service;
	}

	@Loggable
	@GetMapping("/listView")
	public String petrolAllowancePerMonthList() {


		return "petrolAllowancePerMonths";
	}



	@GetMapping("/findPetrolAllowancePerMonthByEmpCode")
	public ResponseEntity<String> findPetrolAllowancePerMonthByEmpCode(@RequestParam("empCode") String empCode) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((PetrolAllowancePerMonthService)service).findByEmpCode(empCode);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}


	}

	@GetMapping("/findPetrolAllowancePerMonthByEmpUserName")
	public ResponseEntity<String> findPetrolAllowancePerMonthByEmpUserName(@RequestParam("empUserName") String empUserName) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((PetrolAllowancePerMonthService)service).findByEmpUserName(empUserName);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}


	}
}
