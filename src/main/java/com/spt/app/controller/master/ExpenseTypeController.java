package com.spt.app.controller.master;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.BaseParentService;
import com.spt.app.service.ExpenseTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/expenseType")
public class ExpenseTypeController extends BaseCommonController {

	static final Logger LOGGER = LoggerFactory.getLogger(ExpenseTypeController.class);


	@Override
	@Resource(name="ExpenseTypeService")
	public void setService(BaseCommonService service) {

		super.service = service;
	}




//
//	@Loggable
//	@GetMapping("/listView")
//	public String listView() {
//		return "expenseListView";
//	}

	@Loggable
	@GetMapping("/listView")
	public String listView() {
		return "expenseTypeListView";
	}





	@Loggable
	@GetMapping("/expenseMainMenu")
	public String mainMenu() {
		return "expenseMainMenu";
	}


	@Loggable
	@GetMapping("/expenseDetail")
	public String expenseDetail(@RequestParam("type") String type, @RequestParam(value = "role",required = false) String role) {
		return "expenseDetail";
	}
	
	@Loggable
	@GetMapping("/createDoc")
	public String createDoc() {
		return "expenseCreateDoc";
	}

	@Loggable
	@GetMapping("/clearExpense")
	public String clearExpense() {
		return "clearExpense";
	}

	@Loggable
	@GetMapping("/clearExpenseDetail")
	public String clearExpenseDetail() {
		return "clearExpenseDetail";
	}

	@Loggable
	@GetMapping("/withdrawExpense")
	public String withdrawExpense() {
		return "withdrawExpense";
	}

	@Loggable
	@GetMapping("/withdrawExpenseDetail")
	public String withdrawExpenseDetail() {
		return "withdrawExpenseDetail";
	}


	@PostMapping(value="/saveExpenseTypeRef",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity saveExpenseTypeRef(HttpServletRequest request) {
		LOGGER.info("-= SAVE DOCUMENT =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((ExpenseTypeService)service).addExpenseTypeReference(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}




	@PostMapping(value="/saveExpenseTypeCompany",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity saveExpenseTypeCompany(HttpServletRequest request) {
		LOGGER.info("-=========================== SaveExpenseTypeCompany =============================-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((ExpenseTypeService)service).addExpenseTypeByCompany(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}




	@PostMapping(value="/saveReimburse",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity saveReimburse(HttpServletRequest request) {
		LOGGER.info("-=========================== SaveExpenseTypeCompany =============================-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {

			responseEntity = ((ExpenseTypeService)service).addReimburse(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}



	@PostMapping(value="/saveExpenseTypeScreen",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity saveExpenseTypeScreen(HttpServletRequest request) {
		LOGGER.info("-=========================== SaveExpenseTypeCompany =============================-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((ExpenseTypeService)service).addExpenseTypeScreen(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}



	@PostMapping(value="/saveExpenseTypeFile",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity saveExpenseTypeFile(HttpServletRequest request) {
		LOGGER.info("-=========================== SaveExpenseTypeCompany =============================-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			LOGGER.info("DATA Info Save ===============> {}",mapClone);
			responseEntity = ((ExpenseTypeService)service).addExpenseTypeFile(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}


	@GetMapping("/findByCode")
	public ResponseEntity<String> findByCode(@RequestParam("code") String code) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((ExpenseTypeService)service).findByCode(code);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}


	@GetMapping("/findExpenseTypeByIdAndExpenseTypeReferencesCompleteReason")
	public ResponseEntity<String> findByCode(@RequestParam("completeReason") String completeReason,
											 @RequestParam("expenseType") Long expenseType) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((ExpenseTypeService)service).findExpenseTypeByIdAndExpenseTypeReferencesCompleteReason(completeReason,expenseType);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}



	@GetMapping("/findExpenseTypeByIdAndExpenseTypeFileAttachmentCode")
	public ResponseEntity<String> findExpenseTypeByIdAndExpenseTypeFileAttachmentCode(@RequestParam("attachmentTypeCode") String attachmentTypeCode,
											 @RequestParam("expenseType") Long expenseType) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((ExpenseTypeService)service).findExpenseTypeByIdAndExpenseTypeFileAttachmentCode(attachmentTypeCode,expenseType);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}


	@GetMapping("/findExpenseTypeByIdAndExpenseTypeCompanyPaAndExpenseTypeCompanyPsaAndExpenseTypeCompanyGlCode")
	public ResponseEntity<String> findExpenseTypeByIdAndExpenseTypeCompanyPaAndExpenseTypeCompanyPsaAndExpenseTypeCompanyGlCode(
			                                                                          @RequestParam("pa") String pa,
			                                                                          @RequestParam("psa") String psa,
			                                                                          @RequestParam("glCode") String glCode,
																					  @RequestParam("expenseType") Long expenseType) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((ExpenseTypeService)service).findExpenseTypeByIdAndExpenseTypeCompanyPaAndExpenseTypeCompanyPsaAndExpenseTypeCompanyGlCode(pa,psa,glCode,expenseType);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}


	@GetMapping("/findExpenseTypeByIdAndReimburseRoleCode")
	public ResponseEntity<String> findExpenseTypeByIdAndReimburseRoleCode(@RequestParam("roleCode") String roleCode,
																					  @RequestParam("expenseType") Long expenseType) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((ExpenseTypeService)service).findExpenseTypeByIdAndReimburseRoleCode(roleCode,expenseType);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}



	@GetMapping("/findExpenseTypeByExpenseTypeScreenStructureField")
	public ResponseEntity<String> findExpenseTypeByExpenseTypeScreenStructureField(@RequestParam("structureField") String structureField,
																		  @RequestParam("expenseType") Long expenseType) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((ExpenseTypeService)service).findExpenseTypeByExpenseTypeScreenStructureField(structureField,expenseType);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

	@Loggable
	@GetMapping("/findAllExpenseType")
	public ResponseEntity<String> findAllExpenseType() {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((ExpenseTypeService)service).findAllExpenseType();

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}


	@GetMapping("/findByFlowType")
	public ResponseEntity<String> findByFlowType(@RequestParam("flowType") String flowType
																		  ) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((ExpenseTypeService)service).findByFlowType(flowType);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}



	@PostMapping(value="/{id}/expenseType",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity putMasterData(@PathVariable Long id, @RequestParam("expenseType") String expenseType) {
		LOGGER.info("==========================  ID PUT PARENT EXPENSETYPE  ========================{} "+expenseType);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((BaseParentService)service).putParent(id, Arrays.asList(expenseType.split(",")) );
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}



}
