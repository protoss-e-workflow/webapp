package com.spt.app.controller.master;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.CompanyService;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/companies")
public class CompanyController extends BaseCommonController {

	static final Logger LOGGER = LoggerFactory.getLogger(CompanyController.class);

	@Override
	@Resource(name="CompanyService")
	public void setService(BaseCommonService service) {

		super.service = service;
	}

	@Loggable
	@GetMapping("/findCompanyAll")
	public ResponseEntity<String> findCompanyAll() {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = (service).load();
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

	@Loggable
	@GetMapping("/findByCode")
	public ResponseEntity<String> findByCode(@RequestParam(name="code",required=false)
													 String code) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((CompanyService)service).findByCode(code);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

	@Loggable
	@GetMapping("/findAllCompanys")
	public ResponseEntity<String> findAllCompanys() {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((CompanyService)service).findAllCompany();

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

}
