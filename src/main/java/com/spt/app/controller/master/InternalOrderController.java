package com.spt.app.controller.master;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.InternalOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

@Controller
@RequestMapping("/internalOrders")
public class InternalOrderController extends BaseCommonController {

    static final Logger LOGGER = LoggerFactory.getLogger(LocationController.class);

    @Override
    @Resource(name="InternalOrderService")
    public void setService(BaseCommonService service) {
        super.service = service;
    }

    @Loggable
    @GetMapping("/findByIoCode")
    public ResponseEntity<String> findByIoCode(@RequestParam(name="ioCode",required=false) String ioCode) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((InternalOrderService)service).findByIoCode(ioCode);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    @Loggable
    @GetMapping("/findByIoCodeAndCompanyCode")
    public ResponseEntity<String> findByIoCodeAndCompanyCode(@RequestParam(name="ioCode",required=false) String ioCode,@RequestParam(name="companyCode",required=false) String companyCode) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((InternalOrderService)service).findByIoCodeAndCompanyCode(ioCode,companyCode);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    @Loggable
    @GetMapping("/checkIoRealtime")
    public ResponseEntity<String> checkIoRealtime(@RequestParam(name="gl",required=true) String gl,@RequestParam(name="costCenter",required=true) String costCenter,@RequestParam(name="io",required=false) String io,
                                                  @RequestParam(name="manOrder",required=false) String manOrder,@RequestParam(name="wbs",required=false) String wbs,@RequestParam("docNumber") String docNumber,@RequestParam("expItemId") String expItemId) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((InternalOrderService)service).checkIoRealtime(gl, costCenter, io, manOrder, wbs,docNumber,expItemId);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    @Loggable
    @GetMapping("/checkBudgetInEwf")
    public ResponseEntity<String> checkBudgetInEwf(@RequestParam(name="gl",required=true) String gl,@RequestParam(name="costCenter",required=true) String costCenter) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((InternalOrderService)service).checkBudgetInEwf(gl, costCenter);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }
}
