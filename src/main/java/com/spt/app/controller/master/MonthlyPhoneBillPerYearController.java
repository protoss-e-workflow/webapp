package com.spt.app.controller.master;

import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.MonthlyPhoneBillPerYearService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

@Controller
@RequestMapping("/monthlyPhoneBillPerYears")
public class MonthlyPhoneBillPerYearController extends BaseCommonController {

    static final Logger LOGGER = LoggerFactory.getLogger(MonthlyPhoneBillController.class);

    @Override
    @Resource(name="MonthlyPhoneBillPerYearService")
    public void setService(BaseCommonService service) {

        super.service = service;
    }


    @GetMapping("/findMonthlyPhoneBillByEmpUserNameAndYear")
    public ResponseEntity<String> findMonthlyPhoneBillByEmpUserName(@RequestParam("userName") String userName,@RequestParam("year")String year) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((MonthlyPhoneBillPerYearService)service).findByEmpUserNameAndYear(userName,year);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }


    }
}
