package com.spt.app.controller.master;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.CompanyService;
import com.spt.app.service.DelegateService;
import com.spt.app.service.RouteDistanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/delegate")
public class DelegateController extends BaseCommonController {

	static final Logger LOGGER = LoggerFactory.getLogger(DelegateController.class);

	@Override
	@Resource(name="DelegateService")
	public void setService(BaseCommonService service) {

		super.service = service;
	}

	@Loggable
	@GetMapping("/listView")
	public String listView() {


		return "delegate";
	}



	@GetMapping(value="/findAssignerSize",produces="application/json")
	@ResponseBody
	public ResponseEntity<String>  findAssignerSize(HttpServletRequest request) {
		Map<String, Object> objectMap = new HashMap<String, Object>();
		objectMap.put("queryString", request.getQueryString());
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String>  responseEntity =  ((DelegateService)service).findAssignerSize(objectMap);
		return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
	}


	@GetMapping(value="/findAssignerByCriteria",produces="application/json", headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<String>  findAssignerByCriteria(HttpServletRequest request) {
		Map<String, Object> objectMap = new HashMap<String, Object>();
		objectMap.put("queryString", request.getQueryString());
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String>  responseEntity =  ((DelegateService)service).findAssignerByCriteria(objectMap);
		return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);

	}


	@Loggable
	@GetMapping("/findAllDelegateByEmpCodeAssignee")
	public ResponseEntity<String> findAllDelegateByEmpCodeAssignee(@RequestParam(name="empCodeAssignee",required=false)
															  String empCodeAssignee) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DelegateService)service).findAllDelegateByEmpCodeAssignee(empCodeAssignee);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}


	@Loggable
	@GetMapping("/findByEmpCodeAssignee")
	public ResponseEntity<String> findByEmpCodeAssignee(@RequestParam(name="empCodeAssignee",required=false)
																		   String empCodeAssignee) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DelegateService)service).findByEmpCodeAssignee(empCodeAssignee);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

	@Loggable
	@GetMapping("/findByEmpCodeAssigneeAndEmpCodeAssignerAndFlowTypeAndStartDateAndEndDate")
	public ResponseEntity<String> findByEmpCodeAssigneeAndEmpCodeAssignerAndFlowTypeAndStartDateAndEndDate(
			@RequestParam(name="empCodeAssignee",required=false) String empCodeAssignee,
			@RequestParam(name="empCodeAssigner",required=false) String empCodeAssigner,
			@RequestParam(name="flowType",required=false) String flowType,
			@RequestParam(name="startDate",required=false) String startDate,
			@RequestParam(name="endDate",required=false) String endDate



	) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DelegateService)service).findByEmpCodeAssigneeAndEmpCodeAssignerAndFlowTypeAndStartDateAndEndDate(empCodeAssignee,empCodeAssigner,flowType,startDate,endDate);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}




}
