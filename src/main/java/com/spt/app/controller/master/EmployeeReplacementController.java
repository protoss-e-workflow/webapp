package com.spt.app.controller.master;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/employeeReplacement")
public class EmployeeReplacementController extends BaseCommonController {

	static final Logger LOGGER = LoggerFactory.getLogger(EmployeeReplacementController.class);


	@Override
	@Resource(name="EmplyeeReplacementService")
	public void setService(BaseCommonService service) {

		super.service = service;
	}
	@Loggable
	@GetMapping("/listView")
	public String listView() {


		return "employeeReplacement";
	}

	@Loggable
	@GetMapping("/findByEmpCodeReplace")
	public ResponseEntity<String> findByEmpCodeReplace(@RequestParam(name="empCodeReplace",required=false)
																   String empCodeReplace) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((EmployeeReplacementService)service).findByEmpCodeReplace(empCodeReplace);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}




	@GetMapping(value="/findUserAgreeSize",produces="application/json")
	@ResponseBody
	public ResponseEntity<String>  findUserAgreeSize(HttpServletRequest request) {
		Map<String, Object> objectMap = new HashMap<String, Object>();
		objectMap.put("queryString", request.getQueryString());
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String>  responseEntity =  ((EmployeeReplacementService)service).findUserAgreeSize(objectMap);
		return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
	}


	@GetMapping(value="/findUserAgreeByCriteria",produces="application/json", headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<String>  findUserAgreeByCriteria(HttpServletRequest request) {
		Map<String, Object> objectMap = new HashMap<String, Object>();
		objectMap.put("queryString", request.getQueryString());
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String>  responseEntity =  ((EmployeeReplacementService)service).findUserAgreeByCriteria(objectMap);
		return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);

	}




	@Loggable
	@GetMapping("/findByEmpCodeReplaceAndEmpCodeAgree")
	public ResponseEntity<String> findByEmpCodeReplaceAndEmpCodeAgree(@RequestParam(name="empCodeReplace",required=false) String empCodeReplace,
													   @RequestParam(name="empCodeAgree",required=false) String empCodeAgree

	) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((EmployeeReplacementService)service).findByEmpCodeReplaceAndEmpCodeAgree(empCodeReplace,empCodeAgree);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}


}
