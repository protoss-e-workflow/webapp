package com.spt.app.controller.master;

import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.ExpenseTypeByCompanyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/expenseByCompanies")
public class ExpenseTypeByCompanyController extends BaseCommonController {

    static final Logger LOGGER = LoggerFactory.getLogger(ExpenseTypeByCompanyController.class);

    @Override
    @Resource(name="ExpenseTypeByCompanyService")
    public void setService(BaseCommonService service) {
        super.service = service;
    }

    @GetMapping("/findByCompanyCode")
    public ResponseEntity findByCompanyCode(HttpServletRequest request){
        LOGGER.info("--- Find Expense Type By Company (Company Code) ---");
        String companyCode = request.getParameter("companyCode");

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((ExpenseTypeByCompanyService)service).findByCompanyCode(companyCode);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    @GetMapping("/findAllExpenseTypeByCompany")
    public ResponseEntity findAllExpenseTypeByCompany(HttpServletRequest request){
        LOGGER.info("--- Find Expense Type By Company (Company Code) ---");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = (service).load();
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    @GetMapping("/findExpenseTypeByCompanyByPaAndPsaAndKeySearch/{pa}/{psa}/{userName}")
    public ResponseEntity findExpenseTypeByCompanyByPaAndPsaAndKeySearch(@PathVariable String pa,@PathVariable String psa,@RequestParam(value = "headOfficeGL") String headOfficeGL,@RequestParam(value = "factoryGL") String factoryGL,@PathVariable String userName,@RequestParam(value = "keySearch") String keySearch){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((ExpenseTypeByCompanyService)service).findExpenseTypeByCompanyByPaAndPsaAndKeySearch(pa,psa,headOfficeGL,factoryGL,userName,keySearch);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    @GetMapping("/findExpenseTypeByCompanyByPaAndPsaAndFavorite/{pa}/{psa}/{userName}")
    public ResponseEntity findExpenseTypeByCompanyByPaAndPsaAndFavorite(@PathVariable String pa,@PathVariable String psa,@PathVariable String userName){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((ExpenseTypeByCompanyService)service).findExpenseTypeByCompanyByPaAndPsaAndFavorite(pa,psa,userName);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }
}
