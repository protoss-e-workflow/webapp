package com.spt.app.controller.master;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.controller.general.EmployeeController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.EmployeeService;
import com.spt.app.service.PetrolAllowancePerMonthHistoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/petrolAllowancePerMonthHistories")
public class PetrolAllowancePerMonthHistoryController extends BaseCommonController {

	static final Logger LOGGER = LoggerFactory.getLogger(PetrolAllowancePerMonthHistoryController.class);

	@Override
	@Resource(name="PetrolAllowancePerMonthHistoryService")
	public void setService(BaseCommonService service) {

		super.service = service;
	}

	@Loggable
	@GetMapping("/listView")
	public String petrolAllowancePerMonthList(Model ui, @RequestParam(name="empCode",required=false) String empCode) {

		List<Map<String,String>> months = new ArrayList<>();
		Map<String,String> map = new HashMap<>();
		Map<String,String> map1 = new HashMap<>();
		Map<String,String> map2 = new HashMap<>();
		Map<String,String> map3 = new HashMap<>();
		Map<String,String> map4 = new HashMap<>();
		Map<String,String> map5 = new HashMap<>();
		Map<String,String> map6 = new HashMap<>();
		Map<String,String> map7 = new HashMap<>();
		Map<String,String> map8 = new HashMap<>();
		Map<String,String> map9 = new HashMap<>();
		Map<String,String> map10 = new HashMap<>();
		Map<String,String> map11 = new HashMap<>();
		Map<String,String> map12 = new HashMap<>();
		map.put("code","0");
		map.put("description","ทุกเดือน");
		map1.put("code","01");
		map1.put("description","มกราคม");
		map2.put("code","02");
		map2.put("description","กุมภาพันธ์");
		map3.put("code","03");
		map3.put("description","มีนาคม");
		map4.put("code","04");
		map4.put("description","เมษายน");
		map5.put("code","05");
		map5.put("description","พฤษภาคม");
		map6.put("code","06");
		map6.put("description","มิถุนายน");
		map7.put("code","07");
		map7.put("description","กรกฏาคม");
		map8.put("code","08");
		map8.put("description","สิงหาคม");
		map9.put("code","09");
		map9.put("description","กันยายน");
		map10.put("code","10");
		map10.put("description","ตุลาคม");
		map11.put("code","11");
		map11.put("description","พฤศจิกายน");
		map12.put("code","12");
		map12.put("description","ธันวาคม");
		months.add(map);
		months.add(map1);
		months.add(map2);
		months.add(map3);
		months.add(map4);
		months.add(map5);
		months.add(map6);
		months.add(map7);
		months.add(map8);
		months.add(map9);
		months.add(map10);
		months.add(map11);
		months.add(map12);
		ui.addAttribute("monthList",months);
		ui.addAttribute("cr_empCode",empCode);
		return "petrolAllowancePerMonthHistories";
	}



	@GetMapping("/findPetrolAllowancePerMonthHistoryByEmpCode")
	public ResponseEntity<String> findPetrolAllowancePerMonthHistoryByEmpCode(@RequestParam("empCode") String empCode) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((PetrolAllowancePerMonthHistoryService)service).findByEmpCode(empCode);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}


	}

	@PostMapping(value="/savePetrolAllowancePerMonthHistoryForExpense",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity savePetrolAllowancePerMonthHistoryForExpense(HttpServletRequest request) {
		LOGGER.info("-= savePetrolAllowancePerMonthHistoryForExpense =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((PetrolAllowancePerMonthHistoryService)service).savePetrolAllowancePerMonthHistoryForExpense(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("Error", headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value="/deletePetrolAllowancePerMonthHistoryForExpense",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity deletePetrolAllowancePerMonthHistoryForExpense(HttpServletRequest request) {
		LOGGER.info("-= deletePetrolAllowancePerMonthHistoryForExpense =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((PetrolAllowancePerMonthHistoryService)service).deletePetrolAllowancePerMonthHistoryForExpense(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("Error", headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value="/updatePetrolAllowancePerMonthHistoryForExpense",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity updatePetrolAllowancePerMonthHistoryForExpense(HttpServletRequest request) {
		LOGGER.info("-= updatePetrolAllowancePerMonthHistoryForExpense =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((PetrolAllowancePerMonthHistoryService)service).updatePetrolAllowancePerMonthHistoryForExpense(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("Error", headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
