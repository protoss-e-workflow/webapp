package com.spt.app.controller.master;

import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.ExpenseTypeReferenceService;
import com.spt.app.service.ExpenseTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

@Controller
@RequestMapping("/expenseReference")
public class ExpenseReferenceController extends BaseCommonController {

	static final Logger LOGGER = LoggerFactory.getLogger(ExpenseReferenceController.class);


	@Override
	@Resource(name="ExpenseTypeReferenceService")
	public void setService(BaseCommonService service) {

		super.service = service;
	}




}
