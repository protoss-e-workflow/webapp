package com.spt.app.controller.master;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.CompanyService;
import com.spt.app.service.ConditionalIOService;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/conditionalIOs")
public class ConditionalIOController extends BaseCommonController {

	static final Logger LOGGER = LoggerFactory.getLogger(ConditionalIOController.class);

	@Override
	@Resource(name="ConditionalIOService")
	public void setService(BaseCommonService service) {

		super.service = service;
	}

	@Loggable
	@GetMapping("/listView")
	public String conditionalIOList() {

//		List<Map> companyList = ((CompanyService)service).findCompanyAlls();
//		modelMap.addAttribute("companyList", findCompanyAlls());

		return "conditionalIOs";
	}

	@PostMapping(value="/saveConditionalIO",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity saveConditionalIO(HttpServletRequest request) {
		LOGGER.info("-= SAVE ROUTE DISTANCE =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((ConditionalIOService)service).saveConditionalIO(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}



	@GetMapping("/findConditionalIOByGlAndIoAndCompany")
	public ResponseEntity<String> findConditionalIOByGlAndIoAndCompany(@RequestParam("gl") String gl, @RequestParam("io") String io, @RequestParam("company") Long company) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((ConditionalIOService)service).findConditionalIOByGlAndIoAndCompany(gl,io,company);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}


	}



	@GetMapping("/findByCompanyCode")
	public ResponseEntity<String> findByCompanyCode(@RequestParam("code") String code) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((ConditionalIOService)service).findByCompanyCode(code);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}


	}
}
