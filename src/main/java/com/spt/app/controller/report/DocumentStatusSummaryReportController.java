package com.spt.app.controller.report;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.DocumentStatusSummaryReportService;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.activation.MimetypesFileTypeMap;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;

@Controller
@RequestMapping("/documentStatusSummary")
public class DocumentStatusSummaryReportController extends BaseCommonController {

    static final Logger LOGGER = LoggerFactory.getLogger(DocumentStatusSummaryReportController.class);

    @Override
    @Resource(name="DocumentStatusSummaryReportService")
    public void setService(BaseCommonService service) {
        super.service = service;
    }

    @Loggable
    @GetMapping("/listView")
    public String listView() {
        return "documentStatusSummary";
    }

    @Loggable
    @GetMapping("/CUS001")
    public ResponseEntity<String> user(HttpServletResponse response,@RequestParam(name="startDate") String startDate,
                                       @RequestParam(name="endDate") String endDate) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity = null;

        ByteArrayInputStream byteArrayInputStream = null;
        try {
            String mimeType = new MimetypesFileTypeMap().getContentType("CUS001.xlsx");
            response.setContentType(mimeType);
            response.addHeader("Content-Disposition","attachment; filename*=UTF-8''"+"CUS001.xlsx");

            String url = "/exportCustom/CUS001?startDate="+startDate+"&endDate="+endDate;

            byte[] fileBytes = ((DocumentStatusSummaryReportService)service).downloadCUS001(url);
            byteArrayInputStream = new ByteArrayInputStream(fileBytes);
            IOUtils.copy(byteArrayInputStream, response.getOutputStream());

            return new ResponseEntity<String>(headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }
}
