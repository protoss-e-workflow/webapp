package com.spt.app.controller.report;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.RoleService;
import com.spt.app.service.UserService;

@Controller
@RequestMapping("/report02")
public class Report02Controller {

	static final Logger LOGGER = LoggerFactory.getLogger(Report02Controller.class);
	
	
	@Loggable
	@GetMapping("/listView")
	public String listView() {
		return "report02";
	}
	

}
