package com.spt.app.controller.application;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.DocumentApproveCostEstimateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/approveCostEstimates")
public class DocumentApproveCostEstimateController extends BaseCommonController {

    static final Logger LOGGER = LoggerFactory.getLogger(DocumentApproveCostEstimateController.class);

    @Override
    @Resource(name="DocumentApproveCostEstimateService")
    public void setService(BaseCommonService service) {
        super.service = service;
    }

    @Loggable
    @GetMapping("/findByDocumentApprove")
    public ResponseEntity<String> findByDocumentApprove(@RequestParam(name="documentApprove",required=false) String documentApprove) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((DocumentApproveCostEstimateService)service).findByDocumentApprove(documentApprove);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    @PostMapping(value="/saveDocumentCostEstimate",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity saveDocumentCostEstimate(HttpServletRequest request) {
        LOGGER.info("-= SAVE DOCUMENT COST ESTIMATE=-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
            responseEntity = ((DocumentApproveCostEstimateService)service).saveDocumentCostEstimate(mapClone);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value="/updateDocumentCostEstimate",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity updateDocumentCostEstimate(HttpServletRequest request) {
        LOGGER.info("-= UPDATE DOCUMENT COST ESTIMATE=-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
            responseEntity = ((DocumentApproveCostEstimateService)service).updateDocumentCostEstimate(mapClone);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(value="/deleteCostEstimate/{id}",produces="application/json", headers = "Accept=application/json")
    @ResponseBody
    public ResponseEntity<String>  deleteCostEstimate(@PathVariable Long id) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((DocumentApproveCostEstimateService)service).deleteCostEstimate(id);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }
}
