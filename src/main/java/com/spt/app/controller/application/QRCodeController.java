package com.spt.app.controller.application;

import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.QRCodeService;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/qrcode")
public class QRCodeController extends BaseCommonController {

    @Override
    @javax.annotation.Resource(name="QRCodeService1")
    public void setService(BaseCommonService service) {
        super.service = service;
    }

    @GetMapping("/generateQrCode")
    @ResponseBody
    public ResponseEntity<Resource> generateQrCode(@RequestParam("text") String text) {

        ResponseEntity<Resource> responseEntity = null;
        try {
            responseEntity = ((QRCodeService)service).generateQrCode(text);
        } catch (Exception e) {
           e.printStackTrace();
        }
        return responseEntity;
    }
}
