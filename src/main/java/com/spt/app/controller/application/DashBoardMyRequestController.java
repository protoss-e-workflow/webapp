package com.spt.app.controller.application;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.DashBoardMyRequestService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Map;

@Controller
@RequestMapping("/dashboardMyRequest")
public class DashBoardMyRequestController extends BaseCommonController{

    @Resource(name = "DashBoardMyRequestService ")
    @Override
    public void setService(BaseCommonService service) {
        super.service = service;
    }


    @Loggable
    @GetMapping(value="/findMyRequestNotification",produces="application/json", headers = "Accept=application/json")
    public ResponseEntity<String> findMyRequestNotification(
            @RequestParam("requester") String requester,
            @RequestParam("docNumber") String docNumber,
            @RequestParam("titleDescription") String titleDescription,
            @RequestParam("documentStatus") String documentStatus
    ){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((DashBoardMyRequestService) service).findMyRequestNotification(requester,docNumber,titleDescription,documentStatus);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }
}
