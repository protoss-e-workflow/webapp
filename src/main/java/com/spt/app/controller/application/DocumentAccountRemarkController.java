package com.spt.app.controller.application;

import com.google.gson.*;
import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.DocumentAccountRemarkService;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/documentAccountRemarks")
public class DocumentAccountRemarkController extends BaseCommonController {

    static final Logger LOGGER = LoggerFactory.getLogger(DocumentAccountRemarkController.class);

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, deser).create();

    @Override
    @Resource(name="DocumentAccountRemarkService")
    public void setService(BaseCommonService service) {

        super.service = service;
    }

    @Loggable
    @GetMapping("/findByDocumentId")
    public ResponseEntity<String> findByDocumentId(@RequestParam(name="docId",required=false) String docId) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((DocumentAccountRemarkService)service).findByDocumentId(docId);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    @PostMapping(value="/save",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity save(HttpServletRequest request) {
        LOGGER.info("-= SAVE DOCUMENT ACCOUNT REMARK =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> result = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
            ResponseEntity<String> responseEntity = (service).save(mapClone);
            if(!"".equals(responseEntity.getBody())){
                Map model = gson.fromJson(responseEntity.getBody(),Map.class);
                String id = String.valueOf(model.get("id"));
                result = ((DocumentAccountRemarkService)service).sendEmail(id);
                return new ResponseEntity<String>(result.getBody(), headers, HttpStatus.OK);
            }else{
                return new ResponseEntity<String>("", headers, HttpStatus.OK);
            }

        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }
}
