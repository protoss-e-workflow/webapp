package com.spt.app.controller.application;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.DocumentApproveService;
import com.spt.app.service.DocumentExpenseItemService;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.activation.MimetypesFileTypeMap;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Base64;

@Controller
@RequestMapping("/expenseItem")
public class DocumentExpenseItemController extends BaseCommonController {

    static final Logger LOGGER = LoggerFactory.getLogger(DocumentExpenseItemController.class);

    @Override
    @Resource(name="DocumentExpenseItemService")
    public void setService(BaseCommonService service) {
        super.service = service;
    }

    @Loggable
    @GetMapping("/findDocumentExpenseItemByDocumentExp")
    public ResponseEntity<String> findDocumentExpenseItemByDocumentExp(@RequestParam(name="id",required=false) String id) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((DocumentExpenseItemService)service).findDocumentExpenseItemByDocumentExp(id);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }



    @Loggable
    @GetMapping("/downloadDocumentExpItemAttachment")
    public ResponseEntity<String> downloadFileDocumentAttachment(@RequestParam("id") Long id,
                                                                 @RequestParam("fileName") String fileName,
                                                                 HttpServletResponse response) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity = null;

        ByteArrayInputStream byteArrayInputStream = null;
        try {
            LOGGER.info("><><><>< downloadFileDocumentAttachment ><><><>");
            String mimeType = new MimetypesFileTypeMap().getContentType(fileName);
            response.setContentType(mimeType);
            response.addHeader("Content-Disposition","attachment; filename*=UTF-8''"+fileName);

            String url = "/documentExpenseItemAttachmentCustom/downloadFileAttachment/"+id;

            byte[] fileBytes = ((DocumentExpenseItemService)service).downloadFileDocumentAttachment(url);
            byteArrayInputStream = new ByteArrayInputStream(fileBytes);
            IOUtils.copy(byteArrayInputStream, response.getOutputStream());

            return new ResponseEntity<String>(headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }




    @Loggable
    @GetMapping("/preViewPDFDocumentExpItemAttachment")
    public ResponseEntity<String> preViewPDFDocumentExpItemAttachment(@RequestParam("id") Long id,
                                                                 @RequestParam("fileName") String fileName,
                                                                 HttpServletResponse response) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity = null;
        InputStream in = null;
        OutputStream outputStream1 = null;
        ByteArrayInputStream inputStream = null;
        ByteArrayInputStream byteArrayInputStream = null;
        try {
            LOGGER.info("><><><>< preViewPDFDocumentExpItemAttachment ><><><>");


            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "inline; filename=\""+fileName);

            String url = "/documentExpenseItemAttachmentCustom/downloadFileAttachment/"+id;

            byte[] fileBytes = ((DocumentExpenseItemService)service).downloadFileDocumentAttachment(url);
            inputStream = new ByteArrayInputStream(fileBytes);
            outputStream1 = response.getOutputStream();
            IOUtils.copy(inputStream, outputStream1);

//            IOUtils.copy(byteArrayInputStream, response.getOutputStream());

            return new ResponseEntity<String>(headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }finally {
            IOUtils.closeQuietly(in);
        }
    }

    @Loggable
    @GetMapping(value = "/preViewIMAGEDocumentExpItemAttachment", produces = "text/html", headers = "Accept=application/json")
    public String preViewIMAGEDocumentExpItemAttachment(@RequestParam("id") Long id,
                                                                      @RequestParam("fileName") String fileName,
                                                                      Model uiModel,
                                                                      HttpServletResponse response) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity = null;
        InputStream in = null;
        OutputStream outputStream1 = null;
        ByteArrayInputStream inputStream = null;
        ByteArrayInputStream byteArrayInputStream = null;
        try {
            LOGGER.info("><><><>< preViewIMAGEDocumentExpItemAttachment ><><><>");




            String url = "/documentExpenseItemAttachmentCustom/downloadFileAttachment/"+id;

            byte[] fileBytes = ((DocumentExpenseItemService)service).downloadFileDocumentAttachment(url);

            String encodedString = Base64.getEncoder().encodeToString(fileBytes);
            uiModel.addAttribute("imageInByte", encodedString);


        } catch (Exception e) {

            e.printStackTrace();
        }finally {
//            IOUtils.closeQuietly(in);
        }

        return "preViewImageFile";
    }

    @Loggable
    @GetMapping("/findDocumentAmountSeperateByAmount")
    public ResponseEntity<String> findByEmpCodeAssignee() {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((DocumentExpenseItemService)service).findDocumentAmountSeperateByType();

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }






    @GetMapping("/findAllDocumentExceptDocStatusCCLAndREJ")
    @ResponseBody
    public ResponseEntity<String> findAllDocumentExceptDocStatusCCLAndREJ(@RequestParam(value = "dateStart", required = false) String dateStart,
                                                                          @RequestParam(value = "dateEnd", required = false) String dateEnd,
                                                                          @RequestParam(value = "requester", required = false) String requester,
                                                                          @RequestParam(value = "docNumber", required = false) String docNumber,
                                                                          @RequestParam(value = "firstResult", required = true) Integer first,
                                                                          @RequestParam(value = "maxResult", required = true) Integer max,
                                                                          HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        try {
            ResponseEntity<String> responseEntity = ((DocumentExpenseItemService)service).findAllDocumentExceptDocStatusCCLAndREJ(dateStart,dateEnd,requester,docNumber,first,max);
            return new ResponseEntity<String>(responseEntity.getBody(),headers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("{\"ERROR\":" +e.getMessage()+ "\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/findAllDocumentExceptDocStatusCCLAndREJSize")
    @ResponseBody
    public ResponseEntity<String> findAllDocumentExceptDocStatusCCLAndREJSize(@RequestParam(value = "dateStart", required = false) String dateStart,
                                                                              @RequestParam(value = "dateEnd", required = false) String dateEnd,
                                                                              @RequestParam(value = "requester", required = false) String requester,
                                                                              @RequestParam(value = "docNumber", required = false) String docNumber,
                                                                              HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        try {
            ResponseEntity<String> responseEntity = ((DocumentExpenseItemService)service).findAllDocumentExceptDocStatusCCLAndREJSize(dateStart,dateEnd,requester,docNumber);
            return new ResponseEntity<String>(responseEntity.getBody(),headers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("{\"ERROR\":" +e.getMessage()+ "\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }




}
