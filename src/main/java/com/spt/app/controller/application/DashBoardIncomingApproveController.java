package com.spt.app.controller.application;

import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

@Controller
@RequestMapping("/dashBoardIncomingApprove")
public class DashBoardIncomingApproveController extends BaseCommonController{

    @Resource(name = "DashBoardIncomingApproveService")
    @Override
    public void setService(BaseCommonService service) {
        super.service = service;
    }
}
