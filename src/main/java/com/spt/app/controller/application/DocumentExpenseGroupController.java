package com.spt.app.controller.application;

import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.DocumentExpenseGroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping("/documentExpenseGroups")
public class DocumentExpenseGroupController extends BaseCommonController {

    static final Logger LOGGER = LoggerFactory.getLogger(DocumentExpenseGroupController.class);

    @Override
    @Resource(name="DocumentExpenseGroupService")
    public void setService(BaseCommonService service) {
        super.service = service;
    }

    @DeleteMapping(value="deleteDocumentExpenseGroup/{id}",produces="application/json", headers = "Accept=application/json")
    @ResponseBody
    public ResponseEntity<String> deleteDocumentAttachment(@PathVariable Long id) {
        LOGGER.info("><><><>< DELETE DOCUMENT EXPENSE GROUP BY ID : {} ",id);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((DocumentExpenseGroupService)service).deleteDocumentExpenseGroup(id);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }
}
