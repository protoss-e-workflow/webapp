package com.spt.app.controller.application;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.CheckAccommodationService;
import com.spt.app.service.CostCenterService;
import flexjson.JSONSerializer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/checkAccommodations")
public class CheckAccommodationController extends BaseCommonController {

    @Override
    @Resource(name="CheckAccommodationService")
    public void setService(BaseCommonService service) {

        super.service = service;
    }

    @Loggable
    @GetMapping("/checkAccommodationAuthorize")
    public ResponseEntity<String> checkAccommodationAuthorize(@RequestParam("docNumber") String docNumber,
                                                              @RequestParam("itemId") String itemId,
                                                              @RequestParam("isIbisStd") String isIbisStd,
                                                              @RequestParam("diffDays") String diffDays,
                                                              @RequestParam("zoneType") String zoneType) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((CheckAccommodationService)service).checkAccommodationAuthorize(docNumber,itemId,isIbisStd,zoneType,diffDays);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }
}
