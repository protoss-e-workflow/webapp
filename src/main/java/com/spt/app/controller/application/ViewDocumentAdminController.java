package com.spt.app.controller.application;

import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.ViewAllDocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/viewDoc")
public class ViewDocumentAdminController extends BaseCommonController{

    private static Logger LOGGER= LoggerFactory.getLogger(ViewDocumentAdminController.class);

    @Override
    @Resource(name = "ViewAllDocumentService")
    public void setService(BaseCommonService service) {
        super.service = service;
    }

    @GetMapping("/listView")
    public String viewAllDocument(){
        return "viewAllDocument";
    }

    @GetMapping(value = "/viewAllDoc",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity<String> viewAllDocList(
            @RequestParam(value = "createdDateStart", required = false) String createdDateStart,
            @RequestParam(value = "createdDateEnd", required = false) String createdDateEnd,
            @RequestParam(value = "creator", required = false) String creator,
            @RequestParam(value = "documentNumber", required = false) String documentNumber,
            @RequestParam(value = "documentType", required = false) String documentType,
            @RequestParam(value = "documentStatus", required = false) String documentStatus,
            @RequestParam(value = "firstResult", required = true) Long first,
            @RequestParam(value = "maxResult", required = true) Long max,
            HttpServletRequest request){

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        LOGGER.info("document all service : {}",documentStatus);

        try {
            ResponseEntity<String> responseEntity = ((ViewAllDocumentService)service).findByCreatedDateStartAndCreatedDateEndAndCreatorAndDocNumberAndDocumentType(
                    createdDateStart,createdDateEnd,creator,documentNumber,documentType,documentStatus,first,max);
            return new ResponseEntity<String>(responseEntity.getBody(),headers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("{\"ERROR\":" +e.getMessage()+ "\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/viewAllDocSize",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity<String> viewAllDoclistSize(
            @RequestParam(value = "createdDateStart",required = false) String createdDateStart,
            @RequestParam(value = "createdDateEnd",required = false) String createdDateEnd,
            @RequestParam(value = "creator",required = false) String creator,
            @RequestParam(value = "documentNumber",required = false) String documentNumber,
            @RequestParam(value = "documentType",required = false) String documentType,
            @RequestParam(value = "documentStatus", required = false) String documentStatus,
            HttpServletRequest request) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        try {
            ResponseEntity<String> responseEntity = ((ViewAllDocumentService)service).pagingFindByCreatedDateStartAndCreatedDateEndAndCreatorAndDocNumberAndDocumentType(
                    createdDateStart,createdDateEnd,creator,documentNumber,documentType,documentStatus);
            LOGGER.info("data {}",responseEntity.getBody());
            return new ResponseEntity<String>(responseEntity.getBody(),headers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("{\"ERROR\":" +e.getMessage()+ "\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
