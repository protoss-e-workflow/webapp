package com.spt.app.controller.application;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.spt.app.constant.ApplicationConstant;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.DocumentExpenseService;
import com.spt.app.service.DocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.spt.app.aop.annotation.Loggable;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
@RequestMapping("/expense")
public class DocumentExpenseController extends BaseCommonController {

	static final Logger LOGGER = LoggerFactory.getLogger(DocumentExpenseController.class);

	@Value("${UrlEmed}")
	private String URL_EMED ;

	@Override
	@Resource(name="DocumentExpenseService")
	public void setService(BaseCommonService service) {
		super.service = service;
	}

	@Loggable
	@GetMapping("/listView")
	public String listView() {
		return "expenseListView";
	}

	@Loggable
	@GetMapping("/expenseDetail")
	public String expenseDetail(ModelMap map,@RequestParam(value = "doc",required = false) String doc) {
		if(null != doc){
			JsonParser parser = new JsonParser();
			ResponseEntity<String> responseEntity = service.get(Long.valueOf(doc));
			JsonElement obj = parser.parse(responseEntity.getBody());
			map.addAttribute("document", obj);
		}
		map.addAttribute("requestStatusCode", ApplicationConstant.MASTER_DATA_REQUEST_STATUS);
		map.addAttribute("URL_EWF",ApplicationConstant.URL_EWF);
		map.addAttribute("URL_EMED",URL_EMED);
		return "expenseDetail";
	}

	@Loggable
	@GetMapping("/createDoc")
	public String createDoc() {
		return "expenseCreateDoc";
	}

	@Loggable
	@GetMapping("/clearExpense")
	public String clearExpense() {
		return "clearExpense";
	}

	@Loggable
	@GetMapping("/clearExpenseDetail")
	public String clearExpenseDetail(ModelMap map,@RequestParam("doc") String doc) {
		Gson gson = new Gson();
		if(null != doc){
			JsonParser parser = new JsonParser();
			ResponseEntity<String> responseEntity = service.get(Long.valueOf(doc));
			JsonElement obj = parser.parse(responseEntity.getBody());
			map.addAttribute("document", obj);
		}
		map.addAttribute("requestStatusCode", ApplicationConstant.MASTER_DATA_REQUEST_STATUS);
		map.addAttribute("flowTypeCode",ApplicationConstant.MASTER_DATA_EXPENSE_TYPE);
		map.addAttribute("URL_EMED",URL_EMED);
		return "clearExpenseDetail";
	}

	@Loggable
	@GetMapping("/reimburseExpense")
	public String reimburseExpense() {
		return "reimburseExpense";
	}

	@Loggable
	@GetMapping("/reimburseExpenseDetail")
	public String reimburseExpenseDetail(ModelMap map,@RequestParam("doc") String doc) {
		if(null != doc){
			JsonParser parser = new JsonParser();
			ResponseEntity<String> responseEntity = service.get(Long.valueOf(doc));
			JsonElement obj = parser.parse(responseEntity.getBody());
			map.addAttribute("document", obj);
		}
		return "reimburseExpenseDetail";
	}

	/* update by iMilkii 2017.09.15 for setup menu */
	@Loggable
	@GetMapping("/expenseMainMenu")
	public String mainMenu() {
		return "expenseMainMenu";
	}

	@PostMapping(value="/saveDocument",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity saveDocument(HttpServletRequest request) {
		LOGGER.info("-= SAVE DOCUMENT EXPENSE =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentService)service).saveDocument(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	@PostMapping(value="/saveDocumentExpenseItem",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity saveDocumentExpenseItem(HttpServletRequest request) {
		LOGGER.info("-= SAVE DOCUMENT EXPENSE ITEM =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentExpenseService)service).saveDocumentExpenseItem(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	@PostMapping(value="/updateDocumentStatus",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity updateDocumentStatus(HttpServletRequest request) {
		LOGGER.info("-= UPDATE DOCUMENT STATUS EXPENSE =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentExpenseService)service).updateDocumentStatus(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/* Manage Document Attachment */
	@PostMapping("/saveDocumentAttachment")
	public ResponseEntity saveDocumentAttachment(MultipartHttpServletRequest multipartHttpServletRequest) {
		LOGGER.info("-= SAVE DOCUMENT ATTACHMENT EXPENSE =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		try {
			String url = "/documentAttachmentCustom/uploadFileAttachment";
			responseEntity = ((DocumentExpenseService)service).uploadFile(multipartHttpServletRequest,url);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	@GetMapping(value="/{id}/documentAttachment",produces="application/json", headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseEntity<String>  getDocumentAttachmentByDocument(@PathVariable Long id) {
		LOGGER.info("><><><>< GET DOCUMENT ATTACHMENT EXPENSE BY DOCUMENT  ><><><><");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DocumentExpenseService)service).findDocumentAttachmentByDocumentId(id);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	@DeleteMapping(value="deleteDocumentAttachment/{id}",produces="application/json", headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<String>  deleteDocumentAttachment(@PathVariable Long id) {
		LOGGER.info("><><><>< DELETE DOCUMENT ATTACHMENT EXPENSE BY ID : {} ",id);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DocumentExpenseService)service).deleteDocumentAttachment(id);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	/* copy document */
	@PostMapping(value="/copyDocument",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity copyDocument(HttpServletRequest request) {
		LOGGER.info("-= COPY DOCUMENT EXPENSE =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentService)service).copyDocument(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	@PostMapping(value="/saveDocumentExpenseItemDetail",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity saveDocumentExpenseItemDetail(HttpServletRequest request) {
		LOGGER.info("-= SAVE DOCUMENT EXPENSE ITEM DETAIL =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentExpenseService)service).saveDocumentExpenseItemDetail(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	@PostMapping(value="/updateDocumentExpenseItemDetail",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity updateDocumentExpenseItemDetail(HttpServletRequest request) {
		LOGGER.info("-= UPDATE DOCUMENT EXPENSE ITEM DETAIL =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentExpenseService)service).updateDocumentExpenseItemDetail(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	/* Manage Document Reference */
	@PostMapping(value="/saveDocumentReference",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity saveDocumentReference(HttpServletRequest request) {
		LOGGER.info("-= SAVE DOCUMENT REFERENCE EXPENSE =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentExpenseService)service).saveDocumentReference(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	@PostMapping(value="/updateDocumentReference",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity updateDocumentReference(HttpServletRequest request) {
		LOGGER.info("-= UPDATE DOCUMENT REFERENCE EXPENSE =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentExpenseService)service).updateDocumentReference(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	@PostMapping(value="/updateDocumentReferenceList",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity updateDocumentReferenceList(HttpServletRequest request) {
		LOGGER.info("-= UPDATE DOCUMENT REFERENCE EXPENSE LIST =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentExpenseService)service).updateDocumentReferenceList(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping(value="/getAuthorizeForLineApprove",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity getAuthorizeForLineApprove(HttpServletRequest request) {
		LOGGER.info("-= GET AUTHORIZE  =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		String responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentExpenseService)service).getAuthorizeForLineApprove(mapClone);

			return new ResponseEntity<String>(responseEntity, headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	/* Manage Document Attachment */
	@PostMapping("/saveDocumentExpenseItemAttachment")
	public ResponseEntity saveDocumentExpenseItemAttachment(MultipartHttpServletRequest multipartHttpServletRequest) {
		LOGGER.info("-= SAVE DOCUMENT EXPENSE ITEM ATTACHMENT =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		try {
			String url = "/documentExpenseItemAttachmentCustom/uploadFile";
			responseEntity = ((DocumentExpenseService)service).uploadFileDocumentExpenseItem(multipartHttpServletRequest,url);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	@GetMapping(value="/findDocumentExpenseItemAttachmentByDocumentExpenseItemId/{id}",produces="application/json", headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseEntity<String>  getDocumentExpenseItemAttachmentByDocumentExpenseItemId(@PathVariable Long id) {
		LOGGER.info("><><><>< GET DOCUMENT ATTACHMENT EXPENSE BY DOCUMENT  ><><><><");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DocumentExpenseService)service).findDocumentExpenseItemAttachmentByDocumentExpenseItemId(id);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	@DeleteMapping(value="deleteDocumentExpenseItemAttachment/{id}",produces="application/json", headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<String>  deleteDocumentExpenseItemAttachment(@PathVariable Long id) {
		LOGGER.info("><><><>< DELETE DOCUMENT EXPENSE ITEM ATTACHMENT BY ID : {} ",id);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DocumentExpenseService)service).deleteDocumentExpenseItemAttachment(id);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	@Loggable
	@GetMapping("/findByDocNumber")
	public ResponseEntity<String> findByDocNumber(@RequestParam(name="docNumber",required=false)
														  String docNumber) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DocumentExpenseService)service).findByDocNumber(docNumber);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

	@PostMapping(value="/expenseTransfer",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity expenseTransfer(HttpServletRequest request) {
		LOGGER.info("-= EXPENSE TRANSFER =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentExpenseService)service).expenseTransfer(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	@Loggable
	@GetMapping(value="/getExpenseTypeCodeForeign")
	public ResponseEntity getExpenseTypeCodeForeign() {
		LOGGER.info("-= GET EXPENSE TYPE CODE FOREIGN  =-");

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		String glCode = "";
		for(String str : ApplicationConstant.EXPENSE_TYPE_FOREIGN){
			glCode += str+",";
		}
		if(glCode != ""){
			glCode = glCode.substring(0,glCode.lastIndexOf(","));
		}
		return new ResponseEntity<String>(glCode, headers, HttpStatus.OK);
	}

	@Loggable
	@PostMapping(value = "/updateCostCenterCode")
	public ResponseEntity updateCostCenterCode(HttpServletRequest request){
		LOGGER.info("-== UPDATE COST CENTER CODE ==-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentExpenseService)service).updateCostCenterCode(mapClone);

			return responseEntity;
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Loggable
	@PostMapping(value = "/updateDocumentExpenseItemAndDocumentExpenseGroupByExpenseTypeByCompany")
	public ResponseEntity updateDocumentExpenseItemAndDocumentExpenseGroupByExpenseTypeByCompany(HttpServletRequest request){
		LOGGER.info("-== updateDocumentExpenseItemAndDocumentExpenseGroupByExpenseTypeByCompany ==-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentExpenseService)service).updateDocumentExpenseItemAndDocumentExpenseGroupByExpenseTypeByCompany(mapClone);

			return responseEntity;
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Loggable
	@GetMapping(value="/{id}",produces="application/json", headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseEntity<String> getDocumentExpenseById(@PathVariable Long id) {
		LOGGER.info("><><><>< GET DOCUMENT EXPENSE BY DOCUMENT ID ><><><><");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = service.get(id);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}
}
