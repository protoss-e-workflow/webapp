package com.spt.app.controller.application;

import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.DashBoardIncomingExpenseService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/dashBoardIncomingExpense")
public class DashBoardIncomingExpenseController extends BaseCommonController{

    @Override
    @Resource(name = "DashBoardIncomingExpenseService")
    public void setService(BaseCommonService service) {
        super.service = service;
    }

    @GetMapping("/findCriteriaWithCompanyCode")
    @ResponseBody
    public ResponseEntity<String> findCriteriaWithCompanyCode(HttpServletRequest request){
        Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("queryString", request.getQueryString());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  ((DashBoardIncomingExpenseService) service).findByCriteriaWithCompanyCode(objectMap);
        return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
    }

    @GetMapping("/findSizeWithCompanyCode")
    @ResponseBody
    public ResponseEntity<String> findSizeWithCompanyCode(HttpServletRequest request){
        Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("queryString", request.getQueryString());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  ((DashBoardIncomingExpenseService) service).findSizeWithCompanyCode(objectMap);
        return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
    }
}
