package com.spt.app.controller.application;


import com.google.gson.*;
import com.spt.app.aop.annotation.Loggable;
import com.spt.app.constant.ApplicationConstant;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.*;

import com.spt.app.service.impl.RequestServiceImpl;
import flexjson.JSON;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.activation.MimetypesFileTypeMap;
import javax.annotation.Resource;
import javax.json.Json;
import javax.json.JsonArray;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Controller
@RequestMapping("/approve")
public class ApproveController  extends BaseCommonController{

	static final Logger LOGGER = LoggerFactory.getLogger(ApproveController.class);

	List<Map> listApprove;

	@Override
	@Resource(name="DocumentApproveService")
	public void setService(BaseCommonService service) {

		super.service = service;
	}

	@Autowired
	EmployeeReplacementService employeeReplacementService;

	@Autowired
	MasterDataService masterDataService;

	@Autowired RequestService requestService;

	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
		public Date deserialize(JsonElement json, Type typeOfT,
								JsonDeserializationContext context) throws JsonParseException {
			return json == null ? null : new Date(json.getAsLong());
		}
	};

	protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();


	@Loggable
	@GetMapping("/listView")
	public String listView(ModelMap model) {
		model.addAttribute("TYPE_REQUEST","APP");
		return "myRequestList";
	}

	@Loggable
	@GetMapping("/approveMainMenu")
	public String mainMenu(Model uiModel) {
		uiModel.addAttribute("approveTypeCode",ApplicationConstant.MASTER_DATA_APPROVE_TYPE);
		return "approveMainMenu";
	}

	@Loggable
	@GetMapping("/createDoc")
	public String createDoc(Model uiModel) {
		uiModel.addAttribute("documentTypeCode",ApplicationConstant.MASTER_DATA_DOCUMENT_TYPE);
		uiModel.addAttribute("approveTypeCode",ApplicationConstant.MASTER_DATA_APPROVE_TYPE);
		uiModel.addAttribute("documentStatusCode",ApplicationConstant.MASTER_DATA_DOCUMENT_STATUS);
		return "approveCreateDoc";
	}

	@Loggable
	@GetMapping("/createDocDetail")
	public String createDocDetail(@RequestParam("doc") String id,Model uiModel) {
		Gson gson = new Gson();
		uiModel.addAttribute("document", gson.toJson(service.get(Long.valueOf(id)).getBody()).replace("\\n", ""));
		uiModel.addAttribute("documentTypeCode",ApplicationConstant.MASTER_DATA_DOCUMENT_TYPE);
		uiModel.addAttribute("approveTypeCode",ApplicationConstant.MASTER_DATA_APPROVE_TYPE);
		uiModel.addAttribute("documentStatusCode",ApplicationConstant.MASTER_DATA_DOCUMENT_STATUS);
		uiModel.addAttribute("carTypeCode",ApplicationConstant.MASTER_DATA_CAR_TYPE);
		uiModel.addAttribute("attachmentTypeCode",ApplicationConstant.MASTER_DATA_ATTACHMENT_TYPE);
		uiModel.addAttribute("airlineCode",ApplicationConstant.MASTER_DATA_AIRLINE);
		uiModel.addAttribute("hotelCode",ApplicationConstant.MASTER_DATA_HOTEL);
		uiModel.addAttribute("flowTypeCode",ApplicationConstant.MASTER_DATA_EXPENSE_TYPE);
		uiModel.addAttribute("URL_EWF",ApplicationConstant.URL_EWF);
		return "approveCreateDocDetail";
	}

	@Loggable
	@GetMapping("/viewCreateDocDetail")
	public String viewCreateDocDetail(@RequestParam("doc") String id,Model uiModel) {
		Gson gson = new Gson();
		uiModel.addAttribute("document", gson.toJson(service.get(Long.valueOf(id)).getBody()).replace("\\n", ""));
		uiModel.addAttribute("documentTypeCode",ApplicationConstant.MASTER_DATA_DOCUMENT_TYPE);
		uiModel.addAttribute("approveTypeCode",ApplicationConstant.MASTER_DATA_APPROVE_TYPE);
		uiModel.addAttribute("documentStatusCode",ApplicationConstant.MASTER_DATA_DOCUMENT_STATUS);
		uiModel.addAttribute("requestStatusCode",ApplicationConstant.MASTER_DATA_REQUEST_STATUS);
		uiModel.addAttribute("carTypeCode",ApplicationConstant.MASTER_DATA_CAR_TYPE);
		uiModel.addAttribute("attachmentTypeCode",ApplicationConstant.MASTER_DATA_ATTACHMENT_TYPE);
		uiModel.addAttribute("actionReasonCode",ApplicationConstant.MASTER_DATA_ACTION_REASON);
		uiModel.addAttribute("airlineCode",ApplicationConstant.MASTER_DATA_AIRLINE);
		uiModel.addAttribute("hotelCode",ApplicationConstant.MASTER_DATA_HOTEL);
		uiModel.addAttribute("URL_EWF",ApplicationConstant.URL_EWF);
		return "approveViewCreateDocDetail";
	}

	@Loggable
	@GetMapping("/createDocSet")
	public String createDocSet(@RequestParam("type") String type,Model uiModel) {
		uiModel.addAttribute("appType", type);
		uiModel.addAttribute("documentTypeCode",ApplicationConstant.MASTER_DATA_DOCUMENT_TYPE);
		uiModel.addAttribute("approveTypeCode",ApplicationConstant.MASTER_DATA_APPROVE_TYPE);
		uiModel.addAttribute("documentStatusCode",ApplicationConstant.MASTER_DATA_DOCUMENT_STATUS);
		return "approveCreateDocSet";
	}

	@Loggable
	@GetMapping("/createDocSetDetail")
	public String createDocSetDetail(@RequestParam("doc") String id,Model uiModel) {
		Gson gson = new Gson();
		uiModel.addAttribute("document", gson.toJson(service.get(Long.valueOf(id)).getBody()).replace("\\n", ""));
		uiModel.addAttribute("documentTypeCode",ApplicationConstant.MASTER_DATA_DOCUMENT_TYPE);
		uiModel.addAttribute("approveTypeCode",ApplicationConstant.MASTER_DATA_APPROVE_TYPE);
		uiModel.addAttribute("documentStatusCode",ApplicationConstant.MASTER_DATA_DOCUMENT_STATUS);
		uiModel.addAttribute("carTypeCode",ApplicationConstant.MASTER_DATA_CAR_TYPE);
		uiModel.addAttribute("attachmentTypeCode",ApplicationConstant.MASTER_DATA_ATTACHMENT_TYPE);
		uiModel.addAttribute("airlineCode",ApplicationConstant.MASTER_DATA_AIRLINE);
		uiModel.addAttribute("hotelCode",ApplicationConstant.MASTER_DATA_HOTEL);
		uiModel.addAttribute("flowTypeCode",ApplicationConstant.MASTER_DATA_EXPENSE_TYPE);
		uiModel.addAttribute("URL_EWF",ApplicationConstant.URL_EWF);
		return "approveCreateDocSetDetail";
	}

	@Loggable
	@GetMapping("/viewCreateDocSetDetail")
	public String viewCreateDocSetDetail(@RequestParam("doc") String id,Model uiModel) {
		Gson gson = new Gson();
		uiModel.addAttribute("document", gson.toJson(service.get(Long.valueOf(id)).getBody()).replace("\\n", ""));
		uiModel.addAttribute("documentTypeCode",ApplicationConstant.MASTER_DATA_DOCUMENT_TYPE);
		uiModel.addAttribute("approveTypeCode",ApplicationConstant.MASTER_DATA_APPROVE_TYPE);
		uiModel.addAttribute("documentStatusCode",ApplicationConstant.MASTER_DATA_DOCUMENT_STATUS);
		uiModel.addAttribute("carTypeCode",ApplicationConstant.MASTER_DATA_CAR_TYPE);
		uiModel.addAttribute("attachmentTypeCode",ApplicationConstant.MASTER_DATA_ATTACHMENT_TYPE);
		uiModel.addAttribute("actionReasonCode",ApplicationConstant.MASTER_DATA_ACTION_REASON);
		uiModel.addAttribute("airlineCode",ApplicationConstant.MASTER_DATA_AIRLINE);
		uiModel.addAttribute("hotelCode",ApplicationConstant.MASTER_DATA_HOTEL);
		uiModel.addAttribute("flowTypeCode",ApplicationConstant.MASTER_DATA_EXPENSE_TYPE);
		uiModel.addAttribute("URL_EWF",ApplicationConstant.URL_EWF);
		return "approveViewCreateDocSetDetail";
	}


	@GetMapping("/calculateDiffDate")
	public ResponseEntity<String> calculateDiffDate(@RequestParam("startDate") String startDate,
													@RequestParam("endDate") String endDate,
													@RequestParam("startTime") String startTime,
													@RequestParam("endTime") String endTime) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		LOGGER.info("-= METHOD CALCULATE DIFF DATE =-");
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");

		String startDateTime    = "";
		String endDateDateTime    = "";
		Date newerDate 		= null;
		Date olderDate 		= null;
		Long diff 			= 0l;
		Long diffInDays 	= 0l;
		Long diffHour 		= 0l;

		try {
			startDateTime 	= startDate.concat(" "+startTime);
			endDateDateTime = endDate.concat(" "+endTime);

			newerDate = formatter.parse(startDateTime);
			olderDate = formatter.parse(endDateDateTime);

			diff 			= (olderDate.getTime() - newerDate.getTime());
			diffInDays 		= diff/ ApplicationConstant.CALCULATE_DIFFERENCE_DAY;
			diffHour		= diff/ ApplicationConstant.CALCULATE_DIFFERENCE_HOURS % ApplicationConstant.TIME_HOURS_PER_DAY_FOR_DAY_BETWEEN;

			/* validate minute over 30 or less 30 */
			if(diffHour >= 8){
				diffInDays = diffInDays + 1;
			}

            return new ResponseEntity<String>(diffInDays.toString(), headers, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
            return new ResponseEntity<String>(headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/* Manage Document */
	@PostMapping(value="/saveDocument",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity saveDocument(HttpServletRequest request) {
        LOGGER.info("-= SAVE DOCUMENT =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
        	responseEntity = ((DocumentService)service).saveDocument(mapClone);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
			e.printStackTrace();
            return new ResponseEntity<String>("Error", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value="/saveDocumentApproveItem",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity saveDocumentApproveItem(HttpServletRequest request) {
        LOGGER.info("-= SAVE DOCUMENT =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
        	responseEntity = ((DocumentApproveService)service).saveDocumentApproveItem(mapClone);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }

    /* Manage Travel Detail */
    @PostMapping(value="/saveTravelDetail",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity saveTravelDetail(HttpServletRequest request) {
        LOGGER.info("-= SAVE TRAVEL DETAIL =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
        	responseEntity = ((DocumentApproveService)service).saveTravelDetail(mapClone);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }

	@GetMapping(value="/{id}/travelDetails",produces="application/json", headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseEntity<String>  getDetail(@PathVariable Long id) {
		LOGGER.info("><><>< GET TRAVEL DETAIL BY DOCUMENT APPROVE ITEM ><><><>< ");
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DocumentApproveService)service).findTravelDetailsByDocumentApproveItemId(id);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}

	}

	@DeleteMapping(value="deleteTravelDetail/{id}",produces="application/json", headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<String>  deleteTravelDetail(@PathVariable Long id) {
		LOGGER.info("><><><>< DELETE TRAVEL DETAIL BY ID : {} ",id);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DocumentApproveService)service).deleteTravelDetails(id);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	/* Manage Travel Member */
    @PostMapping(value="/saveTravelMember",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity saveTravelMember(HttpServletRequest request) {
        LOGGER.info("-= SAVE TRAVEL MEMBER =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
        	responseEntity = ((DocumentApproveService)service).saveTravelMember(mapClone);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }

    @GetMapping(value="/{id}/travelMembers",produces="application/json", headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseEntity<String>  getTravelMember(@PathVariable Long id) {
		LOGGER.info("><><><>< GET TRAVEL MEMBER BY DOCUMENT APPROVE ITEM ><><><><");
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DocumentApproveService)service).findTravelMembersByDocumentApproveItemId(id);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

    @DeleteMapping(value="deleteTravelMember/{id}",produces="application/json", headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<String>  deleteTravelMember(@PathVariable Long id) {
		LOGGER.info("><><><>< DELETE TRAVEL MEMBER BY ID : {} ",id);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DocumentApproveService)service).deleteTravelMembers(id);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

    /* Manage External Member */
    @PostMapping(value="/saveExternalMember",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity saveExternalMember(HttpServletRequest request) {
        LOGGER.info("-= SAVE EXTERNAL MEMBER =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
        	responseEntity = ((DocumentApproveService)service).saveExternalMember(mapClone);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }

    @GetMapping(value="/{id}/externalMembers",produces="application/json", headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseEntity<String>  getExternalMember(@PathVariable Long id) {
		LOGGER.info("><><><>< GET EXTERNAL MEMBER BY DOCUMENT APPROVE ITEM ><><><><");
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DocumentApproveService)service).findExternalMembersByDocumentApproveItemId(id);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

    @DeleteMapping(value="deleteExternalMember/{id}",produces="application/json", headers = "Accept=application/json")
   	@ResponseBody
   	public ResponseEntity<String>  deleteExternalMember(@PathVariable Long id) {
   		LOGGER.info("><><><>< DELETE EXTERNAL MEMBER BY ID : {} ",id);
   		HttpHeaders headers = new HttpHeaders();
           headers.add("Content-Type", "application/json; charset=utf-8");

   		ResponseEntity<String> responseEntity;
   		try {
   			responseEntity = ((DocumentApproveService)service).deleteExternalMembers(id);
   			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
   		} catch (Exception e) {

   			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
   		}
   	}

	/* Manage Car Booking */
	@PostMapping(value="/saveCarBooking",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity saveCarBooking(HttpServletRequest request) {
		LOGGER.info("-= SAVE CAR BOOKING =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentApproveService)service).saveCarBooking(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	@GetMapping(value="/{id}/carBookings",produces="application/json", headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseEntity<String>  getCarBooking(@PathVariable Long id) {
		LOGGER.info("><><>< GET CAR BOOKING BY DOCUMENT APPROVE ITEM ><><><>< ");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DocumentApproveService)service).findCarBookingsByDocumentApproveItemId(id);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}

	}

	/* Manage Hotel Booking */
	@PostMapping(value="/saveHotelBooking",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity saveHotelBooking(HttpServletRequest request) {
		LOGGER.info("-= SAVE HOTEL BOOKING =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentApproveService)service).saveHotelBooking(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	/* Manage Flight Ticket */
	@PostMapping(value="/saveFlightTicket",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity saveFlightTicket(HttpServletRequest request) {
		LOGGER.info("-= SAVE FLIGHT TICKET =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentApproveService)service).saveFlightTicket(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	/* Manage Document Reference */
	@PostMapping(value="/saveDocumentReference",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity saveDocumentReference(HttpServletRequest request) {
		LOGGER.info("-= SAVE DOCUMENT REFERENCE =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentApproveService)service).saveDocumentReference(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	/* Manage Document Attachment */
	@PostMapping("/saveDocumentAttachment")
	public ResponseEntity saveDocumentAttachment(MultipartHttpServletRequest multipartHttpServletRequest) {
		LOGGER.info("-= SAVE DOCUMENT ATTACHMENT =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		try {
			String url = "/documentAttachmentCustom/uploadFileAttachment";
			responseEntity = ((DocumentApproveService)service).uploadFile(multipartHttpServletRequest,url);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	@GetMapping(value="/{id}/documentAttachment",produces="application/json", headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseEntity<String>  getDocumentAttachmentByDocument(@PathVariable Long id) {
		LOGGER.info("><><><>< GET DOCUMENT ATTACHMENT BY DOCUMENT  ><><><><");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DocumentApproveService)service).findDocumentAttachmentByDocumentId(id);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	@DeleteMapping(value="deleteDocumentAttachment/{id}",produces="application/json", headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<String>  deleteDocumentAttachment(@PathVariable Long id) {
		LOGGER.info("><><><>< DELETE DOCUMENT ATTACHMENT BY ID : {} ",id);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DocumentApproveService)service).deleteDocumentAttachment(id);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	/* update document status */
	@PostMapping(value="/updateDocumentStatus",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity updateDocumentStatus(HttpServletRequest request) {
		LOGGER.info("-= UPDATE DOCUMENT STATUS =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentService)service).updateDocumentStatus(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	/* copy document */
	@PostMapping(value="/copyDocument",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity copyDocument(HttpServletRequest request) {
		LOGGER.info("-= COPY DOCUMENT  =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentService)service).copyDocument(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	/* update document case create doc set */
	@PostMapping(value="/updateDocumentApprove",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity updateDocumentApprove(HttpServletRequest request) {
		LOGGER.info("-= UPDATE DOCUMENT APPROVE =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentApproveService)service).updateDocumentApprove(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	/* get employee for create doc approve */
	@Loggable
	@GetMapping("/findAutoCompleteDocRefApp")
	public ResponseEntity<String> findAutoCompleteDocRefApp(@RequestParam("requester") String requester) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String>  responseEntity = null;
		try {
			LOGGER.info("><><>< findAutoCompleteDocRefAppByKeySearch ><><>< ");
			responseEntity = ((DocumentApproveService)service).findAutoCompleteDocRefAppByKeySearch(requester);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

	@Loggable
	@GetMapping("/findAutoCompleteDocRefAppByKeySearch")
	public ResponseEntity<String> findAutoCompleteDocRefAppByKeySearch(@RequestParam("requester") String requester,@RequestParam("keySearch") String keySearch) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String>  responseEntity = null;
		try {
			LOGGER.info("><><>< findAutoCompleteDocRefAppByKeySearch ><><>< ");
			responseEntity = ((DocumentApproveService)service).findAutoCompleteDocRefAppByKeySearch(requester,keySearch);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

	@Loggable
	@GetMapping("/findByDocTypeAndDocStatusCompleteAndAppTypeTravelAndTravelMemberUser")
	public ResponseEntity<String> findByDocTypeAndDocStatusCompleteAndAppTypeTravelAndTravelMemberUser(HttpServletRequest request) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String>  responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			LOGGER.info("><><>< findByDocTypeAndDocStatusCompleteAndAppTypeTravelAndTravelMemberUser ><><>< ");
			responseEntity = ((DocumentApproveService)service).findByDocTypeAndDocStatusCompleteAndAppTypeTravelAndTravelMemberUser(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

	/* download file attachment */
	@Loggable
	@GetMapping("/downloadFileDocumentAttachment")
	public ResponseEntity<String> downloadFileDocumentAttachment(@RequestParam("id") Long id,
																 @RequestParam("fileName") String fileName,
																 HttpServletResponse response) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String>  responseEntity = null;

		ByteArrayInputStream byteArrayInputStream = null;
		try {
			LOGGER.info("><><><>< downloadFileDocumentAttachment ><><><>");
			String mimeType = new MimetypesFileTypeMap().getContentType(fileName);
			response.setContentType(mimeType);
			response.addHeader("Content-Disposition","attachment; filename*=UTF-8''"+fileName);

			String url = "/documentAttachmentCustom/downloadFileAttachment/"+id;

			byte[] fileBytes = ((DocumentApproveService)service).downloadFileDocumentAttachment(url);
			byteArrayInputStream = new ByteArrayInputStream(fileBytes);
			IOUtils.copy(byteArrayInputStream, response.getOutputStream());

			return new ResponseEntity<String>(headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}




	@Loggable
	@GetMapping("/preViewPDFDocumentExpItemAttachment")
	public ResponseEntity<String> preViewPDFDocumentExpItemAttachment(@RequestParam("id") Long id,
																	  @RequestParam("fileName") String fileName,
																	  HttpServletResponse response) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String>  responseEntity = null;
		InputStream in = null;
		OutputStream outputStream1 = null;
		ByteArrayInputStream inputStream = null;
		ByteArrayInputStream byteArrayInputStream = null;
		try {
			LOGGER.info("><><><>< preViewPDFDocumentExpItemAttachment ><><><>");


			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "inline; filename=\""+fileName);

			String url = "/documentAttachmentCustom/downloadFileAttachment/"+id;

			byte[] fileBytes = ((DocumentApproveService)service).downloadFileDocumentAttachment(url);
			inputStream = new ByteArrayInputStream(fileBytes);
			outputStream1 = response.getOutputStream();
			IOUtils.copy(inputStream, outputStream1);

//            IOUtils.copy(byteArrayInputStream, response.getOutputStream());

			return new ResponseEntity<String>(headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}finally {
			IOUtils.closeQuietly(in);
		}
	}

	@Loggable
	@GetMapping(value = "/preViewIMAGEDocumentExpItemAttachment", produces = "text/html", headers = "Accept=application/json")
	public String preViewIMAGEDocumentExpItemAttachment(@RequestParam("id") Long id,
														@RequestParam("fileName") String fileName,
														Model uiModel,
														HttpServletResponse response) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String>  responseEntity = null;
		InputStream in = null;
		OutputStream outputStream1 = null;
		ByteArrayInputStream inputStream = null;
		ByteArrayInputStream byteArrayInputStream = null;
		try {
			LOGGER.info("><><><>< preViewIMAGEDocumentExpItemAttachment ><><><>");




			String url = "/documentAttachmentCustom/downloadFileAttachment/"+id;

			byte[] fileBytes = ((DocumentApproveService)service).downloadFileDocumentAttachment(url);

			String encodedString = Base64.getEncoder().encodeToString(fileBytes);
			uiModel.addAttribute("imageInByte", encodedString);


		} catch (Exception e) {

			e.printStackTrace();
		}finally {
//            IOUtils.closeQuietly(in);
		}

		return "preViewImageFile";
	}







	/* find Request by Document */
	@Loggable
	@GetMapping("/findRequestByDocument/{id}")
	public ResponseEntity<String> findRequestByDocument(@PathVariable Long id) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String>  responseEntity = null;
		try {
			LOGGER.info("><><>< findRequestByDocument ><><>< ");
			responseEntity = ((DocumentApproveService)service).findRequestByDocument(id);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

	@Loggable
	@GetMapping("/findRequestApproverByRequest/{id}")
	public ResponseEntity<String> findRequestApproverByRequest(@PathVariable Long id) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String>  responseEntity = null;
		try {
			LOGGER.info("><><>< findRequestByDocument ><><>< ");
			responseEntity = ((DocumentApproveService)service).findRequestApproverByRequest(id);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

	/* find line approve by requester */
	@PostMapping(value="/getAuthorizeForLineApprove",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity getAuthorizeForLineApprove(HttpServletRequest request) {
		LOGGER.info("-= GET AUTHORIZE  =-");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		String responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentApproveService)service).getAuthorizeForLineApprove(mapClone);

			return new ResponseEntity<String>(responseEntity, headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/* find doc reference case doc expence is use */
	@Loggable
	@GetMapping("/findDocRefByDocTypeExp/{docNumber}")
	public ResponseEntity<String> findDocRefByDocTypeExp(@PathVariable String docNumber) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String>  responseEntity = null;
		try {
			LOGGER.info("><><>< findDocRefByDocTypeExp ><><>< ");
			responseEntity = ((DocumentApproveService)service).findDocRefByDocTypeExp(docNumber);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

	@Loggable
	@GetMapping("/findTravelMemberAndExternalMemberBydocRef/{docNumber}")
	public ResponseEntity<String> findTravelMemberAndExternalMemberBydocRef(@PathVariable String docNumber) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String>  responseEntity = null;
		try {
			LOGGER.info("><><>< findTravelMemberAndExternalMemberBydocRef ><><>< ");
			responseEntity = ((DocumentApproveService)service).findTravelMemberAndExternalMemberBydocRef(docNumber);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

	@Loggable
	@GetMapping("/findTravelDetailsByDocRef/{docNumber}")
	public ResponseEntity<String> findTravelDetailsByDocRef(@PathVariable String docNumber) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String>  responseEntity = null;
		try {
			LOGGER.info("><><>< findTravelDetailsByDocRef ><><>< ");
			responseEntity = ((DocumentApproveService)service).findTravelDetailsByDocRef(docNumber);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

	@Loggable
	@GetMapping("/findMasterDataHotelByCode")
	public ResponseEntity<String> findMasterDataHotelByCode() {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String>  responseEntity = null;
		try {
			LOGGER.info("><><>< findMasterDataHotelByCode ><><>< ");
			responseEntity = ((DocumentApproveService)service).findMasterDataHotelByCode();

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

	@Loggable
	@GetMapping("/findTravelMemberByDocRef/{docNumber}")
	public ResponseEntity<String> findTravelMemberByDocRef(@PathVariable String docNumber) {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String>  responseEntity = null;
		try {
			LOGGER.info("><><>< findTravelMemberByDocRef ><><>< ");
			responseEntity = ((DocumentApproveService)service).findTravelMemberByDocRef(docNumber);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}

	@Loggable
	@GetMapping("/findMasterDataAirlineByCode")
	public ResponseEntity<String> findMasterDataAirlineByCode() {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");
		ResponseEntity<String>  responseEntity = null;
		try {
			LOGGER.info("><><>< findMasterDataAirlineByCode ><><>< ");
			responseEntity = ((DocumentApproveService)service).findMasterDataAirlineByCode();

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("", headers, HttpStatus.OK);
		}
	}


	@GetMapping(value="/findByDocumentId/{id}",produces="application/json", headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseEntity<String>  findByDocumentId(@PathVariable Long id) {
		LOGGER.info("><><>< FIND BY DOCUMENT ID ><><><>< ");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DocumentApproveService)service).findByDocumentId(id);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}


	/* validate flow type approve */
	@Loggable
	@GetMapping(value="/validateFlowTypeApprove")
	public ResponseEntity validateFlowTypeApprove(@RequestParam("cLevel") String cLevel,
												  @RequestParam("psa_requester") String psa_requester,
												  @RequestParam("psa_origin") String psa_origin,
												  @RequestParam("psa_destination") String psa_destination,
												  @RequestParam("location_type") String location_type,
												  @RequestParam("zone_code") String zone_code) {
		LOGGER.info("-= VALIDATE FLOW TYPE APPROVE  =-");

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		String flowTypeName = "";
		try {

			LOGGER.info("><><>< START CASE ><><><>");
			/** validate location is over PSA **/
			if(location_type.equals(ApplicationConstant.LOCATION_TYPE_DOMESTIC)){
				LOGGER.info("><><>< LOCATION D ><><><");
				/** CASE Location type DOMESTIC **/

				/** CASE C LEVEL Requester is LESSTHAN C9 **/
				if(Integer.valueOf(cLevel) <= ApplicationConstant.CLEVEL_11){
					LOGGER.info("><><>< <= C9 ><><><");
					flowTypeName = ApplicationConstant.FLOW_TYPE_H001;
				}else if(Integer.valueOf(cLevel) > ApplicationConstant.CLEVEL_11){
					LOGGER.info("><><>< > C9 ><><><");
					flowTypeName = ApplicationConstant.FLOW_TYPE_H002;
				}
			}else if(location_type.equals(ApplicationConstant.LOCATION_TYPE_FOREIGN)){
				/** CASE Zone ASEAN **/
				if(zone_code.equals(ApplicationConstant.ZONE_CODE_ASEAN)){
					flowTypeName = ApplicationConstant.FLOW_TYPE_H003;
				}
				/** CASE C LEVEL Requester is LESSTHAN C11 **/
				else if(Integer.valueOf(cLevel) <= ApplicationConstant.CLEVEL_11){
					flowTypeName = ApplicationConstant.FLOW_TYPE_H004;
				}
				/** CASE C LEVEL Requester is MORETHAN C11 **/
				else if(Integer.valueOf(cLevel) > ApplicationConstant.CLEVEL_11 ){
					flowTypeName = ApplicationConstant.FLOW_TYPE_H005;
				}
			}


			return new ResponseEntity<String>(flowTypeName, headers, HttpStatus.OK);
		} catch (Exception e) {

			e.printStackTrace();
			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}


	@GetMapping(value="/findByDocumentNumber/{docNumber}",produces="application/json", headers = "Accept=application/json; charset=utf-8")
	@ResponseBody
	public ResponseEntity<String>  findByDocumentNumber(@PathVariable String docNumber) {
		LOGGER.info("><><>< FIND BY DOCUMENT NUMBER ><><><>< ");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DocumentApproveService)service).findByDocumentNumber(docNumber);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

	@Loggable
	@PostMapping(value = "/approveSelected",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
	public ResponseEntity<String> approveSelected(@RequestParam("data") String data){

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		String[] list = data.split(",");
		LOGGER.info("---<><><><> approveSelectedItems <><><><>--- : {}",list);

		String responseEntity = null;

		int id;
		String[] datas;
		ResponseEntity<String> document;
		ResponseEntity<String> approve;
		String actionState = "";
		String approver = "";
		Long documentId;
		String userNameApprover = "";
		Map jsonDoc;

		try{
			for(int k = 0; k < list.length ; k ++) {

				datas = (list[k].replace("|", ",")).split(",");

				document = ((DocumentApproveService) service).findRequestByDocument(Long.valueOf(datas[0]));
				jsonDoc = gson.fromJson(document.getBody(),Map.class);

				approve = ((DocumentApproveService) service).findRequestApproverByRequest(Double.valueOf(jsonDoc.get("id").toString()).longValue());
				listApprove = gson.fromJson(approve.getBody(),List.class);

				for (int i = 0; i < listApprove.size() ; i++) {
					Map temp = listApprove.get(i);

					String userNameApprove = temp.get("userNameApprover") == null?"": temp.get("userNameApprover").toString();
					String requestStatusCode = temp.get("requestStatusCode") == null?"": temp.get("requestStatusCode").toString();
					if (userNameApprove.equals(datas[4].toString()) && requestStatusCode.equals("")) {
						documentId = Double.valueOf(temp.get("id").toString()).longValue();
						userNameApprover = userNameApprove;

						Map<String,String[]> map = new HashMap<>();
						map.put("userName",datas[4].toString().split(","));
						map.put("actionStatus",validateActionState(datas[4]).split(","));
						map.put("documentNumber",datas[1].toString().split(","));
						map.put("docType",datas[3].toString().split(","));
						map.put("documentFlow",jsonDoc.get("docFlow").toString().split(","));
						map.put("processId",datas[2].toString().split(","));
						map.put("documentId",datas[0].toString().split(","));
						map.put("actionReasonCode","".split(","));
						map.put("actionReasonDetail","".split(","));

						LOGGER.info("map ApproveController : {}",map);

						responseEntity = requestService.approveRequest(map);

						break;
					}
				}
			}

			return new ResponseEntity<String>(responseEntity, headers, HttpStatus.OK);

		}catch (Exception e){
			LOGGER.error("{}",e);
			return new ResponseEntity<String>("ERROR", headers, HttpStatus.FORBIDDEN);
		}




	}

	public String validateActionState(String userName){
		String actionState = "";
		Map temp;
		try {
			for (int i = 0; i < listApprove.size(); i++) {
				temp = listApprove.get(i);
				if (userName.equals(temp.get("userNameApprover").toString())) {
					actionState = temp.get("actionState").toString();
					break;
				}
			}
			return actionState;
		}catch (Exception e){
			throw new RuntimeException("Not found state");
		}
	}

	/** method validate Date overlap
	 *	update by iMilkii 2017.10.31
	 **/
	@GetMapping("/validateDateOverlap")
	public ResponseEntity<String> validateDateOverlap(HttpServletRequest request) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		LOGGER.info("-= METHOD VALIDATE DATE OVERLAP =-");

		ResponseEntity<String> responseEntity = null;
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		try {
			responseEntity = ((DocumentApproveService)service).validateDateTimeOverlap(mapClone);

			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(headers, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Loggable
	@GetMapping(value="/deleteDocumentApproveItem")
	public ResponseEntity<String>  deleteDocumentApproveItem(@RequestParam("documentId") Long id,
															 @RequestParam("approveType") String approveType) {
		LOGGER.info("><><><>< DELETE DOCUMENT APPROVE ITEM ");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/json; charset=utf-8");

		ResponseEntity<String> responseEntity;
		try {
			responseEntity = ((DocumentApproveService)service).deleteDocumentApproveItem(id,approveType);
			return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
		}
	}

}
