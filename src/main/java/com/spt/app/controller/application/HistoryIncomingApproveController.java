package com.spt.app.controller.application;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.HistoryIncomingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.Map;

@Controller
@RequestMapping("/historyApprove")
public class HistoryIncomingApproveController extends BaseCommonController{
    static final Logger LOGGER = LoggerFactory.getLogger(HistoryIncomingApproveController.class);

    @Resource(name = "HistoryIncomingService")
    @Override
    public void setService(BaseCommonService service) {
        this.service = service;
    }

    @Loggable
    @GetMapping("/listView")
    public String listView(ModelMap modelMap) {
        modelMap.addAttribute("historyCode","APP");
        return "historyInRequestList";
    }

    @Loggable
    @SuppressWarnings("Duplicates")
    @GetMapping("/approve")
    public ResponseEntity findHistoryExpense(@RequestParam("approver") String approver,
                                             @RequestParam("docNumber") String docNumber,
                                             @RequestParam("titleDescription") String titleDescription,
                                             @RequestParam("docStatus") String docStatus,
                                             @RequestParam("docType") String documentType,
                                             @RequestParam("firstResult") Integer firstResult,
                                             @RequestParam("maxResult") Integer maxResult,
                                             @RequestParam("page") Integer page){

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((HistoryIncomingService) service).findHistory(approver,docNumber,titleDescription,docStatus,documentType,firstResult,maxResult,page);
            return new ResponseEntity<String>(responseEntity.getBody(),headers, HttpStatus.OK);
        }catch (Exception e){
            LOGGER.info(e.getMessage());
            return new ResponseEntity<String>("",headers,HttpStatus.OK);
        }
    }

    @Loggable
    @SuppressWarnings("Duplicates")
    @GetMapping("/approveSize")
    public ResponseEntity findHistoryExpenseSize(@RequestParam("approver") String approver,
                                             @RequestParam("docNumber") String docNumber,
                                             @RequestParam("titleDescription") String titleDescription,
                                             @RequestParam("docStatus") String docStatus,
                                             @RequestParam("docType") String documentType){

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((HistoryIncomingService) service).findHistorySize(approver,docNumber,titleDescription,docStatus,documentType);
            return new ResponseEntity<String>(responseEntity.getBody(),headers, HttpStatus.OK);
        }catch (Exception e){
            LOGGER.info(e.getMessage());
            return new ResponseEntity<String>("",headers,HttpStatus.OK);
        }
    }

}