package com.spt.app.controller.application;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.GLConditionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
@RequestMapping("/gl")
public class GLController extends BaseCommonController {

    private static Logger LOGGER= LoggerFactory.getLogger(GLController.class);

    @Override
    @Resource(name="GLConditionService")
    public void setService(BaseCommonService service) {
        super.service = service;
    }

    @Loggable
    @GetMapping("/listView")
    public String listView(ModelMap model) {
//        model.addAttribute("ApproveCode","APP");
        return "glView";
    }


    @PostMapping(value ="/deleteConditionalGLSet",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity deleteConditonGLSet(HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
            responseEntity = ((GLConditionService)service).deleteConditionGLSet(mapClone);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }

    @GetMapping("/findByGlCreateeAndIoeAndGlSape")
    public ResponseEntity findByGlCreateeAndIoeAndGlSape(@RequestParam("glCreate") String glCreate,
                                                         @RequestParam("io") String io,
                                                         @RequestParam("glSap") String glSap){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity;

        try {
            responseEntity = ((GLConditionService)service).findByGlCreateeAndIoeAndGlSape(glCreate,io,glSap);
            return new ResponseEntity<String>(responseEntity.getBody(),headers,HttpStatus.OK);
        }catch (Exception e){
            LOGGER.info(e.getMessage());
            return new ResponseEntity<String>("",headers,HttpStatus.OK);
        }
    }


    @PostMapping(value = "/deleteGroup",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity deleteGroup(HttpServletRequest request){
        LOGGER.info(" ===================== DELETE CONDITIONAL GL =====");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type","application/json");

        List<String> stringList = new ArrayList<>(Arrays.asList(request.getParameter("ids").split(",")));

        List<Long> idList = new ArrayList<>();
        for (String str : stringList){
            idList.add(Long.valueOf(str));
        }

        ResponseEntity responseEntity = null;

        try{
            for(Long i : idList){
                responseEntity = service.delete(i);
            }
            return new ResponseEntity(responseEntity.getBody(),headers,HttpStatus.OK);
        }catch (Exception e){
            LOGGER.info("ERROR {}",e.getMessage());
            return new ResponseEntity("",headers,HttpStatus.OK);
        }

    }

}
