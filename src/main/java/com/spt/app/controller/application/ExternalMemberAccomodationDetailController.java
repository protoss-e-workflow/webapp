package com.spt.app.controller.application;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.ExternalMemberAccomodationDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

@Controller
@RequestMapping("/externalMembers")
public class ExternalMemberAccomodationDetailController extends BaseCommonController {

    static final Logger LOGGER = LoggerFactory.getLogger(ExternalMemberAccomodationDetailController.class);

    @Override
    @Resource(name="ExternalMemberAccomodationDetailService")
    public void setService(BaseCommonService service) {

        super.service = service;
    }

    @Loggable
    @GetMapping("/findByDocNumberAndItemId")
    public ResponseEntity<String> findByDocNumberAndItemId(@RequestParam(name="docNumber",required=false) String docNumber,@RequestParam(name="itemId",required=false) String itemId) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((ExternalMemberAccomodationDetailService)service).findByDocNumberAndItemId(docNumber,itemId);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }
}
