package com.spt.app.controller.application;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.controller.master.LocationController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.CostCenterService;
import com.spt.app.service.LocationService;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/costCenters")
public class CostCenterController extends BaseCommonController {

    static final Logger LOGGER = LoggerFactory.getLogger(CostCenterController.class);

    @Override
    @Resource(name="CostCenterService")
    public void setService(BaseCommonService service) {

        super.service = service;
    }

    @Loggable
    @GetMapping("/findByEmpUserName")
    public ResponseEntity<String> findByEmpUserName(@RequestParam(name="empUserName",required=false) String empUserName) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        List<Map>  responseEntity ;
        try {

            responseEntity = ((CostCenterService)service).findByEmpUserName(empUserName);

            return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").deepSerialize(responseEntity)), headers, HttpStatus.OK);

        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }
}
