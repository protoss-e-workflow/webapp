package com.spt.app.controller.application;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.spt.app.aop.annotation.Loggable;
import com.spt.app.constant.ApplicationConstant;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.DocumentAdvanceService;
import com.spt.app.service.DocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/advance")
public class DocumentAdvanceController extends BaseCommonController{

    static final Logger LOGGER = LoggerFactory.getLogger(DocumentAdvanceController.class);

    @Override
    @Resource(name="DocumentAdvanceService")
    public void setService(BaseCommonService service) {
        super.service = service;
    }

    @Loggable
    @GetMapping("/listView")
    public String listView(Model uiModel) {
        uiModel.addAttribute("URL_EWF",ApplicationConstant.URL_EWF);
        return "advanceListView";
    }

    @Loggable
    @GetMapping("/advanceDetail")
    public String advanceDetail(ModelMap map, @RequestParam(value = "doc",required = false) String id) {
        if(null != id){
            JsonParser parser = new JsonParser();
            ResponseEntity<String> responseEntity = service.get(Long.valueOf(id));
            JsonElement obj = parser.parse(responseEntity.getBody());
            map.addAttribute("documentData", obj);
        }
        map.addAttribute("requestStatusCode", ApplicationConstant.MASTER_DATA_REQUEST_STATUS);
        return "advanceDetail";
    }

    @Loggable
    @GetMapping("/createDoc")
    public String createDoc(ModelMap map, @RequestParam(value = "doc",required = false) String doc) {
        if(null != doc){
            JsonParser parser = new JsonParser();
            ResponseEntity<String> responseEntity = service.get(Long.valueOf(doc));
            JsonElement obj = parser.parse(responseEntity.getBody());
            map.addAttribute("document", obj);
        }
        return "advanceCreateDoc";
    }

    @PostMapping(value="/saveDocument",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity saveDocument(HttpServletRequest request) {
        LOGGER.info("-= SAVE DOCUMENT ADVANCE=-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
            responseEntity = ((DocumentService)service).saveDocument(mapClone);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value="/updateDocumentAdvance",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity updateDocumentAdvance(HttpServletRequest request) {
        LOGGER.info("-= UPDATE DOCUMENT ADVANCE=-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
            responseEntity = ((DocumentAdvanceService)service).updateDocumentAdvance(mapClone);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value="/updateDocumentStatus",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity updateDocumentStatus(HttpServletRequest request) {
        LOGGER.info("-= UPDATE DOCUMENT STATUS ADVANCE=-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
            responseEntity = ((DocumentAdvanceService)service).updateDocumentStatus(mapClone);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /* Manage Document Reference */
    @PostMapping(value="/saveDocumentReference",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity saveDocumentReference(HttpServletRequest request) {
        LOGGER.info("-= SAVE DOCUMENT REFERENCE =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
            responseEntity = ((DocumentAdvanceService)service).saveDocumentReference(mapClone);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }

    @PostMapping(value="/updateDocumentReference",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity updateDocumentReference(HttpServletRequest request) {
        LOGGER.info("-= UPDATE DOCUMENT REFERENCE =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
            responseEntity = ((DocumentAdvanceService)service).updateDocumentReference(mapClone);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }

    /* Manage Document Attachment */
    @PostMapping("/saveDocumentAttachment")
    public ResponseEntity saveDocumentAttachment(MultipartHttpServletRequest multipartHttpServletRequest) {
        LOGGER.info("-= SAVE DOCUMENT ATTACHMENT ADVANCE =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        try {
            String url = "/documentAttachmentCustom/uploadFileAttachment";
            responseEntity = ((DocumentAdvanceService)service).uploadFile(multipartHttpServletRequest,url);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }

    @PostMapping("/saveSapManual")
    public ResponseEntity saveSapManual(MultipartHttpServletRequest multipartHttpServletRequest) {
        LOGGER.info("-= SAVE DOCUMENT SAP MANUAL ADVANCE =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        try {
            String url = "/documentAttachmentCustom/saveSapManual";
            responseEntity = ((DocumentAdvanceService)service).saveSapManual(multipartHttpServletRequest,url);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }

    @GetMapping(value="/{id}/documentAttachment",produces="application/json", headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseEntity<String>  getDocumentAttachmentByDocument(@PathVariable Long id) {
        LOGGER.info("><><><>< GET DOCUMENT ATTACHMENT ADVANCE BY DOCUMENT  ><><><><");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((DocumentAdvanceService)service).findDocumentAttachmentByDocumentId(id);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }

    @DeleteMapping(value="deleteDocumentAttachment/{id}",produces="application/json", headers = "Accept=application/json")
    @ResponseBody
    public ResponseEntity<String>  deleteDocumentAttachment(@PathVariable Long id) {
        LOGGER.info("><><><>< DELETE DOCUMENT ATTACHMENT ADVANCE BY ID : {} ",id);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((DocumentAdvanceService)service).deleteDocumentAttachment(id);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }



    @Loggable
    @GetMapping("/findByRequesterAndDocumentType")
    public ResponseEntity<String> findByRequesterAndDocumentType(@RequestParam(name="requester",required=false)
                                                                  String requester,
                                                                @RequestParam(name="documentType",required=false)
                                                                  String documentType
                                                  ) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((DocumentAdvanceService)service).findByRequesterAndDocumentType(requester,documentType);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    @Loggable
    @GetMapping("/findByRequesterAndDocNumberAndTitleAndDocType")
    public ResponseEntity<String> findByRequesterAndDocNumberAndTitleAndDocType(
            @RequestParam(name="requester",required = false) String requester,
            @RequestParam(name="documentNumber",required = false) String docNumber,
            @RequestParam(name="titleDescription",required = false) String title,
            @RequestParam(name="docType",required = false) String docType
    ){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((DocumentAdvanceService)service).findByRequesterAndDocNumberAndTitleAndDocType(
                    requester,docNumber,title,docType);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }


    @Loggable
    @GetMapping("/findByDocNumber")
    public ResponseEntity<String> findByDocNumber(@RequestParam(name="docNumber",required=false)
                                                          String docNumber) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((DocumentAdvanceService)service).findByDocNumber(docNumber);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }



    @Loggable
    @GetMapping("/findByRequesterOrderByDocNumber")
    public ResponseEntity<String> findByRequesterOrderByDocNumber(@RequestParam(name="requester",required=false)
                                                          String requester) {




        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((DocumentAdvanceService)service).findByRequesterOrderByDocNumber(requester);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    @PostMapping(value="/getAuthorizeForLineApprove",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity getAuthorizeForLineApprove(HttpServletRequest request) {
        LOGGER.info("-= GET AUTHORIZE FOR ADVANCE =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        String responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
            responseEntity = ((DocumentAdvanceService)service).getAuthorizeForLineApprove(mapClone);

            return new ResponseEntity<String>(responseEntity, headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }

    @PostMapping(value="/copyDocument",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity copyDocument(HttpServletRequest request) {
        LOGGER.info("-= COPY DOCUMENT ADVANCE =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
            responseEntity = ((DocumentService)service).copyDocument(mapClone);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }

    @Loggable
    @GetMapping("/findByDocTypeAndDocStatusCompleteAndUser")
    public ResponseEntity<String> findByDocTypeAndDocStatusCompleteAndUser(HttpServletRequest request) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
            LOGGER.info("><><>< findByDocTypeAndDocStatusCompleteAndUser ><><>< ");
            responseEntity = ((DocumentAdvanceService)service).findByDocTypeAndDocStatusCompleteAndUser(mapClone);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    @Loggable
    @GetMapping("/findByDocTypeAndDocNumberAndDocStatusCompleteAndUser")
    public ResponseEntity<String> findByDocTypeAndDocNumberAndDocStatusCompleteAndUser(HttpServletRequest request) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
            LOGGER.info("><><>< findByDocTypeAndDocNumberAndDocStatusCompleteAndUser ><><>< ");
            responseEntity = ((DocumentAdvanceService)service).findByDocTypeAndDocNumberAndDocStatusCompleteAndUser(mapClone);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    @PostMapping(value="/advanceTransfer",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity advanceTransfer(HttpServletRequest request) {
        LOGGER.info("-= ADVANCE TRANSFER =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
            responseEntity = ((DocumentAdvanceService)service).advanceTransfer(mapClone);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }

    @PostMapping(value="/unlockAdvance",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity unlockAdvance(HttpServletRequest request) {
        LOGGER.info("-= UNLOCK DOCUMENT ADVANCE =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
            responseEntity = ((DocumentAdvanceService)service).unlockAdvance(mapClone);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            return new ResponseEntity<String>("Error", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}



