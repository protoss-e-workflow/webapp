package com.spt.app.controller.application;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.DocumentAdvanceService;
import com.spt.app.service.DocumentApproveService;
import com.spt.app.service.RequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.ldap.InitialLdapContext;
import java.util.ArrayList;
import java.util.Hashtable;

import org.springframework.beans.factory.annotation.Value;


@Controller
@RequestMapping("/requests")
public class RequestController extends BaseCommonController {

    static final Logger LOGGER = LoggerFactory.getLogger(RequestController.class);

    @Value("${url.ldap}")
    String urlLdap = "ldap://10.1.2.1:389";
    @Value("${postfix.ldap}")
    String postfixLdap = "mitrphol.com,panelplus.co.th,mce.co.th,ust.co.th";


    @Override
    @Resource(name = "RequestService")
    public void setService(BaseCommonService service) {
        super.service = service;
    }

    @PostMapping(value = "/createRequest", produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity createRequest(HttpServletRequest request) {
        LOGGER.info("-= CREATE REQUEST =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String, String[]> mapClone = new HashMap<String, String[]>(request.getParameterMap());
        try {
            String result = ((RequestService) service).createRequest(mapClone);

            return new ResponseEntity<String>(result, headers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @Loggable
    @GetMapping("/findByDocumentType")
    public ResponseEntity<String> findByDocumentType(@RequestParam(name = "documentType", required = false)
                                                             String documentType) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((RequestService) service).findByDocumentType(documentType);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }


    /* cancel request create by iMilkii 2017.09.21*/
    @PostMapping(value = "/cancelRequest", produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity cancelRequest(HttpServletRequest request) {
        LOGGER.info("-= CANCEL REQUEST =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String, String[]> mapClone = new HashMap<String, String[]>(request.getParameterMap());
        try {
            String result = ((RequestService) service).cancelRequest(mapClone);

            return new ResponseEntity<String>(result, headers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }


    /**
     * approve request create by iMilkii 2017.09.21
     **/
    @Transactional
    @PostMapping(value = "/approveRequest", produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity approveRequest(HttpServletRequest request) {
        LOGGER.info("-= APPROVE REQUEST =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String, String[]> mapClone = new HashMap<String, String[]>(request.getParameterMap());
        try {
            String result = ((RequestService) service).approveRequest(mapClone);

            return new ResponseEntity<String>(result, headers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }

    @Transactional
    @PostMapping(value = "/approveRequestEmed", produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity approveRequestEmed(HttpServletRequest request) {
        LOGGER.info("-= APPROVE REQUEST EMED =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String, String[]> mapClone = new HashMap<String, String[]>(request.getParameterMap());
        try {
            String result = ((RequestService) service).approveRequestEmed(mapClone,"");

            return new ResponseEntity<String>(result, headers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }


    /**
     * approve request create by iMilkii 2017.09.21
     **/
    @Transactional
    @GetMapping(value = "/approveRequestGet", produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity approveRequestGet(HttpServletRequest request) {
        LOGGER.info("-= APPROVE REQUEST =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String, String[]> mapClone = new HashMap<String, String[]>(request.getParameterMap());
        try {
            String result = ((RequestService) service).approveRequest(mapClone);

            return new ResponseEntity<String>(result, headers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }

    @Transactional
    @GetMapping(value = "/approveRequestEmedGet", produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity approveRequestEmedGet(HttpServletRequest request) {
        LOGGER.info("-= APPROVE REQUEST =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String, String[]> mapClone = new HashMap<String, String[]>(request.getParameterMap());
        try {
            String result = ((RequestService) service).approveRequestEmed(mapClone,"paid");

            return new ResponseEntity<String>(result, headers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }


    @Transactional
    @GetMapping(value = "/approveRequestRedirect/{docNumber}/{userName}/{actionState}", produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public String approveRequestRedirect(@PathVariable String docNumber,
                                         @PathVariable String userName,
                                         @PathVariable String actionState,
                                         Model uiModel) {
        LOGGER.info("-= APPROVE REQUEST REDIRECT =-");


        ResponseEntity<String> responseEntity = null;
//        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
            uiModel.addAttribute("userName", userName);
            uiModel.addAttribute("docNumber", docNumber);
            uiModel.addAttribute("actionState", actionState);
            uiModel.addAttribute("action", "Approve");
            return "checkAuthenEmail";


        } catch (Exception e) {

            return "checkAuthenEmail";

        }
    }


    @Transactional
    @GetMapping(value = "/sendApproveRequest", produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity sendApproveRequest(@RequestParam("docNumber") String docNumber,
                                             @RequestParam("userName") String userName,
                                             @RequestParam("actionState") String actionState) {
        LOGGER.info("-= SEND APPROVE REQUEST  =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        try {

            String result = ((RequestService) service).approveRequestRedirect(docNumber, userName, actionState);


            return new ResponseEntity<String>("1", headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }


    @Loggable
    @GetMapping(value = "/rejectRequestRedirect/{docNumber}/{userName}/{actionState}", produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public String rejectRequestRedirect(@PathVariable String docNumber,
                                        @PathVariable String userName,
                                        @PathVariable String actionState,
                                        Model uiModel) {
        LOGGER.info("-= REJECT REQUEST REDIRECT =-");


        ResponseEntity<String> responseEntity = null;
//        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
            uiModel.addAttribute("userName", userName);
            uiModel.addAttribute("docNumber", docNumber);
            uiModel.addAttribute("actionState", actionState);
            uiModel.addAttribute("action", "Reject");
            return "checkAuthenEmail";


        } catch (Exception e) {

            return "checkAuthenEmail";

        }
    }


    @Transactional
    @GetMapping(value = "/sendRejectRequest", produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity sendRejectRequest(@RequestParam("docNumber") String docNumber,
                                            @RequestParam("userName") String userName,
                                            @RequestParam("actionState") String actionState) {
        LOGGER.info("-= SEND APPROVE REQUEST  =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        try {

            String result = ((RequestService) service).rejectRequestRedirect(docNumber, userName, actionState);


            return new ResponseEntity<String>("1", headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }


    @Loggable
    @GetMapping("/findIncomingRequestExpense")
    public ResponseEntity<String> findIncomingRequestExpense(
            @RequestParam(name = "nextApprover", required = false) String nextApprover,
            @RequestParam(name = "requester", required = false) String requester,
            @RequestParam(name = "documentNumber", required = false) String documentNumber) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((RequestService) service).findIncomingRequestExpense(nextApprover, requester, documentNumber);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    @Loggable
    @GetMapping("/findIncomingRequestApprove")
    public ResponseEntity<String> findIncomingRequestApprove(
            @RequestParam(name = "nextApprover", required = false) String nextApprover,
            @RequestParam(name = "requester", required = false) String requester,
            @RequestParam(name = "documentNumber", required = false) String documentNumber) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((RequestService) service).findIncomingRequestApprove(nextApprover, requester, documentNumber);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }


    /**
     * reject request create by iMilkii 2017.09.28
     **/
    @Transactional
    @PostMapping(value = "/rejectRequest", produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity rejectRequest(HttpServletRequest request) {
        LOGGER.info("-= REJECT REQUEST =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String, String[]> mapClone = new HashMap<String, String[]>(request.getParameterMap());
        try {
            String result = ((RequestService) service).rejectRequest(mapClone);

            return new ResponseEntity<String>(result, headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("Error", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Transactional
    @PostMapping(value = "/rejectRequestEmed", produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity rejectRequestEmed(HttpServletRequest request) {
        LOGGER.info("-= REJECT REQUEST =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String, String[]> mapClone = new HashMap<String, String[]>(request.getParameterMap());
        try {
            String result = ((RequestService) service).rejectRequestEmed(mapClone);

            return new ResponseEntity<String>(result, headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("Error", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * find request by id
     * create by iMilkii 2017.10.16
     **/
    @Transactional
    @GetMapping(value = "/{id}", produces = "application/json", headers = "Accept=application/json; charset=utf-8")
    @ResponseBody
    public ResponseEntity<String> getRequestById(@PathVariable Long id) {
        LOGGER.info("><><>< GET REQUEST BY ID ><><><>< : {} ", id);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((RequestService) service).findById(id);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }

    }



    @PostMapping(value = "/terminateFlow", produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity terminateFlow(HttpServletRequest request) {
        LOGGER.info("-= TERMINATE FLOW REQUEST =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String, String[]> mapClone = new HashMap<String, String[]>(request.getParameterMap());
        try {
            String result = ((RequestService) service).terminateRequest(mapClone);

            return new ResponseEntity<String>(result, headers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }

    @PostMapping(value = "/resendFlow", produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity resendFlow(HttpServletRequest request) {
        LOGGER.info("-= RESEND FLOW REQUEST =-");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        ResponseEntity<String> responseEntity = null;
        Map<String, String[]> mapClone = new HashMap<String, String[]>(request.getParameterMap());
        try {
            String result = ((RequestService) service).resendFlow(mapClone);

            return new ResponseEntity<String>(result, headers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }
    }

    @Loggable
    @GetMapping("/checkAuthenUser")
    private ResponseEntity<String> checkAuthenUser(@RequestParam("userName") String userName,
                                                   @RequestParam("password") String password) {
        String result = "0";
        String postfixLdap = "mitrphol.com,panelplus.co.th,mce.co.th,ust.co.th";
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        LOGGER.info("UserName : {}", userName);
        LOGGER.info("Password : {}", password);
        try {
            boolean isLdapUser = false;

            for (String mailGroup : postfixLdap.split(",")) {
                isLdapUser = checkLdapAuthentication(userName, password, mailGroup);
                if (isLdapUser) {
                    result = "1";
                    break;
                }
            }

            LOGGER.info("Result : {}", result);
            return new ResponseEntity<String>(result, headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("Error", headers, HttpStatus.OK);
        }


    }

    private boolean checkLdapAuthentication(String name, String password, String postfix) {
        //LOGGER.info("in ldapAuthen");
        String urlLdap = "ldap://10.1.2.1:389";
        try {
            String url = urlLdap;
            String username = name + "@" + postfix;
            LOGGER.info("UserName to Find Authen LDAP : {}", username);
            Hashtable<Object, Object> env = new Hashtable<Object, Object>();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            env.put(Context.PROVIDER_URL, url);
            env.put(Context.SECURITY_AUTHENTICATION, "simple");
            env.put(Context.SECURITY_PRINCIPAL, username);
            env.put(Context.SECURITY_CREDENTIALS, password);
            env.put(Context.REFERRAL, "follow");

            try {
                InitialLdapContext initialLdapContext = new InitialLdapContext(env, null);
                return true;
            } catch (NamingException e) {
                e.printStackTrace();
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }


}
