package com.spt.app.controller.application;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.DocumentApproveService;
import com.spt.app.service.DownloadUserManualService;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


import javax.activation.MimetypesFileTypeMap;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;

@Controller
@RequestMapping("/downloadUserManual")
public class DownloadUserManualController extends BaseCommonController {

    static final Logger LOGGER = LoggerFactory.getLogger(DownloadUserManualController.class);

    @Override
    @Resource(name="DownloadUserManualService")
    public void setService(BaseCommonService service) {
        super.service = service;
    }

    @Loggable
    @GetMapping("/user")
    public ResponseEntity<String> user(HttpServletResponse response) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity = null;

        ByteArrayInputStream byteArrayInputStream = null;
        try {
            String mimeType = new MimetypesFileTypeMap().getContentType("MPG_Expense_Approve_Manual.pdf");
            response.setContentType(mimeType);
            response.addHeader("Content-Disposition","attachment; filename*=UTF-8''"+"MPG_Expense_Approve_Manual.pdf");

            String url = "/downloadUserManual/user";

            byte[] fileBytes = ((DownloadUserManualService)service).downloadManualUser(url);
            byteArrayInputStream = new ByteArrayInputStream(fileBytes);
            IOUtils.copy(byteArrayInputStream, response.getOutputStream());

            return new ResponseEntity<String>(headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    @Loggable
    @GetMapping("/approver")
    public ResponseEntity<String> approver(HttpServletResponse response) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity = null;

        ByteArrayInputStream byteArrayInputStream = null;
        try {
            String mimeType = new MimetypesFileTypeMap().getContentType("MPG_Expense_Approve_Manual_for_Approver.pdf");
            response.setContentType(mimeType);
            response.addHeader("Content-Disposition","attachment; filename*=UTF-8''"+"MPG_Expense_Approve_Manual_for_Approver.pdf");

            String url = "/downloadUserManual/approver";

            byte[] fileBytes = ((DownloadUserManualService)service).downloadManualApprove(url);
            byteArrayInputStream = new ByteArrayInputStream(fileBytes);
            IOUtils.copy(byteArrayInputStream, response.getOutputStream());

            return new ResponseEntity<String>(headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }



    @Loggable
    @GetMapping("/account")
    public ResponseEntity<String> account(HttpServletResponse response) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity = null;

        ByteArrayInputStream byteArrayInputStream = null;
        try {
            String mimeType = new MimetypesFileTypeMap().getContentType("MPG_Expense_Approve_Manual_for_Account.pdf");
            response.setContentType(mimeType);
            response.addHeader("Content-Disposition","attachment; filename*=UTF-8''"+"MPG_Expense_Approve_Manual_for_Account.pdf");

            String url = "/downloadUserManual/account";

            byte[] fileBytes = ((DownloadUserManualService)service).downloadManualAccount(url);
            byteArrayInputStream = new ByteArrayInputStream(fileBytes);
            IOUtils.copy(byteArrayInputStream, response.getOutputStream());

            return new ResponseEntity<String>(headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    @Loggable
    @GetMapping("/user2")
    public ResponseEntity<String> user2(HttpServletResponse response) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity = null;

        ByteArrayInputStream byteArrayInputStream = null;
        try {
            String mimeType = new MimetypesFileTypeMap().getContentType("MPG_Expense_Approve_Manual_2.0.pdf");
            response.setContentType(mimeType);
            response.addHeader("Content-Disposition","attachment; filename*=UTF-8''"+"MPG_Expense_Approve_Manual_2.0.pdf");

            String url = "/downloadUserManual/user2";

            byte[] fileBytes = ((DownloadUserManualService)service).downloadManualUser(url);
            byteArrayInputStream = new ByteArrayInputStream(fileBytes);
            IOUtils.copy(byteArrayInputStream, response.getOutputStream());

            return new ResponseEntity<String>(headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }
}
