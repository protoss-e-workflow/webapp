package com.spt.app.controller.application;


import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/checkLinkCustom")
public class CheckLinkController {

    static final Logger LOGGER = LoggerFactory.getLogger(CheckLinkController.class);


    @Value("${SAPServer}")
    protected String SAPServer = "";

    @Value("${OrgSysServer}")
    protected String OrgSysServer = "";

    @Value("${EmailServer}")
    protected String EmailServer = "";

    @Value("${EngineServer}")
    protected String EngineServer = "";

    @Value("${SpecialEngineServer}")
    protected String SpecialEngineServer = "";

    @Value("${FlowServer}")
    protected String FlowServer = "";

    @Value("${ReportServer}")
    protected String ReportServer = "";

    @Value("${JobServer}")
    protected String JobServer = "";

    @Value("${InternalOrgSysServer}")
    protected String InternalOrgSysServer = "";

    @Value("${UrlEmed}")
    protected String URL_EMED = "";



    @Autowired
    RestTemplate restTemplate;


    @GetMapping(value="/checkEndpointProperties",produces = "application/json; charset=utf-8")
    public ResponseEntity<String> checkLink() {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

            String SAPServer = this.SAPServer;
            String OrgSysServer = this.OrgSysServer;
            String EmailServer = this.EmailServer;
            String EngineServer = this.EngineServer;
            String SpecialEngineServer = this.SpecialEngineServer;
            String FlowServer = this.FlowServer;
            String ReportServer = this.ReportServer;
            String JobServer = this.JobServer;
            String InternalOrgSysServer = this.InternalOrgSysServer;
            String URL_EMED = this.URL_EMED;

        Map<String,Object> mapLink = new HashMap<>();
        mapLink.put("EngineServer",EngineServer);
        mapLink.put("SAPServer",SAPServer);
        mapLink.put("OrgSysServer",OrgSysServer);
        mapLink.put("EmailServer",EmailServer);
        mapLink.put("SpecialEngineServer",SpecialEngineServer);
        mapLink.put("FlowServer",FlowServer);
        mapLink.put("ReportServer",ReportServer);
        mapLink.put("JobServer",JobServer);
        mapLink.put("InternalOrgSysServer",InternalOrgSysServer);
        mapLink.put("URL_EMED",URL_EMED);


//        List<Map<String,Object>> resultFromSAP  = restTemplate.getForObject(SAPServer+"/checkLinkCustom/sendListEndPoint", List.class);
//        LOGGER.error("Result From SAP "+resultFromSAP.get(0).get("EngineServer"));

        return new ResponseEntity<String>(new JSONSerializer().prettyPrint(true).serialize(mapLink),headers, HttpStatus.OK);
    }

}