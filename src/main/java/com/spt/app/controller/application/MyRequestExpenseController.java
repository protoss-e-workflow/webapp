package com.spt.app.controller.application;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.MyRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

@Controller
@RequestMapping("/myExpense")
public class MyRequestExpenseController extends BaseCommonController{
    private static Logger LOGGER = LoggerFactory.getLogger(MyRequestExpenseController.class);

    @Resource(name = "MyRequestService")
    @Override
    public void setService(BaseCommonService service) {
        super.service = service;
    }

    @Loggable
    @GetMapping("/listView")
    public String expenseListView(ModelMap modelMap) {
        modelMap.addAttribute("TYPE_REQUEST","ADV");
        LOGGER.info("MyEXPENSE LISTVIEW{}",modelMap.get("TYPE_REQUEST"));
        return "myRequestList";
    }

    @Loggable
    @GetMapping("/getApprover/{id}")
    public ResponseEntity<String> getApprover(@PathVariable("id") Long id){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((MyRequestService)service).getApproverByRequest(id);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }
}
