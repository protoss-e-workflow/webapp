package com.spt.app.controller.application;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.annotation.Resource;

@Controller
@RequestMapping("/incomingRequestExpense")
public class IncomingRequestExpenseController extends BaseCommonController{
    static final Logger LOGGER = LoggerFactory.getLogger(IncomingRequestExpenseController.class);

    @Override
    @Resource(name = "IncomingRequestService")
    public void setService(BaseCommonService service) {
        super.service = service;
    }

    @Loggable
    @GetMapping("/listView")
    public String listView(ModelMap modelMap) {
        modelMap.addAttribute("code","ADV");
        return "incomingList";
    }
}
