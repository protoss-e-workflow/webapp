package com.spt.app.controller.application;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.MyRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

@Controller
@RequestMapping("/myApprove")
public class MyRequestApproveController  extends BaseCommonController{
    static final Logger LOGGER = LoggerFactory.getLogger(MyRequestApproveController.class);

    @Resource(name = "MyRequestService")
    @Override
    public void setService(BaseCommonService service) {
        super.service = service;
    }

    @Loggable
    @GetMapping("/listView")
    public String approveListView(ModelMap modelMap) {
        modelMap.addAttribute("TYPE_REQUEST","APP");
        return "myRequestList";
    }

    @Loggable
    @GetMapping("/getApprover/{id}")
    public ResponseEntity<String> getApprover(@PathVariable("id") Long id){

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            responseEntity = ((MyRequestService)service).getApproverByRequest(id);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<String>("",HttpStatus.OK);
        }

    }
}
