package com.spt.app.controller.application;


import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.PerDiemProcessCalculateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/perDiemProcess")
public class PerDiemProcessCalculateController extends BaseCommonController {

    static final Logger LOGGER = LoggerFactory.getLogger(PerDiemProcessCalculateController.class);

    @Override
    @Resource(name="PerDiemProcessCalculateService")
    public void setService(BaseCommonService service) {
        super.service = service;
    }

    @PostMapping(value="/processCalculatePerDiemForeign",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity processCalculatePerDiemForeign(HttpServletRequest request) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
            responseEntity = ((PerDiemProcessCalculateService)service).processCalculatePerDiemForeign(mapClone);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value="/processCalculatePerDiemDomestic",produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8")
    public ResponseEntity processCalculatePerDiemDomestic(HttpServletRequest request) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity = null;
        Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
        try {
            responseEntity = ((PerDiemProcessCalculateService)service).processCalculatePerDiemDomestic(mapClone);
            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
