package com.spt.app.controller.application;

import com.spt.app.aop.annotation.Loggable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/requestStatusReport")
public class RequestStatusReport {
    static final Logger LOGGER = LoggerFactory.getLogger(RequestStatusReport.class);


    @Loggable
    @GetMapping("/listView")
    public String listView() {
        return "requestStatusReportView";
    }
}