package com.spt.app.controller.application;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.service.ReportService;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

// import java.util.HashMap;
// import java.util.Locale;
// import java.util.Map;
// import java.util.List;

@Controller
@RequestMapping("/report")
public class FormReportController {

    static final Logger LOGGER = LoggerFactory.getLogger(FormReportController.class);

    @Autowired
    ReportService reportService;

    @Autowired
    MessageSource messageSource;



    @GetMapping(value="/printApproveReport",produces="application/json")
    @ResponseBody
    public ResponseEntity<String> printApproveReport(@RequestParam(value = "docNum", required = false) String docNum,
                                                     @RequestParam(value = "language", required = false) String language,
                                                     HttpServletResponse response) throws IOException,ServletException {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy-HHmm");
        String currentDate = df.format(c.getTime());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ByteArrayInputStream inputByteStream = null;
        Map<String,String> mapParam = new HashMap();

        try {
            response.setContentType("application/x-pdf");
            response.addHeader("Content-Disposition", "attachment; filename=formApprove_"+currentDate+".pdf");



            mapParam.put("REPORT_APPROVE","REPORT_APPROVE");
            mapParam.put("LABEL_DOC_NUMBER","LABEL_DOC_NUMBER");
            mapParam.put("LABEL_EMPLOYEE_NAME","LABEL_EMPLOYEE_NAME");
            mapParam.put("LABEL_POSITION","LABEL_POSITION");
            mapParam.put("LABEL_DOCUMENT_CREATOR_NAME","LABEL_DOCUMENT_CREATOR_NAME");
            mapParam.put("LABEL_DOCUMENT_DATE","LABEL_DOCUMENT_DATE");
            mapParam.put("LABEL_COMPANY_AGENCY","LABEL_COMPANY_AGENCY");
            mapParam.put("LABEL_PSA_DETAIL","LABEL_PSA_DETAIL");
            mapParam.put("LABEL_COST_CENTER","LABEL_COST_CENTER");
            mapParam.put("LABEL_TRAVEL_TYPE","LABEL_TRAVEL_TYPE");
            mapParam.put("LABEL_COMPANY_AGENCY_NAME","LABEL_COMPANY_AGENCY_NAME");
            mapParam.put("LABEL_COMPANY_AGENCY_CODE","LABEL_COMPANY_AGENCY_CODE");
            mapParam.put("LABEL_LOCATION_EMPLOYEE","LABEL_LOCATION_EMPLOYEE");

            mapParam.put("LABEL_TRAVEL_INFORMATION","LABEL_TRAVEL_INFORMATION");
            mapParam.put("LABEL_ORIGIN","LABEL_ORIGIN");
            mapParam.put("LABEL_DESTINATION","LABEL_DESTINATION");
            mapParam.put("LABEL_START_DATE","LABEL_START_DATE");
            mapParam.put("LABEL_END_DATE","LABEL_END_DATE");
            mapParam.put("LABEL_REMARK","LABEL_REMARK");

            mapParam.put("LABEL_COST_DETAIL_AND_ADVANCE","LABEL_COST_DETAIL_AND_ADVANCE");

            mapParam.put("LABEL_TRAVEL_MEMBER_NAME","LABEL_TRAVEL_MEMBER_NAME");
            mapParam.put("LABEL_EMPLOYEE_CODE","LABEL_EMPLOYEE_CODE");


            mapParam.put("LABEL_ADVANCE","LABEL_ADVANCE");
            mapParam.put("LABEL_MONEY_AMOUNT_BATH","LABEL_MONEY_AMOUNT_BATH");
            mapParam.put("LABEL_BORROW_DATE","LABEL_BORROW_DATE");
            mapParam.put("LABEL_DUE_DATE","LABEL_DUE_DATE");
            mapParam.put("LABEL_BANK_NUMBER","LABEL_BANK_NUMBER");
            mapParam.put("LABEL_CONTRACT_ACCOUNT","LABEL_CONTRACT_ACCOUNT");
            mapParam.put("LABEL_PAYMENT_TYPE","LABEL_PAYMENT_TYPE");


            mapParam.put("LABEL_ADMIN_INFORMATION","LABEL_ADMIN_INFORMATION");
            mapParam.put("LABEL_RESERVE_CAR","LABEL_RESERVE_CAR");
            mapParam.put("LABEL_BOOKING_HOTEL","LABEL_BOOKING_HOTEL");
            mapParam.put("LABEL_TICKET_BOOKING","LABEL_TICKET_BOOKING");
            mapParam.put("LABEL_CONTRACT_ADMIN","LABEL_CONTRACT_ADMIN");

            mapParam.put("LABEL_LINE_APPROVER","LABEL_LINE_APPROVER");
            mapParam.put("LABEL_POSITION","LABEL_POSITION");
            mapParam.put("LABEL_ROLES","LABEL_ROLES");
            mapParam.put("LABEL_STATUS","LABEL_STATUS");
            mapParam.put("LABEL_LIST_COST","LABEL_LIST_COST");

            String mapMessageStr = mapParam.toString();
            byte[] fileBytes = reportService.printApproveReport(docNum,mapMessageStr,language);
            if(fileBytes != null){
                inputByteStream = new ByteArrayInputStream(fileBytes);
                IOUtils.copy(inputByteStream, response.getOutputStream());
            }
            return new ResponseEntity<String>("1",headers, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<String>("{\"ERROR\":" +e.getMessage()+ "\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {
            IOUtils.closeQuietly(inputByteStream);
        }
    }


    @GetMapping(value="/printAdvanceReport",produces="application/json")
    @ResponseBody
    public ResponseEntity<String> printAdvanceReport(@RequestParam(value = "docNum", required = false) String docNum,
                                                     @RequestParam(value = "language", required = false) String language,
                                                     HttpServletResponse response) throws IOException,ServletException {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy-HHmm");
        String currentDate = df.format(c.getTime());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ByteArrayInputStream inputByteStream = null;
        Map<String,String> mapParam = new HashMap();

        try {
            response.setContentType("application/x-pdf");
            response.addHeader("Content-Disposition", "attachment; filename=formAdvance_"+currentDate+".pdf");


            mapParam.put("REPORT_ADVANCE","REPORT_ADVANCE");
            mapParam.put("LABEL_DOC_NUMBER","LABEL_DOC_NUMBER");
            mapParam.put("LABEL_EMPLOYEE_NAME","LABEL_EMPLOYEE_NAME");
            mapParam.put("LABEL_POSITION","LABEL_POSITION");
            mapParam.put("LABEL_DOCUMENT_CREATOR_NAME","LABEL_DOCUMENT_CREATOR_NAME");
            mapParam.put("LABEL_DOCUMENT_DATE","LABEL_DOCUMENT_DATE");
            mapParam.put("LABEL_COMPANY_AGENCY","LABEL_COMPANY_AGENCY");
            mapParam.put("LABEL_PSA_DETAIL","LABEL_PSA_DETAIL");
            mapParam.put("LABEL_COST_CENTER","LABEL_COST_CENTER");
            mapParam.put("LABEL_TRAVEL_TYPE","LABEL_TRAVEL_TYPE");
            mapParam.put("LABEL_COMPANY_AGENCY_NAME","LABEL_COMPANY_AGENCY_NAME");
            mapParam.put("LABEL_COMPANY_AGENCY_CODE","LABEL_COMPANY_AGENCY_CODE");
            mapParam.put("LABEL_LOCATION_EMPLOYEE","LABEL_LOCATION_EMPLOYEE");

            mapParam.put("LABEL_TRAVEL_INFORMATION","LABEL_TRAVEL_INFORMATION");
            mapParam.put("LABEL_ORIGIN","LABEL_ORIGIN");
            mapParam.put("LABEL_DESTINATION","LABEL_DESTINATION");
            mapParam.put("LABEL_START_DATE","LABEL_START_DATE");
            mapParam.put("LABEL_END_DATE","LABEL_END_DATE");
            mapParam.put("LABEL_REMARK","LABEL_REMARK");

            mapParam.put("LABEL_COST_DETAIL_AND_ADVANCE","LABEL_COST_DETAIL_AND_ADVANCE");

            mapParam.put("LABEL_TRAVEL_MEMBER_NAME","LABEL_TRAVEL_MEMBER_NAME");
            mapParam.put("LABEL_EMPLOYEE_CODE","LABEL_EMPLOYEE_CODE");


            mapParam.put("LABEL_ADVANCE","LABEL_ADVANCE");
            mapParam.put("LABEL_MONEY_AMOUNT_BATH","LABEL_MONEY_AMOUNT_BATH");
            mapParam.put("LABEL_BORROW_DATE","LABEL_BORROW_DATE");
            mapParam.put("LABEL_DUE_DATE","LABEL_DUE_DATE");
            mapParam.put("LABEL_BANK_NUMBER","LABEL_BANK_NUMBER");
            mapParam.put("LABEL_CONTRACT_ACCOUNT","LABEL_CONTRACT_ACCOUNT");
            mapParam.put("LABEL_PAYMENT_TYPE","LABEL_PAYMENT_TYPE");


            mapParam.put("LABEL_ADMIN_INFORMATION","LABEL_ADMIN_INFORMATION");
            mapParam.put("LABEL_RESERVE_CAR","LABEL_RESERVE_CAR");
            mapParam.put("LABEL_BOOKING_HOTEL","LABEL_BOOKING_HOTEL");
            mapParam.put("LABEL_TICKET_BOOKING","LABEL_TICKET_BOOKING");
            mapParam.put("LABEL_CONTRACT_ADMIN","LABEL_CONTRACT_ADMIN");

            mapParam.put("LABEL_LINE_APPROVER","LABEL_LINE_APPROVER");
            mapParam.put("LABEL_POSITION","LABEL_POSITION");
            mapParam.put("LABEL_ROLES","LABEL_ROLES");
            mapParam.put("LABEL_STATUS","LABEL_STATUS");
            mapParam.put("LABEL_LIST_COST","LABEL_LIST_COST");
            mapParam.put("LABEL_DOC_REF","LABEL_DOC_REF");
            mapParam.put("LABEL_ADVANCE_REMARK","LABEL_ADVANCE_REMARK");
            mapParam.put("LABEL_ADVANCE_PURPOSE","LABEL_ADVANCE_PURPOSE");

            String mapMessageStr = mapParam.toString();
            byte[] fileBytes = reportService.printAdvanceReport(docNum,mapMessageStr,language);
            if(fileBytes != null){
                inputByteStream = new ByteArrayInputStream(fileBytes);
                IOUtils.copy(inputByteStream, response.getOutputStream());
            }
            return new ResponseEntity<String>("1",headers, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<String>("{\"ERROR\":" +e.getMessage()+ "\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {
            IOUtils.closeQuietly(inputByteStream);
        }
    }


    @GetMapping(value="/printExpenseReport",produces="application/json")
    @ResponseBody
    public ResponseEntity<String> printExpenseReport(@RequestParam(value = "docNum", required = false) String docNum,
                                                     @RequestParam(value = "language", required = false) String language,
                                                     HttpServletResponse response) throws IOException,ServletException {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy-HHmm");
        String currentDate = df.format(c.getTime());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ByteArrayInputStream inputByteStream = null;
        Map<String,String> mapParam = new HashMap();

        try {
            response.setContentType("application/x-pdf");
            response.addHeader("Content-Disposition", "attachment; filename=formExpense_"+currentDate+".pdf");


            mapParam.put("REPORT_EXPENSE","REPORT_EXPENSE");
            mapParam.put("LABEL_DOC_NUMBER","LABEL_DOC_NUMBER");
            mapParam.put("LABEL_EMPLOYEE_NAME","LABEL_EMPLOYEE_NAME");
            mapParam.put("LABEL_POSITION","LABEL_POSITION");
            mapParam.put("LABEL_DOCUMENT_CREATOR_NAME","LABEL_DOCUMENT_CREATOR_NAME");
            mapParam.put("LABEL_DOCUMENT_DATE","LABEL_DOCUMENT_DATE");
            mapParam.put("LABEL_COMPANY_AGENCY","LABEL_COMPANY_AGENCY");
            mapParam.put("LABEL_PSA_DETAIL","LABEL_PSA_DETAIL");
            mapParam.put("LABEL_COST_CENTER","LABEL_COST_CENTER");
            mapParam.put("LABEL_TRAVEL_TYPE","LABEL_TRAVEL_TYPE");
            mapParam.put("LABEL_COMPANY_AGENCY_NAME","LABEL_COMPANY_AGENCY_NAME");
            mapParam.put("LABEL_COMPANY_AGENCY_CODE","LABEL_COMPANY_AGENCY_CODE");
            mapParam.put("LABEL_LOCATION_EMPLOYEE","LABEL_LOCATION_EMPLOYEE");

            mapParam.put("LABEL_TRAVEL_INFORMATION","LABEL_TRAVEL_INFORMATION");
            mapParam.put("LABEL_ORIGIN","LABEL_ORIGIN");
            mapParam.put("LABEL_DESTINATION","LABEL_DESTINATION");
            mapParam.put("LABEL_START_DATE","LABEL_START_DATE");
            mapParam.put("LABEL_END_DATE","LABEL_END_DATE");
            mapParam.put("LABEL_REMARK","LABEL_REMARK");

            mapParam.put("LABEL_COST_DETAIL_AND_ADVANCE","LABEL_COST_DETAIL_AND_ADVANCE");

            mapParam.put("LABEL_TRAVEL_MEMBER_NAME","LABEL_TRAVEL_MEMBER_NAME");
            mapParam.put("LABEL_EMPLOYEE_CODE","LABEL_EMPLOYEE_CODE");


            mapParam.put("LABEL_ADVANCE","LABEL_ADVANCE");
            mapParam.put("LABEL_MONEY_AMOUNT_BATH","LABEL_MONEY_AMOUNT_BATH");
            mapParam.put("LABEL_BORROW_DATE","LABEL_BORROW_DATE");
            mapParam.put("LABEL_DUE_DATE","LABEL_DUE_DATE");
            mapParam.put("LABEL_BANK_NUMBER","LABEL_BANK_NUMBER");
            mapParam.put("LABEL_CONTRACT_ACCOUNT","LABEL_CONTRACT_ACCOUNT");
            mapParam.put("LABEL_PAYMENT_TYPE","LABEL_PAYMENT_TYPE");


            mapParam.put("LABEL_ADMIN_INFORMATION","LABEL_ADMIN_INFORMATION");
            mapParam.put("LABEL_RESERVE_CAR","LABEL_RESERVE_CAR");
            mapParam.put("LABEL_BOOKING_HOTEL","LABEL_BOOKING_HOTEL");
            mapParam.put("LABEL_TICKET_BOOKING","LABEL_TICKET_BOOKING");
            mapParam.put("LABEL_CONTRACT_ADMIN","LABEL_CONTRACT_ADMIN");

            mapParam.put("LABEL_LINE_APPROVER","LABEL_LINE_APPROVER");
            mapParam.put("LABEL_POSITION","LABEL_POSITION");
            mapParam.put("LABEL_ROLES","LABEL_ROLES");
            mapParam.put("LABEL_STATUS","LABEL_STATUS");
            mapParam.put("LABEL_LIST_COST","LABEL_LIST_COST");
            mapParam.put("LABEL_DOC_REF","LABEL_DOC_REF");
            mapParam.put("LABEL_ADVANCE_REMARK","LABEL_ADVANCE_REMARK");
            mapParam.put("LABEL_ADVANCE_PURPOSE","LABEL_ADVANCE_PURPOSE");


            mapParam.put("LABEL_DOCUMENT_REFERENCE","LABEL_DOCUMENT_REFERENCE");
            mapParam.put("LABEL_APPROVE_DOCUMENT_REFERENCE","LABEL_APPROVE_DOCUMENT_REFERENCE");
            mapParam.put("LABEL_DOCUMENT_ADVANCE","LABEL_DOCUMENT_ADVANCE");
            mapParam.put("LABEL_EXPENSE_DETAIL","LABEL_EXPENSE_DETAIL");
            mapParam.put("LABEL_LIST_EXPENSE","LABEL_LIST_EXPENSE");
            mapParam.put("LABEL_MONEY_AMOUNT_BATH","LABEL_MONEY_AMOUNT_BATH");

            String mapMessageStr = mapParam.toString();
            byte[] fileBytes = reportService.printExpenseReport(docNum,mapMessageStr,language);
            if(fileBytes != null){
                inputByteStream = new ByteArrayInputStream(fileBytes);
                IOUtils.copy(inputByteStream, response.getOutputStream());
            }
            return new ResponseEntity<String>("1",headers, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<String>("{\"ERROR\":" +e.getMessage()+ "\"}", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        } finally {
            IOUtils.closeQuietly(inputByteStream);
        }
    }






}
