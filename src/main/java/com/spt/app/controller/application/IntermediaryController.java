package com.spt.app.controller.application;

import com.google.gson.Gson;
import com.spt.app.aop.annotation.Loggable;
import com.spt.app.constant.ApplicationConstant;
import com.spt.app.controller.general.BaseCommonController;
import com.spt.app.service.*;
import com.spt.app.service.impl.EmployeeProfileServiceImpl;
import com.spt.app.service.impl.ParameterServiceImpl;
import flexjson.JSONSerializer;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.activation.MimetypesFileTypeMap;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/intermediaries")
public class IntermediaryController {

    static final Logger LOGGER = LoggerFactory.getLogger(IntermediaryController.class);

    @Autowired
    EmplyeeProfileService emplyeeProfileService;

    @Autowired
    CostCenterService costCenterService;

    @Autowired
    ParameterService parameterService;

    @Autowired
    DocumentApproveService documentApproveService;


    /* for autocomplete employee transaction */
    @Loggable
    @GetMapping("/findAutoCompleteEmployeeByKeySearchAndUserName")
    public ResponseEntity<String> findAutoCompleteEmployeeByKeySearch(@RequestParam(name="userName",required=false) String userName,
                                                                      @RequestParam(name="keySearch",required=false) String keySearch) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            LOGGER.info("><><>< findAutoCompleteEmployeeByKeySearchAndUserName  : {}  ",userName);
            responseEntity = emplyeeProfileService.findAutoCompleteEmployeeByKeySearchUserIn(keySearch,userName);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }


    /* for get employee profile */
    @Loggable
    @GetMapping("/findAutoCompleteEmployeeByUserName")
    public ResponseEntity<String> findAutoCompleteEmployeeByUserName(@RequestParam(name="userName",required=false) String userName) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            LOGGER.info("><><>< findAutoCompleteEmployeeByUserName  : {}  ",userName);
            responseEntity = emplyeeProfileService.findEmployeeProfileByEmployeeCode(userName);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    /* for autocomplete employee master */
    @Loggable
    @GetMapping("/findAutoCompleteEmployeeByKeySearch")
    public ResponseEntity<String> findAutoCompleteEmployeeByKeySearch(@RequestParam(name="keySearch",required=false) String keySearch) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            LOGGER.info("><><>< findAutoCompleteEmployeeByKeySearch  : {}  ",keySearch);
            if(keySearch == null || keySearch.equals("null")){
                keySearch = "%";
            }
            responseEntity = emplyeeProfileService.findAutoCompleteEmployeeByKeySearch(keySearch);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    @Loggable
    @GetMapping("/findParameterDueDateAdvance")
    public ResponseEntity<String> findParameterDueDateAdvance() {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity = null;
        try {
            LOGGER.info("><><>< findParameterDueDateAdvance ><><>< ");
            responseEntity = parameterService.findByCode(ApplicationConstant.PARAMETER_DUEDATE_ADVANCE);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    @Loggable
    @GetMapping("/findParameterDetailDueDateAdvance/{id}")
    public ResponseEntity<String> findParameterDetailDueDateAdvance(@PathVariable Long id) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity = null;
        try {
            LOGGER.info("><><>< findParameterDetailDueDateAdvance ><><>< ");
            responseEntity = parameterService.findParameterDetailsByParameterId(id);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }


    /* for check psa */
    @Loggable
    @GetMapping("/findEmployeeProfileByCostCenter")
    public ResponseEntity<String> findEmployeeProfileByCostCenter(@RequestParam(name="costCenter",required=false) String costCenter) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            LOGGER.info("><><>< findEmployeeProfileByCostCenter  : {}  ",costCenter);
            responseEntity = emplyeeProfileService.findEmployeeProfileByCostCenter(costCenter);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    /* for reserve car of document approve */
    @Loggable
    @GetMapping("/findParameterReserveCarLink")
    public ResponseEntity<String> findParameterReserveCarLink() {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity = null;
        try {
            LOGGER.info("><><>< findParameterReserveCarLink ><><>< ");
            responseEntity = parameterService.findByCode(ApplicationConstant.PARAMETER_LINK_RESERVE_CAR);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }



    @Loggable
    @GetMapping("/getApproveLine")
    public ResponseEntity<String> getApproveLine(@RequestParam(name="userName",required=false) String userName) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            LOGGER.info("><><>< getApproveLine  : {}  ",userName);
            responseEntity = emplyeeProfileService.getApproveLine(userName);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    /* for check psa */
    @Loggable
    @GetMapping("/findByPa/{pa}")
    public ResponseEntity<String> findByPa(@PathVariable("pa") String pa) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            LOGGER.info("><><>< findByPa  : {}  ",pa);
            responseEntity = emplyeeProfileService.findByPa(pa);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    /* for check psa */
    @Loggable
    @GetMapping("/findByPsa/{psa}")
    public ResponseEntity<String> findByPsa(@PathVariable("psa") String psa) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            LOGGER.info("><><>< findByPsa  : {}  ",psa);
            responseEntity = emplyeeProfileService.findByPsa(psa);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    /* for reserve car of document approve */
    @Loggable
    @GetMapping("/findParameterHotelReservationTemplete")
    public ResponseEntity<String> findParameterHotelReservationTemplete() {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity = null;
        try {
            LOGGER.info("><><>< findParameterHotelReservationTemplete ><><>< ");
            responseEntity = parameterService.findByCode(ApplicationConstant.PARAMETER_HOTEL_RESERVATION);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    /* for reserve flight of document approve */
    @Loggable
    @GetMapping("/findParameterFlightReservationFileDownload")
    public ResponseEntity<String> findParameterFlightReservationFileDownload() {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity = null;
        try {
            LOGGER.info("><><>< findParameterFlightReservationFileDownload ><><>< ");
            responseEntity = parameterService.findByCode(ApplicationConstant.PARAMETER_FLIGHT_RESERVATION);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    @Loggable
    @GetMapping("/downloadFileReservationTemplate")
    public ResponseEntity<String> downloadFileReservationTemplate(@RequestParam("reservationType") String reservationType,
                                                                  @RequestParam("fileName") String fileName,
                                                                  @RequestParam("flightType") String flightType,
                                                                  HttpServletResponse response) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity = null;

        ByteArrayInputStream byteArrayInputStream = null;
        try {
            LOGGER.info("><><><>< downloadFileReservationTemplate : {}",fileName);
            String mimeType = new MimetypesFileTypeMap().getContentType(fileName);
            response.setContentType(mimeType);
            response.addHeader("Content-Disposition","attachment; filename*=UTF-8''"+fileName);

            String url = "/documentApproveItemCustom/downloadFileReservation/"+reservationType;

            byte[] fileBytes = documentApproveService.downloadFileReservationTemplete(url);
            byteArrayInputStream = new ByteArrayInputStream(fileBytes);
            IOUtils.copy(byteArrayInputStream, response.getOutputStream());

            return new ResponseEntity<String>(headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /* for check psa */
    @Loggable
    @GetMapping("/findAdminByPapsa/{papsa}/{requester}")
    public ResponseEntity<String> findAdminByPsa(@PathVariable("papsa") String papsa,
                                                 @PathVariable("requester") String requester) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            LOGGER.info("><><>< findAdminByPapsa  : {}  ",papsa);
            responseEntity = emplyeeProfileService.findAdminByPapsaAndRequester(papsa,requester);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /* for reserve car of document approve */
    @Loggable
    @GetMapping("/findParameterCarReservationFileDownload")
    public ResponseEntity<String> findParameterCarReservationFileDownload() {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity = null;
        try {
            LOGGER.info("><><>< findParameterCarReservationFileDownload ><><>< ");
            responseEntity = parameterService.findByCode(ApplicationConstant.PARAMETER_CAR_RESERVATION);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {

            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.OK);
        }
    }

    /* for check psa */
    @Loggable
    @GetMapping("/findAccountByPapsa/{papsa}/{requester}")
    public ResponseEntity<String> findAccountByPapsa(@PathVariable("papsa") String papsa,
                                                    @PathVariable("requester") String requester) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            LOGGER.info("><><>< findAccountByPapsa  : {}  ",papsa);
            responseEntity = emplyeeProfileService.findAccountByPapsaAndRequester(papsa,requester);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /* for check psa */
    @Loggable
    @GetMapping("/getBankAccount/{requester}")
    public ResponseEntity<String> getBankAccount(@PathVariable("requester") String requester) {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String> responseEntity;
        try {
            LOGGER.info("><><>< getBankAccount ");
            responseEntity = emplyeeProfileService.getBankAccount(requester);

            return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return new ResponseEntity<String>("", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
