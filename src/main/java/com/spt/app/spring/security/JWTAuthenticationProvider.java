package com.spt.app.spring.security;

import com.google.gson.*;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class JWTAuthenticationProvider implements AuthenticationProvider {

    @Value("${accessTokenURI}")
    protected String engineServer;

    @Value("${ewfApplicationKey}")
    protected String ewfApplicationKey;

    @Value("${DefaultPassword}")
    protected String defaultPassword;

    @Value("${init.token.name}")
    private String initToken;

    @Autowired
    CustomUserModel customUserModel;

    static final Logger LOGGER = LoggerFactory.getLogger(DBAuthenticationProvider.class);

    RestTemplate restTemplate = new RestTemplate();

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

    @SuppressWarnings("Duplicates")
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        LOGGER.info("========== JWTAuthenticationProvider ==========");
        String name = authentication.getName().toLowerCase();;
        String password = authentication.getCredentials().toString();

        if (checkAuthentication(name, password)) {
            LOGGER.info("JWT Credential Success");
            // use the credentials
            // and authenticate against the third-party system
            return new UsernamePasswordAuthenticationToken(name, password, new ArrayList<>());
        } else {
            LOGGER.error("JWT Credential Not Found !!");
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    @SuppressWarnings("Duplicates")
    private boolean checkAuthentication(String userName, String rawPassword){
        try {
            if(defaultPassword.equals(rawPassword)){
                customUserModel.addValue("access_token", initToken);
                return true;
            }else{
                HttpHeaders headers = new HttpHeaders();
                headers.add("App-Key", ewfApplicationKey);
                headers.add("Cache-Control", "no-cache");
                headers.add("Content-Type", "application/x-www-form-urlencoded");

                MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
                map.add("username", userName);
                map.add("password", rawPassword);

                HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

                String json = restTemplate.postForEntity(engineServer,entity,String.class).getBody();

                JSONObject jsonObject = new JSONObject(json);

                if(jsonObject.has("success")){
                    JSONObject successObject = new JSONObject(jsonObject.get("success").toString());
                    JSONObject dataObject = new JSONObject(successObject.get("data").toString());
                    customUserModel.addValue("access_token", dataObject.get("access_token"));
                    return true;
                }else{
                    return false;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private String getDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        return result.toString();
    }
}
