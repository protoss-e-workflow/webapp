package com.spt.app.spring.security;

import com.spt.app.util.CookieUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.web.client.RestTemplate;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler implements LogoutSuccessHandler, LogoutHandler {
    private static Logger LOGGER = LoggerFactory.getLogger(CustomLogoutSuccessHandler.class);

    @Value("${services.auth}")
    private String authService;

    @Value("${ewfApplicationKey}")
    private String ewfApplicationKey;

    @Value("${logoutTokenURI}")
    private String logoutTokenURI;

    @Value("${jwt.token.name}")
    private String jwtTokenName;

    @Value("${init.token.name}")
    private String initToken;

    @Value("${init.username}")
    private String initUsername;

    @Value("${cookie.url}")
    private String cookieUrl;

    @Override
    public void onLogoutSuccess(
            HttpServletRequest request,
            HttpServletResponse response,
            Authentication authentication)
            throws IOException, ServletException {
        super.onLogoutSuccess(request, response, authentication);
    }

    @Override
    public void logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) {
        LOGGER.info("============= log out e-Workflow success ===============");
        RestTemplate restTemplate = new RestTemplate();
        try {
            String token = CookieUtil.getValue(httpServletRequest, jwtTokenName);
            if(initToken.equals(token)){
                CookieUtil.clear(httpServletResponse, jwtTokenName, cookieUrl);
                CookieUtil.clear(httpServletResponse, initUsername, cookieUrl);
                httpServletResponse.sendRedirect(authService);
            }else{
                LOGGER.info("log out in else");
                HttpHeaders headers = new HttpHeaders();
                headers.add("App-Key", ewfApplicationKey);

                String value = "?access_token="+token;
                String url = logoutTokenURI+value;
                ResponseEntity<String> reponseEntity = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<String>(headers), String.class);
//            JSONObject jsonObject = new JSONObject(reponseEntity);
                CookieUtil.clear(httpServletResponse, jwtTokenName, cookieUrl);
                CookieUtil.clear(httpServletResponse, initUsername, cookieUrl);
                httpServletResponse.sendRedirect(authService);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}