package com.spt.app.spring.security;

import com.spt.app.util.CookieUtil;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class JwtFilter extends OncePerRequestFilter {
    private static Logger LOGGER = LoggerFactory.getLogger(JwtFilter.class);
    RestTemplate restTemplate = new RestTemplate();

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        LOGGER.info("========= doFilterInternal e-Workflow ==========");
        String ewfApplicationKey = this.getFilterConfig().getInitParameter("ewfApplicationKey");
        String validateTokenURI = this.getFilterConfig().getInitParameter("validateTokenURI");
        String jwtTokenName = this.getFilterConfig().getInitParameter("jwtTokenName");
        String initToken = this.getFilterConfig().getInitParameter("initToken");
        String token = CookieUtil.getValue(httpServletRequest, jwtTokenName);
        if(initToken.equals(token)){
            httpServletRequest.getSession(true).setAttribute("access_token", initToken);
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        }else{
            Map<String, Object> map = authenticateValidate(token, ewfApplicationKey, validateTokenURI);
            LOGGER.info("validate : {}", map);

            if(map.size() <= 0){
                String authService = this.getFilterConfig().getInitParameter("services.auth");
                httpServletResponse.sendRedirect(authService);
            }else{
                httpServletRequest.getSession(true).setAttribute("userName", map.get("username").toString());
                httpServletRequest.getSession(true).setAttribute("access_token", token);
                filterChain.doFilter(httpServletRequest, httpServletResponse);
            }
        }
    }

    public Map<String, Object> authenticateValidate(String token, String appKey, String serverUrl){
        Map<String, Object> stringMap = new HashMap<>();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("App-Key", appKey);
            headers.add("Cache-Control", "no-cache");
            headers.add("Content-Type", "application/x-www-form-urlencoded");

            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
            map.add("access_token", token);

            HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);
            String json = restTemplate.postForEntity(serverUrl,entity,String.class).getBody();

            JSONObject jsonObject = new JSONObject(json);
            if(jsonObject.has("success")){
                JSONObject dataSuccessObject = new JSONObject(jsonObject.get("success").toString());
                String dataCodeObject = dataSuccessObject.get("code").toString();
                JSONObject dataObject = new JSONObject(dataSuccessObject.get("data").toString());
                JSONObject userInfoObject = new JSONObject(dataObject.get("user_info").toString());
                String userNameObject = userInfoObject.get("username").toString();

                stringMap.put("username", userNameObject);
                stringMap.put("code", dataCodeObject);
                return stringMap;
            }else{
                return stringMap;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return stringMap;
        }
    }
}
