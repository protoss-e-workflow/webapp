package com.spt.app.spring.configuration;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

public class ControllerHandleInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String langSession = RequestContextUtils.getLocaleResolver(request).resolveLocale(request).toString();
        if(langSession.equals("en") || langSession.equals("en_US"))
            LocaleContextHolder.setLocale(new Locale("en"));
        else
            LocaleContextHolder.setLocale(new Locale("th"));
        request.getSession(true).setAttribute("locale_lang",langSession);
        return super.preHandle(request, response, handler);
    }
}
