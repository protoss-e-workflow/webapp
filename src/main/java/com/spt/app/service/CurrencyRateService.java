package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface CurrencyRateService {

    public ResponseEntity<String> findByActionDate(String date);
}
