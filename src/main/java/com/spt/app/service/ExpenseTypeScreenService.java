package com.spt.app.service;

import org.springframework.http.ResponseEntity;

/**
 * Created by korrakote on 8/28/17.
 */
public interface ExpenseTypeScreenService {



    public ResponseEntity<String> findByStructureField(String structureField);



}
