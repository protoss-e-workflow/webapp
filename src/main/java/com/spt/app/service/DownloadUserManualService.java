package com.spt.app.service;

public interface DownloadUserManualService {

    public byte[] downloadManualUser(String url);
    public byte[] downloadManualApprove(String url);
    public byte[] downloadManualAccount(String url);

}
