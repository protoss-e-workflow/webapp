package com.spt.app.service;

import java.util.List;
import java.util.Map;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

public interface ParameterService {
	
	public ResponseEntity<String> findParameterDetailsByParameterId(Long id);
	public List<Map> findByParameterCodeIn(String code);
	public ResponseEntity<String> findByCode(String code);
	public ResponseEntity<String> findByParameterDetailCode(String code);
}
