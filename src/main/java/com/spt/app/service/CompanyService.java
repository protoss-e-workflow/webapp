package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface CompanyService {


    public ResponseEntity<String> findAllCompany();
    public ResponseEntity<String> findByCode(String code);

}
