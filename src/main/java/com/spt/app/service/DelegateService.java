package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.Map;

/**
 * Created by korrakote on 8/28/17.
 */
public interface DelegateService {





    public ResponseEntity<String> findAssignerByCriteria(Map<String,Object> objectMap);
    public ResponseEntity<String> findAssignerSize(Map<String,Object> objectMap);
    public ResponseEntity<String> findAllDelegateByEmpCodeAssignee(String empCodeAssignee);
    public ResponseEntity<String> findByEmpCodeAssignee(String empCodeAssignee);
    public ResponseEntity<String> findByEmpCodeAssigneeAndEmpCodeAssignerAndFlowTypeAndStartDateAndEndDate(
                                                                                                             String empCodeAssignee,
                                                                                                             String empCodeAssigner,
                                                                                                             String flowType,
                                                                                                             String startDate,
                                                                                                             String endDate
                                                                                                          );
    public ResponseEntity<String> findByEmpUserNameAssignerAndFlowTypeAndStartDate(
            String empUserNameAssigner,
            String flowType );


}
