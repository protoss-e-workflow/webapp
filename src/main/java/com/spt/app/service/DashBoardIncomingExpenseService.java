package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface DashBoardIncomingExpenseService {

    public ResponseEntity<String> findByCriteriaWithCompanyCode(Map<String, Object> objectMap);

    public ResponseEntity<String> findSizeWithCompanyCode(Map<String, Object> objectMap);

}
