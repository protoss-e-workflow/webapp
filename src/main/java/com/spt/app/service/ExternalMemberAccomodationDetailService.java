package com.spt.app.service;

import org.springframework.http.ResponseEntity;

public interface ExternalMemberAccomodationDetailService {

    public ResponseEntity<String> findByDocNumberAndItemId(String docNumber,String itemId);
}
