package com.spt.app.service;

import org.springframework.http.ResponseEntity;

public interface BaseCodeService {

    public ResponseEntity<String> findByCode(String code);
}
