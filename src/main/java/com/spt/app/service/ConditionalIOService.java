package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface ConditionalIOService {
	
	public List<Map>  findByCompany(Long company);
	public ResponseEntity<String>  findByCompanyCode(String code);
	public ResponseEntity<String> saveConditionalIO(Map<String, String[]> jsonConditionalIO);
	public ResponseEntity<String> findConditionalIOByGlAndIoAndCompany(String gl, String io,Long company);

}
