package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface RequestApproverService {



    public ResponseEntity<String> findByRequest(Long request);

}
