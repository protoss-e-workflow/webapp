package com.spt.app.service;

import org.springframework.http.ResponseEntity;

public interface MonthlyPhoneBillPerYearService {

    public ResponseEntity<String> findByEmpUserNameAndYear(String empUserName,String year);
}
