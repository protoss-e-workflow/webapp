package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface DieselAllowancePerMonthHistoryService {

	public ResponseEntity<String>   findByEmpCodeIgnoreCaseContainingAndMonthIgnoreCaseContainingAndYearIgnoreCaseContaining(String empCode, String month, String year);
	public ResponseEntity<String> saveDieselAllowancePerMonthHistoryForExpense(Map<String,String[]> jsonDocument);
	public ResponseEntity<String> deleteDieselAllowancePerMonthHistoryForExpense(Map<String,String[]> jsonDocument);
	public ResponseEntity<String> updateDieselAllowancePerMonthHistoryForExpense(Map<String,String[]> jsonDocument);
}
