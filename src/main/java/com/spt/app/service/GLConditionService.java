package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface GLConditionService {

    public ResponseEntity<String> deleteConditionGLSet(Map<String, String[]> json);

    public ResponseEntity<String> findByGlCreateeAndIoeAndGlSape(String glCreate, String io, String glSap);
}
