package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface RouteDistanceService {
	
	public List<Map>  findByLocationFromDescriptionAndLocationToDescription(String locationFromDescription, String locationToDescription);
	public ResponseEntity<String> findByLocationFromAndLocationTo(Long locationFrom, Long locationTo);
	public ResponseEntity<String> saveRouteDistance(Map<String, String[]> jsonRouteDistance);
	public ResponseEntity<String> calculateRouteDistanceFromLocation(String locationFrom,String locationTo,String companyCode);
	public ResponseEntity<String> calculateRouteDistanceFromDistance(String distance,String companyCode);

}
