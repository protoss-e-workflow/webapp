package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface DashBoardMyRequestService {
    ResponseEntity<String> findMyRequestNotification(
            String requester,
            String docNumber,
            String titleDescription,
            String documentStatus
    );
}
