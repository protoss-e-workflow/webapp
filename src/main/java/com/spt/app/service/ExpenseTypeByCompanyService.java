package com.spt.app.service;

import org.springframework.http.ResponseEntity;

public interface ExpenseTypeByCompanyService {

    public ResponseEntity<String> findByCompanyCode(String companyCode);
    public ResponseEntity<String> findExpenseTypeByCompanyByPaAndPsaAndKeySearch(String pa,String psa,String headOfficeGL,String factoryGL,String userName,String keySearch);
    public ResponseEntity<String> findExpenseTypeByCompanyByPaAndPsaAndFavorite(String pa,String psa,String userName);
}
