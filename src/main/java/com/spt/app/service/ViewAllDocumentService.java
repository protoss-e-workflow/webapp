package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ViewAllDocumentService {
    public ResponseEntity<String> findByCreatedDateStartAndCreatedDateEndAndCreatorAndDocNumberAndDocumentType(
            String createdDateStart,
            String createdDateEnd,
            String creator,
            String documentNumber,
            String documentType,
            String documentStatus,
            Long first,
            Long max
    );

    public ResponseEntity<String> pagingFindByCreatedDateStartAndCreatedDateEndAndCreatorAndDocNumberAndDocumentType(
            String createdDateStart,
            String createdDateEnd,
            String creator,
            String documentNumber,
            String documentType,
            String documentStatus
    );
}
