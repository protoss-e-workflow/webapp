package com.spt.app.service;

import java.util.List;
import java.util.Map;

public interface FlowManageService {
	
	public List<Map> getAuthorizationForExpenseByEmployee(String employee,String param,String approveType);
	public List<Map> getAuthorizationForApproveByEmployee(String employee,String param,String approveType);
    public Map   createDocumentFlow(String typeCode,String employee,String param,String docType,String documentNumber,String approveType,String psa);
    public Map   cancelDocument(String employee,String documentNumber);
    public Map   approveDocument(String employee,String actionStatus, String documentNumber,String docType,String documentFlow,String processId);
    public Map   rejectDocument(String employee,String actionStatus, String documentNumber,String docType,String documentFlow,String processId);
    public List<Map> getAuthorizationForExpenseByEmployee(String employee,String param,String approveType,String papsa);
    public List<Map> getAuthorizationForApproveByEmployee(String employee,String param,String approveType,String papsa);
	
	
}
