package com.spt.app.service;

import org.springframework.http.ResponseEntity;

public interface MyRequestService {
    ResponseEntity<String> getApproverByRequest(Long id);
}
