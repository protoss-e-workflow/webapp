package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface HistoryIncomingService {
    ResponseEntity<String> findHistory(String approver,
                                       String docNumber,
                                       String titleDescription,
                                       String docStatus,
                                       String documentType,
                                       Integer firstResult,
                                       Integer maxResult,
                                       Integer page);

    ResponseEntity<String> findHistorySize(
            String approver,
            String docNumber,
            String titleDescription,
            String docStatus,
            String documentType
    );
}
