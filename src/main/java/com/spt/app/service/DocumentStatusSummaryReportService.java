package com.spt.app.service;

public interface DocumentStatusSummaryReportService {

    public byte[] downloadCUS001(String url);
}
