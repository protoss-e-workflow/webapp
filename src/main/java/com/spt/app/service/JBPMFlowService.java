package com.spt.app.service;

import java.util.List;
import java.util.Map;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

public interface JBPMFlowService {
	
	public String   getAuthorizationForExpense(String employee,Double amount,String empLevel, String param);
	public String   createDocumentFlow(String employee, String documentFlow, String param,String docType,String documentNumber);
    public String   cancelDocument(String employee,String documentNumber);
    public String   sendActionToFlow(String employee,String param,String docType,String documentNumber);
    public String   findWaitingDocumentByEmployee(String user,String docType,String param);
    public String   getCurrentNode(String processId);
    public String   getActive(String processId);

}
