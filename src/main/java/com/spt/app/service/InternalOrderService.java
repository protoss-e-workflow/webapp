package com.spt.app.service;

import org.springframework.http.ResponseEntity;

public interface InternalOrderService {

    public ResponseEntity<String> findByIoCode(String ioCode);
    public ResponseEntity<String> findByIoCodeAndCompanyCode(String ioCode,String companyCode);
    public ResponseEntity<String> checkIoRealtime(String gl,String costCenter,String io,String manOrder,String wbs,String docNumber,String expItemId);
    public ResponseEntity<String> checkBudgetInEwf(String gl,String costCenter);
}
