package com.spt.app.service;

import java.util.List;
import java.util.Map;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

public interface ReportService {
	
	public ResponseEntity<String>   getCriteriaByCode(String code);
	public ResponseEntity<Resource>   genReport(Map<String,Object> objectMap);
	public byte[]  printApproveReport(String docNumber,String mapMessage,String language);
	public byte[]  printAdvanceReport(String docNumber,String mapMessage,String language);
	public byte[]  printExpenseReport(String docNumber,String mapMessage,String language);
}
