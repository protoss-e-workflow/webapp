package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface LocationService {
	
	public ResponseEntity<String> findByCode(String code);
	public List<Map>  findByLocationType(String locationType);
	public ResponseEntity<String> findByCodeAndLocationType(String code, String locationType);
	public ResponseEntity<String> findByLocationTypeAndDescription(String locationType, String description);
	public List<Map>  findByCodeIgnoreCaseContainingAndDescriptionIgnoreCaseContainingAndLocationType(String code, String description, String locationType);

}
