package com.spt.app.service;

import org.springframework.http.ResponseEntity;

public interface DocumentExpenseGroupService {

    public ResponseEntity<String> deleteDocumentExpenseGroup(Long id);
}
