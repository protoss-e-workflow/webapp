package com.spt.app.service;

public interface FlowConfigureService {
	
	public String   findByTypeCodeAndStateVerifyAndStateAproveAndStateAdminAndStateHrAndStateAccountAndStateFinance(
			String typeCode,
			String stateVerify,
			String stateAprove,
			String stateAdmin,
			String stateHr,
			String stateAccount,
			String stateFinance);
	public String   findByFlowCode(String code);
}
