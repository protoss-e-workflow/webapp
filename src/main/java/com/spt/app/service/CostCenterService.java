package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface CostCenterService {

    public List<Map> findByEmpUserName(String empUserName);
}
