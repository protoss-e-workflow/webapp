package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface PetrolAllowancePerMonthHistoryService {
	
	public List<Map>  findByEmpCodeIgnoreCaseContainingAndMonthIgnoreCaseContainingAndYearIgnoreCaseContaining(String empCode, String month, String year);
	public ResponseEntity<String> findByEmpCode(String empCode);
	public ResponseEntity<String> savePetrolAllowancePerMonthHistoryForExpense(Map<String,String[]> jsonDocument);
	public ResponseEntity<String> deletePetrolAllowancePerMonthHistoryForExpense(Map<String,String[]> jsonDocument);
	public ResponseEntity<String> updatePetrolAllowancePerMonthHistoryForExpense(Map<String,String[]> jsonDocument);

}
