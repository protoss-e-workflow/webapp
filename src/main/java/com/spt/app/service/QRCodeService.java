package com.spt.app.service;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;

public interface QRCodeService {

    ResponseEntity<Resource> generateQrCode(String text);
}
