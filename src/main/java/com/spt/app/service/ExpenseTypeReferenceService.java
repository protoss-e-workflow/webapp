package com.spt.app.service;

import org.springframework.http.ResponseEntity;

/**
 * Created by korrakote on 8/28/17.
 */
public interface ExpenseTypeReferenceService {

    public ResponseEntity<String> findByCompleteReason(String completeReason);


}
