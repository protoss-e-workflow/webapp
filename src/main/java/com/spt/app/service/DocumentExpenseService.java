package com.spt.app.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.Map;

public interface DocumentExpenseService extends DocumentService {

    public ResponseEntity<String> saveDocumentExpenseItem(Map<String, String[]> jsonDocument);
    public ResponseEntity<String> saveDocumentExpenseItemDetail(Map<String, String[]> jsonDocument);
    public ResponseEntity<String> updateDocumentExpenseItemDetail(Map<String, String[]> jsonDocument);

    /* Manager Document Attachment */
    public ResponseEntity<String> uploadFile(MultipartHttpServletRequest multipartHttpServletRequest, String url);
    public ResponseEntity<String> findDocumentAttachmentByDocumentId(Long id);
    public ResponseEntity<String> deleteDocumentAttachment(Long id);

    public ResponseEntity<String> saveDocumentReference(Map<String,String[]> jsonDocument);
    public ResponseEntity<String> updateDocumentReference(Map<String,String[]> jsonDocument);
    public ResponseEntity<String> updateDocumentReferenceList(Map<String,String[]> jsonDocument);

    public String getAuthorizeForLineApprove(Map<String,String[]> map);

    public ResponseEntity<String> uploadFileDocumentExpenseItem(MultipartHttpServletRequest multipartHttpServletRequest, String url);
    public ResponseEntity<String> findDocumentExpenseItemAttachmentByDocumentExpenseItemId(Long id);
    public ResponseEntity<String> deleteDocumentExpenseItemAttachment(Long id);
    public ResponseEntity<String> expenseTransfer(Map<String,String[]> jsonDocument);
    public ResponseEntity<String> updateCostCenterCode(Map<String,String[]> jsonDocument);
    public ResponseEntity<String> updateDocumentExpenseItemAndDocumentExpenseGroupByExpenseTypeByCompany(Map<String,String[]> jsonDocument);
}
