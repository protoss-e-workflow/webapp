package com.spt.app.service;

import org.springframework.http.ResponseEntity;

public interface InternalMemberAccomodationDetailService {

    public ResponseEntity<String> findByDocNumberAndItemId(String docNumber, String itemId);
}
