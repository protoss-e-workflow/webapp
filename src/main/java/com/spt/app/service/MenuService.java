package com.spt.app.service;

import java.util.List;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

public interface MenuService {
	
	public ResponseEntity<String>   putRole(Long id,List<String> associateIdLs);
	public ResponseEntity<String>   deleteRole(Long id,List<String> associateIdLs);
	public ResponseEntity<String>   deleteAllRole(Long id);
}
