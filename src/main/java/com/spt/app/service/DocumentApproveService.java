package com.spt.app.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.Map;

public interface DocumentApproveService extends DocumentService{

	/* Manage Document Approve Item */
	public ResponseEntity<String> saveDocumentApproveItem(Map<String,String[]> jsonDocument);
    public ResponseEntity<String> updateDocumentApprove(Map<String,String[]> jsonDocument);


    /* Manage Travel Detail */
    public ResponseEntity<String> saveTravelDetail(Map<String,String[]> jsonDocument);
    public ResponseEntity<String> findTravelDetailsByDocumentApproveItemId(Long id);
    public ResponseEntity<String> deleteTravelDetails(Long id);
    
    /* Manage Travel Member */
    public ResponseEntity<String> saveTravelMember(Map<String,String[]> jsonDocument);
    public ResponseEntity<String> findTravelMembersByDocumentApproveItemId(Long id);
    public ResponseEntity<String> deleteTravelMembers(Long id);
    
    /* Manage External Member */
    public ResponseEntity<String> saveExternalMember(Map<String,String[]> jsonDocument);
    public ResponseEntity<String> findExternalMembersByDocumentApproveItemId(Long id);
    public ResponseEntity<String> deleteExternalMembers(Long id);

    /* Manage Car Booking */
    public ResponseEntity<String> saveCarBooking(Map<String,String[]> jsonDocument);
    public ResponseEntity<String> findCarBookingsByDocumentApproveItemId(Long id);
    
    /* Manage Hotel Booking */
    public ResponseEntity<String> saveHotelBooking(Map<String,String[]> jsonDocument);

    /* Manage Flight Ticket */
    public ResponseEntity<String> saveFlightTicket(Map<String,String[]> jsonDocument);
    
    /* Manage Document Reference */
    public ResponseEntity<String> saveDocumentReference(Map<String,String[]> jsonDocument);

    /* Manager Document Attachment */
    public ResponseEntity<String> uploadFile(MultipartHttpServletRequest multipartHttpServletRequest, String url);
    public ResponseEntity<String> findDocumentAttachmentByDocumentId(Long id);
    public ResponseEntity<String> deleteDocumentAttachment(Long id);
    public byte[] downloadFileDocumentAttachment(String url);

    /* Manage Autocomplete document reference for document approve */
    public ResponseEntity<String> findAutoCompleteDocRefAppByKeySearch(String keySearch);
    public ResponseEntity<String> findAutoCompleteDocRefAppByKeySearch(String requester,String keySearch);
    public ResponseEntity<String> findByDocTypeAndDocStatusCompleteAndAppTypeTravelAndTravelMemberUser(Map<String,String[]> jsonDocument);

    /* Manage Request */
    public ResponseEntity<String> findRequestByDocument(Long id);
    public ResponseEntity<String> findRequestApproverByRequest(Long id);
    public String getAuthorizeForLineApprove(Map<String,String[]> map);

    /* Manage Doc refence */
    public ResponseEntity<String> findDocRefByDocTypeExp(String docNumber);
    public ResponseEntity<String> findTravelMemberAndExternalMemberBydocRef(String docNumber);
    public ResponseEntity<String> findTravelDetailsByDocRef(String docNumber);
    public ResponseEntity<String> findMasterDataHotelByCode();
    public ResponseEntity<String> findMasterDataAirlineByCode();
    public ResponseEntity<String> findTravelMemberByDocRef(String docNumber);

    public ResponseEntity<String> findByDocumentId(Long id);
    public ResponseEntity<String> findByDocumentNumber(String docNumber);

    public byte[] downloadFileReservationTemplete(String urlParam);
    public ResponseEntity<String> validateDateTimeOverlap(Map<String,String[]> dataMap);
    public ResponseEntity<String> deleteDocumentApproveItem(Long id,String approveType);


}
