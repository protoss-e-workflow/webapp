package com.spt.app.service;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

public interface UserAuthorizationService {

	public Object getUserDetail(String username);
	public ResponseEntity<String> save(Map parameter);
	public ResponseEntity<String> putRole(Long id,List<String> associateIdLs);
	public ResponseEntity<String> findRoleByRoleName(String roleName);
}
