package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface MonthlyPhoneBillService {
	
	public List<Map>  findByEmpCodeIgnoreCaseContaining(String empCode);
	public ResponseEntity<String> findByEmpCode(String empCode);
	public ResponseEntity<String> findByEmpUserName(String empUserName);

}
