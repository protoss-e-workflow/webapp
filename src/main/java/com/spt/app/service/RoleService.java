package com.spt.app.service;

import java.util.List;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

public interface RoleService {
	
	public ResponseEntity<String> findByRoleName(String roleName);
	public ResponseEntity<String> findUserByRole(Long id);
	public ResponseEntity<String> findAllRole();
}
