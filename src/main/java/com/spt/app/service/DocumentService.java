package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface DocumentService {

    public ResponseEntity<String> saveDocument(Map<String,String[]> jsonDocument);
    public ResponseEntity<String> updateDocumentStatus(Map<String,String[]> jsonDocument);
    public ResponseEntity<String> copyDocument(Map<String,String[]> jsonDocument);
    public ResponseEntity<String> generateDocumentNumber(Map<String,String[]> jsonDocument);
    public ResponseEntity<String> findByRequesterAndDocumentType(String requester,String documentType);
    public ResponseEntity<String> findByDocNumber(String docNumber);
    public ResponseEntity<String> findByRequesterOrderByDocNumber(String requester);
    public ResponseEntity<String> findByRequesterAndDocNumberAndTitleAndDocType(String requester,String docNumber,String title,String docType);

}
