package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface PetrolAllowancePerMonthService {
	
	public List<Map<String,String>>  findByEmpCodeIgnoreCaseContaining(String empCode);
	public ResponseEntity<String> findByEmpCode(String empCode);
	public ResponseEntity<String> findByEmpUserName(String empUserName);
}
