package com.spt.app.service;

import java.util.List;
import java.util.Map;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

public interface EmployeeReplacementService {
	
	public ResponseEntity<String>   findByEmpUserNameReplace(String userName);
	public ResponseEntity<String>   findByEmpUserNameAgree(String userName);
	public ResponseEntity<String>   findByEmpCodeReplace(String empCodeReplace);
	public ResponseEntity<String> findUserAgreeByCriteria(Map<String,Object> objectMap);
	public ResponseEntity<String> findUserAgreeSize(Map<String,Object> objectMap);
	public ResponseEntity<String> findByEmpCodeReplaceAndEmpCodeAgree(String empCodeReplace,String empCodeAgree);
}
