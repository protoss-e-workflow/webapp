package com.spt.app.service.impl;

import com.spt.app.service.*;
import com.spt.app.util.HttpRequestParamConvertUtil;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.File;
import java.net.URLEncoder;
import java.util.*;

@Service("DocumentAdvanceService")
public class DocumentAdvanceServiceImpl extends AbstractEngineService implements BaseCommonService,BaseCodeService,DocumentAdvanceService{

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        super.restTemplate = restTemplate;
    }

    @Autowired
    FlowManageService flowManageService;

    public ResponseEntity<String> findByCode(String code) {
        return null;
    }

    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        return null;
    }

    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        return null;
    }

    public ResponseEntity<String> save(Map parameter) {
        return null;
    }

    public ResponseEntity<String> savePut(Long id, Map parameter) {
        return null;
    }

    public ResponseEntity<String> delete(Long id) {
        return null;
    }

    public ResponseEntity<String> get(Long id) {
        String url = "/documents/"+id;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    public ResponseEntity<String> load() {
        return null;
    }

    public ResponseEntity<String> saveDocument(Map<String,String[]> jsonDocument) {
        LOGGER.info("-= SERVICE SAVE DOCUMENT =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{
            Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("costCenter",jsonDocument,mapRelation);

            String url = "/documentCustom/?"+ HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            List<String> stringList = new ArrayList<String>();
            stringList.add("documentAdvance");

            reponseEntity = postWithJsonRelation(jsonDocument,stringList, HttpMethod.POST,url.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }

        return  reponseEntity;
    }

    public ResponseEntity<String> updateDocumentStatus(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE UPDATE DOCUMENT STATUS ADVANCE =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{

            String url = "/documentCustom/updateDocumentStatus";

            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }
        return  reponseEntity;
    }

    public ResponseEntity<String> copyDocument(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE COPY DOCUMENT ADVANCE =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{
            String url = "/documentCustom/copyDocument";
            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }
        return  reponseEntity;
    }

    public ResponseEntity<String> uploadFile(MultipartHttpServletRequest multipartHttpServletRequest, String urlParam) {
        String url = this.EngineServer +urlParam;
        LOGGER.info("url :{}", url);

        ResponseEntity<String> reponseEntity;
        try {
            MultipartFile multipathFile = multipartHttpServletRequest.getFile("file");
            String fileName 	=  multipartHttpServletRequest.getParameter("filename");
            String attachment 	= multipartHttpServletRequest.getParameter("attachmentType");
            String document 	= multipartHttpServletRequest.getParameter("document");

            File convFile = new File(multipathFile.getOriginalFilename());
            multipathFile.transferTo(convFile);

            MediaType mediaType = MediaType.MULTIPART_FORM_DATA;
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(mediaType);


            MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();
            FileSystemResource file = new FileSystemResource(convFile);

            body.add("file", file);
            body.add("name",fileName);
            body.add("attachmentType", attachment);
            body.add("document", document);


            HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<MultiValueMap<String, Object>>(body, headers);

            FormHttpMessageConverter converter = new FormHttpMessageConverter();
            converter.setSupportedMediaTypes(Arrays.asList(mediaType));

            restTemplate.getMessageConverters().add(converter);
            reponseEntity = restTemplate.postForEntity(url, entity, String.class);
            convFile.delete();
        } catch (Exception e) {
            e.printStackTrace();
            reponseEntity = new ResponseEntity("ERROR", HttpStatus.OK);
        }

        return reponseEntity;
    }


    public ResponseEntity<String> findDocumentAttachmentByDocumentId(Long id) {
        String url = "/documentAttachmentCustom/getDocumentAttachment/"+id+"/documentAttachments";
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }


    public ResponseEntity<String> deleteDocumentAttachment(Long id) {
        String url = "/documentAttachments/"+id;
        return deleteSend(HttpMethod.DELETE, url);
    }

    public ResponseEntity<String> saveDocumentReference(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE SAVE DOCUMENT REFERENCE ADVANCE =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{
            Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("document",jsonDocument,mapRelation);
            String url = "/documentReferenceCustom/?"+HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }
        return  reponseEntity;
    }

    public ResponseEntity<String> updateDocumentReference(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE UPDATE DOCUMENT REFERENCE ADVANCE =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{
            Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("document",jsonDocument,mapRelation);
            String url = "/documentReferenceCustom/updateDocumentReference?"+HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }
        return  reponseEntity;
    }

    @Override
    public ResponseEntity<String> generateDocumentNumber(Map<String, String[]> jsonDocument) {
        return null;
    }

    @Override
    public ResponseEntity<String> findByRequesterAndDocumentType(String requester,String documentType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByRequesterAndDocumentType";
        String url = "/documents"+"/search/"+method+"?requester="+requester+"&documentType="+documentType;
        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> findByDocNumber(String docNumber) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByDocNumber";
        String url = "/documents"+"/search/"+method+"?docNumber="+docNumber;
        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> findByRequesterOrderByDocNumber(String requester) {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByRequesterOrderByDocNumber";
        String url = "/documents"+"/search/"+method+"?requester="+requester;
        return getResultString(url,entity);

    }

    public String getAuthorizeForLineApprove(Map<String,String[]> map) {
        LOGGER.info("-=GET AUTHORIZE FOR ADVANCE  =-");
        String userName =  map.get("requester")[0];   // ===== user of requester   ex. "karoons" =====;
        String approveType  =  map.get("approveType")[0];   // ===== for case document approve =====;
        String expenseAmountStr = "";
        String papsa = map.get("papsa")[0];
        String reponseEntity = null;

        try{
            Map<String, String> mapDetail = new HashMap<String, String>();
            applyRelateEntity("details",map,mapDetail);
            List<Map> listDetail = gson.fromJson(mapDetail.get("details"),List.class);
            for (int i = 0; i < listDetail.size(); i++) {
                if(i == listDetail.size()-1){
                    expenseAmountStr += String.valueOf(listDetail.get(i).get("flowType"))+";"+String.valueOf(listDetail.get(i).get("amount"))+";"+String.valueOf(listDetail.get(i).get("costCenter"));
                }else{
                    expenseAmountStr += String.valueOf(listDetail.get(i).get("flowType"))+";"+String.valueOf(listDetail.get(i).get("amount"))+";"+String.valueOf(listDetail.get(i).get("costCenter"))+"#";
                }
            }

            if("".equals(papsa)){
                reponseEntity = gson.toJson(flowManageService.getAuthorizationForExpenseByEmployee(userName,expenseAmountStr,approveType));
            }else{
                reponseEntity = gson.toJson(flowManageService.getAuthorizationForExpenseByEmployee(userName,expenseAmountStr,approveType,papsa));
            }


        }catch (Exception e){

        }
        return reponseEntity;
    }

    public ResponseEntity<String> updateDocumentAdvance(Map<String,String[]> jsonDocument) {
        LOGGER.info("-= SERVICE UPDATE DOCUMENT ADVANCE =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{

            String url = "/documentCustom/updateDocumentAdvance";
            List<String> stringList = new ArrayList<String>();
            stringList.add("documentAdvance");

            reponseEntity = postWithJsonRelation(jsonDocument,stringList, HttpMethod.POST,url.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }

        return  reponseEntity;
    }

    @Override
    public ResponseEntity<String> findByDocTypeAndDocStatusCompleteAndUser(Map<String,String[]> jsonDocument) {
        String url = "/documentCustom/findByDocTypeAndDocStatusCompleteAndUser?userName="+jsonDocument.get("userName")[0];
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> findByDocTypeAndDocNumberAndDocStatusCompleteAndUser(Map<String,String[]> jsonDocument) {
        String url = "/documentCustom/findByDocTypeAndDocNumberAndDocStatusCompleteAndUser?userName="+jsonDocument.get("userName")[0]+"&docNumber="+jsonDocument.get("docNumber")[0];
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> findByRequesterAndDocNumberAndTitleAndDocType(String requester, String docNumber, String title, String docType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String dataParam = "?requester="+requester+"&documentType="+docType+"&docNumber="+docNumber+"&titleDescription="+title;
        String method = "findByRequesterAndDocumentTypeAndDocNumberIgnoreCaseContainingAndTitleDescriptionIgnoreCaseContainingOrderByCreatedDateDesc";
        String url = "/documents"+"/search/"+method+dataParam;
        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> advanceTransfer(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE ADVANCE TRANSFER =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{
            String url = "/expenseTransferCustom/advanceTransfer";
            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }
        return  reponseEntity;
    }

    public ResponseEntity<String> unlockAdvance(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE UNLOCK DOCUMENT ADVANCE =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{

            String url = "/documentCustom/unlockAdvance";

            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }
        return  reponseEntity;
    }

    public ResponseEntity<String> saveSapManual(MultipartHttpServletRequest multipartHttpServletRequest, String urlParam) {
        String url = this.EngineServer +urlParam;
        LOGGER.info("url :{}", url);

        ResponseEntity<String> reponseEntity;
        try {
            MultipartFile multipathFile = multipartHttpServletRequest.getFile("file");
            String fileName 	=  multipartHttpServletRequest.getParameter("filename");
            String attachment 	= multipartHttpServletRequest.getParameter("attachmentType");
            String document 	= multipartHttpServletRequest.getParameter("document");
            String docClear 	= multipartHttpServletRequest.getParameter("docClear");

            File convFile = new File(multipathFile.getOriginalFilename());
            multipathFile.transferTo(convFile);

            MediaType mediaType = MediaType.MULTIPART_FORM_DATA;
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(mediaType);


            MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();
            FileSystemResource file = new FileSystemResource(convFile);

            body.add("file", file);
            body.add("name",fileName);
            body.add("attachmentType", attachment);
            body.add("document", document);
            body.add("docClear", docClear);


            HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<MultiValueMap<String, Object>>(body, headers);

            FormHttpMessageConverter converter = new FormHttpMessageConverter();
            converter.setSupportedMediaTypes(Arrays.asList(mediaType));

            restTemplate.getMessageConverters().add(converter);
            reponseEntity = restTemplate.postForEntity(url, entity, String.class);
            convFile.delete();
        } catch (Exception e) {
            e.printStackTrace();
            reponseEntity = new ResponseEntity("ERROR", HttpStatus.OK);
        }

        return reponseEntity;
    }
}