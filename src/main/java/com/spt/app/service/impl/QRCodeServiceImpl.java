package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCodeService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.QRCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service("QRCodeService1")
public class QRCodeServiceImpl extends AbstractEngineService implements BaseCommonService,BaseCodeService,QRCodeService {

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {

        super.restTemplate = restTemplate;
    }

    public ResponseEntity<String> findByCode(String code) {
        return null;
    }

    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        return null;
    }

    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        return null;
    }

    public ResponseEntity<String> save(Map parameter) {
        return null;
    }

    public ResponseEntity<String> savePut(Long id, Map parameter) {
        return null;
    }

    public ResponseEntity<String> delete(Long id) {
        return null;
    }

    public ResponseEntity<String> get(Long id) {
        return null;
    }

    public ResponseEntity<String> load() {
        return null;
    }

    @Override
    public ResponseEntity<Resource> generateQrCode(String text) {
        String url = "/qrcode?text="+text;
        return loadReqource(url);
    }
}
