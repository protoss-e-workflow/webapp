package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCodeService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.CostCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service("CostCenterService")
public class CostCenterServiceImpl extends AbstractEngineService implements BaseCommonService,BaseCodeService,CostCenterService {

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {

        super.restTemplate = restTemplate;
    }

    private String urlBase = "costCenters";

    @Override
    public ResponseEntity<String> findByCode(String code) {
        return null;
    }

    @Override
    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        return null;
    }

    @Override
    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        return null;
    }

    @Override
    public ResponseEntity<String> save(Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> savePut(Long id, Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> delete(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> get(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> load() {
        return null;
    }

    @Override
    public List<Map> findByEmpUserName(String empUserName) {
        String method = "findByEmpUserName";
        String url = "/"+urlBase+"/search/"+method+"?empUserName="+empUserName;
        String jsonCostCenter = getResultByExchange(url).getBody();
        Map CostCenterMap = gson.fromJson(jsonCostCenter, Map.class);
        List<Map> contentsCostCenter =  (List<Map>) CostCenterMap.get("content");
        return contentsCostCenter;
    }
}
