package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.HistoryIncomingService;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Map;

@Service("HistoryIncomingService")
public class HistoryIncomingServiceImpl extends AbstractEngineService implements BaseCommonService,HistoryIncomingService{

    private static Logger LOGGER = LoggerFactory.getLogger(HistoryIncomingServiceImpl.class);

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        super.restTemplate = restTemplate;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
            try {
                queryString = URLDecoder.decode(queryString, "UTF-8");
            } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
            //queryString = queryString.replaceAll("%2C", ",");
        }

        String username = queryString.substring(queryString.lastIndexOf("=") + 1,queryString.length());

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
//        String dataParam = "?approver="+approver+"&docNumber="+documentNumber+"&titleDescription="+titleDescription+"&docStatus="+documentStatus+"&docType="+docType;
        String method = "/findDocumentApproveByUserNameApproverAndDocType?";
        String url = "/documentCustom" + "/search" + method + queryString;

//        String method = "search/findByDocument_DocNumberIgnoreCaseContainingAndDocument_TitleDescriptionIgnoreCaseContainingAndDocument_DocumentTypeAndDocument_DocumentStatusInOrderByIdDesc";
//        String url = "/requests/"+method+"?sort=id,desc&"+queryString+"&projection=haveDocAndApprovers";

        ResponseEntity<String> responseEntity = getResultString(url,entity);
        LOGGER.info("DATA : {}",responseEntity.getBody());
        
//        org.json.JSONObject jsonObject = new org.json.JSONObject(responseEntity.getBody());
//        LOGGER.info("CONTENT : {}",jsonObject.get("content"));

        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
            try {
                queryString = URLDecoder.decode(queryString, "UTF-8");
            } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
            //queryString = queryString.replaceAll("%2C", ",");
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "serach/findDocumentApproveByUserNameApproverAndDocType";
        String url = "/requests/"+method+"?page=0&size=1&"+queryString+"&projection=haveDocAndApprovers";
        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> save(Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> savePut(Long id, Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> delete(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> get(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> load() {
        return null;
    }

    @Override
    @SuppressWarnings("Duplicates")
    public ResponseEntity<String> findHistory(String approver,
                                              String docNumber,
                                              String titleDescription,
                                              String docStatus,
                                              String documentType,
                                              Integer firstResult,
                                              Integer maxResult,
                                              Integer page) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
//        String method = "/findByDocNumberAndTitleDescriptionAndDocumentTypeAndDocumentStatusInAndRequestInOrderBySendDateByDocNumberDesc";
        String method = "/findDocumentApproveByUserNameApproverAndDocType";
//        String url = "/documents/search" + method + "?userNameApprover=" + approver + "&docNumber=" +docNumber+
//                "&titleDescription="+ titleDescription+"&documentStatus="+docStatus+"&documentType=" + documentType;
        String url = "/documentCustom/search" + method + "?approver=" + approver + "&docNumber=" +docNumber+
                "&titleDescription="+ titleDescription+"&docStatus="+docStatus+"&docType=" + documentType+
                "&firstResult=" + firstResult +"&maxResult="+ maxResult + "&page=" + page;
        ResponseEntity<String> responseEntity = getResultString(url,entity);

        LOGGER.info("data : {}",responseEntity.toString());

        return responseEntity;
    }

    @Override
    public ResponseEntity<String> findHistorySize(String approver, String docNumber, String titleDescription, String docStatus, String documentType) {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.add("Content-Type", "application/json; charset=utf-8");

            HttpEntity<String> entity = new HttpEntity<String>("", headers);
//            String method = "/findByDocNumberAndTitleDescriptionAndDocumentTypeAndDocumentStatusInAndRequestInOrderBySendDateByDocNumberDesc";
            String method = "/findDocumentApproveByUserNameApproverAndDocTypeSize";
//            String url = "/documents/search" + method + "?page=0&size=1&userNameApprover=" + approver + "&docNumber=" +docNumber+
//                    "&titleDescription="+ titleDescription+"&documentStatus="+docStatus+"&documentType=" + documentType;

            String url = "/documentCustom/search" + method + "?approver=" + approver + "&docNumber=" +docNumber+
            "&titleDescription="+ titleDescription+"&docStatus="+docStatus+"&docType=" + documentType;

            ResponseEntity<String> responseEntity = getResultString(url,entity);

            LOGGER.info("data : {}",responseEntity.toString());

            return responseEntity;
    }
}
