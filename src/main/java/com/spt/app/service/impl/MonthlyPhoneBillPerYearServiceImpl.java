package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.MonthlyPhoneBillPerYearService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service("MonthlyPhoneBillPerYearService")
public class MonthlyPhoneBillPerYearServiceImpl extends AbstractEngineService implements BaseCommonService,MonthlyPhoneBillPerYearService {

    static final Logger LOGGER = LoggerFactory.getLogger(MonthlyPhoneBillServiceImpl.class);

    private String urlBase = "monthlyPhoneBillPerYears";


    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {

        super.restTemplate = restTemplate;
    }

    @Override
    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        return null;
    }

    @Override
    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        return null;
    }

    @Override
    public ResponseEntity<String> save(Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> savePut(Long id, Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> delete(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> get(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> load() {
        return null;
    }

    @Override
    public ResponseEntity<String> findByEmpUserNameAndYear(String empUserName, String year) {
        String method = "findByUserNameAndYear";
        String url = "/"+urlBase+"/search/"+method+"?userName="+empUserName+"&year="+year;
        return getResultByExchange(url);
    }
}
