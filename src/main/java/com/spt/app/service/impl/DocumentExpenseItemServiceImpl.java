package com.spt.app.service.impl;

import com.spt.app.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

@Service("DocumentExpenseItemService")
public class DocumentExpenseItemServiceImpl extends AbstractEngineService implements BaseCommonService,BaseCodeService,DocumentExpenseItemService {
    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {

        super.restTemplate = restTemplate;
    }

    public ResponseEntity<String> findByCode(String code) {
        return null;
    }

    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        return null;
    }

    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        return null;
    }

    public ResponseEntity<String> save(Map parameter) {
        return null;
    }

    public ResponseEntity<String> savePut(Long id, Map parameter) {
        return null;
    }

    public ResponseEntity<String> delete(Long id) {
        String url = "/documentExpenseItemCustom/delete/"+id;
        return deleteSend(HttpMethod.DELETE, url);
    }

    public ResponseEntity<String> get(Long id) {
        String url = "/documentExpenseItems/"+id;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    public ResponseEntity<String> load() {
        return null;
    }

    @Override
    public ResponseEntity<String> findDocumentExpenseItemByDocumentExp(String id) {
        String url = "/documentExpenseItems/search/findByDocumentExp?documentExp="+id;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }



    @Override
    public byte[] downloadFileDocumentAttachment(String urlParam) {
        String url = this.EngineServer +urlParam;
        LOGGER.info("url :{}", url);
        HttpHeaders headers = new HttpHeaders();

        HttpEntity<String> entity = new HttpEntity<String>("parameter",headers);
        ResponseEntity<byte[]> responseEntity = restTemplate.exchange(url,HttpMethod.GET,entity,byte[].class);

        return responseEntity.getBody();
    }

    @Override
    public ResponseEntity findDocumentAmountSeperateByType() {

        String url = this.EngineServer +"/documentCustom/findDocumentAmountSeperateByType";


        ResponseEntity<String> responseEntity = restTemplate.exchange(url,HttpMethod.GET,null,String.class);

        return responseEntity;

    }

    @Override
    public ResponseEntity<String> findAllDocumentExceptDocStatusCCLAndREJ(String dateStart, String dateEnd, String requester, String docNumber, Integer first, Integer max) {
        String method = "/documentCustom/findAllDocumentExceptDocStatusCCLAndREJ";
        String url = method + "?" + "dateStart="+ dateStart +"&dateEnd="+dateEnd+"&requester="+requester+"&docNumber="+docNumber+"&firstResult="+first+"&maxResult="+max;
        return getResultStringAndSetUser(url);
    }

    @Override
    public ResponseEntity<String> findAllDocumentExceptDocStatusCCLAndREJSize(String dateStart, String dateEnd, String requester, String docNumber) {
        String method = "/documentCustom/findAllDocumentExceptDocStatusCCLAndREJSize";
        String url = method + "?" + "dateStart="+ dateStart +"&dateEnd="+dateEnd+"&requester="+requester+"&docNumber="+docNumber;
        return getResultStringAndSetUser(url);
    }


}
