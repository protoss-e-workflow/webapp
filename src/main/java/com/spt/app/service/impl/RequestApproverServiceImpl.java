package com.spt.app.service.impl;

import com.google.gson.*;
import com.spt.app.service.*;
import com.spt.app.util.HttpRequestParamConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service("RequestApproverService")
public class RequestApproverServiceImpl extends AbstractEngineService implements BaseCommonService,BaseCodeService,RequestApproverService {

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {

        super.restTemplate = restTemplate;
    }




    public ResponseEntity<String> findByCode(String code) {
        return null;
    }

    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        return null;
    }

    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        return null;
    }

    public ResponseEntity<String> save(Map parameter) {
        return null;
    }

    public ResponseEntity<String> savePut(Long id, Map parameter) {
        return null;
    }

    public ResponseEntity<String> delete(Long id) {
        return null;
    }

    public ResponseEntity<String> get(Long id) {
        return null;
    }

    public ResponseEntity<String> load() {
        return null;
    }



    @Override
    public ResponseEntity<String> findByRequest(Long request) {
        String method = "findByRequest";
        String url = "/requestApprovers"+"/search/"+method+"?request="+request;
        return getResultByExchange(url);
    }
}
