package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.DocumentApproveCostEstimateService;
import com.spt.app.util.HttpRequestParamConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("DocumentApproveCostEstimateService")
public class DocumentApproveCostEstimateServiceImpl extends AbstractEngineService implements BaseCommonService, DocumentApproveCostEstimateService {

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        super.restTemplate = restTemplate;
    }

    @Override
    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        return null;
    }

    @Override
    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        return null;
    }

    @Override
    public ResponseEntity<String> save(Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> savePut(Long id, Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> delete(Long id) {
        String url = "/documentApproveCostEstimates/"+id;
        return deleteSend(HttpMethod.DELETE, url);
    }

    @Override
    public ResponseEntity<String> get(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> load() {
        return null;
    }

    @Override
    public ResponseEntity<String> findByDocumentApprove(String documentApprove) {
        String method = "findByDocumentApproveEquals";
        String url = "/documentApproveCostEstimates/search/"+method+"?documentApprove="+documentApprove;
        return getResultByExchange(url);
    }

    @Override
    public ResponseEntity<String> saveDocumentCostEstimate(Map<String,String[]> jsonDocument) {
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{
            Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("document",jsonDocument,mapRelation);

            String url = "/documentCostEstimateCustom/?"+ HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            List<String> stringList = new ArrayList<String>();
            stringList.add("documentApprove");

            reponseEntity = postWithJsonRelation(jsonDocument,stringList, HttpMethod.POST,url.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }

        return  reponseEntity;
    }

    @Override
    public ResponseEntity<String> updateDocumentCostEstimate(Map<String,String[]> jsonDocument) {
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{
            Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("document",jsonDocument,mapRelation);

            String url = "/documentCostEstimateCustom/updateDocumentCostEstimate?"+ HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            List<String> stringList = new ArrayList<String>();
            stringList.add("documentApprove");

            reponseEntity = postWithJsonRelation(jsonDocument,stringList, HttpMethod.POST,url.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }

        return  reponseEntity;
    }

    public ResponseEntity<String> deleteCostEstimate(Long id) {
        String url = "/documentApproveCostEstimates/"+id;
        return deleteSend(HttpMethod.DELETE, url);
    }
}
