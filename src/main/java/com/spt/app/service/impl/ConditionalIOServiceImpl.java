package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.ConditionalIOService;
import com.spt.app.util.HttpRequestParamConvertUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;

@Service("ConditionalIOService")
public class ConditionalIOServiceImpl extends AbstractEngineService implements BaseCommonService,ConditionalIOService {

	static final Logger LOGGER = LoggerFactory.getLogger(ConditionalIOServiceImpl.class);
	
	private String urlBase = "/conditionalIos";

	@Override
	public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Content-Type", "application/json; charset=utf-8");

		String queryString = "";
		if(objectMap.get("queryString") != null){
			queryString = String.valueOf(objectMap.get("queryString"));
			try {
				queryString = URLDecoder.decode(queryString, "UTF-8");
			} catch (UnsupportedEncodingException e) { e.printStackTrace(); }
		}

		HttpEntity<String> entity = new HttpEntity<String>("", headers);
		String method = "findByCompany";
		String url = "/"+urlBase+"/search/"+method+"?sort=createdDate,desc&"+queryString;
		return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> findSize(Map<String, Object> objectMap) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Content-Type", "application/json; charset=utf-8");

		String queryString = "";
		if(objectMap.get("queryString") != null){
			queryString = String.valueOf(objectMap.get("queryString"));
			try {
				queryString = URLDecoder.decode(queryString, "UTF-8");
			} catch (UnsupportedEncodingException e) { e.printStackTrace(); }
			//queryString = queryString.replaceAll("%2C", ",");
		}
		HttpEntity<String> entity = new HttpEntity<String>("", headers);
		String method = "findByCompany";
		String url = "/"+urlBase+"/search/"+method+"?page=0&size=1&"+queryString;

		LOGGER.info("url="+url);
		return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> save(Map parameter) {

		String url = "/"+urlBase;
		return postWithJson(parameter, HttpMethod.POST, url);
	}

	@Override
	public ResponseEntity<String> delete(Long id) {

		String url = "/"+urlBase+"/"+id;
		return deleteSend(HttpMethod.DELETE, url);
	}

	@Override
	public ResponseEntity<String> get(Long id) {

		String url = "/"+urlBase+"/"+id;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> load() {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/"+urlBase;
        
        LOGGER.info("url="+url);
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> savePut(Long id, Map parameter) {

		String url = "/"+urlBase+"/"+id;
		return putWithJson(parameter, HttpMethod.PUT, url);
	}


	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {

		super.restTemplate = restTemplate;
	}

	@Override
	public List<Map> findByCompany(Long company) {
		String url = "/"+urlBase+"/search/findByCompany?company="+company;
		String jsonConditionalIO = getResultByExchange(url).getBody();
		Map ConditionalIOMap = gson.fromJson(jsonConditionalIO, Map.class);
		List<Map> contentsConditionalIO =  (List<Map>) ConditionalIOMap.get("content");
		return contentsConditionalIO;	}



	public ResponseEntity<String> saveConditionalIO(Map<String, String[]> jsonConditionalIO) {
		LOGGER.info("-= SERVICE SAVE ROUTE DISTANCE =-");
		LOGGER.info("><><>< jsonConditionalIO : {} ",jsonConditionalIO);
		ResponseEntity<String> reponseEntity = null;
		try{
			Map<String, String> mapRelation = new HashMap<String, String>();
			applyRelateEntity("company",jsonConditionalIO,mapRelation);

			String url = "/conditionalIOCustom/?"+HttpRequestParamConvertUtil.toUrlParam(mapRelation);
			List<String> stringList = new ArrayList<String>();
			stringList.add("conditionalIO");
			reponseEntity = postWithJsonRelation(jsonConditionalIO,stringList,HttpMethod.POST,url.toString());
		}catch (Exception e) {

			e.printStackTrace();
		}
		return  reponseEntity;
	}

	@Override
	public ResponseEntity<String> findConditionalIOByGlAndIoAndCompany(String gl, String io,Long company) {

		String method = "findByGlAndIoAndCompany";
		String url = "/"+urlBase+"/search/"+method+"?gl="+gl+"&io="+io+"&company="+company;
		return getResultByExchange(url);
	}



	@Override
	public ResponseEntity<String> findByCompanyCode(String code) {

		String method = "findByCompanyCode?code="+code;
		String url = "/"+urlBase+"/search/"+method;
		return getResultByExchange(url);


	}



}
