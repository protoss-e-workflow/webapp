package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.DashBoardIncomingExpenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Map;

@Service("DashBoardIncomingExpenseService")
public class DashBoardIncomingExpenseServiceImpl extends AbstractEngineService implements BaseCommonService,DashBoardIncomingExpenseService {

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        super.restTemplate = restTemplate;
    }

    @Override
    @SuppressWarnings("Duplicates")
    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
            try {
                queryString = URLDecoder.decode(queryString, "UTF-8");
            } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
            //queryString = queryString.replaceAll("%2C", ",");
        }

        LOGGER.info("query string : {}",queryString);

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByNextApproverInAndDocument_RequesterIgnoreCaseContainingAndDocument_DocNumberIgnoreCaseContainingAndDocument_DocumentStatusAndDocument_DocumentTypeInOrderByDocument_SendDateDesc";
        String url = "/requests/search/"+method+"?"+queryString + "&projection=haveDoc";
        LOGGER.info("Url to find My Request ====> "+url);
        try{
            ResponseEntity<String> entity1 =  getResultString(url,entity);

            return  entity1;
        }catch (Exception e){
            LOGGER.info("{}",e.getMessage());
            return new ResponseEntity<String>("",headers, HttpStatus.OK);
        }
    }

    @Override
    @SuppressWarnings("Duplicates")
    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
            try {
                queryString = URLDecoder.decode(queryString, "UTF-8");
            } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
            //queryString = queryString.replaceAll("%2C", ",");
        }

        LOGGER.info("query string : {}",queryString);

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByNextApproverInAndDocument_RequesterIgnoreCaseContainingAndDocument_DocNumberIgnoreCaseContainingAndDocument_DocumentStatusAndDocument_DocumentTypeInOrderByDocument_SendDateDesc";
        String url = "/requests/search/"+method+"?page=0&size=1&"+queryString;

        LOGGER.info("url="+url);
        try{
            ResponseEntity<String> entity1 =  getResultString(url,entity);

            return  entity1;
        }catch (Exception e){
            LOGGER.info("{}",e.getMessage());
            return new ResponseEntity<String>("",headers, HttpStatus.OK);
        }
    }

    @Override
    public ResponseEntity<String> save(Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> savePut(Long id, Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> delete(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> get(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> load() {
        return null;
    }

    @Override
    public ResponseEntity<String> findByCriteriaWithCompanyCode(Map<String, Object> objectMap) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
            try {
                queryString = URLDecoder.decode(queryString, "UTF-8");
            } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
            //queryString = queryString.replaceAll("%2C", ",");
        }

        LOGGER.info("query string : {}",queryString);

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByNextApproverInAndDocument_RequesterIgnoreCaseContainingAndDocument_DocNumberIgnoreCaseContainingAndDocument_DocumentStatusAndDocument_DocumentTypeInAndDocument_CompanyCodeOrderByDocument_SendDateDesc";
        String url = "/requests/search/"+method+"?"+queryString + "&projection=haveDoc";
        LOGGER.info("Url to find My Request ====> "+url);
        try{
            ResponseEntity<String> entity1 =  getResultString(url,entity);

            return  entity1;
        }catch (Exception e){
            LOGGER.info("{}",e.getMessage());
            return new ResponseEntity<String>("",headers, HttpStatus.OK);
        }
    }

    @Override
    public ResponseEntity<String> findSizeWithCompanyCode(Map<String, Object> objectMap) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
            try {
                queryString = URLDecoder.decode(queryString, "UTF-8");
            } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
            //queryString = queryString.replaceAll("%2C", ",");
        }

        LOGGER.info("query string : {}",queryString);

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByNextApproverInAndDocument_RequesterIgnoreCaseContainingAndDocument_DocNumberIgnoreCaseContainingAndDocument_DocumentStatusAndDocument_DocumentTypeInAndDocument_CompanyCodeOrderByDocument_SendDateDesc";
        String url = "/requests/search/"+method+"?page=0&size=1&"+queryString;

        LOGGER.info("url="+url);
        try{
            ResponseEntity<String> entity1 =  getResultString(url,entity);

            return  entity1;
        }catch (Exception e){
            LOGGER.info("{}",e.getMessage());
            return new ResponseEntity<String>("",headers, HttpStatus.OK);
        }
    }
}
