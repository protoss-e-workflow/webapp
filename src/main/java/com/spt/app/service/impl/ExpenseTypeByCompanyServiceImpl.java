package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.ExpenseTypeByCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Map;

@Service("ExpenseTypeByCompanyService")
public class ExpenseTypeByCompanyServiceImpl extends AbstractEngineService implements BaseCommonService,ExpenseTypeByCompanyService {

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {

        super.restTemplate = restTemplate;
    }

    public ResponseEntity<String> findByCode(String code) {
        return null;
    }



    private String urlBase = "expenseTypeByCompanies";

    @Override
    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findExpenseTypeByCompanyByExpenseTypeId";
        String url = "/"+urlBase+"/search/"+method+"?sort=id,desc&"+queryString;

        LOGGER.info("================ URL Find By Criteria Expense Reference ==================== [{}]",url);
        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findExpenseTypeByCompanyByExpenseTypeId";
        String url = "/"+urlBase+"/search/"+method+"?page=0&size=1&"+queryString;

        LOGGER.info("================ URL Find Size Expense Reference==================== [{}]",url);

        return getResultString(url,entity);
    }


    @Override
    public ResponseEntity<String> save(Map parameter) {
        String url = "/"+urlBase;
        return postWithJson(parameter, HttpMethod.POST,url);
    }

    @Override
    public ResponseEntity<String> savePut(Long id, Map parameter) {
        String url = "/"+urlBase+"/"+id;
        return putWithJson(parameter, HttpMethod.PUT, url);
    }

    @Override
    public ResponseEntity<String> delete(Long id) {
        String url = "/"+urlBase+"/"+id;
        return deleteSend(HttpMethod.DELETE, url);
    }


    @Override
    public ResponseEntity<String> get(Long id) {
        String url = "/"+urlBase+"/"+id;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> load() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/"+urlBase;

        LOGGER.info("url="+url);
        return getResultString(url,entity);
    }


    public ResponseEntity<String> findByCompanyCode(String companyCode) {
        String url = "/expenseTypeByCompanies/search/findByCompanyCode?companyCode="+companyCode;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> findExpenseTypeByCompanyByPaAndPsaAndKeySearch(String pa,String psa,String headOfficeGL,String factoryGL,String userName,String keySearch) {
        String url = "/expenseTypeCompany/findExpenseTypeByCompanyByPaAndPsaAndKeySearchAndHeadOfficeGLAndFactoryGLAndRole?pa="+pa+"&psa="+psa+"&headOfficeGL="+headOfficeGL+"&factoryGL="+factoryGL+"&userName="+userName+"&keySearch="+keySearch;
       LOGGER.info("URL FOR SEARCH EXPENSE====================> {}",url);
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> findExpenseTypeByCompanyByPaAndPsaAndFavorite(String pa,String psa,String userName) {
        String url = "/expenseTypeCompany/findExpenseTypeByCompanyByPaAndPsaAndFavoriteAndRole?pa="+pa+"&psa="+psa+"&userName="+userName;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }
}
