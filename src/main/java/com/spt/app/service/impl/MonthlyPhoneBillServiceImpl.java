package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.EmplyeeProfileService;
import com.spt.app.service.MonthlyPhoneBillService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service("MonthlyPhoneBillService")
public class MonthlyPhoneBillServiceImpl extends AbstractEngineService implements BaseCommonService,MonthlyPhoneBillService {

	static final Logger LOGGER = LoggerFactory.getLogger(MonthlyPhoneBillServiceImpl.class);
	
	private String urlBase = "monthlyPhoneBills";

	@Autowired
	EmplyeeProfileService emplyeeProfileService;
	
	@Override
	public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Content-Type", "application/json; charset=utf-8");

		String queryString = "";
		if(objectMap.get("queryString") != null){
			queryString = String.valueOf(objectMap.get("queryString"));
			try {
				queryString = URLDecoder.decode(queryString, "UTF-8");
			} catch (UnsupportedEncodingException e) { e.printStackTrace(); }
		}

		HttpEntity<String> entity = new HttpEntity<String>("", headers);
		String method = "findByEmpCodeIgnoreCaseContaining";
		String url = "/"+urlBase+"/search/"+method+"?sort=createdDate,desc&"+queryString;
		ResponseEntity<String> responseEntity = getResultString(url,entity);

		Map monthlyPhoneBillMap = gson.fromJson(responseEntity.getBody(), Map.class);
		List<Map<String,String>> contentsMonthlyPhoneBillMap =  (List<Map<String,String>>) monthlyPhoneBillMap.get("content");
		for(Map<String,String> map: contentsMonthlyPhoneBillMap){
			ResponseEntity<String> result = emplyeeProfileService.findEmployeeProfileByEmployeeCode(map.get("empUserName"));
			Map<String,String> userMap = gson.fromJson(result.getBody(), Map.class);
			map.put("empName",userMap.get("FOA")+userMap.get("FNameTH")+" "+userMap.get("LNameTH"));
		}
		monthlyPhoneBillMap.put("content",contentsMonthlyPhoneBillMap);
		String jsonStrem = gson.toJson(monthlyPhoneBillMap, Map.class);
		return ResponseEntity.ok().body(jsonStrem);
	}

	@Override
	public ResponseEntity<String> findSize(Map<String, Object> objectMap) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Content-Type", "application/json; charset=utf-8");

		String queryString = "";
		if(objectMap.get("queryString") != null){
			queryString = String.valueOf(objectMap.get("queryString"));
			try {
				queryString = URLDecoder.decode(queryString, "UTF-8");
			} catch (UnsupportedEncodingException e) { e.printStackTrace(); }
			//queryString = queryString.replaceAll("%2C", ",");
		}
		HttpEntity<String> entity = new HttpEntity<String>("", headers);
		String method = "findByEmpCodeIgnoreCaseContaining";
		String url = "/"+urlBase+"/search/"+method+"?page=0&size=1&"+queryString;

		LOGGER.info("url="+url);
		return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> save(Map parameter) {

		String url = "/"+urlBase;
		return postWithJson(parameter, HttpMethod.POST, url);
	}

	@Override
	public ResponseEntity<String> delete(Long id) {

		String url = "/"+urlBase+"/"+id;
		return deleteSend(HttpMethod.DELETE, url);
	}

	@Override
	public ResponseEntity<String> get(Long id) {

		String url = "/"+urlBase+"/"+id;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> load() {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/"+urlBase;
        
        LOGGER.info("url="+url);
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> savePut(Long id, Map parameter) {

		String url = "/"+urlBase+"/"+id;
		return putWithJson(parameter, HttpMethod.PUT, url);
	}


	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {

		super.restTemplate = restTemplate;
	}

	@Override
	public ResponseEntity<String> findByEmpCode(String empCode) {

		String method = "findByEmpCode";
		String url = "/"+urlBase+"/search/"+method+"?empCode="+empCode;
		return getResultByExchange(url);
	}

	@Override
	public ResponseEntity<String> findByEmpUserName(String empUserName) {

		String method = "findByEmpUserName";
		String url = "/"+urlBase+"/search/"+method+"?empUserName="+empUserName;
		return getResultByExchange(url);
	}


	@Override
	public List<Map> findByEmpCodeIgnoreCaseContaining(String empCode) {

		String url = "/"+urlBase+"/search/findByEmpCodeIgnoreCaseContaining?empCode="+empCode;
		String jsonMonthlyPhoneBill = getResultByExchange(url).getBody();
		Map MonthlyPhoneBillMap = gson.fromJson(jsonMonthlyPhoneBill, Map.class);
		List<Map> contentsMonthlyPhoneBill =  (List<Map>) MonthlyPhoneBillMap.get("content");
		return contentsMonthlyPhoneBill;
	}


}
