package com.spt.app.service.impl;

import com.spt.app.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service("ExpenseTypeFileService")
public class ExpenseTypeFileServiceImpl extends AbstractEngineService implements BaseCommonService,BaseParentService,BaseCodeService,ExpenseTypeFileService {

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {

        super.restTemplate = restTemplate;
    }

    public ResponseEntity<String> findByCode(String code) {
        return null;
    }



    private String urlBase = "expenseTypeFiles";

    @Override
    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findExpenseTypeFileByExpenseTypeId";
        String url = "/"+urlBase+"/search/"+method+"?sort=id,desc&"+queryString;

        LOGGER.info("================ URL Find By Criteria Expense Reference ==================== [{}]",url);
        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findExpenseTypeFileByExpenseTypeId";
        String url = "/"+urlBase+"/search/"+method+"?page=0&size=1&"+queryString;

        LOGGER.info("================ URL Find Size Expense Reference==================== [{}]",url);

        return getResultString(url,entity);
    }


    @Override
    public ResponseEntity<String> save(Map parameter) {
        String url = "/"+urlBase;
        return postWithJson(parameter, HttpMethod.POST,url);
    }

    @Override
    public ResponseEntity<String> savePut(Long id, Map parameter) {
        String url = "/"+urlBase+"/"+id;
        return putWithJson(parameter, HttpMethod.PUT, url);
    }

    @Override
    public ResponseEntity<String> delete(Long id) {
        String url = "/"+urlBase+"/"+id;
        return deleteSend(HttpMethod.DELETE, url);
    }


    @Override
    public ResponseEntity<String> get(Long id) {
        String url = "/"+urlBase+"/"+id;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> load() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/"+urlBase;

        LOGGER.info("url="+url);
        return getResultString(url,entity);
    }


    @Override
    public ResponseEntity<String> findByAttachmentTypeCode(String code) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByAttachmentTypeCode";
        String url = "/"+urlBase+"/search/"+method+"?code="+code;
        return getResultString(url,entity);
    }


    @Override
    public ResponseEntity<String> putParent(Long id, List<String> associateIdLs) {

        String url = "/"+urlBase+"/"+id;
        String associatePath = "/expenseType";
        return putParent(associateIdLs,url,associatePath);
    }


}
