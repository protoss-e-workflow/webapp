package com.spt.app.service.impl;

import com.google.gson.*;
import com.spt.app.constant.ApplicationConstant;
import com.spt.app.service.*;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.spt.app.util.HttpRequestParamConvertUtil;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.*;

@Service("DocumentAccessService")
public class DocumentAccessServiceImpl extends AbstractEngineService implements DocumentAccessService {

    @Autowired
    FlowManageService flowManageService;


	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {

		super.restTemplate = restTemplate;
	}

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();
    
    public ResponseEntity<String> findByCode(String code) {
        return null;
    }

	@Override
	public Set<String> getDocumentAccessPerson(String docId) {
		// TODO Auto-generated method stub
		String result = getResultString("/documentCustom/getDocumentAccessPerson?doc="+docId);
		return gson.fromJson(result, Set.class);
	}

    

}
