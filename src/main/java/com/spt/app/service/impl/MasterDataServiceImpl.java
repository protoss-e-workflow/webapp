package com.spt.app.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.spt.app.controller.general.SecurityController;
import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCodeService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.BaseParentService;
import com.spt.app.service.MasterDataService;
import com.spt.app.service.RoleService;

@Service("MasterDataService")
public class MasterDataServiceImpl extends AbstractEngineService implements BaseCommonService,BaseCodeService,BaseParentService,MasterDataService{

	static final Logger LOGGER = LoggerFactory.getLogger(MasterDataServiceImpl.class);
	
	private String urlBase = "masterDataDetails";
	
	@Override
	public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        }
        
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByCodeIgnoreCaseContainingAndNameIgnoreCaseContainingAndOrgCodeIgnoreCaseContainingAndMasterdataIn";
        String url = "/"+urlBase+"/search/"+method+"?sort=id,desc&"+queryString;
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> findSize(Map<String, Object> objectMap) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        }
        
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByCodeIgnoreCaseContainingAndNameIgnoreCaseContainingAndOrgCodeIgnoreCaseContainingAndMasterdataIn";
        String url = "/"+urlBase+"/search/"+method+"?page=0&size=1&"+queryString;
        
        LOGGER.info("url="+url);
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> save(Map parameter) {

		String url = "/"+urlBase;
		return postWithJson(parameter, HttpMethod.POST, url);
	}

	@Override
	public ResponseEntity<String> delete(Long id) {

		String url = "/"+urlBase+"/"+id;
		return deleteSend(HttpMethod.DELETE, url);
	}

	@Override
	public ResponseEntity<String> get(Long id) {

		String url = "/"+urlBase+"/"+id;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> load() {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/"+urlBase;
        
        LOGGER.info("url="+url);
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> savePut(Long id,Map parameter) {

		String url = "/"+urlBase+"/"+id;
		return putWithJson(parameter, HttpMethod.PUT, url);
	}


	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {

		super.restTemplate = restTemplate;
	}

	@Override
	public ResponseEntity<String> findByCode(String code) {

		String method = "findByCode";
	    String url = "/masterDatas/search/"+method+"?code="+code;
		return getResultByExchange(url);
	}

	@Override
	public ResponseEntity<String> findByMasterDataByCodeByOrgCode(Long id,String code,String orgCode) {

		String method = "findByCodeIgnoreCaseContainingAndNameIgnoreCaseContainingAndOrgCodeIgnoreCaseContainingAndMasterdataIn";
	    String url = "/"+urlBase+"/search/"+method+"?sort=id,desc&name=&code="+code+"&masterdata="+id+"&orgCode="+orgCode;
		return getResultByExchange(url);
	}


	@Override
	public ResponseEntity<String> putParent(Long id, List<String> associateIdLs) {

		String url = "/"+urlBase+"/"+id;
		String associatePath = "/masterdata";
		return putParent(associateIdLs,url,associatePath);
	}

	@Override
	public List<Map> findByMasterCodeIn(String code) {


		String url = "/"+urlBase+"/search/findByMasterdataIn?masterdata="+code;
		String jsonMasterData = getResultByExchange(url).getBody();
		Map masterDataMap = gson.fromJson(jsonMasterData, Map.class);
		List<Map> contentsMasterData =  (List<Map>) masterDataMap.get("content");
		return contentsMasterData;
	}


	@Override
	public ResponseEntity<String> findMasterDataDetailByMasterdataCode() {

		String method = "findMasterDataDetailByMasterdataCode";
		String url = "/"+urlBase+"/search/"+method;
		return getResultByExchange(url);
	}


	@Override
	public ResponseEntity<String> findSequenceNumberApproveTypeInMasterDataDetail(String detailCode) {

		String method = "findSequenceNumberApproveTypeInMasterDataDetail";
		String url = "/"+urlBase+"/search/"+method+"?detailCode="+detailCode;
		return getResultByExchange(url);
	}

	public ResponseEntity<String> findMasterDataDetailApproveTypeByCode(String code) {
		String method = "findMasterDataDetailApproveTypeByCode";
		String url = "/"+urlBase+"/search/"+method+"?code="+code;
		return getResultByExchange(url);
	}

	public ResponseEntity<String> findMasterDataDetailOfAttachmentTypeAll() {

		String method = "findMasterDataDetailOfAttachmentTypeAll";
		String url = "/"+urlBase+"/search/"+method;
		return getResultByExchange(url);
	}

	public ResponseEntity<String> findMasterDataDetailAttachmentTypeByCode(String code) {
		String method = "findMasterDataDetailAttachmentTypeByCode";
		String url = "/"+urlBase+"/search/"+method+"?code="+code;
		return getResultByExchange(url);
	}

	@Override
	public ResponseEntity<String> findByMasterdataInOrderByCode(String masterdata) {
		String method = "findByMasterdataInOrderByCode";
		String url = "/"+urlBase+"/search/"+method+"?masterdata="+masterdata;
		return getResultByExchange(url);
	}

	@Override
	public ResponseEntity<String> findByMasterdataInAndMasterDataDetailCodeOrderByCode(String masterdata, String code) {
		String method = "findByMasterdataInAndMasterDataDetailCodeOrderByCode";
		String url = "/"+urlBase+"/search/"+method+"?masterdata="+masterdata+"&code="+code;
		return getResultByExchange(url);
	}

	@Override
	public ResponseEntity<String> findByMasterdataInAndMasterDataDetailCodeOrderByCodeList(String masterdata, String code) {
		String method = "findByMasterdataInAndMasterDataDetailCodeOrderByCodeList";
		String url = "/"+urlBase+"/search/"+method+"?masterdata="+masterdata+"&code="+code;
		return getResultByExchange(url);
	}

	@Override
	public ResponseEntity<String> findByMasterdataInAndMasterDataDetailCodeLikeOrderByCode(String masterdata, String code) {
		String method = "findByMasterdataInAndMasterDataDetailCodeLikeOrderByCode";
		String url = "/" + urlBase + "/search/" + method + "?masterdata=" + masterdata + "&code=" + code;
		return getResultByExchange(url);

	}
	public List<Map> findAutoCompleteHotelByKeySearch(String code) {

		List<Map> listHotel =  findByMasterCodeIn(code);
		LOGGER.info("><><><  listHotel : {} ",listHotel.size());
		return listHotel;
	}

	@Override
	public ResponseEntity<String> findByVariable1(String userHead) {
		String method = "findByVariable1";
		String url = "/" + urlBase + "/search/" + method + "?userHead=" + userHead;
		return getResultByExchange(url);
	}
}
