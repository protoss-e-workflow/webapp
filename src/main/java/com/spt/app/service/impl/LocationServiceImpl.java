package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.LocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service("LocationService")
public class LocationServiceImpl extends AbstractEngineService implements BaseCommonService,LocationService {

	static final Logger LOGGER = LoggerFactory.getLogger(LocationServiceImpl.class);

	private String urlBase = "locations";

	@Override
	public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Content-Type", "application/json; charset=utf-8");

		String queryString = "";
		if(objectMap.get("queryString") != null){
			queryString = String.valueOf(objectMap.get("queryString"));
			try {
				queryString = URLDecoder.decode(queryString, "UTF-8");
			} catch (UnsupportedEncodingException e) { e.printStackTrace(); }
			//queryString = queryString.replaceAll("%2C", ",");
		}

		HttpEntity<String> entity = new HttpEntity<String>("", headers);
		String method = "findByCodeIgnoreCaseContainingAndDescriptionIgnoreCaseContainingAndLocationType";
		String url = "/"+urlBase+"/search/"+method+"?sort=createdDate,updateBy,code,desc&"+queryString;
		return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> findSize(Map<String, Object> objectMap) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Content-Type", "application/json; charset=utf-8");

		String queryString = "";
		if(objectMap.get("queryString") != null){
			queryString = String.valueOf(objectMap.get("queryString"));
			try {
				queryString = URLDecoder.decode(queryString, "UTF-8");
			} catch (UnsupportedEncodingException e) { e.printStackTrace(); }
			//queryString = queryString.replaceAll("%2C", ",");
		}
		HttpEntity<String> entity = new HttpEntity<String>("", headers);
		String method = "findByCodeIgnoreCaseContainingAndDescriptionIgnoreCaseContainingAndLocationType";
		String url = "/"+urlBase+"/search/"+method+"?page=0&size=1&"+queryString;

		LOGGER.info("url="+url);
		return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> save(Map parameter) {

		String url = "/"+urlBase;
		return postWithJson(parameter, HttpMethod.POST, url);
	}

	@Override
	public ResponseEntity<String> delete(Long id) {

		String url = "/"+urlBase+"/"+id;
		return deleteSend(HttpMethod.DELETE, url);
	}

	@Override
	public ResponseEntity<String> get(Long id) {

		String url = "/"+urlBase+"/"+id;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> load() {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Content-Type", "application/json; charset=utf-8");

		HttpEntity<String> entity = new HttpEntity<String>("", headers);
		String url = "/"+urlBase;

		LOGGER.info("url="+url);
		return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> savePut(Long id, Map parameter) {

		String url = "/"+urlBase+"/"+id;
		return putWithJson(parameter, HttpMethod.PUT, url);
	}


	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {

		super.restTemplate = restTemplate;
	}

	@Override
	public ResponseEntity<String> findByCode(String code) {

		String method = "findByCode";
		String url = "/"+urlBase+"/search/"+method+"?code="+code;
		return getResultByExchange(url);
	}



	@Override
	public List<Map>  findByLocationType(String locationType) {

		String method = "findByLocationType";
		String url = "/"+urlBase+"/search/"+method+"?locationType="+locationType;
		String jsonLocation = getResultByExchange(url).getBody();
		Map locationMap = gson.fromJson(jsonLocation, Map.class);
		List<Map> contentsLocation =  (List<Map>) locationMap.get("content");
		return contentsLocation;
	}




	@Override
	public ResponseEntity<String> findByCodeAndLocationType(String code, String locationType) {

		String method = "findByCodeAndLocationType";
		String url = "/"+urlBase+"/search/"+method+"?code="+code+"&locationType="+locationType;
		return getResultByExchange(url);
	}

	@Override
	public ResponseEntity<String> findByLocationTypeAndDescription(String locationType, String description) {
		String method = "findByLocationTypeAndDescription";
		String url = "/"+urlBase+"/search/"+method+"?locationType="+locationType+"&description="+description;
		return getResultByExchange(url);
	}

	@Override
	public List<Map> findByCodeIgnoreCaseContainingAndDescriptionIgnoreCaseContainingAndLocationType(String code,String description,String locationType) {

		String url = "/"+urlBase+"/search/findByCodeIgnoreCaseContainingAndDescriptionIgnoreCaseContainingAndLocationType?code="+code+"&description="+description+"&locationType="+locationType;
		String jsonLocation = getResultByExchange(url).getBody();
		Map locationMap = gson.fromJson(jsonLocation, Map.class);
		List<Map> contentsLocation =  (List<Map>) locationMap.get("content");
		return contentsLocation;
	}
}
