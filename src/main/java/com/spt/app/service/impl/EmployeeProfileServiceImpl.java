package com.spt.app.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.spt.app.service.*;
import jdk.nashorn.internal.parser.JSONParser;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.common.base.Splitter;
import com.spt.app.controller.general.SecurityController;
import com.spt.app.spring.security.CustomUserModel;

import com.spt.app.util.StringUtil;

@Service("EmplyeeProfileService")
public class EmployeeProfileServiceImpl extends AbstractOrgSysEngineService implements EmplyeeProfileService{

	static final Logger LOGGER = LoggerFactory.getLogger(EmployeeProfileServiceImpl.class);
	

	@Autowired
	CustomUserModel customUserModel;
	
	@Autowired
	EmployeeReplacementService emplyeeReplacementService;

	@Autowired
	MasterDataService masterDataService;

	@Override
	public ResponseEntity<String> findEmployeeProfileByEmployeeCode(String employeeCode) {

		String url = "/profile/"+employeeCode;
		ResponseEntity<String> restResult = getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
        Map userMap = gson.fromJson(restResult.getBody(), Map.class);
		
		String dataProfile = String.valueOf(userMap.get("profile")).replaceAll("Corp.,", "Corp").replaceAll("Co.,", "Co.")
				.replaceAll("Safety,H","Safety H").replaceAll("Safety , H","Safety H");
		dataProfile = StringUtil.trimStringByString(dataProfile,"{","}");

		
		Map<String, String> userProfileMap = Splitter.on(", ").withKeyValueSeparator("=").split(dataProfile);

		MediaType mediaType = new MediaType("application","json", Charset.forName("UTF-8"));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(mediaType);


		
		return new ResponseEntity(gson.toJson(userProfileMap), headers, HttpStatus.OK);
	}

	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {

		super.restTemplate = restTemplate;
	}

	@Override
	public ResponseEntity<String> findEmployeeProfileByKeySearchUserIn(String keySearch,String listEmplyee) {

		String url = "/profile/searchOther/"+keySearch+"?userIn="+listEmplyee;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> findEmployeeProfileByKeySearch(String keySearch) {

		String url = "/profile/search/"+keySearch;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> findAutoCompleteEmployeeByKeySearch(String keySearch) {

		ResponseEntity<String> resultDataList =  findEmployeeProfileByKeySearch(keySearch);
		return getDataforAutoComplete(resultDataList) ;
	}

	@Override
	public ResponseEntity<String> findAutoCompleteEmployeeByKeySearchUserIn(String keySearch,String userName) {
		ResponseEntity<String> result = emplyeeReplacementService.findByEmpUserNameAgree(userName);
		Map userMap = gson.fromJson(result.getBody(), Map.class);
		List<Map<String,String>> restBodyList = (List<Map<String, String>>) userMap.get("content");
		StringBuffer sb = new StringBuffer();
		boolean checkPercentEmpUserNameReplace = false;
		sb.append(","+userName);
		for(Map<String,String> employeeReplacemant:restBodyList){
			sb.append(","+employeeReplacemant.get("empUserNameReplace"));
			if("%".equals(employeeReplacemant.get("empUserNameReplace"))){
				checkPercentEmpUserNameReplace = true;
			}
		}
		if("null".equals(String.valueOf(keySearch))){
			keySearch = "%";
		}
		String listEmplyee = sb.substring(1).toString();

		ResponseEntity<String> resultDataList = null;

		if(checkPercentEmpUserNameReplace){
			resultDataList = findEmployeeProfileByKeySearch(keySearch);
		}else{
			resultDataList = findEmployeeProfileByKeySearchUserIn(keySearch,listEmplyee) ;
		}

		return getDataforAutoComplete(resultDataList) ;
	}

	@Override
	public ResponseEntity<String> findEmployeeProfileByCostCenter(String costCenter) {
		String url = "/profile/maxLineByCostWithCom/"+costCenter;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> findMaxLineByCost(String costCenter) {
		String url = "/profile/maxLineByCost/"+costCenter;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> findMaxLineByCostWithCom(String costCenter) {
		String url = "/profile/maxLineByCostWithCom/"+costCenter;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> getApproveLine(String userName) {



		String url = "/profile/getApproveLine/"+userName;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);




	}

	@Override
	public ResponseEntity<String> findByPa(String pa) {
		String url = "/profile/pa/"+pa;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> findByPsa(String psa) {
		String url = "/profile/psa/"+psa;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> findAdminByPapsaAndRequester(String papsa, String requester) {
		String url = "/profile/admin/"+papsa+"/"+requester;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> findAccountByPapsaAndRequester(String papsa, String requester) {
		String url = "/profile/account/"+papsa+"/"+requester;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> getBankAccount(String requester) {
		String url = "/profile/getBankAccount/"+requester;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}


	private ResponseEntity<String> getDataforAutoComplete(ResponseEntity<String> resultDataList){
		Map userMap = gson.fromJson(resultDataList.getBody(), Map.class);
		List<Map<String,String>> restBodyList = (List<Map<String, String>>) userMap.get("restBodyList");

		Locale currentLocale = LocaleContextHolder.getLocale();
		
		List<Map<String,String>> userData = new ArrayList();
		Map<String,String> userModel  = null;
		 if(restBodyList !=null) for(Map<String,String> restBody:restBodyList){

			 userModel = new HashMap<String,String>();
			 userModel.put("employeeCode", String.valueOf(restBody.get("EmployeeId")));
			 userModel.put("companyCode", String.valueOf(restBody.get("Position_PA_ID")));
			 userModel.put("psa", String.valueOf(restBody.get("Personal_PSA_ID")));
			 userModel.put("deptCode", String.valueOf(restBody.get("Org_ID_800")));
			 userModel.put("costCenter", String.valueOf(restBody.get("Personal_Cost_center")));
			 userModel.put("userName", String.valueOf(restBody.get("User_name")).toLowerCase());
			 userModel.put("personalId", String.valueOf(restBody.get("Personal_ID")).toLowerCase());
			 userModel.put("empLv", String.valueOf(restBody.get("EESG_ID")).toLowerCase());
			 userModel.put("psaName", String.valueOf(restBody.get("Personal_PSA_Name")).toLowerCase());

			 try{
				 ResponseEntity<String> specialCostCenter = masterDataService.findByMasterdataInAndMasterDataDetailCodeOrderByCode("M025",restBody.get("User_name").toLowerCase());
				 Map specialCostCenterMap = gson.fromJson(specialCostCenter.getBody(),Map.class);
				 if(specialCostCenterMap!=null){
					 LOGGER.info("specialCostCenter    :   {}",specialCostCenterMap.get("variable1"));
					 userModel.put("costCenter", String.valueOf(specialCostCenterMap.get("variable1")));
				 }
			 }catch (Exception e){//Error
			 }

			 LOGGER.info("currentLocale.getLanguage() : {}",currentLocale.getLanguage());
			 if(Locale.US.equals(currentLocale) || currentLocale.getLanguage().equals("en") || currentLocale.getLanguage().equals("en_US")){
				   userModel.put("name", String.valueOf(restBody.get("NameEN")));
				   userModel.put("position", String.valueOf(restBody.get("PositionEN")));
				   userModel.put("companyName", String.valueOf(restBody.get("Org_Name_EN_100")));
				   userModel.put("deptName", String.valueOf(restBody.get("Org_Name_EN_800")));
				}else{
					userModel.put("name", String.valueOf(restBody.get("FOA")) + String.valueOf(restBody.get("FNameTH")) + " " + String.valueOf(restBody.get("LNameTH")));
					userModel.put("position", String.valueOf(restBody.get("PositionTH")));
					userModel.put("companyName", String.valueOf(restBody.get("Position_PA_Name")));
					userModel.put("deptName", String.valueOf(restBody.get("Org_Name_TH_800")));
				}
			 
			 userData.add(userModel);
		 }
		 
//		 LOGGER.info("==============================json={}",userData);
		 return ResponseEntity
	                .ok()
	                .body(gson.toJson(userData, List.class));
	}

	@Override
	public String findAdminByPapsaAndCca(String papsa, String cca) {
		String url = "/profile/admin/"+papsa+"/"+cca+"/xxx";
		ResponseEntity<String> restResult =  getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
		Map userMap = gson.fromJson(restResult.getBody(), Map.class);
		String dataProfile = String.valueOf(userMap.get("profile"));
		return dataProfile;
	}

	@Override
	public String findAccountByPapsaAndCca(String papsa, String cca) {
		String url = "/profile/account/"+papsa+"/"+cca+"/xxx";
		ResponseEntity<String> restResult =   getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
		Map userMap = gson.fromJson(restResult.getBody(), Map.class);
		String dataProfile = String.valueOf(userMap.get("profile"));
		return dataProfile;
	}

	@Override
	public String findHrByPapsaAndCca(String papsa, String cca,String employeeStr) {
		String url = "/profile/hr/"+papsa+"/"+cca+"/"+employeeStr;
		ResponseEntity<String> restResult =   getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
		Map userMap = gson.fromJson(restResult.getBody(), Map.class);
		String dataProfile = String.valueOf(userMap.get("profile"));
		return dataProfile;
	}

	@Override
	public String findFinanceByPapsaAndCca(String papsa, String cca) {
		String url = "/profile/finance/"+papsa+"/"+cca+"/xxx";
		ResponseEntity<String> restResult =   getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
		Map userMap = gson.fromJson(restResult.getBody(), Map.class);
		String dataProfile = String.valueOf(userMap.get("profile"));
		return dataProfile;
	}
	

}
