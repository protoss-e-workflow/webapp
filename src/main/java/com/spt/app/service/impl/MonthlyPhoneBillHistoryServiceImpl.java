package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.MonthlyPhoneBillHistoryService;
import com.spt.app.util.HttpRequestParamConvertUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("MonthlyPhoneBillHistoryService")
public class MonthlyPhoneBillHistoryServiceImpl extends AbstractEngineService implements BaseCommonService,MonthlyPhoneBillHistoryService {

	static final Logger LOGGER = LoggerFactory.getLogger(MonthlyPhoneBillHistoryServiceImpl.class);
	
	private String urlBase = "/monthlyPhoneBillHistories";

	@Override
	public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Content-Type", "application/json; charset=utf-8");

		String queryString = "";
		if(objectMap.get("queryString") != null){
			queryString = String.valueOf(objectMap.get("queryString"));
			try {
				queryString = URLDecoder.decode(queryString, "UTF-8");
			} catch (UnsupportedEncodingException e) { e.printStackTrace(); }
		}

		HttpEntity<String> entity = new HttpEntity<String>("", headers);
		String method = "findByEmpCodeIgnoreCaseContainingAndMonthIgnoreCaseContainingAndYearIgnoreCaseContaining";
		String url = "/"+urlBase+"/search/"+method+"?sort=createdDate,desc&"+queryString;
		return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> findSize(Map<String, Object> objectMap) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Content-Type", "application/json; charset=utf-8");

		String queryString = "";
		if(objectMap.get("queryString") != null){
			queryString = String.valueOf(objectMap.get("queryString"));
			try {
				queryString = URLDecoder.decode(queryString, "UTF-8");
			} catch (UnsupportedEncodingException e) { e.printStackTrace(); }
			//queryString = queryString.replaceAll("%2C", ",");
		}
		HttpEntity<String> entity = new HttpEntity<String>("", headers);
		String method = "findByEmpCodeIgnoreCaseContainingAndMonthIgnoreCaseContainingAndYearIgnoreCaseContaining";
		String url = "/"+urlBase+"/search/"+method+"?page=0&size=1&"+queryString;

		LOGGER.info("url="+url);
		return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> save(Map parameter) {

		String url = "/"+urlBase;
		return postWithJson(parameter, HttpMethod.POST, url);
	}

	@Override
	public ResponseEntity<String> delete(Long id) {

		String url = "/"+urlBase+"/"+id;
		return deleteSend(HttpMethod.DELETE, url);
	}

	@Override
	public ResponseEntity<String> get(Long id) {

		String url = "/"+urlBase+"/"+id;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> load() {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/"+urlBase;
        
        LOGGER.info("url="+url);
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> savePut(Long id, Map parameter) {

		String url = "/"+urlBase+"/"+id;
		return putWithJson(parameter, HttpMethod.PUT, url);
	}


	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {

		super.restTemplate = restTemplate;
	}




	@Override
	public ResponseEntity<String> findByEmpCodeIgnoreCaseContainingAndMonthIgnoreCaseContainingAndYearIgnoreCaseContaining(String empCode,String month, String year) {

		String url = "/"+urlBase+"/search/findByEmpCodeIgnoreCaseContainingAndMonthIgnoreCaseContainingAndYearIgnoreCaseContaining?empCode="+empCode+"&month="+month+"&year="+year;
		String jsonMonthlyPhoneBill = getResultByExchange(url).getBody();
		Map monthlyPhoneBillMap = gson.fromJson(jsonMonthlyPhoneBill, Map.class);
		List<Map> contentsMonthlyPhoneBill =  (List<Map>) monthlyPhoneBillMap.get("content");
		String jsonStrem = gson.toJson(contentsMonthlyPhoneBill, Map.class);
		return ResponseEntity.ok().body(jsonStrem);
	}

	@Override
	public ResponseEntity<String> saveMonthlyPhoneBillHistoryForExpense(Map<String, String[]> jsonDocument) {
		LOGGER.info("-= Service Save Monthly Phone Bill History For Expense =-");
		ResponseEntity<String> reponseEntity = null;
		try{
			String url = "/monthlyPhoneBillHistoryCustom/saveMonthlyPhoneBillHistoryForExpense";
			reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
		}catch (Exception e) {
			e.printStackTrace();
		}

		return  reponseEntity;
	}

	@Override
	public ResponseEntity<String> deleteMonthlyPhoneBillHistoryForExpense(Map<String, String[]> jsonDocument) {
		LOGGER.info("-= Service Delete Monthly Phone Bill History For Expense =-");
		ResponseEntity<String> reponseEntity = null;
		try{
			Map<String, String> mapRelation = new HashMap<String, String>();
			applyRelateEntity("expenseItem",jsonDocument,mapRelation);
			applyRelateEntity("month",jsonDocument,mapRelation);
			applyRelateEntity("year",jsonDocument,mapRelation);

			String url = "/monthlyPhoneBillHistoryCustom/deleteMonthlyPhoneBillHistoryForExpense?"+ HttpRequestParamConvertUtil.toUrlParam(mapRelation);

			reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
		}catch (Exception e) {
			e.printStackTrace();
		}

		return  reponseEntity;
	}

	@Override
	public ResponseEntity<String> updateMonthlyPhoneBillHistoryForExpense(Map<String, String[]> jsonDocument) {
		LOGGER.info("-= Service Update Monthly Phone Bill History For Expense =-");
		ResponseEntity<String> reponseEntity = null;
		try{
			Map<String, String> mapRelation = new HashMap<String, String>();
			applyRelateEntity("expenseItem",jsonDocument,mapRelation);
			applyRelateEntity("month",jsonDocument,mapRelation);
			applyRelateEntity("year",jsonDocument,mapRelation);

			String url = "/monthlyPhoneBillHistoryCustom/updateMonthlyPhoneBillHistoryForExpense?"+HttpRequestParamConvertUtil.toUrlParam(mapRelation);

			reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
		}catch (Exception e) {
			e.printStackTrace();
		}

		return  reponseEntity;
	}
}
