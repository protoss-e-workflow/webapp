package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.IncomingRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Map;

@Service("IncomingRequestService")
public class IncomingRequestServiceImpl extends AbstractEngineService implements BaseCommonService,IncomingRequestService {

    private static Logger LOGGER = LoggerFactory.getLogger(IncomingRequestServiceImpl.class);

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        super.restTemplate = restTemplate;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        LOGGER.info("<><><><> IncomingRequestServiceImpl findByCriteria <><><><>");
        HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        	try {
				queryString = URLDecoder.decode(queryString, "UTF-8");
			} catch (UnsupportedEncodingException e) { e.printStackTrace(); }
        	//queryString = queryString.replaceAll("%2C", ",");
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByNextApproverAndDocument_DocNumberIgnoreCaseContainingAndDocument_TitleDescriptionIgnoreCaseContainingAndDocument_DocumentTypeAndDocument_DocumentStatusInOrderByDocument_SendDateDesc";
        String url = "/requests/search/"+method+"?sort=id,desc&"+queryString;
        try{
            return getResultString(url,entity);
        }catch (Exception e){
            LOGGER.info("{}",e.getMessage());
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.OK);
        }

    }

    @SuppressWarnings("Duplicates")
    @Override
    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        LOGGER.info("<><><><> IncomingRequestServiceImpl findSize <><><><>");
        HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        	try {
				queryString = URLDecoder.decode(queryString, "UTF-8");
			} catch (UnsupportedEncodingException e) { e.printStackTrace(); }
        	//queryString = queryString.replaceAll("%2C", ",");
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByNextApproverAndDocument_DocNumberIgnoreCaseContainingAndDocument_TitleDescriptionIgnoreCaseContainingAndDocument_DocumentTypeAndDocument_DocumentStatusInOrderByDocument_SendDateDesc";
        String url = "/requests/search/"+method+"?page=0&size=1&"+queryString;

        LOGGER.info("url="+url);
        try{
            return getResultString(url,entity);
        }catch (Exception e){
            LOGGER.info("{}",e.getMessage());
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.OK);
        }
    }

    @Override
    public ResponseEntity<String> save(Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> savePut(Long id, Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> delete(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> get(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> load() {
        return null;
    }
}
