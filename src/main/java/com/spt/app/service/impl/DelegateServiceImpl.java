package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.DelegateService;
import com.spt.app.service.ExpenseTypeScreenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service("DelegateService")
public class DelegateServiceImpl extends AbstractEngineService implements BaseCommonService,DelegateService {

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {

        super.restTemplate = restTemplate;
    }

    public ResponseEntity<String> findByCode(String code) {
        return null;
    }



    private String urlBase = "delegates";

    @Override
    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
            try {
                queryString = URLDecoder.decode(queryString, "UTF-8");
            } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/delegateCustom/"+"?page=0&size=10&"+queryString;


        return getResultString(url,entity);


    }

    @Override
    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
            try {
                queryString = URLDecoder.decode(queryString, "UTF-8");
            } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/delegateCustom/"+"?page=0&size=1&"+queryString;

        LOGGER.info("================ URL Find Size Expense Reference==================== [{}]",url);




        return getResultString(url,entity);



    }


    @Override
    public ResponseEntity<String> findAssignerByCriteria(Map<String, Object> objectMap) {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
            try {
                queryString = URLDecoder.decode(queryString, "UTF-8");
            } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByEmpCodeAssigneeAndFlowType";
        String url = "/"+urlBase+"/search/"+method+"?sort=id,desc&"+queryString;

        LOGGER.info("================ URL Find By Criteria Assigner ==================== [{}]",url);
        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> findAssignerSize(Map<String, Object> objectMap) {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
            try {
                queryString = URLDecoder.decode(queryString, "UTF-8");
            } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByEmpCodeAssigneeAndFlowType";
        String url = "/"+urlBase+"/search/"+method+"?page=0&size=1&"+queryString;

        LOGGER.info("================ URL Find Size Assigner==================== [{}]",url);

        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> findAllDelegateByEmpCodeAssignee(String empCodeAssignee) {

        String method = "findAllDelegateByEmpCodeAssignee";
        String url = "/"+urlBase+"/search/"+method+"?empCodeAssignee="+empCodeAssignee;
        return getResultByExchange(url);
    }

    @Override
    public ResponseEntity<String> findByEmpCodeAssignee(String empCodeAssignee) {
        String method = "findByEmpCodeAssignee";
        String url = "/"+urlBase+"/search/"+method+"?empCodeAssignee="+empCodeAssignee;
        return getResultByExchange(url);
    }

    @Override
    public ResponseEntity<String> findByEmpCodeAssigneeAndEmpCodeAssignerAndFlowTypeAndStartDateAndEndDate(String empCodeAssignee, String empCodeAssigner, String flowType, String startDate, String endDate) {
        String method = "findByEmpCodeAssigneeAndEmpCodeAssignerAndFlowTypeAndStartDateAndEndDate";
        String url = "/"+urlBase+"/search/"+method+"?empCodeAssignee="+empCodeAssignee+"&empCodeAssigner="+empCodeAssigner+"&flowType="+flowType+"&startDate="+startDate+"&endDate="+endDate;
        return getResultByExchange(url);
    }


    @Override
    public ResponseEntity<String> save(Map parameter) {
        String url = "/"+urlBase;
        return postWithJson(parameter, HttpMethod.POST,url);
    }

    @Override
    public ResponseEntity<String> savePut(Long id, Map parameter) {
        String url = "/"+urlBase+"/"+id;
        return putWithJson(parameter, HttpMethod.PUT, url);
    }

    @Override
    public ResponseEntity<String> delete(Long id) {
        String url = "/"+urlBase+"/"+id;
        return deleteSend(HttpMethod.DELETE, url);
    }


    @Override
    public ResponseEntity<String> get(Long id) {
        String url = "/"+urlBase+"/"+id;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> load() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/"+urlBase;

        LOGGER.info("url="+url);
        return getResultString(url,entity);
    }

	@Override
	public ResponseEntity<String> findByEmpUserNameAssignerAndFlowTypeAndStartDate(String empUserNameAssigner, String flowType) {
		String method = "findByEmpUserNameAssignerAndFlowTypeAndStartDate";
		String url = "/"+urlBase+"/search/"+method+"?empUserNameAssigner="+empUserNameAssigner+"&flowType="+flowType;
        return getResultByExchange(url);
	}



}
