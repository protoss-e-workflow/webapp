package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.DashBoardMyRequestService ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Map;

@Service("DashBoardMyRequestService ")
public class DashboardMyRequestServiceImpl extends AbstractEngineService implements BaseCommonService,DashBoardMyRequestService {

    @Autowired
    @Override
    public void setRestTemplate(RestTemplate restTemplate) {
        super.restTemplate = restTemplate;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
            try {
                queryString = URLDecoder.decode(queryString, "UTF-8");
            } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
            //queryString = queryString.replaceAll("%2C", ",");
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByRequesterAndDocNumberIgnoreCaseContainingAndTitleDescriptionIgnoreCaseContainingAndDocumentTypeAndDocumentStatusInOrderByCreatedDateDesc";
        String url = "/documents/search/"+method+"?"+queryString;
        LOGGER.info("Url to find My Request ====> "+url);
        try{
            ResponseEntity<String> entity1 =  getResultString(url,entity);

            return  entity1;
        }catch (Exception e){
            LOGGER.info("{}",e.getMessage());
            return new ResponseEntity<String>("",headers, HttpStatus.OK);
        }
    }

    @SuppressWarnings("Duplicates")
    @Override
    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
            try {
                queryString = URLDecoder.decode(queryString, "UTF-8");
            } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
            //queryString = queryString.replaceAll("%2C", ",");
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByRequesterAndDocNumberIgnoreCaseContainingAndTitleDescriptionIgnoreCaseContainingAndDocumentTypeAndDocumentStatusInOrderByCreatedDateDesc";
        String url = "/documents/search/"+method+"?page=0&size=1&"+queryString;

        try{
            ResponseEntity<String> entity1 =  getResultString(url,entity);

            return  entity1;
        }catch (Exception e){
            LOGGER.info("{}",e.getMessage());
            return new ResponseEntity<String>("",headers, HttpStatus.OK);
        }
    }

    @Override
    public ResponseEntity<String> save(Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> savePut(Long id, Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> delete(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> get(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> load() {
        return null;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public ResponseEntity<String> findMyRequestNotification(
            String requester,
            String docNumber,
            String titleDescription,
            String documentStatus
        ) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "requester=" + requester + "&docNumber=" + docNumber + "&titleDescription=" + titleDescription + "&documentStatus=" + documentStatus;

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findMyRequestNotification";
        String url = "/documentCustom/"+method+"?"+queryString;
        try{
            ResponseEntity<String> entity1 =  getResultString(url,entity);
            return  entity1;
        }catch (Exception e){
            LOGGER.info("{}",e.getMessage());
            return new ResponseEntity<String>("",headers, HttpStatus.OK);
        }
    }
}
