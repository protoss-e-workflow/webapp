package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.EmployeeReplacementService;
import com.spt.app.service.LocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service("EmplyeeReplacementService")
public class EmployeeReplacementServiceImpl extends AbstractEngineService implements EmployeeReplacementService,BaseCommonService {

	static final Logger LOGGER = LoggerFactory.getLogger(EmployeeReplacementServiceImpl.class);

	private String urlBase = "employeeReplacements";

	
	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {

		super.restTemplate = restTemplate;
	}


	@Override
	public ResponseEntity<String> findByEmpUserNameReplace(String userName) {
		String method = "findByEmpUserNameReplace";
		String url = "/"+urlBase+"/search/"+method+"?empUserNameReplace="+userName;
		return getResultByExchange(url);
	}

	@Override
	public ResponseEntity<String> findByEmpCodeReplace(String empCodeReplace) {
		String method = "findByEmpCodeReplace";
		String url = "/"+urlBase+"/search/"+method+"?empCodeReplace="+empCodeReplace;
		return getResultByExchange(url);
	}

	@Override
	public ResponseEntity<String> findUserAgreeByCriteria(Map<String, Object> objectMap) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Content-Type", "application/json; charset=utf-8");

		String queryString = "";
		if(objectMap.get("queryString") != null){
			queryString = String.valueOf(objectMap.get("queryString"));
			try {
				queryString = URLDecoder.decode(queryString, "UTF-8");
			} catch (UnsupportedEncodingException e) { e.printStackTrace(); }
		}

		HttpEntity<String> entity = new HttpEntity<String>("", headers);
		String method = "findByEmpCodeReplaceAndEmpCodeAgreeIgnoreCaseContaining";
		String url = "/"+urlBase+"/search/"+method+"?sort=id,desc&"+queryString;

		return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> findUserAgreeSize(Map<String, Object> objectMap) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Content-Type", "application/json; charset=utf-8");

		String queryString = "";
		if(objectMap.get("queryString") != null){
			queryString = String.valueOf(objectMap.get("queryString"));
			try {
				queryString = URLDecoder.decode(queryString, "UTF-8");
			} catch (UnsupportedEncodingException e) { e.printStackTrace(); }
		}

		HttpEntity<String> entity = new HttpEntity<String>("", headers);
		String method = "findByEmpCodeReplaceAndEmpCodeAgreeIgnoreCaseContaining";
		String url = "/"+urlBase+"/search/"+method+"?page=0&size=1&"+queryString;


		return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> findByEmpCodeReplaceAndEmpCodeAgree(String empCodeReplace, String empCodeAgree) {
		String method = "findByEmpCodeReplaceAndEmpCodeAgree";
		String url = "/"+urlBase+"/search/"+method+"?empCodeReplace="+empCodeReplace+"&empCodeAgree="+empCodeAgree;
		return getResultByExchange(url);
	}

	@Override
	public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Content-Type", "application/json; charset=utf-8");

		String queryString = "";
		if(objectMap.get("queryString") != null){
			queryString = String.valueOf(objectMap.get("queryString"));
		}

		HttpEntity<String> entity = new HttpEntity<String>("", headers);
		String url = "/employeeReplacementCustom/"+"?page=0&size=10&"+queryString;


		return getResultString(url,entity);


	}

	@Override
	public ResponseEntity<String> findSize(Map<String, Object> objectMap) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Content-Type", "application/json; charset=utf-8");

		String queryString = "";
		if(objectMap.get("queryString") != null){
			queryString = String.valueOf(objectMap.get("queryString"));
		}

		HttpEntity<String> entity = new HttpEntity<String>("", headers);
		String url = "/employeeReplacementCustom/"+"?page=0&size=1&"+queryString;

		LOGGER.info("================ URL Find Size Expense Reference==================== [{}]",url);




		return getResultString(url,entity);



	}


	@Override
	public ResponseEntity<String> save(Map parameter) {
		String url = "/"+urlBase;
		return postWithJson(parameter, HttpMethod.POST,url);
	}

	@Override
	public ResponseEntity<String> savePut(Long id, Map parameter) {
		String url = "/"+urlBase+"/"+id;
		return putWithJson(parameter, HttpMethod.PUT, url);
	}

	@Override
	public ResponseEntity<String> delete(Long id) {
		String url = "/"+urlBase+"/"+id;
		return deleteSend(HttpMethod.DELETE, url);
	}


	@Override
	public ResponseEntity<String> get(Long id) {
		String url = "/"+urlBase+"/"+id;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> load() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Content-Type", "application/json; charset=utf-8");

		HttpEntity<String> entity = new HttpEntity<String>("", headers);
		String url = "/"+urlBase;

		LOGGER.info("url="+url);
		return getResultString(url,entity);
	}


	@Override
	public ResponseEntity<String> findByEmpUserNameAgree(String userName) {

		String method = "findByEmpUserNameAgree";
		String url = "/"+urlBase+"/search/"+method+"?empUserNameAgree="+userName;
		return getResultByExchange(url);
	}

}
