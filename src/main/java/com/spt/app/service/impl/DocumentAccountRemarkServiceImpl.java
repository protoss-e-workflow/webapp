package com.spt.app.service.impl;


import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCodeService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.DocumentAccountRemarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service("DocumentAccountRemarkService")
public class DocumentAccountRemarkServiceImpl extends AbstractEngineService implements BaseCommonService,BaseCodeService,DocumentAccountRemarkService {

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {

        super.restTemplate = restTemplate;
    }

    @Override
    public ResponseEntity<String> findByCode(String code) {
        return null;
    }

    @Override
    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        return null;
    }

    @Override
    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        return null;
    }

    @Override
    public ResponseEntity<String> save(Map parameter) {
        String url = "/documentAccountRemarks";
        return postWithJson(parameter, HttpMethod.POST,url);
    }

    @Override
    public ResponseEntity<String> savePut(Long id, Map parameter) {
        String url = "/documentAccountRemarks/"+id;
        return putWithJson(parameter, HttpMethod.PUT, url);
    }

    @Override
    public ResponseEntity<String> delete(Long id) {
        String url = "/documentAccountRemarks/"+id;
        return deleteSend(HttpMethod.DELETE, url);
    }

    @Override
    public ResponseEntity<String> get(Long id) {
        String url = "/documentAccountRemarks/"+id;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> load() {
        return null;
    }

    @Override
    public ResponseEntity<String> findByDocumentId(String docId) {
        String method = "findByDocumentId";
        String url = "/documentAccountRemarks/search/"+method+"?document="+docId;
        return getResultByExchange(url);
    }

    @Override
    public ResponseEntity<String> sendEmail(String docId) {
        String url = "/sendMailAccount/"+docId;
        return getResultByExchange(url);
    }
}
