package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.ExternalMemberAccomodationDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Map;

@Service("ExternalMemberAccomodationDetailService")
public class ExternalMemberAccomodationDetailServiceImpl extends AbstractEngineService implements BaseCommonService,ExternalMemberAccomodationDetailService{

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {

        super.restTemplate = restTemplate;
    }

    private String urlBase = "externalMemberAccomodationDetails";

    @Override
    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        return null;
    }

    @Override
    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        return null;
    }

    @Override
    public ResponseEntity<String> save(Map parameter) {
        String url = "/"+urlBase;
        return postWithJson(parameter, HttpMethod.POST,url);
    }

    @Override
    public ResponseEntity<String> savePut(Long id, Map parameter) {
        String url = "/"+urlBase+"/"+id;
        return putWithJson(parameter, HttpMethod.PUT, url);
    }

    @Override
    public ResponseEntity<String> delete(Long id) {
        String url = "/"+urlBase+"/"+id;
        return deleteSend(HttpMethod.DELETE, url);
    }


    @Override
    public ResponseEntity<String> get(Long id) {
        String url = "/"+urlBase+"/"+id;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> load() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/"+urlBase;

        LOGGER.info("url="+url);
        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> findByDocNumberAndItemId(String docNumber, String itemId) {
        String method = "findByDocNumberAndItemId";
        String url = "/"+urlBase+"/search/"+method+"?docNumber="+docNumber+"&itemId="+itemId;
        return getResultByExchange(url);
    }
}
