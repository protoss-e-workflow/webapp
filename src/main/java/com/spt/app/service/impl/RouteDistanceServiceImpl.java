package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.RouteDistanceService;
import com.spt.app.util.HttpRequestParamConvertUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;

@Service("RouteDistanceService")
public class RouteDistanceServiceImpl extends AbstractEngineService implements BaseCommonService,RouteDistanceService {

	static final Logger LOGGER = LoggerFactory.getLogger(RouteDistanceServiceImpl.class);
	
	private String urlBase = "routeDistances";

	@Override
	public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Content-Type", "application/json; charset=utf-8");

		String queryString = "";
		if(objectMap.get("queryString") != null){
			queryString = String.valueOf(objectMap.get("queryString"));
			try {
				queryString = URLDecoder.decode(queryString, "UTF-8");
			} catch (UnsupportedEncodingException e) { e.printStackTrace(); }
		}

		HttpEntity<String> entity = new HttpEntity<String>("", headers);
		String method = "findByLocationFromDescriptionAndLocationToDescription";
		String url = "/"+urlBase+"/search/"+method+"?sort=createdDate,desc&"+queryString;
		return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> findSize(Map<String, Object> objectMap) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Content-Type", "application/json; charset=utf-8");

		String queryString = "";
		if(objectMap.get("queryString") != null){
			queryString = String.valueOf(objectMap.get("queryString"));
			try {
				queryString = URLDecoder.decode(queryString, "UTF-8");
			} catch (UnsupportedEncodingException e) { e.printStackTrace(); }
			//queryString = queryString.replaceAll("%2C", ",");
		}
		HttpEntity<String> entity = new HttpEntity<String>("", headers);
		String method = "findByLocationFromDescriptionAndLocationToDescription";
		String url = "/"+urlBase+"/search/"+method+"?page=0&size=1&"+queryString;

		LOGGER.info("url="+url);
		return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> save(Map parameter) {

		String url = "/"+urlBase;
		return postWithJson(parameter, HttpMethod.POST, url);
	}

	@Override
	public ResponseEntity<String> delete(Long id) {

		String url = "/"+urlBase+"/"+id;
		return deleteSend(HttpMethod.DELETE, url);
	}

	@Override
	public ResponseEntity<String> get(Long id) {

		String url = "/"+urlBase+"/"+id;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> load() {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/"+urlBase;
        
        LOGGER.info("url="+url);
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> savePut(Long id, Map parameter) {

		String url = "/"+urlBase+"/"+id;
		return putWithJson(parameter, HttpMethod.PUT, url);
	}


	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {

		super.restTemplate = restTemplate;
	}

	@Override
	public ResponseEntity<String> findByLocationFromAndLocationTo(Long locationFrom, Long locationTo) {

		String method = "findByLocationFromAndLocationTo";
		String url = "/"+urlBase+"/search/"+method+"?locationFrom="+locationFrom+"&locationTo="+locationTo;
		return getResultByExchange(url);
	}


	@Override
	public List<Map> findByLocationFromDescriptionAndLocationToDescription(String locationFromDescription, String locationToDescription) {

		String url = "/"+urlBase+"/search/findByLocationFromDescriptionAndLocationToDescription?locationFromDescription="+locationFromDescription+"&locationToDescription="+locationToDescription;
		String jsonRouteDistance = getResultByExchange(url).getBody();
		Map RouteDistanceMap = gson.fromJson(jsonRouteDistance, Map.class);
		List<Map> contentsRouteDistance =  (List<Map>) RouteDistanceMap.get("content");
		return contentsRouteDistance;
	}

	public ResponseEntity<String> saveRouteDistance(Map<String, String[]> jsonRouteDistance) {
		LOGGER.info("-= SERVICE SAVE ROUTE DISTANCE =-");
		LOGGER.info("><><>< jsonRouteDistance : {} ",jsonRouteDistance);
		ResponseEntity<String> reponseEntity = null;
		try{
			Map<String, String> mapRelation = new HashMap<String, String>();
			applyRelateEntity("locationFrom",jsonRouteDistance,mapRelation);
			applyRelateEntity("locationTo",jsonRouteDistance,mapRelation);

			String url = "/routeDistanceCustom/?"+HttpRequestParamConvertUtil.toUrlParam(mapRelation);
			List<String> stringList = new ArrayList<String>();
			stringList.add("routeDistance");
			reponseEntity = postWithJsonRelation(jsonRouteDistance,stringList,HttpMethod.POST,url.toString());
		}catch (Exception e) {

			e.printStackTrace();
		}
		return  reponseEntity;
	}

	@Override
	public ResponseEntity<String> calculateRouteDistanceFromLocation(String locationFrom, String locationTo, String companyCode) {
		String url = "/routeDistanceCustom/calculateRouteDistanceFromLocation?locationFrom="+locationFrom+"&locationTo="+locationTo+"&companyCode="+companyCode;
		return getResultByExchange(url);
	}

	@Override
	public ResponseEntity<String> calculateRouteDistanceFromDistance(String distance, String companyCode) {
		String url = "/routeDistanceCustom/calculateRouteDistanceFromDistance?distance="+distance+"&companyCode="+companyCode;
		return getResultByExchange(url);
	}
}
