package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.InternalOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service("InternalOrderService")
public class InternalOrderServiceImpl extends AbstractEngineService implements BaseCommonService,InternalOrderService {

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        super.restTemplate = restTemplate;
    }

    @Override
    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        return null;
    }

    @Override
    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        return null;
    }

    @Override
    public ResponseEntity<String> save(Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> savePut(Long id, Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> delete(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> get(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> load() {
        return null;
    }

    @Override
    public ResponseEntity<String> findByIoCode(String ioCode) {
        String method = "findByIoCode";
        String url = "/internalOrders/search/"+method+"?ioCode="+ioCode;
        return getResultByExchange(url);
    }

    @Override
    public ResponseEntity<String> findByIoCodeAndCompanyCode(String ioCode,String companyCode) {
        String method = "findByIoCodeAndCompanyCodeOrderByDateTimeDesc";
        String url = "/internalOrders/search/"+method+"?ioCode="+ioCode+"&companyCode="+companyCode;
        return getResultByExchange(url);
    }

    @Override
    public ResponseEntity<String> checkIoRealtime(String gl,String costCenter,String io,String manOrder,String wbs,String docNumber,String expItemId) {
        String method = "checkIoRealtime";
        String url = "/internalOrderCustom/"+method+"?gl="+gl+"&costCenter="+costCenter+"&io="+io+"&manOrder="+manOrder+"&wbs="+wbs+"&docNumber="+docNumber+"&expItemId="+expItemId;
        return getResultByExchange(url);
    }

    @Override
    public ResponseEntity<String> checkBudgetInEwf(String gl,String costCenter) {
        String method = "checkBudgetInEwf";
        String url = "/internalOrderCustom/"+method+"?gl="+gl+"&costCenter="+costCenter;
        return getResultByExchange(url);
    }
}
