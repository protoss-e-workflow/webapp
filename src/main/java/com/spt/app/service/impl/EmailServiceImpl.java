package com.spt.app.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.common.base.Splitter;
import com.spt.app.controller.general.SecurityController;
import com.spt.app.service.AbstractEmailService;
import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.AbstractOrgSysEngineService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.EmailService;
import com.spt.app.service.EmplyeeProfileService;
import com.spt.app.service.UserService;

@Service("EmailService")
public class EmailServiceImpl extends AbstractEmailService implements EmailService{

	static final Logger LOGGER = LoggerFactory.getLogger(EmailServiceImpl.class);

	
	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {

		super.restTemplate = restTemplate;
	}


	@Override
	public String getMailServerUrl() {

		return super.getEmailServerPath();
	}

}
