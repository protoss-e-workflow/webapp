package com.spt.app.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.spt.app.controller.general.SecurityController;
import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCodeService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.BaseParentService;
import com.spt.app.service.MasterDataService;
import com.spt.app.service.ParameterService;
import com.spt.app.service.RoleService;

@Service("ParameterService")
public class ParameterServiceImpl extends AbstractEngineService implements BaseCommonService,BaseCodeService,BaseParentService,ParameterService{

	static final Logger LOGGER = LoggerFactory.getLogger(ParameterServiceImpl.class);
	
	private String urlBase = "parameterDetails";
	
	@Override
	public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        }
        
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByCodeIgnoreCaseContainingAndNameIgnoreCaseContainingAndParameterIn";
        String url = "/"+urlBase+"/search/"+method+"?sort=id,desc&"+queryString;
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> findSize(Map<String, Object> objectMap) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        }
        
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByCodeIgnoreCaseContainingAndNameIgnoreCaseContainingAndParameterIn";
        String url = "/"+urlBase+"/search/"+method+"?page=0&size=1&"+queryString;
        
        LOGGER.info("url="+url);
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> save(Map parameter) {

		String url = "/"+urlBase;
		return postWithJson(parameter, HttpMethod.POST, url);
	}

	@Override
	public ResponseEntity<String> delete(Long id) {

		String url = "/"+urlBase+"/"+id;
		return deleteSend(HttpMethod.DELETE, url);
	}

	@Override
	public ResponseEntity<String> get(Long id) {

		String url = "/"+urlBase+"/"+id;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> load() {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/"+urlBase;
        
        LOGGER.info("url="+url);
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> savePut(Long id,Map parameter) {

		String url = "/"+urlBase+"/"+id;
		return putWithJson(parameter, HttpMethod.PUT, url);
	}


	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {

		super.restTemplate = restTemplate;
	}

	@Override
	public ResponseEntity<String> findByCode(String code) {

		String method = "findByCode";
	    String url = "/parameters/search/"+method+"?code="+code;
		return getResultByExchange(url);
	}

	@Override
	public ResponseEntity<String> putParent(Long id, List<String> associateIdLs) {

		String url = "/"+urlBase+"/"+id;
		String associatePath = "/parameter";
		return putParent(associateIdLs,url,associatePath);
	}


	@Override
	public ResponseEntity<String> findParameterDetailsByParameterId(Long id) {

		String url = "/parameters/"+id+"/details";
		return getResultByExchange(url);
	}

	@Override
	public List<Map> findByParameterCodeIn(String code) {

		String url = "/parameterDetails/search/findByParameterCodeIn?parameter="+code;
		String jsonParameter = getResultByExchange(url).getBody();
		Map parameterMap = gson.fromJson(jsonParameter, Map.class);
    	List<Map> contentsParameter =  (List<Map>) parameterMap.get("content");
    	return contentsParameter;
	}

	@Override
	public ResponseEntity<String> findByParameterDetailCode(String code) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.add("Content-Type", "application/json; charset=utf-8");

		HttpEntity<String> entity = new HttpEntity<String>("", headers);

		String url = "/parameterDetails/search/findByCodeIgnoreCaseContaining?code=" + code;
		return getResultString(url,entity);

	}
}
