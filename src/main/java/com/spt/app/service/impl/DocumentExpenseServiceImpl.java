package com.spt.app.service.impl;

import com.google.gson.*;
import com.spt.app.service.*;
import com.spt.app.util.HttpRequestParamConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.File;
import java.lang.reflect.Type;
import java.util.*;

@Service("DocumentExpenseService")
public class DocumentExpenseServiceImpl extends AbstractEngineService implements BaseCommonService,BaseCodeService,DocumentExpenseService {


    @Autowired
    FlowManageService flowManageService;

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {

        super.restTemplate = restTemplate;
    }

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };


    protected Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, deser).create();

    public ResponseEntity<String> findByCode(String code) {
        return null;
    }

    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        return null;
    }

    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        return null;
    }

    public ResponseEntity<String> save(Map parameter) {
        return null;
    }

    public ResponseEntity<String> savePut(Long id, Map parameter) {
        return null;
    }

    public ResponseEntity<String> delete(Long id) {
        return null;
    }

    public ResponseEntity<String> get(Long id) {
        String url = "/documents/"+id;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    public ResponseEntity<String> load() {
        return null;
    }

    public ResponseEntity<String> saveDocument(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE SAVE DOCUMENT EXPENSE =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{
            Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("costCenter",jsonDocument,mapRelation);
//            applyRelateEntity("company",jsonDocument,mapRelation);
//            applyRelateEntity("department",jsonDocument,mapRelation);

            String url = "/documentCustom/?"+ HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            List<String> stringList = new ArrayList<String>();
            stringList.add("documentExpense");

            reponseEntity = postWithJsonRelation(jsonDocument,stringList, HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }

        return  reponseEntity;
    }

    public ResponseEntity<String> saveDocumentExpenseItem(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE SAVE DOCUMENT EXPENSE ITEM =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{
            Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("document",jsonDocument,mapRelation);
            applyRelateEntity("expenseItem",jsonDocument,mapRelation);

            String url = "/documentExpenseItemCustom/saveDocumentExpenseItem/?"+ HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            reponseEntity = postWithJson(jsonDocument, HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }

        return  reponseEntity;
    }

    public ResponseEntity<String> updateDocumentStatus(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE UPDATE DOCUMENT STATUS EXPENSE =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{

            String url = "/documentCustom/updateDocumentStatus";

            reponseEntity = postWithJson(jsonDocument, HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }
        return  reponseEntity;
    }

    public ResponseEntity<String> copyDocument(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE COPY DOCUMENT EXPENSE =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{

            String url = "/documentCustom/copyDocument";

            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }
        return  reponseEntity;
    }

    public ResponseEntity<String> uploadFile(MultipartHttpServletRequest multipartHttpServletRequest, String urlParam) {
        String url = this.EngineServer +urlParam;
        LOGGER.info("-= SERVICE UPLOAD FILE EXPENSE =-");

        ResponseEntity<String> reponseEntity;
        try {
            MultipartFile multipathFile = multipartHttpServletRequest.getFile("file");
            String name 	=  multipartHttpServletRequest.getParameter("filename");
            String attachment 	= multipartHttpServletRequest.getParameter("attachmentType");
            String document 	= multipartHttpServletRequest.getParameter("document");

            File convFile = new File(multipathFile.getOriginalFilename());
            multipathFile.transferTo(convFile);

            MediaType mediaType = MediaType.MULTIPART_FORM_DATA;
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(mediaType);


            MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();
            FileSystemResource file = new FileSystemResource(convFile);

            body.add("file", file);
            body.add("name",name);
            body.add("attachmentType", attachment);
            body.add("document", document);


            HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<MultiValueMap<String, Object>>(body, headers);

            FormHttpMessageConverter converter = new FormHttpMessageConverter();
            converter.setSupportedMediaTypes(Arrays.asList(mediaType));

            restTemplate.getMessageConverters().add(converter);
            reponseEntity = restTemplate.postForEntity(url, entity, String.class);
            convFile.delete();
        } catch (Exception e) {

            e.printStackTrace();
            reponseEntity = new ResponseEntity("ERROR", HttpStatus.OK);
        }

        return reponseEntity;
    }


    public ResponseEntity<String> findDocumentAttachmentByDocumentId(Long id) {
        String url = "/documentAttachmentCustom/getDocumentAttachment/"+id+"/documentAttachments";
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }


    public ResponseEntity<String> deleteDocumentAttachment(Long id) {
        String url = "/documentAttachments/"+id;
        return deleteSend(HttpMethod.DELETE, url);
    }

    @Override
    public ResponseEntity<String> generateDocumentNumber(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE GENERATE DOCUMENT NUMBER =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{
            Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("company",jsonDocument,mapRelation);

            String url = "/documentCustom/generateDocumentNumber/?"+ HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            reponseEntity = postWithJson(jsonDocument, HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }
        return  reponseEntity;
    }

    @Override
    public ResponseEntity<String> findByRequesterAndDocumentType(String requester,String documentType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByRequester";
        String url = "/documents"+"/search/"+method+"?requester="+requester+"&documentType="+documentType;
        return getResultString(url,entity);
    }
    @Override
    public ResponseEntity<String> findByDocNumber(String docNumber) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByDocNumber";
        String url = "/documents"+"/search/"+method+"?docNumber="+docNumber;
        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> findByRequesterOrderByDocNumber(String requester) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByRequesterOrderByDocNumber";
        String url = "/documents"+"/search/"+method+"?requester="+requester;
        return getResultString(url,entity);
    }

    public ResponseEntity<String> saveDocumentExpenseItemDetail(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE SAVE DOCUMENT EXPENSE ITEM DETAIL =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{
            Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("expTypeByCompany",jsonDocument,mapRelation);
            applyRelateEntity("document",jsonDocument,mapRelation);

            String url = "/documentExpenseItemCustom/saveDocumentExpenseItemDetail/?"+ HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            List<String> list = new ArrayList<>();
            list.add("docExpItemDetail");

            reponseEntity = postWithJsonRelation(jsonDocument,list, HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }

        return  reponseEntity;
    }

    @Override
    public ResponseEntity<String> updateDocumentExpenseItemDetail(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE UPDATE DOCUMENT EXPENSE ITEM DETAIL =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{
            Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("expTypeByCompany",jsonDocument,mapRelation);
            applyRelateEntity("document",jsonDocument,mapRelation);

            String url = "/documentExpenseItemCustom/updateDocumentExpenseItemDetail/?"+ HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            List<String> list = new ArrayList<>();
            list.add("docExpItemDetail");

            reponseEntity = postWithJsonRelation(jsonDocument,list, HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }

        return  reponseEntity;
    }

    public ResponseEntity<String> saveDocumentReference(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE SAVE DOCUMENT REFERENCE EXPENSE =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{
            Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("document",jsonDocument,mapRelation);
            String url = "/documentReferenceCustom/?"+HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }
        return  reponseEntity;
    }

    public ResponseEntity<String> updateDocumentReference(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE UPDATE DOCUMENT REFERENCE EXPENSE =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{
            Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("document",jsonDocument,mapRelation);
            String url = "/documentReferenceCustom/updateDocumentReference?"+HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }
        return  reponseEntity;
    }

    public ResponseEntity<String> updateDocumentReferenceList(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE UPDATE DOCUMENT REFERENCE EXPENSE LIST =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{
            Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("document",jsonDocument,mapRelation);
            applyRelateEntity("documentReference",jsonDocument,mapRelation);
            String url = "/documentReferenceCustom/updateDocumentReferenceList?"+HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }
        return  reponseEntity;
    }

    @Override
    public ResponseEntity<String> findByRequesterAndDocNumberAndTitleAndDocType(String requester, String docNumber, String title, String docType) {
        return null;
    }

    @Override
    public String getAuthorizeForLineApprove(Map<String,String[]> map) {
        LOGGER.info("-=GET AUTHORIZE FOR LINE EXPENSE  =-");
        String userName =  map.get("requester")[0];   // ===== user of requester   ex. "karoons" =====;
        String approveType  =  map.get("approveType")[0];   // ===== for case document approve =====;
        String expenseAmountStr = "";
        String papsa = map.get("papsa")[0];
        String reponseEntity = null;

        try{
            Map<String, String> mapDetail = new HashMap<String, String>();
            applyRelateEntity("details",map,mapDetail);

            List<Map> listDetail = gson.fromJson(mapDetail.get("details"),List.class);

            for (int i = 0; i < listDetail.size(); i++) {
                if(i == listDetail.size()-1){
                    expenseAmountStr += String.valueOf(listDetail.get(i).get("flowType"))+";"+String.valueOf(listDetail.get(i).get("amount"))+";"+String.valueOf(listDetail.get(i).get("costCenter"));
                }else{
                    expenseAmountStr += String.valueOf(listDetail.get(i).get("flowType"))+";"+String.valueOf(listDetail.get(i).get("amount"))+";"+String.valueOf(listDetail.get(i).get("costCenter"))+"#";
                }
            }

            if("".equals(papsa)){
                reponseEntity = gson.toJson(flowManageService.getAuthorizationForExpenseByEmployee(userName,expenseAmountStr,approveType));
            }else{
                reponseEntity = gson.toJson(flowManageService.getAuthorizationForExpenseByEmployee(userName,expenseAmountStr,approveType,papsa));
            }

        }catch (Exception e){

        }
        return reponseEntity;
    }

    public ResponseEntity<String> uploadFileDocumentExpenseItem(MultipartHttpServletRequest multipartHttpServletRequest, String urlParam) {
        String url = this.EngineServer +urlParam;
        LOGGER.info("-= SERVICE UPLOAD FILE EXPENSE ITEM ATTACHMENT =-");

        ResponseEntity<String> reponseEntity;
        try {
            MultipartFile multipathFile = multipartHttpServletRequest.getFile("file");
            String name 	=  multipartHttpServletRequest.getParameter("filename");
            String attachment 	= multipartHttpServletRequest.getParameter("attachmentCode");
            String documentExpenseItem 	= multipartHttpServletRequest.getParameter("documentExpenseItem");

            File convFile = new File(multipathFile.getOriginalFilename());
            multipathFile.transferTo(convFile);

            MediaType mediaType = MediaType.MULTIPART_FORM_DATA;
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(mediaType);


            MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();
            FileSystemResource file = new FileSystemResource(convFile);

            body.add("file", file);
            body.add("name",name);
            body.add("attachmentCode", attachment);
            body.add("documentExpenseItem", documentExpenseItem);


            HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<MultiValueMap<String, Object>>(body, headers);

            FormHttpMessageConverter converter = new FormHttpMessageConverter();
            converter.setSupportedMediaTypes(Arrays.asList(mediaType));

            restTemplate.getMessageConverters().add(converter);
            reponseEntity = restTemplate.postForEntity(url, entity, String.class);
            convFile.delete();
        } catch (Exception e) {

            e.printStackTrace();
            reponseEntity = new ResponseEntity("ERROR", HttpStatus.OK);
        }

        return reponseEntity;
    }


    public ResponseEntity<String> findDocumentExpenseItemAttachmentByDocumentExpenseItemId(Long id) {
        String url = "/documentExpenseItemAttachmentCustom/getDocumentExpItemAttachmentByDocumentExpenseItemId/"+id;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }


    public ResponseEntity<String> deleteDocumentExpenseItemAttachment(Long id) {
        String url = "/documentExpItemAttachments/"+id;
        return deleteSend(HttpMethod.DELETE, url);
    }

    @Override
    public ResponseEntity<String> expenseTransfer(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE EXPENSE TRANSFER =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{
            String url = "/expenseTransferCustom/expenseTransfer";
            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }
        return  reponseEntity;
    }

    @Override
    public ResponseEntity<String> updateCostCenterCode(Map<String,String[]> jsonDocument) {
        LOGGER.info("-= SERVICE UPDATE COST CENTER CODE =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{

            String url = "/documentCustom/updateCostCenterCode";

            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return  reponseEntity;
    }

    @Override
    public ResponseEntity<String> updateDocumentExpenseItemAndDocumentExpenseGroupByExpenseTypeByCompany(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE UPDATE DOCUMENT EXPENSE ITEM AND DOCUMENT EXPENSE GROUP BY EXPENSE TYPE BY COMPANY =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{

            String url = "/documentCustom/updateDocumentExpenseItemAndDocumentExpenseGroupByExpenseTypeByCompany";

            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return  reponseEntity;
    }
}
