package com.spt.app.service.impl;

import com.spt.app.service.*;
import com.spt.app.util.HttpRequestParamConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("ExpenseTypeService")
public class ExpenseTypeServiceImpl extends AbstractEngineService implements BaseCommonService,BaseParentService,BaseCodeService,ExpenseTypeService {

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {

        super.restTemplate = restTemplate;
    }
    private String urlBase = "expenseTypes";

    public ResponseEntity<String> findByCode(String code) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByCode";
        String url = "/"+urlBase+"/search/"+method+"?code="+code;
        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> findExpenseTypeByIdAndExpenseTypeReferencesCompleteReason(String completeReason, Long expenseType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findExpenseTypeByIdAndExpenseTypeReferencesCompleteReason";
        String url = "/"+urlBase+"/search/"+method+"?completeReason="+completeReason+"&expenseType="+expenseType;
        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> findExpenseTypeByIdAndExpenseTypeFileAttachmentCode(String attachmentTypeCode, Long expenseType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findExpenseTypeByIdAndExpenseTypeFileAttachmentCode";
        String url = "/"+urlBase+"/search/"+method+"?attachmentTypeCode="+attachmentTypeCode+"&expenseType="+expenseType;
        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> findExpenseTypeByIdAndReimburseRoleCode(String roleCode, Long expenseType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findExpenseTypeByIdAndReimburseRoleCode";
        String url = "/"+urlBase+"/search/"+method+"?roleCode="+roleCode+"&expenseType="+expenseType;
        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> findExpenseTypeByExpenseTypeScreenStructureField(String structureField, Long expenseType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findExpenseTypeByExpenseTypeScreenStructureField";
        String url = "/"+urlBase+"/search/"+method+"?structureField="+structureField+"&expenseType="+expenseType;
        return getResultString(url,entity);
    }

//    @Override
//    public ResponseEntity<String> findExpenseTypeByIdAndExpenseTypeScreenStructureField(String structureField, Long expenseType) {
//        HttpHeaders headers = new HttpHeaders();
//        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
//        headers.add("Content-Type", "application/json; charset=utf-8");
//
//        HttpEntity<String> entity = new HttpEntity<String>("", headers);
//        String method = "findExpenseTypeByIdAndExpenseTypeScreenStructureField";
//        String url = "/"+urlBase+"/search/"+method+"?structureField="+structureField+"&expenseType="+expenseType;
//        return getResultString(url,entity);
//    }

    @Override
    public ResponseEntity<String> findExpenseTypeByIdAndExpenseTypeCompanyPaAndExpenseTypeCompanyPsaAndExpenseTypeCompanyGlCode(String pa, String psa, String glCode, Long expenseType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findExpenseTypeByIdAndExpenseTypeCompanyPaAndExpenseTypeCompanyPsaAndExpenseTypeCompanyGlCode";
        String url = "/"+urlBase+"/search/"+method+"?pa="+pa+"&psa="+psa+"&glCode="+glCode+"&expenseType="+expenseType;
        return getResultString(url,entity);
    }


    @Override
    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
            try {
                queryString = URLDecoder.decode(queryString, "UTF-8");
            } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
            //queryString = queryString.replaceAll("%2C", ",");
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByDescriptionIgnoreCaseContaining";
        String url = "/"+urlBase+"/search/"+method+"?sort=id,desc&"+queryString;

        LOGGER.info("================ URL Find By Criteria==================== [{}]",url);
        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
            try {
                queryString = URLDecoder.decode(queryString, "UTF-8");
            } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
            //queryString = queryString.replaceAll("%2C", ",");
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByDescriptionIgnoreCaseContaining";
        String url = "/"+urlBase+"/search/"+method+"?page=0&size=1&"+queryString;

        LOGGER.info("================ URL Find Size==================== [{}]",url);

        return getResultString(url,entity);
    }


    @Override
    public ResponseEntity<String> save(Map parameter) {
        String url = "/expenseTypes";
        return postWithJson(parameter, HttpMethod.POST,url);
    }

    @Override
    public ResponseEntity<String> savePut(Long id, Map parameter) {
        LOGGER.info(" CASE UPDATE   EXPENSE TYPE ID  = {}  ",id);
        String url = "/expenseTypes/"+id;
        return putWithJson(parameter, HttpMethod.PUT, url);
    }

    @Override
    public ResponseEntity<String> delete(Long id) {
        String url = "/expenseTypes/"+id;
        return deleteSend(HttpMethod.DELETE, url);
    }


    @Override
    public ResponseEntity<String> get(Long id) {
        String url = "/expenseTypes/"+id;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> load() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/expenseTypes";

        LOGGER.info("url="+url);
        return getResultString(url,entity);
    }



    public ResponseEntity<String> addExpenseTypeReference(Map<String, String[]> mapClone) {

        LOGGER.info("-= SERVICE SAVE ExpenseTypeRef =-");
        ResponseEntity<String> reponseEntity = null;
        try{

            Map<String, String> mapRelation = new HashMap<String, String>();

            applyRelateEntity("expenseType",mapClone,mapRelation);

            String url = "/expenseTypeReference/?"+ HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            reponseEntity = postWithJson(mapClone, HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }

        return  reponseEntity;


    }

    public ResponseEntity<String> addExpenseTypeByCompany(Map<String, String[]> mapClone) {
        LOGGER.info("-= SERVICE SAVE ExpenseType Company =-");
        ResponseEntity<String> reponseEntity = null;
        try{

            Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("expenseType",mapClone,mapRelation);
            applyRelateEntity("company",mapClone,mapRelation);
            String url = "/expenseTypeCompany/?"+ HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            reponseEntity = postWithJson(mapClone, HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }

        return  reponseEntity;
    }

    public ResponseEntity<String> addReimburse(Map<String, String[]> mapClone) {


        LOGGER.info("-= SERVICE SAVE Reimburse =-");
        ResponseEntity<String> reponseEntity = null;
        try{

            Map<String, String> mapRelation = new HashMap<String, String>();

            applyRelateEntity("expenseType",mapClone,mapRelation);
            String url = "/reimburse/?"+ HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            reponseEntity = postWithJson(mapClone, HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }

        return  reponseEntity;


    }

    public ResponseEntity<String> addExpenseTypeScreen(Map<String, String[]> mapClone) {
        LOGGER.info("-= SERVICE SAVE Expense Type Screen =-");
        ResponseEntity<String> reponseEntity = null;
        try{

            Map<String, String> mapRelation = new HashMap<String, String>();

            applyRelateEntity("expenseType",mapClone,mapRelation);

            String url = "/expenseTypeScreen/?"+ HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            reponseEntity = postWithJson(mapClone, HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }

        return  reponseEntity;
    }

    public ResponseEntity<String> addExpenseTypeFile(Map<String, String[]> mapClone) {
        LOGGER.info("-= SERVICE SAVE Expense Type File =-");
        ResponseEntity<String> reponseEntity = null;
        try{

            Map<String, String> mapRelation = new HashMap<String, String>();

            applyRelateEntity("expenseType",mapClone,mapRelation);

            String url = "/expenseTypeFile/?"+ HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            reponseEntity = postWithJson(mapClone, HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }

        return  reponseEntity;
    }

    @Override
    public ResponseEntity<String> findAllExpenseType() {

        String method = "findAllExpenseType";
        String url = "/"+urlBase+"/search/"+method;
        return getResultByExchange(url);
    }

    @Override
    public ResponseEntity<String> findByFlowType(String flowType) {

        String method = "findByFlowType";
        String url = "/"+urlBase+"/search/"+method+"?flowType="+flowType;
        return getResultByExchange(url);
    }



    @Override
    public ResponseEntity<String> putParent(Long id, List<String> associateIdLs) {

        String url = "/"+urlBase+"/"+id;
        String associatePath = "/expenseType";
        return putParent(associateIdLs,url,associatePath);
    }
}
