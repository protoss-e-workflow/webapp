package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCodeService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.DocumentExpenseGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service("DocumentExpenseGroupService")
public class DocumentExpenseGroupServiceImpl extends AbstractEngineService implements BaseCommonService,BaseCodeService,DocumentExpenseGroupService{

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {

        super.restTemplate = restTemplate;
    }

    @Override
    public ResponseEntity<String> findByCode(String code) {
        return null;
    }

    @Override
    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        return null;
    }

    @Override
    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        return null;
    }

    @Override
    public ResponseEntity<String> save(Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> savePut(Long id, Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> delete(Long id) {
        String url = "/documentExpenseGroups/"+id;
        return deleteSend(HttpMethod.DELETE, url);
    }

    @Override
    public ResponseEntity<String> get(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> load() {
        return null;
    }

    public ResponseEntity<String> deleteDocumentExpenseGroup(Long id) {
        String url = "/documentExpenseGroupCustom/delete/"+id;
        return deleteSend(HttpMethod.DELETE, url);
    }
}
