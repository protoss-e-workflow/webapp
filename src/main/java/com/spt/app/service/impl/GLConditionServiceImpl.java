package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.GLConditionService;
import com.spt.app.util.HttpRequestParamConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;

@Service("GLConditionService")
public class GLConditionServiceImpl extends AbstractEngineService implements BaseCommonService,GLConditionService {

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        super.restTemplate = restTemplate;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
            try {
                queryString = URLDecoder.decode(queryString, "UTF-8");
            } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
            //queryString = queryString.replaceAll("%2C", ",");
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "";
        String url = "/conditionalGLs/"+method+"?sort=id,desc&"+queryString;
        return getResultString(url,entity);
    }

    @SuppressWarnings("Duplicates")
    @Override
    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
            try {
                queryString = URLDecoder.decode(queryString, "UTF-8");
            } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
            //queryString = queryString.replaceAll("%2C", ",");
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "";
        String url = "/conditionalGLs/"+method+"?page=0&size=1&"+queryString;
        LOGGER.info("url="+url);
        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> save(Map parameter) {
        String url = "/conditionalGLs";
        return postWithJson(parameter, HttpMethod.POST, url);
    }

    @Override
    public ResponseEntity<String> savePut(Long id, Map parameter) {
        String url = "/conditionalGLs/"+id;
        return putWithJson(parameter, HttpMethod.PUT, url);
    }

    @Override
    public ResponseEntity<String> delete(Long id) {
        String url = "/conditionalGLs/"+id;
        return deleteSend(HttpMethod.DELETE, url);
    }

    @Override
    public ResponseEntity<String> get(Long id) {
        String url = "/conditionalGLs/"+id;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> load() {
        return null;
    }
    

    @Override
    public ResponseEntity<String> findByGlCreateeAndIoeAndGlSape(String glCreate, String io, String glSap) {
        String method = "findByGlCreateIgnoreCaseAndIoIgnoreCaseAndGlSapIgnoreCase?glCreate="+glCreate
                + "&io=" + io + "&glSap=" + glSap;
        String url = "/conditionalGLs/search/"+method;
        ResponseEntity responseEntity = null;
        try {
            responseEntity = getResultByExchange(url);
        }catch (Exception e){
            LOGGER.info("findByGlCreateeAndIoeAndGlSape {}",e.getMessage());
        }
        return getResultByExchange(url);
    }

    @Override
    public ResponseEntity<String> deleteConditionGLSet(Map<String, String[]> jsonConditionalGL) {
        String url = "/conditionalGLs/deleteConditionalGLWithIds?ids="+jsonConditionalGL;
//        return deleteSend(HttpMethod.DELETE, url);

        LOGGER.info("-= SERVICE DELETE GL Conditional SET=-");
        LOGGER.info("><><>< jsonConditionalIO : {} ",jsonConditionalGL);
        ResponseEntity<String> reponseEntity = null;
        try{
//            Map<String, String> mapRelation = new HashMap<String, String>();
//            applyRelateEntity("company",jsonConditionalGL,mapRelation);

//            String url = "/conditionalGLCompany/?"+ HttpRequestParamConvertUtil.toUrlParam(mapRelation);
            List<String> stringList = new ArrayList<String>();
            stringList.add("conditionalGL");
//            reponseEntity = delete(jsonConditionalGL,stringList,HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }
        return  reponseEntity;
    }
}
