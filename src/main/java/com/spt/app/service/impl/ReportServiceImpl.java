package com.spt.app.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.spt.app.controller.general.SecurityController;
import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCodeService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.BaseParentService;
import com.spt.app.service.MasterDataService;
import com.spt.app.service.ReportService;
import com.spt.app.service.RoleService;

@Service("ReportService")
public class ReportServiceImpl extends AbstractEngineService implements BaseCommonService,BaseCodeService,ReportService {

	static final Logger LOGGER = LoggerFactory.getLogger(ReportServiceImpl.class);
	
	
	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {

		super.restTemplate = restTemplate;
	}

	@Override
	public ResponseEntity<String> findByCode(String code) {

		String method = "findByCode";
	    String url = "/reportConfigurations/search/"+method+"?code="+code;
		return getResultByExchange(url);
	}

	@Override
	public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {

		return null;
	}

	@Override
	public ResponseEntity<String> findSize(Map<String, Object> objectMap) {

		return null;
	}

	@Override
	public ResponseEntity<String> save(Map parameter) {

		return null;
	}

	@Override
	public ResponseEntity<String> savePut(Long id, Map parameter) {

		return null;
	}

	@Override
	public ResponseEntity<String> delete(Long id) {

		return null;
	}

	@Override
	public ResponseEntity<String> get(Long id) {

		return null;
	}

	@Override
	public ResponseEntity<String> load() {

		return null;
	}

	@Override
	public ResponseEntity<String> getCriteriaByCode(String code) {

		String method = "findReportConfigurationIn";
	    String url = "/reportConfigurations/search/"+method+"?sort=sequenceDisplay,asc&size=10000&reportConfiguration="+code;
		return getResultByExchange(url);
	}

	@Override
	public ResponseEntity<Resource> genReport(Map<String, Object> objectMap) {

		 String queryString = "";
	        if(objectMap.get("queryString") != null){
	        	queryString = String.valueOf(objectMap.get("queryString"));
	        	try {
					queryString = URLDecoder.decode(queryString, "UTF-8");
				} catch (UnsupportedEncodingException e) { e.printStackTrace(); }
	        }
		return loadReqource("/export/"+String.valueOf(objectMap.get("reportCode"))+"/?"+queryString);
	}

	@Override
	public byte[] printApproveReport(String docNumber, String mapMessage, String language) {
		Map<String,Object> jsonString = new HashMap();
		jsonString.put("docNum",docNumber);
		jsonString.put("mapMessage",mapMessage);
		jsonString.put("language",language);
		String json = gson.toJson(jsonString);
		// LOGGER.debug("json : {}",json);
		String url = "/report/printApproveReport";
		return printReportWithPost(json, HttpMethod.POST,url);
	}


	@Override
	public byte[] printAdvanceReport(String docNumber, String mapMessage, String language) {
		Map<String,Object> jsonString = new HashMap();
		jsonString.put("docNum",docNumber);
		jsonString.put("mapMessage",mapMessage);
		jsonString.put("language",language);
		String json = gson.toJson(jsonString);
		// LOGGER.debug("json : {}",json);
		String url = "/report/printAdvanceReport";
		return printReportWithPost(json, HttpMethod.POST,url);
	}


	@Override
	public byte[] printExpenseReport(String docNumber, String mapMessage, String language) {
		Map<String,Object> jsonString = new HashMap();
		jsonString.put("docNum",docNumber);
		jsonString.put("mapMessage",mapMessage);
		jsonString.put("language",language);
		String json = gson.toJson(jsonString);
		// LOGGER.debug("json : {}",json);
		String url = "/report/printExpenseReport";
		return printReportWithPost(json, HttpMethod.POST,url);
	}

}
