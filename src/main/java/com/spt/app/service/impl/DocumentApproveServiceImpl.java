package com.spt.app.service.impl;

import com.google.gson.*;
import com.spt.app.constant.ApplicationConstant;
import com.spt.app.service.*;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.spt.app.util.HttpRequestParamConvertUtil;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.*;

@Service("DocumentApproveService")
public class DocumentApproveServiceImpl extends AbstractEngineService implements BaseCommonService,BaseCodeService,DocumentApproveService {

    @Autowired
    FlowManageService flowManageService;


	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {

		super.restTemplate = restTemplate;
	}

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();
    
    public ResponseEntity<String> findByCode(String code) {
        return null;
    }

    @SuppressWarnings("Duplicates")
    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
            try {
                queryString = URLDecoder.decode(queryString, "UTF-8");
            } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
            //queryString = queryString.replaceAll("%2C", ",");
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByDocNumberIgnoreCaseAndTitleDescriptionIgnoreCaseAndDocumentStatusIgnoreCase";
        String url = "/documents/search/"+method+"?sort=id,desc&"+queryString;
        return getResultString(url,entity);
    }

    @SuppressWarnings("Duplicates")
    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
            try {
                queryString = URLDecoder.decode(queryString, "UTF-8");
            } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
            //queryString = queryString.replaceAll("%2C", ",");
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByDocNumberIgnoreCaseAndTitleDescriptionIgnoreCaseAndDocumentStatusIgnoreCase";
        String url = "/documents/search/"+method+"?page=0&size=1&"+queryString;

        LOGGER.info("url="+url);
        return getResultString(url,entity);
    }

    
    public ResponseEntity<String> save(Map parameter) {
        return null;
    }

    
    public ResponseEntity<String> savePut(Long id, Map parameter) {
        return null;
    }

    
    public ResponseEntity<String> delete(Long id) {
        return null;
    }

    
    public ResponseEntity<String> get(Long id) {
    	String url = "/documents/"+id;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    
    public ResponseEntity<String> load() {
        return null;
    }


	public ResponseEntity<String> saveTravelDetail(Map<String, String[]> jsonDocument) {
		LOGGER.info("-= SERVICE SAVE TRAVEL DETAIL =-");
        ResponseEntity<String> reponseEntity = null;
        try{
        	Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("origin",jsonDocument,mapRelation);
            applyRelateEntity("destination",jsonDocument,mapRelation);
            applyRelateEntity("documentApproveItem",jsonDocument,mapRelation);
            
            convertStringToDateFormat("startDate", jsonDocument);
            convertStringToDateFormat("endDate", jsonDocument);
    		
            LOGGER.info("####### BEFORE CALL UTIL #######");
                                   
            String url = "/travelDetailCustom/?"+HttpRequestParamConvertUtil.toUrlParam(mapRelation);
            
            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {

        	e.printStackTrace();
		}
        
        return  reponseEntity;
	}

	public ResponseEntity<String> findTravelDetailsByDocumentApproveItemId(Long id) {
		String url = "/travelDetailCustom/getTravelDetail/"+id+"/travelDetails";
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}



    public ResponseEntity<String> saveDocument(Map<String,String[]> jsonDocument) {
    	LOGGER.info("-= SERVICE SAVE DOCUMENT =-");
        ResponseEntity<String> reponseEntity = null;
        try{
        	Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("costCenter",jsonDocument,mapRelation);
//            applyRelateEntity("company",jsonDocument,mapRelation);
//            applyRelateEntity("department",jsonDocument,mapRelation);
            
            LOGGER.info("####### BEFORE CALL UTIL #######");
                                   
            String url = "/documentCustom/?"+HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            List<String> stringList = new ArrayList<String>();
            stringList.add("documentAdvance");
            stringList.add("documentApprove");
            
            reponseEntity = postWithJsonRelation(jsonDocument,stringList,HttpMethod.POST,url.toString());
        }catch (Exception e) {

        	e.printStackTrace();
		}
        
        return  reponseEntity;
    }

	public ResponseEntity<String> saveDocumentApproveItem(Map<String, String[]> jsonDocument) {
		LOGGER.info("-= SERVICE SAVE DOCUMENT APPROVE ITEM =-");
        ResponseEntity<String> reponseEntity = null;
        try{
        	                              
        	Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("document",jsonDocument,mapRelation);
            
            String url = "/documentApproveItemCustom/?"+HttpRequestParamConvertUtil.toUrlParam(mapRelation);
            
            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {

        	e.printStackTrace();
		}
        
        return  reponseEntity;
	}


	public ResponseEntity<String> deleteTravelDetails(Long id) {
		String url = "/travelDetails/"+id;
		return deleteSend(HttpMethod.DELETE, url);
	}


	public ResponseEntity<String> saveTravelMember(Map<String, String[]> jsonDocument) {
		LOGGER.info("-= SERVICE SAVE TRAVEL MEMBER =-");
        ResponseEntity<String> reponseEntity = null;
        try{
        	Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("documentApproveItem",jsonDocument,mapRelation);
                		                                   
            String url = "/travelMemberCustom/?"+HttpRequestParamConvertUtil.toUrlParam(mapRelation);
            
            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {

        	e.printStackTrace();
		}
        return  reponseEntity;
	}


	public ResponseEntity<String> findTravelMembersByDocumentApproveItemId(Long id) {
		String url = "/travelMemberCustom/getTravelMember/"+id+"/travelMembers";
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}


	public ResponseEntity<String> deleteTravelMembers(Long id) {
		String url = "/travelMembers/"+id;
		return deleteSend(HttpMethod.DELETE, url);
	}


	public ResponseEntity<String> saveExternalMember(Map<String, String[]> jsonDocument) {
		LOGGER.info("-= SERVICE SAVE EXTERNAL MEMBER =-");
        ResponseEntity<String> reponseEntity = null;
        try{
        	Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("documentApproveItem",jsonDocument,mapRelation);
                		                                   
            String url = "/externalMemberCustom/?"+HttpRequestParamConvertUtil.toUrlParam(mapRelation);
            
            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {

        	e.printStackTrace();
		}
        return  reponseEntity;
	}


	public ResponseEntity<String> findExternalMembersByDocumentApproveItemId(Long id) {
		String url = "/externalMemberCustom/getExternalMember/"+id+"/externalMembers";
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}


	public ResponseEntity<String> deleteExternalMembers(Long id) {
		String url = "/externalMembers/"+id;
		return deleteSend(HttpMethod.DELETE, url);
	}

    public ResponseEntity<String> saveCarBooking(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE SAVE CAR BOOKING =-");
        ResponseEntity<String> reponseEntity = null;
        try{
            Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("origin",jsonDocument,mapRelation);
            applyRelateEntity("destination",jsonDocument,mapRelation);
            applyRelateEntity("documentApproveItem",jsonDocument,mapRelation);

            convertStringToDateFormat("startDate", jsonDocument);
            convertStringToDateFormat("endDate", jsonDocument);

            String url = "/carBookingCustom/?"+HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }
        return  reponseEntity;
    }

    @Override
    public ResponseEntity<String> findCarBookingsByDocumentApproveItemId(Long id) {
        String url = "/carBookingCustom/getCarBooking/"+id+"/carBookings";
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }


    public ResponseEntity<String> saveDocumentReference(Map<String, String[]> jsonDocument) {
		LOGGER.info("-= SERVICE SAVE DOCUMENT REFERENCE =-");
        ResponseEntity<String> reponseEntity = null;
        try{
            Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("document",jsonDocument,mapRelation);


            String url = "/documentReferenceCustom/?"+HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }
        return  reponseEntity;
	}


	public ResponseEntity<String> saveHotelBooking(Map<String, String[]> jsonDocument) {
		LOGGER.info("-= SERVICE SAVE HOTEL BOOKING =-");
        ResponseEntity<String> reponseEntity = null;
        try{
            Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("documentApproveItem",jsonDocument,mapRelation);

            convertStringToDateFormat("startDate", jsonDocument);
            convertStringToDateFormat("endDate", jsonDocument);

            String url = "/hotelBookingCustom/?"+HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }
        return  reponseEntity;
	}

    public ResponseEntity<String> saveFlightTicket(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE SAVE FLIGHT TICKET =-");
        ResponseEntity<String> reponseEntity = null;
        try{
            Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("documentApproveItem",jsonDocument,mapRelation);


            String url = "/flightTicketCustom/?"+HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }
        return  reponseEntity;
    }


    public ResponseEntity<String> updateDocumentStatus(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE UPDATE DOCUMENT STATUS =-");
        ResponseEntity<String> reponseEntity = null;
        try{

            String url = "/documentCustom/updateDocumentStatus";

            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }
        return  reponseEntity;
    }


    public ResponseEntity<String> uploadFile(MultipartHttpServletRequest multipartHttpServletRequest, String urlParam) {
        String url = this.EngineServer +urlParam;
        LOGGER.info("url :{}", url);

        ResponseEntity<String> reponseEntity;
        try {
            MultipartFile multipathFile = multipartHttpServletRequest.getFile("file");
            String fileName 	=  multipartHttpServletRequest.getParameter("filename");
            String attachment 	= multipartHttpServletRequest.getParameter("attachmentType");
            String document 	= multipartHttpServletRequest.getParameter("document");

            String name = URLEncoder.encode(multipathFile.getOriginalFilename(),"UTF-8");
            String fileType = FilenameUtils.getExtension(name);
            File convFile = new File(multipathFile.getOriginalFilename());
            multipathFile.transferTo(convFile);

            MediaType mediaType = MediaType.MULTIPART_FORM_DATA;
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(mediaType);


            MultiValueMap<String, Object> body = new LinkedMultiValueMap<String, Object>();
            FileSystemResource file = new FileSystemResource(convFile);

            body.add("file", file);
            //body.add("fileType",fileType);
            body.add("name",fileName);
            body.add("attachmentType", attachment);
            body.add("document", document);


            HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<MultiValueMap<String, Object>>(body, headers);

            FormHttpMessageConverter converter = new FormHttpMessageConverter();
            converter.setSupportedMediaTypes(Arrays.asList(mediaType));

            restTemplate.getMessageConverters().add(converter);
            reponseEntity = restTemplate.postForEntity(url, entity, String.class);
            convFile.delete();
        } catch (Exception e) {

            e.printStackTrace();
            reponseEntity = new ResponseEntity("ERROR", HttpStatus.OK);
        }

        return reponseEntity;
    }


    public ResponseEntity<String> findDocumentAttachmentByDocumentId(Long id) {
        String url = "/documentAttachmentCustom/getDocumentAttachment/"+id+"/documentAttachments";
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }


    public ResponseEntity<String> deleteDocumentAttachment(Long id) {
        String url = "/documentAttachments/"+id;
        return deleteSend(HttpMethod.DELETE, url);
    }

    @Override
    public byte[] downloadFileDocumentAttachment(String urlParam) {
        String url = this.EngineServer +urlParam;
        LOGGER.info("url :{}", url);
        HttpHeaders headers = new HttpHeaders();

        HttpEntity<String> entity = new HttpEntity<String>("parameter",headers);
        ResponseEntity<byte[]> responseEntity = restTemplate.exchange(url,HttpMethod.GET,entity,byte[].class);

        return responseEntity.getBody();
    }

    @Override
    public ResponseEntity<String> findAutoCompleteDocRefAppByKeySearch(String keySearch) {

        String url = "/documentCustom/findByDocumentTypeAndDocumentStatusAndApproveType?requester="+keySearch;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> findAutoCompleteDocRefAppByKeySearch(String requester,String keySearch) {

        String url = "/documentCustom/findByDocumentTypeAndDocumentStatusAndApproveTypeAndKeySearch?requester="+requester+"&keySearch="+keySearch;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> findByDocTypeAndDocStatusCompleteAndAppTypeTravelAndTravelMemberUser(Map<String,String[]> jsonDocument) {
        String url = "/documentCustom/findByDocTypeAndDocStatusCompleteAndAppTypeTravelAndTravelMemberUser?userName="+jsonDocument.get("userName")[0];
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> findRequestByDocument(Long id) {
        String url = "/requestCustom/findRequestByDocument/"+id;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> findRequestApproverByRequest(Long id) {
        String url = "/requestCustom/findRequestApproverByRequest/"+id;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public String getAuthorizeForLineApprove(Map<String,String[]> map) {
        LOGGER.info("-=GET AUTHORIZE FOR LINE APPROVE  =-");
        String userName     =  map.get("requester")[0];   // ===== user of requester   ex. "karoons" =====;
        String approveType  =  map.get("approveType")[0];   // ===== for case document approve =====;
        String papsa = map.get("papsa")[0];
        String expenseAmountStr = "";
        String reponseEntity = null;

        try{
            Map<String, String> mapDetail = new HashMap<String, String>();
            applyRelateEntity("details",map,mapDetail);

            List<Map> listDetail = gson.fromJson(mapDetail.get("details"),List.class);

            for (int i = 0; i < listDetail.size(); i++) {
                if(i == listDetail.size()-1){
                    expenseAmountStr += String.valueOf(listDetail.get(i).get("flowType"))+";"+String.valueOf(listDetail.get(i).get("amount"))+";"+String.valueOf(listDetail.get(i).get("costCenter"));
                }else{
                    expenseAmountStr += String.valueOf(listDetail.get(i).get("flowType"))+";"+String.valueOf(listDetail.get(i).get("amount"))+";"+String.valueOf(listDetail.get(i).get("costCenter"))+"#";
                }
            }

            /** modify by iMilkii 2017.10.17 for doc approve case flight ticket */
            reponseEntity = gson.toJson(flowManageService.getAuthorizationForApproveByEmployee(userName,expenseAmountStr,approveType,papsa));
//            reponseEntity = gson.toJson(flowManageService.getAuthorizationForApproveByEmployee(userName,expenseAmountStr));

        }catch (Exception e){

        }
        return reponseEntity;
    }

    @Override
    public ResponseEntity<String> findDocRefByDocTypeExp(String docNumber) {
        String url = "/documentReferenceCustom/findDocRefByDocTypeExp?documentNumber="+docNumber;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }


    public ResponseEntity<String> updateDocumentApprove(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= UPDATE DOCUMENT APPROVE =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{

            List<String> stringList = new ArrayList<String>();
            stringList.add("documentAdvance");


            convertStringToDateFormat("startDate", jsonDocument);
            convertStringToDateFormat("endDate", jsonDocument);

            String url = "/documentCustom/updateDocumentApprove";

            reponseEntity = postWithJsonRelation(jsonDocument,stringList,HttpMethod.POST,url.toString());
        }catch (Exception e) {

            e.printStackTrace();
        }
        return  reponseEntity;
    }


    public ResponseEntity<String> copyDocument(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= SERVICE COPY DOCUMENT =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{

            String url = "/documentCustom/copyDocument";

            reponseEntity = postWithJson(jsonDocument,HttpMethod.POST,url.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }
        return  reponseEntity;
    }

    @Override
    public ResponseEntity<String> generateDocumentNumber(Map<String, String[]> jsonDocument) {
        return null;
    }

    @Override
    public ResponseEntity<String> findByRequesterAndDocumentType(String requester,String documentType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByRequester";
        String url = "/documents"+"/search/"+method+"?requester="+requester+"&documentType="+documentType;
        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> findByDocNumber(String docNumber) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByDocNumber";
        String url = "/documents"+"/search/"+method+"?docNumber="+docNumber;
        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> findByRequesterOrderByDocNumber(String requester) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByRequesterOrderByDocNumber";
        String url = "/documents"+"/search/"+method+"?requester="+requester;
        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> findByRequesterAndDocNumberAndTitleAndDocType(String requester, String docNumber, String title, String docType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String dataParam = "?requester=" + requester + "&docNumber=" + docNumber + "&titleDescription=" + title + "&documentType=" + docType;
        String method = "findByRequesterAndDocNumberIgnoreCaseContainingAndTitleDescriptionIgnoreCaseContainingAndDocumentType";
        String url = "/documents" + "/search/" + method + dataParam;
        return getResultString(url, entity);
    }

    public ResponseEntity<String> findTravelMemberAndExternalMemberBydocRef(String docNumber) {
        String url = "/documentCustom/findTravelMemberAndExternalMemberBydocRef?documentNumber="+docNumber;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> findTravelDetailsByDocRef(String docNumber) {
        String url = "/documentCustom/findTravelDetailsByDocRef?documentNumber="+docNumber;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> findMasterDataHotelByCode() {
        String url = "/documentCustom/findMasterDataHotelByCode";
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> findMasterDataAirlineByCode() {
        String url = "/documentCustom/findMasterDataAirlineByCode";
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> findTravelMemberByDocRef(String docNumber) {
        String url = "/documentCustom/findTravelMemberByDocRef?documentNumber="+docNumber;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> findByDocumentId(Long id) {
        String url = "/documents/"+id;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public ResponseEntity<String> findByDocumentNumber(String docNumber) {
        String method = "findByDocNumber";
        String url = "/documents" + "/search/" + method + "?docNumber="+docNumber;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

    @Override
    public byte[] downloadFileReservationTemplete(String urlParam) {
        String url = this.EngineServer + urlParam;
        LOGGER.info("url :{}", url);
        HttpHeaders headers = new HttpHeaders();

        HttpEntity<String> entity = new HttpEntity<String>("parameter", headers);
        ResponseEntity<byte[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, byte[].class);

        return responseEntity.getBody();
    }

    @Override
    public ResponseEntity<String> validateDateTimeOverlap(Map<String, String[]> dataMap) {
        LOGGER.info("-= SERVICE VALIDATE DATE TIME OVERLAP =-");
        ResponseEntity<String> reponseEntity = null;
        try{

            String url = "/travelDetailCustom/validateDateTimeOverlap/";

            reponseEntity = postWithJson(dataMap,HttpMethod.POST,url.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }
        return  reponseEntity;
    }

    @Override
    public ResponseEntity<String> deleteDocumentApproveItem(Long id,String approveType) {
        String url = "/documentCustom/deleteDocumentApproveItem?docId="+id+"&approveType="+approveType;
        return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
    }

}
