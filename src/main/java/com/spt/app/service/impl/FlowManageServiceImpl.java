package com.spt.app.service.impl;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.spt.app.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.app.constant.ApplicationConstant;
import com.spt.app.util.ComparatorActionRole;
import com.spt.app.util.ComparatorAttorney;
import com.spt.app.util.NumberUtil;

@Service("FlowManageService")
public class FlowManageServiceImpl implements FlowManageService{
	
	@Value("${InternalOrgSysServer}")
	private String osxServer ;

	static final Logger LOGGER = LoggerFactory.getLogger(FlowManageServiceImpl.class);
	
	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

   
	
	@Autowired
	JBPMFlowService jbpmFlowService;
	
	@Autowired
	EmplyeeProfileService emplyeeProfileService;
	
	@Autowired
	FlowConfigureService flowConfigureService;

	@Autowired
	EmailService emailService;
	
	@Autowired
	MessageSource messageSource;
	
	@Autowired
	DelegateService delegateService;

	@Autowired
	MasterDataService masterDataService;

	@Deprecated
	public Map getAuthorizationByEmployeeMap(String typeCode,String employeeStr, String expenseAmountStr,String approveType) {

				/* Get Data Of User */
				ResponseEntity<String> objRet = emplyeeProfileService.findEmployeeProfileByEmployeeCode(employeeStr);
				Map userMap = gson.fromJson(objRet.getBody(), Map.class);
				String attorneyId = "210";
				if(userMap.get("Attorney_ID")!=null && !String.valueOf(userMap.get("Attorney_ID")).isEmpty()){
					attorneyId = String.valueOf(userMap.get("Attorney_ID"));
				}
				int empLevelInt  =   NumberUtil.toInteger(attorneyId).intValue();String empLevel  =   String.valueOf(empLevelInt);
				String cLevel    =   String.valueOf(userMap.get("EESG_ID"));
				String papas     =   String.valueOf(userMap.get("Personal_PA_ID")) + "-"+String.valueOf(userMap.get("Personal_PSA_ID"));
				String lineOrgId =   String.valueOf(userMap.get("LineOrgID"));
				String costCenter =   String.valueOf(userMap.get("Personal_Cost_center"));
				String flagOverRight = "N";

				
				/* Fine line by data */
				String[] expenseAmountLs = expenseAmountStr.split("#");
				
				ArrayList<Map> actionRoleVRFLs = new ArrayList<Map>();
				ArrayList<Map> actionRoleAPRLs = new ArrayList<Map>();
				ArrayList<Map> actionRoleACCLs = new ArrayList<Map>();
				ArrayList<Map> actionRoleFNCLs = new ArrayList<Map>();
				ArrayList<Map> actionRoleHRLs = new ArrayList<Map>();
				ArrayList<Map> actionRoleADMLs = new ArrayList<Map>();
				

				Map<String,String> ccaMapCheck = new HashMap<String,String>();
				for(String expenseAmount:expenseAmountLs){
					String[] expenseAmountData = expenseAmount.split(";");
					
					String flowCode = expenseAmountData[0];
					Double amount   = Double.parseDouble(expenseAmountData[1]);


					/** validate approve type case Flight H001 */
					if(!String.valueOf(approveType).equals("null") && approveType.equals(ApplicationConstant.APPROVE_TYPE_FLIGHT_BOOKING_CODE)){
						if(flowCode.contains(ApplicationConstant.FLOW_TYPE_H001)){
							flagOverRight = "Y";
						}
					}else if(!String.valueOf(approveType).equals("null") && approveType.equals(ApplicationConstant.APPROVE_TYPE_EXPENSE_OVER_RIGHT)){
						flagOverRight = "Y";
					}

					/* find CCA to max line org */
					if(expenseAmountData.length > 2){
						String cca = expenseAmountData[2];
						if(!costCenter.equals(cca)){
							lineOrgId = ccaMapCheck.get(cca);
							if(lineOrgId == null){
								ResponseEntity<String>  maxLineOrgObj = emplyeeProfileService.findMaxLineByCostWithCom(cca);
								Map maxLineOrgMap = gson.fromJson(maxLineOrgObj.getBody(), Map.class);
								String profile = String.valueOf(maxLineOrgMap.get("profile"));
								lineOrgId = profile.split("#")[0];

								//cca
								ccaMapCheck.put(cca,lineOrgId);
							}

						}
					}


					String param    = "{\"documentFlow\":\""+flowCode+"\",\"papsa\":\""+papas+"\",\"line_org\":\""+lineOrgId+"\",\"inp_req_c\":\""+cLevel+"\",\"flag_over_right\":\""+flagOverRight+"\",\"req_app_url\":\""+osxServer+"/profile\"}";
					
					LOGGER.info("==============================param   = {}",param);
					
					String result   = jbpmFlowService.getAuthorizationForExpense(employeeStr, amount, empLevel, param);

					//get map flow
					String datailFlow   = flowConfigureService.findByFlowCode(flowCode);
					
					try {
						List<Map<String,String>> lineApproveDataLs = getLineApproveData(employeeStr,result,datailFlow);
						String json = gson.toJson(lineApproveDataLs,List.class);
						LOGGER.info("==============================json={}",json);

						if(lineApproveDataLs!=null && lineApproveDataLs.size() >= 1 && lineApproveDataLs.get(0) != null && !"NONE".equals(lineApproveDataLs.get(0).get("userName"))){
							actionRoleVRFLs.add(lineApproveDataLs.get(0));//VRF
						}

						if(lineApproveDataLs!=null && lineApproveDataLs.size() >= 2 && lineApproveDataLs.get(1)  != null && !"NONE".equals(lineApproveDataLs.get(1).get("userName"))){
							actionRoleAPRLs.add(lineApproveDataLs.get(1));//APR
						}

						if(lineApproveDataLs!=null && lineApproveDataLs.size() >= 3 && lineApproveDataLs.get(2)  != null && !"NONE".equals(lineApproveDataLs.get(2).get("userName"))){
							actionRoleACCLs.add(lineApproveDataLs.get(2));//ACC
						}

						if(lineApproveDataLs!=null && lineApproveDataLs.size() >= 4 && lineApproveDataLs.get(3)  != null && !"NONE".equals(lineApproveDataLs.get(3).get("userName"))){
							actionRoleFNCLs.add(lineApproveDataLs.get(3));//FNC
						}

						if(lineApproveDataLs!=null && lineApproveDataLs.size() >= 5 && lineApproveDataLs.get(4)  != null && !"NONE".equals(lineApproveDataLs.get(4).get("userName"))){
							actionRoleHRLs.add(lineApproveDataLs.get(4));//HR
						}

						if(lineApproveDataLs!=null && lineApproveDataLs.size() >= 6 &&  lineApproveDataLs.get(5) != null && !"NONE".equals(lineApproveDataLs.get(5).get("userName"))){
							actionRoleADMLs.add(lineApproveDataLs.get(5));//ADM
						}

					} catch (Exception e) { e.printStackTrace();  }
					
					
					
				}
				
				String stateVerify  = "N";
				String stateAprove  = "N";
				String stateAdmin   = "N";
				String stateHr      = "N";
				String stateAccount = "N";
				String stateFinance = "N";
				
				Map actionRoleVRF = null;
				if(!actionRoleVRFLs.isEmpty()){
					actionRoleVRF = Collections.max(actionRoleVRFLs, new ComparatorAttorney());stateVerify  = "Y";
				}
				Map actionRoleAPR = null;
				if(!actionRoleAPRLs.isEmpty()){
					actionRoleAPR = Collections.max(actionRoleAPRLs, new ComparatorAttorney());stateAprove  = "Y";
				}
				Map actionRoleACC = null;
				if(!actionRoleACCLs.isEmpty()){
					actionRoleACC = Collections.max(actionRoleACCLs, new ComparatorAttorney());stateAccount = "Y";
				}
				Map actionRoleFNC = null;
				if(!actionRoleFNCLs.isEmpty()){
					actionRoleFNC = Collections.max(actionRoleFNCLs, new ComparatorAttorney());stateFinance = "Y";
				}
				Map actionRoleHR = null;
				if(!actionRoleHRLs.isEmpty()){
					actionRoleHR = Collections.max(actionRoleHRLs, new ComparatorAttorney());stateHr      = "Y";
				}
				Map actionRoleADM = null;
				if(!actionRoleADMLs.isEmpty()){
					actionRoleADM = Collections.max(actionRoleADMLs, new ComparatorAttorney());stateAdmin   = "Y";
				}
				 
				
				String flow = flowConfigureService.findByTypeCodeAndStateVerifyAndStateAproveAndStateAdminAndStateHrAndStateAccountAndStateFinance(typeCode,stateVerify, stateAprove, stateAdmin, stateHr, stateAccount, stateFinance);
				Map jsonObject = gson.fromJson(flow,Map.class);

				LOGGER.info("><><><<<<< jsonObject : {} ",jsonObject);
				
				List<Map> lineApproveData = new ArrayList();
				

				try {
					Double sequenceVerify = Double.parseDouble(String.valueOf(jsonObject.get("sequenceVerify")));
					if(actionRoleVRF !=null && sequenceVerify!=null){
						actionRoleVRF.put("actionRole", sequenceVerify.intValue());
						lineApproveData.add(actionRoleVRF);
					}
				} catch (NumberFormatException e) { }

				try {
					Double sequenceAprove = Double.parseDouble(String.valueOf(jsonObject.get("sequenceAprove")));
					if(actionRoleAPR !=null && sequenceAprove!=null){
						actionRoleAPR.put("actionRole", sequenceAprove.intValue());
						lineApproveData.add(actionRoleAPR);
					}
				} catch (NumberFormatException e) { }

				try {
					Double sequenceAdmin = Double.parseDouble(String.valueOf(jsonObject.get("sequenceAdmin")));
					if(actionRoleADM !=null && sequenceAdmin!=null){
						actionRoleADM.put("actionRole", sequenceAdmin.intValue());
						lineApproveData.add(actionRoleADM);
					}
				} catch (NumberFormatException e) { }

				try {
					Double sequenceHr = Double.parseDouble(String.valueOf(jsonObject.get("sequenceHr")));
					if(actionRoleHR !=null && sequenceHr!=null){
						actionRoleHR.put("actionRole", sequenceHr.intValue());
						lineApproveData.add(actionRoleHR);
					}
				} catch (NumberFormatException e) { }

				try {
					Double sequenceAccount = Double.parseDouble(String.valueOf(jsonObject.get("sequenceAccount")));
					if(actionRoleACC !=null && sequenceAccount!=null){
						actionRoleACC.put("actionRole", sequenceAccount.intValue());
						lineApproveData.add(actionRoleACC);
					}
				} catch (NumberFormatException e) { }

				try {
					Double sequenceFinance = Double.parseDouble(String.valueOf(jsonObject.get("sequenceFinance")));
					if(actionRoleFNC !=null && sequenceFinance!=null){
						actionRoleFNC.put("actionRole", sequenceFinance.intValue());
						lineApproveData.add(actionRoleFNC);
					}
				} catch (NumberFormatException e) { }
				
				
				
				Collections.sort(lineApproveData, new ComparatorActionRole()) ;
				
				LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));
				
				Map<String,Object> resultMap = new HashMap<String,Object>();
				resultMap.put("lineApproveData", lineApproveData);
				resultMap.put("documentFlow", String.valueOf(jsonObject.get("code")));
				
				return resultMap;
	}

	public Map getAuthorizationByEmployeeMap(String typeCode,String employeeStr, String expenseAmountStr,String approveType,String papsa) {

				/* Get Data Of User */
		ResponseEntity<String> objRet = emplyeeProfileService.findEmployeeProfileByEmployeeCode(employeeStr);
		Map userMap = gson.fromJson(objRet.getBody(), Map.class);
		String attorneyId = "210";
		if(userMap.get("Attorney_ID")!=null && !String.valueOf(userMap.get("Attorney_ID")).isEmpty()){
			attorneyId = String.valueOf(userMap.get("Attorney_ID"));
		}
		int empLevelInt  =   NumberUtil.toInteger(attorneyId).intValue();String empLevel  =   String.valueOf(empLevelInt);
		String cLevel    =   String.valueOf(userMap.get("EESG_ID"));
		String papas     =   String.valueOf(papsa);
		String lineOrgId =   String.valueOf(userMap.get("LineOrgID"));
		String costCenter =   String.valueOf(userMap.get("Personal_Cost_center"));
		String ccaDocument = "";
		String flagOverRight = "N";


				/* Fine line by data */
		String[] expenseAmountLs = expenseAmountStr.split("#");

		ArrayList<Map> actionRoleVRFLs = new ArrayList<Map>();
		ArrayList<Map> actionRoleAPRLs = new ArrayList<Map>();
		ArrayList<Map> actionRoleACCLs = new ArrayList<Map>();
		ArrayList<Map> actionRoleFNCLs = new ArrayList<Map>();
		ArrayList<Map> actionRoleHRLs = new ArrayList<Map>();
		ArrayList<Map> actionRoleADMLs = new ArrayList<Map>();


		Map<String,String> ccaMapCheck = new HashMap<String,String>();
		for(String expenseAmount:expenseAmountLs){
			String[] expenseAmountData = expenseAmount.split(";");

			String flowCode = expenseAmountData[0];
			Double amount   = Double.parseDouble(expenseAmountData[1]);


			/** validate approve type case Flight H001 */
			if(!String.valueOf(approveType).equals("null") && approveType.equals(ApplicationConstant.APPROVE_TYPE_FLIGHT_BOOKING_CODE)){
				if(flowCode.contains(ApplicationConstant.FLOW_TYPE_H001)){
					flagOverRight = "Y";
				}
			}else if(!String.valueOf(approveType).equals("null") && approveType.equals(ApplicationConstant.APPROVE_TYPE_EXPENSE_OVER_RIGHT)){
				flagOverRight = "Y";
			}

					/* find CCA to max line org */
			if(expenseAmountData.length > 2){
				String cca = expenseAmountData[2];
				ccaDocument = cca;
				if(!costCenter.equals(cca)){
					lineOrgId = ccaMapCheck.get(cca);
					if(lineOrgId == null){
						ResponseEntity<String>  maxLineOrgObj = emplyeeProfileService.findMaxLineByCostWithCom(cca);
						Map maxLineOrgMap = gson.fromJson(maxLineOrgObj.getBody(), Map.class);
						String profile = String.valueOf(maxLineOrgMap.get("profile"));
						lineOrgId = profile.split("#")[0];

						//cca
						ccaMapCheck.put(cca,lineOrgId);
					}

				}
			}

			LOGGER.info("LINE ORG   :   {}",lineOrgId);
			LOGGER.info("PAPSA   :   {}",papas);


			String param    = "{\"documentFlow\":\""+flowCode+"\",\"papsa\":\""+papas+"\",\"line_org\":\""+lineOrgId+"\",\"inp_req_c\":\""+cLevel+"\",\"flag_over_right\":\""+flagOverRight+"\",\"req_app_url\":\""+osxServer+"/profile\"}";

			LOGGER.info("==============================param   = {}",param);

			String result   = jbpmFlowService.getAuthorizationForExpense(employeeStr, amount, empLevel, param);

			//get map flow
			String datailFlow   = flowConfigureService.findByFlowCode(flowCode);

			try {
				
				List<Map<String,String>> lineApproveDataLs = getLineApproveDataCCA(result,datailFlow, papas, ccaDocument,employeeStr);
				//List<Map<String,String>> lineApproveDataLs = getLineApproveData(result,datailFlow);
				String json = gson.toJson(lineApproveDataLs,List.class);
				//LOGGER.info("==============================json={}",json);

				if(lineApproveDataLs!=null && lineApproveDataLs.size() >= 1 && lineApproveDataLs.get(0) != null && !"NONE".equals(lineApproveDataLs.get(0).get("userName"))){
					actionRoleVRFLs.add(lineApproveDataLs.get(0));//VRF
				}

				if(lineApproveDataLs!=null && lineApproveDataLs.size() >= 2 && lineApproveDataLs.get(1)  != null && !"NONE".equals(lineApproveDataLs.get(1).get("userName"))){
					actionRoleAPRLs.add(lineApproveDataLs.get(1));//APR
				}

				if(lineApproveDataLs!=null && lineApproveDataLs.size() >= 3 && lineApproveDataLs.get(2)  != null && !"NONE".equals(lineApproveDataLs.get(2).get("userName"))){
					actionRoleACCLs.add(lineApproveDataLs.get(2));//ACC
				}

				if(lineApproveDataLs!=null && lineApproveDataLs.size() >= 4 && lineApproveDataLs.get(3)  != null && !"NONE".equals(lineApproveDataLs.get(3).get("userName"))){
					actionRoleFNCLs.add(lineApproveDataLs.get(3));//FNC
				}

				if(lineApproveDataLs!=null && lineApproveDataLs.size() >= 5 && lineApproveDataLs.get(4)  != null && !"NONE".equals(lineApproveDataLs.get(4).get("userName"))){
					actionRoleHRLs.add(lineApproveDataLs.get(4));//HR
				}

				if(lineApproveDataLs!=null && lineApproveDataLs.size() >= 6 &&  lineApproveDataLs.get(5) != null && !"NONE".equals(lineApproveDataLs.get(5).get("userName"))){
					actionRoleADMLs.add(lineApproveDataLs.get(5));//ADM
				}

			} catch (Exception e) { e.printStackTrace();  }



		}

		String stateVerify  = "N";
		String stateAprove  = "N";
		String stateAdmin   = "N";
		String stateHr      = "N";
		String stateAccount = "N";
		String stateFinance = "N";

		Map actionRoleVRF = null;
		if(!actionRoleVRFLs.isEmpty()){
			actionRoleVRF = Collections.max(actionRoleVRFLs, new ComparatorAttorney());stateVerify  = "Y";
		}
		Map actionRoleAPR = null;
		if(!actionRoleAPRLs.isEmpty()){
			actionRoleAPR = Collections.max(actionRoleAPRLs, new ComparatorAttorney());stateAprove  = "Y";
		}
		Map actionRoleACC = null;
		if(!actionRoleACCLs.isEmpty()){
			actionRoleACC = Collections.max(actionRoleACCLs, new ComparatorAttorney());stateAccount = "Y";
		}
		Map actionRoleFNC = null;
		if(!actionRoleFNCLs.isEmpty()){
			actionRoleFNC = Collections.max(actionRoleFNCLs, new ComparatorAttorney());stateFinance = "Y";
		}
		Map actionRoleHR = null;
		if(!actionRoleHRLs.isEmpty()){
			actionRoleHR = Collections.max(actionRoleHRLs, new ComparatorAttorney());stateHr      = "Y";
		}
		Map actionRoleADM = null;
		if(!actionRoleADMLs.isEmpty()){
			actionRoleADM = Collections.max(actionRoleADMLs, new ComparatorAttorney());stateAdmin   = "Y";
		}


		String flow = flowConfigureService.findByTypeCodeAndStateVerifyAndStateAproveAndStateAdminAndStateHrAndStateAccountAndStateFinance(typeCode,stateVerify, stateAprove, stateAdmin, stateHr, stateAccount, stateFinance);
		Map jsonObject = gson.fromJson(flow,Map.class);

		List<Map> lineApproveData = new ArrayList();


		try {
			Double sequenceVerify = Double.parseDouble(String.valueOf(jsonObject.get("sequenceVerify")));
			if(actionRoleVRF !=null && sequenceVerify!=null){
				actionRoleVRF.put("actionRole", sequenceVerify.intValue());
				lineApproveData.add(actionRoleVRF);
			}
		} catch (NumberFormatException e) { }

		try {
			Double sequenceAprove = Double.parseDouble(String.valueOf(jsonObject.get("sequenceAprove")));
			if(actionRoleAPR !=null && sequenceAprove!=null){
				actionRoleAPR.put("actionRole", sequenceAprove.intValue());
				lineApproveData.add(actionRoleAPR);
			}
		} catch (NumberFormatException e) { }

		try {
			Double sequenceAdmin = Double.parseDouble(String.valueOf(jsonObject.get("sequenceAdmin")));
			if(actionRoleADM !=null && sequenceAdmin!=null){
				actionRoleADM.put("actionRole", sequenceAdmin.intValue());
				lineApproveData.add(actionRoleADM);
			}
		} catch (NumberFormatException e) { }

		try {
			Double sequenceHr = Double.parseDouble(String.valueOf(jsonObject.get("sequenceHr")));
			if(actionRoleHR !=null && sequenceHr!=null){
				actionRoleHR.put("actionRole", sequenceHr.intValue());
				lineApproveData.add(actionRoleHR);
			}
		} catch (NumberFormatException e) { }

		try {
			Double sequenceAccount = Double.parseDouble(String.valueOf(jsonObject.get("sequenceAccount")));
			if(actionRoleACC !=null && sequenceAccount!=null){
				actionRoleACC.put("actionRole", sequenceAccount.intValue());
				lineApproveData.add(actionRoleACC);
			}
		} catch (NumberFormatException e) { }

		try {
			Double sequenceFinance = Double.parseDouble(String.valueOf(jsonObject.get("sequenceFinance")));
			if(actionRoleFNC !=null && sequenceFinance!=null){
				actionRoleFNC.put("actionRole", sequenceFinance.intValue());
				lineApproveData.add(actionRoleFNC);
			}
		} catch (NumberFormatException e) { }



		Collections.sort(lineApproveData, new ComparatorActionRole()) ;

		LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));

		Map<String,Object> resultMap = new HashMap<String,Object>();
		resultMap.put("lineApproveData", lineApproveData);
		resultMap.put("documentFlow", String.valueOf(jsonObject.get("code")));

		return resultMap;
	}
	
	@Override
	public List<Map> getAuthorizationForExpenseByEmployee(String employeeStr, String expenseAmountStr,String approveType) {
		//List<Map> lineApproveData = new ArrayList();
		Map<String,Object> resultMap = getAuthorizationByEmployeeMap(ApplicationConstant.FLOW_TYPE_CODE_EXPENSE,employeeStr,expenseAmountStr,approveType);
		return (List<Map>) resultMap.get("lineApproveData");
	}
	

	@Override
	public List<Map> getAuthorizationForApproveByEmployee(String employeeStr, String expenseAmountStr,String approveType) {
		//List<Map> lineApproveData = new ArrayList();
		Map<String,Object> resultMap = getAuthorizationByEmployeeMap(ApplicationConstant.FLOW_TYPE_CODE_APPROVE,employeeStr,expenseAmountStr,approveType);
		return (List<Map>) resultMap.get("lineApproveData");
	}

	@Override
	public List<Map> getAuthorizationForExpenseByEmployee(String employeeStr, String expenseAmountStr,String approveType,String papsa) {
		//List<Map> lineApproveData = new ArrayList();
		Map<String,Object> resultMap = getAuthorizationByEmployeeMap(ApplicationConstant.FLOW_TYPE_CODE_EXPENSE,employeeStr,expenseAmountStr,approveType,papsa);
		return (List<Map>) resultMap.get("lineApproveData");
	}


	@Override
	public List<Map> getAuthorizationForApproveByEmployee(String employeeStr, String expenseAmountStr,String approveType,String papsa) {
		//List<Map> lineApproveData = new ArrayList();
		Map<String,Object> resultMap = getAuthorizationByEmployeeMap(ApplicationConstant.FLOW_TYPE_CODE_APPROVE,employeeStr,expenseAmountStr,approveType,papsa);
		return (List<Map>) resultMap.get("lineApproveData");
	}
	
	
	
	private List<Map<String,String>> getLineApproveData(String employeeStr,String lineApproveStr,String datailFlow) throws Exception{
		LOGGER.info("==============================lineApproveStr={}",lineApproveStr);
		String[] lineDataLs = lineApproveStr.split("#");
		Map userMap = null;
		
		Locale currentLocale = LocaleContextHolder.getLocale();
		

		List<Map<String,String>> datailFlowMapList = (List<Map<String, String>>) gson.fromJson(datailFlow,Map.class).get("content");
		//List<Map<String,String>> datailFlowMapList = gson.fromJson(datailFlow,List.class);
		Map<String,String> datailFlowMap = datailFlowMapList.get(0);
		
		
		List<Map<String,String>> lineApproveData = new ArrayList();
		Map<String,String> lindApprove  = null;
	    for(int i=0,j=lineDataLs.length;i<j;i++){
	    	String lineData = lineDataLs[i];

	    	LOGGER.info("==============================lineData={}",lineData);
			String userName = getUserName(lineData);

			LOGGER.info("============================== userName={}",userName);
			//Check Delegate
			try {
				ResponseEntity<String> delegateDataResponse = delegateService.findByEmpUserNameAssignerAndFlowTypeAndStartDate(userName, datailFlowMap.get("code"));
				String empUserNameAssignee = (String) gson.fromJson(delegateDataResponse.getBody(),Map.class).get("empUserNameAssigner");
				if(empUserNameAssignee!=null && !empUserNameAssignee.trim().isEmpty()){
					userName = empUserNameAssignee;
					//LOGGER.info("==============================delegateData={}",userName);
				}
			} catch (Exception e) { /* No Data of delegate */}

			try{
				ResponseEntity<String> specialDelegate = masterDataService.findByMasterdataInAndMasterDataDetailCodeOrderByCode(ApplicationConstant.MASTER_DATA_SPECIAL_DELEGATE,employeeStr);
				Map specialDelegateMap = gson.fromJson(specialDelegate.getBody(),Map.class);
				if(specialDelegateMap != null && userName.toLowerCase().equals(specialDelegateMap.get("variable1").toString().toLowerCase())){
					userName = specialDelegateMap.get("variable2").toString();
				}
			}catch (Exception e){/* No Data of delegate */}
			
			
			if("N".equals(datailFlowMap.get("variable2")) && i==0){//VRF
				userName = "NONE";
			}
			if("N".equals(datailFlowMap.get("variable3")) && i==1){//APP
				userName = "NONE";
			}
			if("N".equals(datailFlowMap.get("variable5")) && i==4){//HR
				userName = "NONE";
			}
			if("N".equals(datailFlowMap.get("variable4")) && i==5){//ADM
				userName = "NONE";
			}
			if("N".equals(datailFlowMap.get("variable6")) && i==2){//ACC
				userName = "NONE";
			}
			if("N".equals(datailFlowMap.get("variable7")) && i==3){//FNC
				userName = "NONE";
			}
				
				
			
			if(userName!=null && userName.trim().length() > 0 && !"NONE".equals(userName)){
				lindApprove = new HashMap();
				
				ResponseEntity<String> obj = emplyeeProfileService.findEmployeeProfileByEmployeeCode(userName);
				userMap = gson.fromJson(obj.getBody(), Map.class);
				
				lindApprove.put("userName", userName);
				lindApprove.put("attorneyId", String.valueOf(NumberUtil.toInteger(String.valueOf(userMap.get("Attorney_ID")), 99999999)) );
				lindApprove.put("actionRoleCode", ApplicationConstant.ROLE_ACTION_STATUS.get(i));
				lindApprove.put("actionRoleFlow", ApplicationConstant.ROLE_ACTION_JBPM.get(i));
				lindApprove.put("actionRoleName", messageSource.getMessage("LABEL_ROLE_ACTION_STATUS_"+ApplicationConstant.ROLE_ACTION_STATUS.get(i), null, Locale.US));
				lindApprove.put("name_en", String.valueOf(userMap.get("NameEN")));
				lindApprove.put("position_en", String.valueOf(userMap.get("PositionEN")));
				lindApprove.put("name", String.valueOf(userMap.get("FOA")) + String.valueOf(userMap.get("FNameTH")) + " " + String.valueOf(userMap.get("LNameTH")));
				lindApprove.put("position", String.valueOf(userMap.get("PositionTH")));
				
				lineApproveData.add(lindApprove);
				
			}else{
				lindApprove = new HashMap();
				lindApprove.put("userName", "NONE");
				lineApproveData.add(lindApprove);
			}
		}
		
		return lineApproveData;
	}
	
	private List<Map<String,String>> getLineApproveDataCCA(String lineApproveStr,String datailFlow,String papsa,String cca,String employeeStr) throws Exception{
		LOGGER.info("==============================lineApproveStr={}",lineApproveStr);
		String[] lineDataLs = lineApproveStr.split("#");
		Map userMap = null;
		
		Locale currentLocale = LocaleContextHolder.getLocale();
		

		List<Map<String,String>> datailFlowMapList = (List<Map<String, String>>) gson.fromJson(datailFlow,Map.class).get("content");
		//List<Map<String,String>> datailFlowMapList = gson.fromJson(datailFlow,List.class);
		Map<String,String> datailFlowMap = datailFlowMapList.get(0);

		
		List<Map<String,String>> lineApproveData = new ArrayList();
		Map<String,String> lindApprove  = null;
	    for(int i=0,j=lineDataLs.length;i<j;i++){
	    	String lineData = lineDataLs[i];

	    	
	    	LOGGER.info("==============================lineData={}",lineData);
	    	
	    	String userName = getUserName(lineData);
	    	if(i==4){//HR
	    		lineData  = emplyeeProfileService.findHrByPapsaAndCca(papsa, cca,employeeStr);
	    		userName = getUserNameFromServiceCCA(lineData);
			}
			if(i==5){//ADM
				lineData  = emplyeeProfileService.findAdminByPapsaAndCca(papsa, cca);
	    		userName = getUserNameFromServiceCCA(lineData);
			}
			if(i==2){//ACC
				lineData  = emplyeeProfileService.findAccountByPapsaAndCca(papsa, cca);
	    		userName = getUserNameFromServiceCCA(lineData);
			}
			if(i==3){//FNC
				lineData  = emplyeeProfileService.findFinanceByPapsaAndCca(papsa, cca);
	    		userName = getUserNameFromServiceCCA(lineData);
			}
				
			
			
			
			//Check Delegate
			try {
				ResponseEntity<String> delegateDataResponse = delegateService.findByEmpUserNameAssignerAndFlowTypeAndStartDate(userName, datailFlowMap.get("code"));
				String empUserNameAssignee = (String) gson.fromJson(delegateDataResponse.getBody(),Map.class).get("empUserNameAssigner");
				if(empUserNameAssignee!=null && !empUserNameAssignee.trim().isEmpty()){
					userName = empUserNameAssignee;
					//LOGGER.info("==============================delegateData={}",userName);
				}
			} catch (Exception e) { /* No Data of delegate */}

			try{
				ResponseEntity<String> specialDelegate = masterDataService.findByMasterdataInAndMasterDataDetailCodeOrderByCode(ApplicationConstant.MASTER_DATA_SPECIAL_DELEGATE,employeeStr);
				Map specialDelegateMap = gson.fromJson(specialDelegate.getBody(),Map.class);

				if(specialDelegateMap != null && userName.toLowerCase().equals(specialDelegateMap.get("variable1").toString().toLowerCase())){
					userName = specialDelegateMap.get("variable2").toString();
				}
			}catch (Exception e){e.printStackTrace();/* No Data of delegate */}
			
			
			if("N".equals(datailFlowMap.get("variable2")) && i==0){//VRF
				userName = "NONE";
			}
			if("N".equals(datailFlowMap.get("variable3")) && i==1){//APP
				userName = "NONE";
			}
			if("N".equals(datailFlowMap.get("variable5")) && i==4){//HR
				userName = "NONE";
			}
			if("N".equals(datailFlowMap.get("variable4")) && i==5){//ADM
				userName = "NONE";
			}
			if("N".equals(datailFlowMap.get("variable6")) && i==2){//ACC
				userName = "NONE";
			}
			if("N".equals(datailFlowMap.get("variable7")) && i==3){//FNC
				userName = "NONE";
			}
				
				
			
			if(userName!=null && userName.trim().length() > 0 && !"NONE".equals(userName)){
				lindApprove = new HashMap();
				
				ResponseEntity<String> obj = emplyeeProfileService.findEmployeeProfileByEmployeeCode(userName);
				userMap = gson.fromJson(obj.getBody(), Map.class);
				
				lindApprove.put("userName", userName);
				lindApprove.put("attorneyId", String.valueOf(NumberUtil.toInteger(String.valueOf(userMap.get("Attorney_ID")), 99999999)) );
				lindApprove.put("actionRoleCode", ApplicationConstant.ROLE_ACTION_STATUS.get(i));
				lindApprove.put("actionRoleFlow", ApplicationConstant.ROLE_ACTION_JBPM.get(i));
				lindApprove.put("actionRoleName", messageSource.getMessage("LABEL_ROLE_ACTION_STATUS_"+ApplicationConstant.ROLE_ACTION_STATUS.get(i), null, Locale.US));
				lindApprove.put("name_en", String.valueOf(userMap.get("NameEN")));
				lindApprove.put("position_en", String.valueOf(userMap.get("PositionEN")));
				lindApprove.put("name", String.valueOf(userMap.get("FOA")) + String.valueOf(userMap.get("FNameTH")) + " " + String.valueOf(userMap.get("LNameTH")));
				lindApprove.put("position", String.valueOf(userMap.get("PositionTH")));
				
				lineApproveData.add(lindApprove);
				
			}else{
				lindApprove = new HashMap();
				lindApprove.put("userName", "NONE");
				lineApproveData.add(lindApprove);
			}
		}

		LOGGER.info("lineApproveData   :   {}",lineApproveData);
		
		return lineApproveData;
	}
	
	
	private String getUserName(String lineData) throws Exception{
		String[] physicalDataLs = lineData.split(":");
		String userName = null;
		if(physicalDataLs.length > 2){
			userName = physicalDataLs[2].split("-")[0].toLowerCase();
		}
		
		
		return userName;
	}
	
	private String getUserNameFromServiceCCA(String lineData) throws Exception{
		String[] physicalDataLs = lineData.split(":");
		String userName = null;
		if(physicalDataLs.length >= 1){
			userName = physicalDataLs[1].split("-")[0].toLowerCase();
		}
		
		
		return userName;
	}


	@Override
	////{"employee":"karoons","v_person":"boonchaip","app_person":"boonchaip","acc_person":"praphaiporni","fin_person":"phanrapas","hr_person":"karoons","adm_person":"karoons","documentFlow":"MPG:DocFlowExpense:0.99-DocFlowExpense.FullApproveAdvanceNoHRAdmin","rest_server_url":"http://172.16.13.77:8066/OSX/profile"}
	public Map createDocumentFlow(String typeCode,String employee, String param,String docType,String documentNumber,String approveType,String papsa) {

		
		
		Map<String,Object> objMap 	 = getAuthorizationByEmployeeMap(typeCode,employee, param,approveType,papsa);
		List<Map>  lineApprove       = (List<Map>) objMap.get("lineApproveData");
		String documentFlow          = (String) objMap.get("documentFlow");
		String mailServerUrl         = emailService.getMailServerUrl();
		
		Map<String,String> objectJBPM = new HashMap<String,String>();
		objectJBPM.put("employee", employee);
		objectJBPM.put("v_person", "none");
		objectJBPM.put("app_person", "none");
		objectJBPM.put("acc_person", "none");
		objectJBPM.put("fin_person", "none");
		objectJBPM.put("hr_person", "none");
		objectJBPM.put("adm_person", "none");
		objectJBPM.put("documentFlow", documentFlow);
		objectJBPM.put("rest_server_url", mailServerUrl);// Mail Server

		for(Map approver:lineApprove){
			LOGGER.info("><>< #### APPROVER : {} ",approver);
			objectJBPM.put(String.valueOf(approver.get("actionRoleFlow")), String.valueOf(approver.get("userName")));
		}
		
		//LOGGER.info("==============================flow={}",gson.toJson(objectJBPM, Map.class));
		String objectJBPMString = gson.toJson(objectJBPM, Map.class);
		
		String processId   = jbpmFlowService.createDocumentFlow(employee, documentFlow, objectJBPMString,docType,documentNumber);
		
		LOGGER.info("==============================process Id={}",processId);
		
		Map<String,Object> resultMap = new HashMap<String,Object>();
		resultMap.put("documentFlow", documentFlow);
		resultMap.put("processId", processId);
		resultMap.put("lineApprove", lineApprove);
		return resultMap;
	}


	@Override
	public Map cancelDocument(String employee, String documentNumber) {

		Map<String,Object> resultMap = new HashMap<String,Object>();
		String result = jbpmFlowService.cancelDocument(employee, documentNumber);
		resultMap.put("result", result);
		return resultMap;
	}


	@Override
	public Map approveDocument(String employee,String actionStatus, String documentNumber,String docType,String documentFlow,String processId) {

		Map<String,Object> resultMap = new HashMap<String,Object>();
		String taskId = null;
		
		/* Find Task Id */
		Map<String,String> objectParamProcess = new HashMap<String,String>();
		objectParamProcess.put("documentFlow", documentFlow);
		LOGGER.info("><><>< OBJECT PARAM PROCESS : {} ",gson.toJson(objectParamProcess, Map.class));
		String processListStr = jbpmFlowService.findWaitingDocumentByEmployee(employee, docType, gson.toJson(objectParamProcess, Map.class));
		List<Map<String,String>>  processList       = gson.fromJson(processListStr, List.class);
		for(Map<String,String> process:processList){
			String[] processStream = process.get("content").split("#");
			String documentFlowModel   = processStream[0];
			String processIdModel      = processStream[1];
			String taskIdModel         = processStream[2];
			String docTypeModel        = processStream[3];
			String documentNumberModel = processStream[4];
			
			if(documentNumber.equals(documentNumberModel) && processId.equals(processIdModel)){
				taskId = taskIdModel;
				break;
			}
		}
		
		/* Send to Approve */
		Map<String,String> objectJBPM = new HashMap<String,String>();
		for(String key:ApplicationConstant.ROLE_ACTION_UPDATE_JBPM.keySet()){
			objectJBPM.put(ApplicationConstant.ROLE_ACTION_UPDATE_JBPM.get(key), ApplicationConstant.FLOW_STATUS_APPROVED);
		}
		objectJBPM.put("documentFlow", documentFlow);
		objectJBPM.put("taskId", taskId);
		String result = jbpmFlowService.sendActionToFlow(employee, gson.toJson(objectJBPM, Map.class), docType, documentNumber);
		
		LOGGER.info("><><>< taskId : {} ",taskId);
		
		resultMap.put("result", result);
		return resultMap;
	}


	@Override
	public Map rejectDocument(String employee,String actionStatus, String documentNumber,String docType,String documentFlow,String processId) {

		LOGGER.info("><><>< FLOW MANAGE SERVICE ><><><><");
		Map<String,Object> resultMap = new HashMap<String,Object>();
		String taskId = null;

		/* Find Task Id */
		Map<String,String> objectParamProcess = new HashMap<String,String>();
		objectParamProcess.put("documentFlow", documentFlow);
		LOGGER.info("><><>< OBJECT PARAM PROCESS : {} ",gson.toJson(objectParamProcess, Map.class));
		String processListStr = jbpmFlowService.findWaitingDocumentByEmployee(employee, docType, gson.toJson(objectParamProcess, Map.class));
		LOGGER.info("><><>< PROCESS : {} ",processListStr);
		List<Map<String,String>>  processList       = gson.fromJson(processListStr, List.class);
		for(Map<String,String> process:processList){
			String[] processStream = process.get("content").split("#");
			String documentFlowModel   = processStream[0];
			String processIdModel      = processStream[1];
			String taskIdModel         = processStream[2];
			String docTypeModel        = processStream[3];
			String documentNumberModel = processStream[4];

			if(documentNumber.equals(documentNumberModel) && processId.equals(processIdModel)){
				taskId = taskIdModel;
				break;
			}
		}

		/* Send to Approve */
		Map<String,String> objectJBPM = new HashMap<String,String>();
		for(String key:ApplicationConstant.ROLE_ACTION_UPDATE_JBPM.keySet()){
			objectJBPM.put(ApplicationConstant.ROLE_ACTION_UPDATE_JBPM.get(key), ApplicationConstant.FLOW_STATUS_REJECTED);
		}
		objectJBPM.put("documentFlow", documentFlow);
		objectJBPM.put("taskId", taskId);

		LOGGER.info("><><>< OBJECT JBPM ><><><>< : {} ",objectJBPM);
		String result = jbpmFlowService.sendActionToFlow(employee, gson.toJson(objectJBPM, Map.class), docType, documentNumber);


		resultMap.put("result", result);
		return resultMap;
	}


}
