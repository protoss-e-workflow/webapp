package com.spt.app.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.common.base.Splitter;
import com.spt.app.controller.general.SecurityController;
import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.AbstractFlowService;
import com.spt.app.service.AbstractOrgSysEngineService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.EmplyeeProfileService;
import com.spt.app.service.JBPMFlowService;
import com.spt.app.service.UserService;

@Service("JBPMFlowService")
public class JBPMFlowServiceImpl extends AbstractFlowService implements JBPMFlowService{

	static final Logger LOGGER = LoggerFactory.getLogger(JBPMFlowServiceImpl.class);


	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {

		super.restTemplate = restTemplate;
	}

	@Override
	public String getAuthorizationForExpense(String employee, Double amount, String empLevel, String param) {

		
		return getResultString("/getAuthorization?employee="+employee+"&pass=xxxx&amount="+String.valueOf(amount)+"&empLevel="+empLevel+"&param={param}",param);
	}

	@Override
	public String createDocumentFlow(String employee, String documentFlow, String param,String docType,String documentNumber) {

		return getResultString("/createDocumentFlow?creator="+employee+"&pass=xxxx&docType="+docType+"&documentNumber="+documentNumber+"&documentFlow="+documentFlow+"&param={param}",param);
	}

	@Override
	public String cancelDocument(String employee, String documentNumber) {

		return getResultString("/terminateDocumentJob?user="+employee+"&pass=xxxx&documentNumber="+documentNumber);
	}


	@Override
	public String sendActionToFlow(String employee, String param, String docType, String documentNumber) {

		return getResultString("/sendActionToFlow?user="+employee+"&pass=xxxx&docType="+docType+"&documentNumber="+documentNumber+"&param={param}",param);
	}

	@Override
	public String findWaitingDocumentByEmployee(String user, String docType, String param) {

		return getResultString("/findWaitingDocumentByEmployee?user="+user+"&pass=xxxx&docType="+docType+"&param={param}",param);
	}

	@Override
	public String getCurrentNode(String processId) {

		return getResultString("/getCurrentNode?user=admin&pass=admin&pid="+processId);
	}

	@Override
	public String getActive(String processId) {

		return getResultString("/getActive?user=admin&pass=admin&pid="+processId);
	}



}
