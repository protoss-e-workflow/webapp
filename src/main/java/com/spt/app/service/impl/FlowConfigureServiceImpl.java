package com.spt.app.service.impl;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.FlowConfigureService;

@Service("FlowConfigureService")
public class FlowConfigureServiceImpl extends AbstractEngineService implements FlowConfigureService{

	static final Logger LOGGER = LoggerFactory.getLogger(FlowConfigureServiceImpl.class);


	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {

		super.restTemplate = restTemplate;
	}


	@Override
	public String findByTypeCodeAndStateVerifyAndStateAproveAndStateAdminAndStateHrAndStateAccountAndStateFinance(String typeCode,
			String stateVerify, String stateAprove, String stateAdmin, String stateHr, String stateAccount,
			String stateFinance) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByTypeCodeAndStateVerifyAndStateAproveAndStateAdminAndStateHrAndStateAccountAndStateFinance";
        String url = "/flowConfigures/search/"+method+"?typeCode="+typeCode+"&stateVerify="+stateVerify+
        		"&stateAprove="+stateAprove+
        		"&stateAdmin="+stateAdmin+
        		"&stateHr="+stateHr+
        		"&stateAccount="+stateAccount+
        		"&stateFinance="+stateFinance;
        return getResultString(url);
	}


	@Override
	public String findByFlowCode(String code) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByCodeAndFlagActive";
        String url = "/masterDataDetails/search/"+method+"?code="+code+"&flagActive=true";
        return getResultString(url);
	}

	
}
