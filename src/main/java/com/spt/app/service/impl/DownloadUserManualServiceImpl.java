package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCodeService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.DownloadUserManualService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service("DownloadUserManualService")
public class DownloadUserManualServiceImpl extends AbstractEngineService implements BaseCommonService,BaseCodeService,DownloadUserManualService {


    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        super.restTemplate = restTemplate;
    }

    @Override
    public ResponseEntity<String> findByCode(String code) {
        return null;
    }

    @Override
    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        return null;
    }

    @Override
    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        return null;
    }

    @Override
    public ResponseEntity<String> save(Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> savePut(Long id, Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> delete(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> get(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> load() {
        return null;
    }

    @Override
    public byte[] downloadManualUser(String urlParam) {
        String url = this.EngineServer +urlParam;
        HttpHeaders headers = new HttpHeaders();

        LOGGER.info("url   :   {}",url);

        HttpEntity<String> entity = new HttpEntity<String>("parameter",headers);
        ResponseEntity<byte[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET,entity,byte[].class);

        return responseEntity.getBody();
    }

    @Override
    public byte[] downloadManualApprove(String urlParam) {
        String url = this.EngineServer +urlParam;
        HttpHeaders headers = new HttpHeaders();

        HttpEntity<String> entity = new HttpEntity<String>("parameter",headers);
        ResponseEntity<byte[]> responseEntity = restTemplate.exchange(url,HttpMethod.GET,entity,byte[].class);

        return responseEntity.getBody();
    }




       @Override
    public byte[] downloadManualAccount(String urlParam) {
        String url = this.EngineServer +urlParam;
        HttpHeaders headers = new HttpHeaders();

        HttpEntity<String> entity = new HttpEntity<String>("parameter",headers);
        ResponseEntity<byte[]> responseEntity = restTemplate.exchange(url,HttpMethod.GET,entity,byte[].class);

        return responseEntity.getBody();
    }
}
