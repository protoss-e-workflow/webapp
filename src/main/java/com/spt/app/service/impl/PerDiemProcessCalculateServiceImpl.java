package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.AbstractSpecialService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.PerDiemProcessCalculateService;
import com.spt.app.util.HttpRequestParamConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("PerDiemProcessCalculateService")
public class PerDiemProcessCalculateServiceImpl extends AbstractSpecialService implements BaseCommonService,PerDiemProcessCalculateService {

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        super.restTemplate = restTemplate;
    }

    @Override
    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        return null;
    }

    @Override
    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        return null;
    }

    @Override
    public ResponseEntity<String> save(Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> savePut(Long id, Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> delete(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> get(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> load() {
        return null;
    }

    @Override
    public ResponseEntity<String> processCalculatePerDiemForeign(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= Service Process Calculate PerDiem Foreign =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{
            Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("empLevel",jsonDocument,mapRelation);
            applyRelateEntity("sellingRate",jsonDocument,mapRelation);
            applyRelateEntity("documentExpense",jsonDocument,mapRelation);
            applyRelateEntity("documentExpenseItem",jsonDocument,mapRelation);

            String url = "/perDiemProcessCalculateCustom/processCalculatePerDiemForeign?"+ HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            reponseEntity = postWithJson(jsonDocument, HttpMethod.POST,url.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }

        return  reponseEntity;
    }

    @Override
    public ResponseEntity<String> processCalculatePerDiemDomestic(Map<String, String[]> jsonDocument) {
        LOGGER.info("-= Service Process Calculate PerDiem Domestic =-");
        LOGGER.info("><><>< jsonDocument : {} ",jsonDocument);
        ResponseEntity<String> reponseEntity = null;
        try{
            Map<String, String> mapRelation = new HashMap<String, String>();
            applyRelateEntity("empLevel",jsonDocument,mapRelation);
            applyRelateEntity("documentExpense",jsonDocument,mapRelation);
            applyRelateEntity("documentExpenseItem",jsonDocument,mapRelation);

            String url = "/perDiemProcessCalculateCustom/processCalculatePerDiemDomestic?"+ HttpRequestParamConvertUtil.toUrlParam(mapRelation);

            reponseEntity = postWithJson(jsonDocument, HttpMethod.POST,url.toString());
        }catch (Exception e) {
            e.printStackTrace();
        }

        return  reponseEntity;
    }
}
