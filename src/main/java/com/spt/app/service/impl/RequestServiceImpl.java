package com.spt.app.service.impl;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.spt.app.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.app.constant.ApplicationConstant;

@Service("RequestService")
public class RequestServiceImpl extends AbstractEngineService implements BaseCommonService,BaseCodeService,RequestService {

    @Override
    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {

        super.restTemplate = restTemplate;
    }

    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

    @Autowired
    FlowManageService flowManageService;

    @Autowired
    JBPMFlowService jbpmFlowService;

    @Autowired
    DocumentExpenseService documentExpenseService;


    @Autowired
    DocumentApproveService documentApproveService;


    public ResponseEntity<String> findByCode(String code) {
        return null;
    }

    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        return null;
    }

    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        return null;
    }

    public ResponseEntity<String> save(Map parameter) {
        return null;
    }

    public ResponseEntity<String> savePut(Long id, Map parameter) {
        return null;
    }

    public ResponseEntity<String> delete(Long id) {
        return null;
    }

    public ResponseEntity<String> get(Long id) {
        return null;
    }

    public ResponseEntity<String> load() {
        return null;
    }

    @Override
    public String createRequest(Map<String, String[]> map) {
        String employeeStr =  map.get("requester")[0];   // ===== user of requester   ex. "karoons" =====;
        String approveType =  map.get("approveType")[0];   // ===== user of requester   ex. "karoons" =====;
        String papsa = map.get("pa")[0]+"-"+map.get("psa")[0];
        String expenseAmountStr = "";
        String resultString = null;
        try{
            Map<String, String> mapDetail = new HashMap<String, String>();
            applyRelateEntity("details",map,mapDetail);

            List<Map> listDetail = gson.fromJson(mapDetail.get("details"),List.class);

            for (int i = 0; i < listDetail.size(); i++) {
                if(i == listDetail.size()-1){
                    expenseAmountStr += String.valueOf(listDetail.get(i).get("flowType"))+";"+String.valueOf(listDetail.get(i).get("amount"))+";"+String.valueOf(listDetail.get(i).get("costCenter"));
                }else{
                    expenseAmountStr += String.valueOf(listDetail.get(i).get("flowType"))+";"+String.valueOf(listDetail.get(i).get("amount"))+";"+String.valueOf(listDetail.get(i).get("costCenter"))+"#";
                }
            }

            /** get real document number **/
            ResponseEntity<String> document = documentExpenseService.generateDocumentNumber(map);
            Map documentMap = gson.fromJson(document.getBody(),Map.class);

            try {
            	Map mapCreateFlow = flowManageService.createDocumentFlow(ApplicationConstant.DOC_TYPE_TO_FLOW_DOC_TYPE.get(String.valueOf(documentMap.get("documentType"))) ,employeeStr,expenseAmountStr,documentMap.get("documentType").toString(),documentMap.get("tmpDocNumber").toString(),approveType,papsa);
                String documentFlow = mapCreateFlow.get("documentFlow").toString();
                String processId = mapCreateFlow.get("processId").toString();
                String lineApprove = gson.toJson(mapCreateFlow.get("lineApprove"),List.class);
                LOGGER.info("============ lineApprove      :     {}",lineApprove);
                resultString = getResultString("/requestCustom/createRequest?document="+Double.valueOf(documentMap.get("id").toString()).longValue()+"&processId="+processId+"&docFlow="+documentFlow+"&lineApprove={lineApprove}",lineApprove);
                getResultString("/flowMail/"+String.valueOf(documentMap.get("tmpDocNumber"))+"/VRF/I");
            }catch (Exception e){
                e.printStackTrace();
                LOGGER.error("ERROR CREATE REQUEST SERVICE");
                throw new RuntimeException(e.getMessage());
            }

        }catch (Exception e){
            e.printStackTrace();
            LOGGER.error("ERROR CREATE REQUEST SERVICE");
            throw new RuntimeException(e.getMessage());
        }

        return resultString;
    }

    @Override
    public String cancelRequest(Map<String, String[]> map) {
        String employeeStr =  map.get("requester")[0];   // ===== user of requester   ex. "karoons" =====;
        String docNumber =  map.get("docNumber")[0];
        String documentId =  map.get("id")[0];
        String expenseAmountStr = "";
        String resultString = null;
        try{

            LOGGER.info("><><>< employeeStr : {} ",employeeStr);
            LOGGER.info("><><>< docNumber : {} ",docNumber);
            LOGGER.info("><><>< documentId : {} ",documentId);


            Map<String, String> mapDetail = new HashMap<String, String>();
            applyRelateEntity("details",map,mapDetail);

            List<Map> listDetail = gson.fromJson(mapDetail.get("details"),List.class);

            for (int i = 0; i < listDetail.size(); i++) {
                if(i == listDetail.size()-1){
                    expenseAmountStr += String.valueOf(listDetail.get(i).get("flowType"))+";"+String.valueOf(listDetail.get(i).get("amount"));
                }else{
                    expenseAmountStr += String.valueOf(listDetail.get(i).get("flowType"))+";"+String.valueOf(listDetail.get(i).get("amount"))+"#";
                }
            }

            try {
                Map mapCancelFlow = flowManageService.cancelDocument(employeeStr,docNumber);

                LOGGER.info("><><>< mapCreateFlow : {} ",mapCancelFlow);
                String result = mapCancelFlow.get("result").toString();
                resultString = getResultString("/requestCustom/cancelRequest?document="+Double.valueOf(documentId).longValue()+"&result="+result);
            }catch (Exception e){
                e.printStackTrace();
            }

        }catch (Exception e){
            e.printStackTrace();
            LOGGER.error("ERROR CREATE REQUEST SERVICE");
        }

        return resultString;
    }

    @Override
    public String approveRequest(Map<String, String[]> map) {
        String userName =  map.get("userName")[0];
        String actionStatus =  map.get("actionStatus")[0];
        String documentNumber =  map.get("documentNumber")[0];
        String docType =  map.get("docType")[0];
        String documentFlow =  map.get("documentFlow")[0];
        String processId =  map.get("processId")[0];
        String documentId =  map.get("documentId")[0];
        String actionReasonCode =  map.get("actionReasonCode")[0];
        String actionReasonDetail =  map.get("actionReasonDetail")[0];

        LOGGER.info("><><>< userName : {} ",userName);
        LOGGER.info("><><>< actionStatus : {} ",actionStatus);
        LOGGER.info("><><>< documentNumber : {} ",documentNumber);
        LOGGER.info("><><>< docType : {} ",docType);
        LOGGER.info("><><>< documentFlow : {} ",documentFlow);
        LOGGER.info("><><>< processId : {} ",processId);
        LOGGER.info("><><>< actionReasonCode : {} ",actionReasonCode);
        LOGGER.info("><><>< actionReasonDetail : {} ",actionReasonDetail);

        String resultString = null;
        String stateReturn = "";
        String flowActive = "";
        try{
            Map mapApproveFlow = flowManageService.approveDocument(userName,actionStatus,documentNumber,docType,documentFlow,processId);
            LOGGER.info("><><>< mapApproveFlow : {} ",mapApproveFlow);

            if(mapApproveFlow.get("result") != null){
                stateReturn = jbpmFlowService.getCurrentNode(processId);
                flowActive  = jbpmFlowService.getActive(processId);
            }

            LOGGER.info("><><>< STATE FLOW : {} ",stateReturn);
            LOGGER.info("><><>< FLOW ACTIVE : {} ",flowActive);

            resultString = getResultString("/requestCustom/approveRequest?document="+Double.valueOf(documentId).longValue()+"&state="+stateReturn+"&flowActive="+flowActive+"&actionReason="+actionReasonCode+"&actionReasonDetail="+actionReasonDetail);
        }catch (Exception e){
            e.printStackTrace();
        }

        return resultString;
    }

    @Override
    public String approveRequestEmed(Map<String, String[]> map,String action) {
        String userName =  map.get("userName")[0];
        String actionStatus =  map.get("actionStatus")[0];
        String documentNumber =  map.get("documentNumber")[0];
        String docType =  map.get("docType")[0];
        String documentFlow =  map.get("documentFlow")[0];
        String processId =  map.get("processId")[0];
        String documentId =  map.get("documentId")[0];
        String actionReasonCode =  map.get("actionReasonCode")[0];
        String actionReasonDetail =  map.get("actionReasonDetail")[0];

        LOGGER.info("><><>< userName : {} ",userName);
        LOGGER.info("><><>< actionStatus : {} ",actionStatus);
        LOGGER.info("><><>< documentNumber : {} ",documentNumber);
        LOGGER.info("><><>< docType : {} ",docType);
        LOGGER.info("><><>< documentFlow : {} ",documentFlow);
        LOGGER.info("><><>< processId : {} ",processId);
        LOGGER.info("><><>< actionReasonCode : {} ",actionReasonCode);
        LOGGER.info("><><>< actionReasonDetail : {} ",actionReasonDetail);

        try{
            String stateReturn = "";
            String flowActive = "";

            if("paid".equals(action)){
                stateReturn = ApplicationConstant.JBPM_STATE_RETURN_FINANCE;
                flowActive = ApplicationConstant.FLOW_STATUS_INACTIVE;
            }else{
                stateReturn = ApplicationConstant.JBPM_STATE_RETURN_FINANCE;
                flowActive = ApplicationConstant.FLOW_STATUS_ACTIVE;
            }

            resultString = getResultString("/requestCustom/approveRequest?document="+Double.valueOf(documentId).longValue()+"&state="+stateReturn+"&flowActive="+flowActive+"&actionReason="+actionReasonCode+"&actionReasonDetail="+actionReasonDetail);
        }catch (Exception e){
            e.printStackTrace();
        }

        return resultString;
    }


    @Override
    public String approveRequestRedirect(String docNumber,String userNames,String actionState) {

        ResponseEntity<String> documentResult =  documentApproveService.findByDocumentNumber(docNumber);
        LOGGER.info("Document Object : {}",documentResult.getBody());
        Map<String,String> parameterMap = gson.fromJson(String.valueOf(documentResult.getBody()), Map.class);
        LOGGER.info("Document Object Map : {}",parameterMap);

        String userName = userNames ;
        String actionStatus =actionState ;
        String documentNumber = String.valueOf(parameterMap.get("docNumber")) ;
        String docType = String.valueOf(parameterMap.get("documentType")) ;
        String documentFlow = String.valueOf(parameterMap.get("docFlow")) ;
        String processId = String.valueOf(parameterMap.get("processId")) ;
        String documentId = String.valueOf(parameterMap.get("id")) ;

        LOGGER.info("><><>< userName : {} ",userName);
        LOGGER.info("><><>< actionStatus : {} ",actionStatus);
        LOGGER.info("><><>< documentNumber : {} ",documentNumber);
        LOGGER.info("><><>< docType : {} ",docType);
        LOGGER.info("><><>< documentFlow : {} ",documentFlow);
        LOGGER.info("><><>< processId : {} ",processId);
        LOGGER.info("><><>< documentId : {} ",documentId);


        String resultString = null;
        String stateReturn = "";
        String flowActive = "";


        try{
            Map mapApproveFlow = flowManageService.approveDocument(userName,actionStatus,documentNumber,docType,documentFlow,processId);
            LOGGER.info("><><>< mapApproveFlow : {} ",mapApproveFlow);

            if(mapApproveFlow.get("result") != null){
                stateReturn = jbpmFlowService.getCurrentNode(processId);
                flowActive  = jbpmFlowService.getActive(processId);
            }

            LOGGER.info("><><>< STATE FLOW : {} ",stateReturn);
            LOGGER.info("><><>< FLOW ACTIVE : {} ",flowActive);
            LOGGER.info("><><>< DOCUMENT : {} ",Double.valueOf(documentId).longValue());
            LOGGER.info("><><>< STATE : {} ",stateReturn);
            LOGGER.info("><><>< FLOW ACTIVE : {} ",flowActive);

            resultString = getResultString("/requestCustom/approveRequest?document="+Double.valueOf(documentId).longValue()+"&state="+stateReturn+"&flowActive="+flowActive+"&actionReason="+"&actionReasonDetail=");
            resultString ="1";
            LOGGER.info("Return Result : {}","1");
        }catch (Exception e){
            e.printStackTrace();
            resultString ="-1";
            LOGGER.info("Return Result : {}","-1");
            return resultString;
        }

        return resultString;
    }

    @Override
    public String rejectRequestRedirect(String docNumber, String userNames, String actionState) {



        ResponseEntity<String> documentResult =  documentApproveService.findByDocumentNumber(docNumber);
        LOGGER.info("Document Object : {}",documentResult.getBody());
        Map<String,String> parameterMap = gson.fromJson(String.valueOf(documentResult.getBody()), Map.class);
        LOGGER.info("Document Object Map : {}",parameterMap);



        String userName = userNames ;
        String actionStatus =actionState ;
        String documentNumber = String.valueOf(parameterMap.get("docNumber")) ;
        String docType = String.valueOf(parameterMap.get("documentType")) ;
        String documentFlow = String.valueOf(parameterMap.get("docFlow")) ;
        String processId = String.valueOf(parameterMap.get("processId")) ;
        String documentId = String.valueOf(parameterMap.get("id")) ;



//

        LOGGER.info("><><>< userName : {} ",userName);
        LOGGER.info("><><>< actionStatus : {} ",actionStatus);
        LOGGER.info("><><>< documentNumber : {} ",documentNumber);
        LOGGER.info("><><>< docType : {} ",docType);
        LOGGER.info("><><>< documentFlow : {} ",documentFlow);
        LOGGER.info("><><>< processId : {} ",processId);
        LOGGER.info("><><>< documentId : {} ",documentId);


        String resultString = null;
        String stateReturn = "";
        String flowActive = "";


        try{
            Map mapApproveFlow = flowManageService.rejectDocument(userName,actionStatus,documentNumber,docType,documentFlow,processId);


            LOGGER.info("><><>< mapApproveFlow : {} ",mapApproveFlow);

            if(mapApproveFlow.get("result") != null){
                stateReturn = jbpmFlowService.getCurrentNode(processId);
                flowActive  = jbpmFlowService.getActive(processId);
            }

            LOGGER.info("><><>< STATE FLOW : {} ",stateReturn);
            LOGGER.info("><><>< FLOW ACTIVE : {} ",flowActive);
            LOGGER.info("><><>< DOCUMENT : {} ",Double.valueOf(documentId).longValue());
            LOGGER.info("><><>< STATE : {} ",stateReturn);
            LOGGER.info("><><>< FLOW ACTIVE : {} ",flowActive);

            resultString = getResultString("/requestCustom/rejectRequest?document="+Double.valueOf(documentId).longValue()+"&state="+stateReturn+"&flowActive="+flowActive+"&actionReason="+"&actionReasonDetail=");

            resultString ="1";
            LOGGER.info("Return Result : {}","1");
        }catch (Exception e){
            e.printStackTrace();
            resultString ="-1";
            LOGGER.info("Return Result : {}","-1");
            return resultString;
        }

        return resultString;
    }


    @Override
    public String rejectRequest(Map<String, String[]> map) {
        String userName =  map.get("userName")[0];
        String actionStatus =  map.get("actionStatus")[0];
        String documentNumber =  map.get("documentNumber")[0];
        String docType =  map.get("docType")[0];
        String documentFlow =  map.get("documentFlow")[0];
        String processId =  map.get("processId")[0];
        String documentId =  map.get("documentId")[0];
        String actionReasonCode =  map.get("actionReasonCode")[0];
        String actionReasonDetail =  map.get("actionReasonDetail")[0];




        LOGGER.info("><><>< userName : {} ",userName);
        LOGGER.info("><><>< actionStatus : {} ",actionStatus);
        LOGGER.info("><><>< documentNumber : {} ",documentNumber);
        LOGGER.info("><><>< docType : {} ",docType);
        LOGGER.info("><><>< documentFlow : {} ",documentFlow);
        LOGGER.info("><><>< processId : {} ",processId);
        LOGGER.info("><><>< documentId : {} ",documentId);

        String resultString = null;
        String stateReturn = "";
        String flowActive = "";
        try{
            LOGGER.info("><><>< REJECT REQUEST SERVICE ><><><><");
            Map mapApproveFlow = flowManageService.rejectDocument(userName,actionStatus,documentNumber,docType,documentFlow,processId);

            LOGGER.info("><><><  mapApproveFlow : {} ",mapApproveFlow);
            if(mapApproveFlow != null && mapApproveFlow.get("result") != null){
                stateReturn = jbpmFlowService.getCurrentNode(processId);
                flowActive  = jbpmFlowService.getActive(processId);
            }

            LOGGER.info("><><><  stateReturn : {} ",stateReturn);
            LOGGER.info("><><><  flowActive : {} ",flowActive);

            resultString = getResultString("/requestCustom/rejectRequest?document="+Double.valueOf(documentId).longValue()+"&state="+stateReturn+"&flowActive="+flowActive+"&actionReason="+actionReasonCode+"&actionReasonDetail="+actionReasonDetail);
        }catch (Exception e){
            e.printStackTrace();
        }

        return resultString;
    }

    @Override
    public String rejectRequestEmed(Map<String, String[]> map) {
        String userName =  map.get("userName")[0];
        String actionStatus =  map.get("actionStatus")[0];
        String documentNumber =  map.get("documentNumber")[0];
        String docType =  map.get("docType")[0];
        String documentFlow =  map.get("documentFlow")[0];
        String processId =  map.get("processId")[0];
        String documentId =  map.get("documentId")[0];
        String actionReasonCode =  map.get("actionReasonCode")[0];
        String actionReasonDetail =  map.get("actionReasonDetail")[0];


        LOGGER.info("><><>< userName : {} ",userName);
        LOGGER.info("><><>< actionStatus : {} ",actionStatus);
        LOGGER.info("><><>< documentNumber : {} ",documentNumber);
        LOGGER.info("><><>< docType : {} ",docType);
        LOGGER.info("><><>< documentFlow : {} ",documentFlow);
        LOGGER.info("><><>< processId : {} ",processId);
        LOGGER.info("><><>< documentId : {} ",documentId);

        String resultString = null;
        String stateReturn = "";
        String flowActive = "";
        try{
            LOGGER.info("><><>< REJECT REQUEST SERVICE ><><><><");

            stateReturn = ApplicationConstant.JBPM_STATE_RETURN_ACCOUNT;
            flowActive  = ApplicationConstant.FLOW_STATUS_INACTIVE;

            resultString = getResultString("/requestCustom/rejectRequest?document="+Double.valueOf(documentId).longValue()+"&state="+stateReturn+"&flowActive="+flowActive+"&actionReason="+actionReasonCode+"&actionReasonDetail="+actionReasonDetail);
        }catch (Exception e){
            e.printStackTrace();
        }

        return resultString;
    }

    @Override
    public ResponseEntity<String> findById(Long id) {
        String method = "findById";
        String url = "/requests"+"/search/"+method+"?id="+id;
        return getResultByExchange(url);
    }

    @Override
    public ResponseEntity<String> findByDocumentType(String documentType) {
        String method = "findByDocumentType";
        String url = "/requests"+"/search/"+method+"?documentType="+documentType;
        return getResultByExchange(url);
    }

    @Override
    @SuppressWarnings("Duplicates")
    public ResponseEntity<String> findIncomingRequestExpense(String nextApprover,String requester,String docNumber) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByNextApproverInAndDocument_RequesterIgnoreCaseContainingAndDocument_DocNumberIgnoreCaseContainingAndDocument_DocumentStatusOrderByDocument_SendDateDesc";
        String url = "/requests"+"/search/"+method+"?nextApprover="+nextApprover+"&requester="+requester+"&docNumber="+docNumber+"&doc_status=ONP&projection=haveDoc";
        return getResultString(url,entity);
    }

    @Override
    @SuppressWarnings("Duplicates")
    public ResponseEntity<String> findIncomingRequestApprove(String nextApprover, String requester, String docNumber) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "findByNextApproverInAndDocument_RequesterIgnoreCaseContainingAndDocument_DocNumberIgnoreCaseContainingAndDocument_DocumentStatusOrderByDocument_SendDateDesc";
        String url = "/requests"+"/search/"+method+"?nextApprover="+nextApprover+"&requester="+requester+"&docNumber="+docNumber+"&doc_status=ONP&projection=haveDoc";
        return getResultString(url,entity);
    }

    @Override
    public String terminateRequest(Map<String, String[]> map) {
        LOGGER.error("TERMINATE REQUEST SERVICE");
        String employeeStr =  map.get("requester")[0];
        String docNumber =  map.get("docNumber")[0];
        String documentId =  map.get("id")[0];
        String resultString = null;
        try{
            LOGGER.info("><><>< employeeStr : {} ",employeeStr);
            LOGGER.info("><><>< docNumber : {} ",docNumber);
            LOGGER.info("><><>< documentId : {} ",documentId);
            try {
                Map mapCancelFlow = flowManageService.cancelDocument(employeeStr,docNumber);
                LOGGER.info("><><>< Terminate Flow : {} ",mapCancelFlow);
                resultString = "success";
            }catch (Exception e){
                resultString = "error";
                e.printStackTrace();
            }
        }catch (Exception e){
            resultString = "error";
            e.printStackTrace();
            LOGGER.error("ERROR TERMINATE REQUEST SERVICE");
        }

        return resultString;
    }

    @Override
    public String resendFlow(Map<String, String[]> map) {
        String employeeStr =  map.get("requester")[0];
        String approveType =  map.get("approveType")[0];
        String papsa = map.get("pa")[0]+"-"+map.get("psa")[0];
        String expenseAmountStr = "";
        String resultString = null;
        try{
            Map<String, String> mapDetail = new HashMap<String, String>();
            applyRelateEntity("details",map,mapDetail);

            List<Map> listDetail = gson.fromJson(mapDetail.get("details"),List.class);

            for (int i = 0; i < listDetail.size(); i++) {
                if(i == listDetail.size()-1){
                    expenseAmountStr += String.valueOf(listDetail.get(i).get("flowType"))+";"+String.valueOf(listDetail.get(i).get("amount"))+";"+String.valueOf(listDetail.get(i).get("costCenter"));
                }else{
                    expenseAmountStr += String.valueOf(listDetail.get(i).get("flowType"))+";"+String.valueOf(listDetail.get(i).get("amount"))+";"+String.valueOf(listDetail.get(i).get("costCenter"))+"#";
                }
            }

            String document = getResultString("/documents/"+map.get("id")[0]);
            Map documentMap = gson.fromJson(document,Map.class);

            try {
                Map mapCreateFlow = flowManageService.createDocumentFlow(ApplicationConstant.DOC_TYPE_TO_FLOW_DOC_TYPE.get(String.valueOf(documentMap.get("documentType"))) ,employeeStr,expenseAmountStr,documentMap.get("documentType").toString(),documentMap.get("tmpDocNumber").toString(),approveType,papsa);
                String documentFlow = mapCreateFlow.get("documentFlow").toString();
                String processId = mapCreateFlow.get("processId").toString();
                String lineApprove = gson.toJson(mapCreateFlow.get("lineApprove"),List.class);
                LOGGER.info("============ lineApprove      :     {}",lineApprove);
                resultString = getResultString("/requestCustom/resendFlow?document="+Double.valueOf(documentMap.get("id").toString()).longValue()+"&processId="+processId+"&docFlow="+documentFlow+"&lineApprove={lineApprove}",lineApprove);
            }catch (Exception e){
                e.printStackTrace();
                LOGGER.error("ERROR RESEND FLOW SERVICE");
            }

        }catch (Exception e){
            e.printStackTrace();
            LOGGER.error("ERROR RESEND FLOW SERVICE");
        }

        return resultString;
    }
}
