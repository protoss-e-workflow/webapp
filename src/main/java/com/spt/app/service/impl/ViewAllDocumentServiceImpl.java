package com.spt.app.service.impl;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.ViewAllDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service("ViewAllDocumentService")
public class ViewAllDocumentServiceImpl extends AbstractEngineService implements BaseCommonService,ViewAllDocumentService{


    @Override
    @Autowired
    @SuppressWarnings("All")
    public void setRestTemplate(RestTemplate restTemplate) {
        super.restTemplate = restTemplate;
    }

    @Override
    @SuppressWarnings("Duplicates")
    public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
            try {
                queryString = URLDecoder.decode(queryString, "UTF-8");
            } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
//            queryString = queryString.replaceAll("%2C", ",");
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "";
        String url ="/documentCustom/viewDocAdminSize" +method+"?"+queryString;
        return getResultString(url,entity);
//        return null;
    }

    @Override
    @SuppressWarnings("Duplicates")
    public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
            queryString = String.valueOf(objectMap.get("queryString"));
            try {
                queryString = URLDecoder.decode(queryString, "UTF-8");
            } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
//            queryString = queryString.replaceAll("%2C", ",");
        }

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "";
        String url = "/documentCustom/viewDocAdminSize"+method+"?"+queryString;
        LOGGER.info("url="+url);
        return getResultString(url,entity);
//        return null;
    }

    @Override
    public ResponseEntity<String> save(Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> savePut(Long id, Map parameter) {
        return null;
    }

    @Override
    public ResponseEntity<String> delete(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> get(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<String> load() {
        return null;
    }

    @Override
    public ResponseEntity<String> findByCreatedDateStartAndCreatedDateEndAndCreatorAndDocNumberAndDocumentType(
            String createdDateStart, String createdDateEnd, String creator, String documentNumber, String documentType,String documentStatus, Long first, Long max) {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        LOGGER.info("document all service : {}",documentStatus);

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "startDate=" + createdDateStart + "&endDate=" + createdDateEnd + "&creator=" + creator
                + "&documentNumber=" + documentNumber
                + "&documentType=" + documentType
                + "&documentStatus=" + documentStatus
                + "&firstResult=" + first + "&maxResult=" + max;
        String url = "/documentCustom/viewDocAdmin?" + method;

        LOGGER.info("URL : {}",url);

        return getResultString(url,entity);
    }

    @Override
    public ResponseEntity<String> pagingFindByCreatedDateStartAndCreatedDateEndAndCreatorAndDocNumberAndDocumentType(
            String createdDateStart, String createdDateEnd, String creator, String documentNumber, String documentType,String documentStatus) {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String method = "startDate=" + createdDateStart + "&endDate=" + createdDateEnd + "&creator=" + creator
                + "&documentNumber=" + documentNumber
                + "&documentType=" + documentType
                + "&documentStatus=" + documentStatus;
        String url = "/documentCustom/viewDocAdminSize?" + method;

        LOGGER.info("URL : {}",url);

        return getResultString(url,entity);
    }
}
