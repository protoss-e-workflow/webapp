package com.spt.app.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.spt.app.controller.general.SecurityController;
import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.BaseCodeService;
import com.spt.app.service.BaseCommonService;
import com.spt.app.service.MenuService;

@Service("MenuService")
public class MenuServiceImpl extends AbstractEngineService implements BaseCommonService,BaseCodeService,MenuService {

	static final Logger LOGGER = LoggerFactory.getLogger(MenuServiceImpl.class);
	
	private String urlBase = "menus";
	
	@Override
	public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        }
        
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/"+urlBase+"?size=10000&"+queryString;
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> findSize(Map<String, Object> objectMap) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        }
        
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/"+urlBase+"?page=0&size=1&"+queryString;
        
        LOGGER.info("url="+url);
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> save(Map parameter) {

		String url = "/"+urlBase+"";
		return postWithJson(parameter, HttpMethod.POST, url);
	}

	@Override
	public ResponseEntity<String> delete(Long id) {

		String url = "/"+urlBase+"/"+id;
		return deleteSend(HttpMethod.DELETE, url);
	}

	@Override
	public ResponseEntity<String> get(Long id) {

		String url = "/"+urlBase+"/"+id;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> load() {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/"+urlBase+"";
        
        LOGGER.info("url="+url);
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> savePut(Long id,Map parameter) {

		String url = "/"+urlBase+"/"+id;
		return putWithJson(parameter, HttpMethod.PUT, url);
	}
	

	@Override
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {

		super.restTemplate = restTemplate;
	}

	@Override
	public ResponseEntity<String> findByCode(String code) {


				String method = "findByCode";
			    String url = "/"+urlBase+"/search/"+method+"?code="+code;
				return getResultByExchange(url);
	}

	@Override
	public ResponseEntity<String> putRole(Long id, List<String> associateIdLs) {

		String url = "/"+urlBase+"/"+id;
		String associatePath = "/roles";
		return putAssociate(associateIdLs,url,associatePath);
	}

	@Override
	public ResponseEntity<String> deleteRole(Long id, List<String> associateIdLs) {

		String url = "/"+urlBase+"/"+id;
		String associatePath = "/roles/";
		return deleteAssociate(associateIdLs,url,associatePath);
	}

	@Override
	public ResponseEntity<String> deleteAllRole(Long id) {

		String url = "/"+urlBase+"/"+id;
		String associatePath = "/roles";
		return deleteAllAssociate(url,associatePath);
	}

}
