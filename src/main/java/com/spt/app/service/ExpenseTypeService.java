package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.Map;

/**
 * Created by korrakote on 8/28/17.
 */
public interface ExpenseTypeService {


    public ResponseEntity<String> findByCode(String code);
    public ResponseEntity<String> findExpenseTypeByIdAndExpenseTypeReferencesCompleteReason(String completeReason,
                                                                                            Long expenseType);
    public ResponseEntity<String> findExpenseTypeByIdAndExpenseTypeFileAttachmentCode(String attachmentTypeCode,
                                                                                            Long expenseType);

    public ResponseEntity<String> findExpenseTypeByIdAndReimburseRoleCode(String roleCode,
                                                                          Long expenseType);


    public ResponseEntity<String> findExpenseTypeByExpenseTypeScreenStructureField(String structureField,
                                                                          Long expenseType);


    public ResponseEntity<String> findExpenseTypeByIdAndExpenseTypeCompanyPaAndExpenseTypeCompanyPsaAndExpenseTypeCompanyGlCode(
                                                                                                                               String pa,
                                                                                                                               String psa,
                                                                                                                               String glCode,
                                                                                                                                Long expenseType);
    public ResponseEntity<String> addExpenseTypeReference(Map<String, String[]> mapClone);
    public ResponseEntity<String> addExpenseTypeByCompany(Map<String, String[]> mapClone);
    public ResponseEntity<String> addReimburse(Map<String, String[]> mapClone);
    public ResponseEntity<String> addExpenseTypeScreen(Map<String, String[]> mapClone);
    public ResponseEntity<String> addExpenseTypeFile(Map<String, String[]> mapClone);


    public ResponseEntity<String> findAllExpenseType();
    public ResponseEntity<String> findByFlowType(String flowType);

}
