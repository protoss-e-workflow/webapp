package com.spt.app.service;

import org.springframework.http.ResponseEntity;

public interface DocumentAccountRemarkService {

    public ResponseEntity<String> findByDocumentId(String docId);
    public ResponseEntity<String> sendEmail(String docId);
}
