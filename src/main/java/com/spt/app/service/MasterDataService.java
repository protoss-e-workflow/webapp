package com.spt.app.service;

import java.util.List;
import java.util.Map;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

public interface MasterDataService {
	
	public ResponseEntity<String> findByMasterDataByCodeByOrgCode(Long id,String code,String orgCode);
	public List<Map> findByMasterCodeIn(String code);
	public ResponseEntity<String> findMasterDataDetailByMasterdataCode();
	public ResponseEntity<String> findSequenceNumberApproveTypeInMasterDataDetail(String detailCode);
	public ResponseEntity<String> findMasterDataDetailApproveTypeByCode(String code);
	public ResponseEntity<String> findMasterDataDetailOfAttachmentTypeAll();
	public ResponseEntity<String> findMasterDataDetailAttachmentTypeByCode(String code);
	public ResponseEntity<String> findByMasterdataInOrderByCode(String masterdata);
	public ResponseEntity<String> findByMasterdataInAndMasterDataDetailCodeOrderByCode(String masterdata,String code);
	public ResponseEntity<String> findByMasterdataInAndMasterDataDetailCodeOrderByCodeList(String masterdata,String code);
	public ResponseEntity<String> findByMasterdataInAndMasterDataDetailCodeLikeOrderByCode(String masterdata,String code);
	public List<Map> findAutoCompleteHotelByKeySearch(String code);
	public ResponseEntity<String> findByVariable1(String userHead);

}
