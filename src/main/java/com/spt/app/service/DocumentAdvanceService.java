package com.spt.app.service;


import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.Map;

public interface DocumentAdvanceService extends DocumentService{

    /* Manager Document Attachment */
    public ResponseEntity<String> uploadFile(MultipartHttpServletRequest multipartHttpServletRequest, String url);
    public ResponseEntity<String> saveSapManual(MultipartHttpServletRequest multipartHttpServletRequest, String url);
    public ResponseEntity<String> findDocumentAttachmentByDocumentId(Long id);
    public ResponseEntity<String> deleteDocumentAttachment(Long id);

    /* Manage Document Reference */
    public ResponseEntity<String> saveDocumentReference(Map<String,String[]> jsonDocument);
    public ResponseEntity<String> updateDocumentReference(Map<String,String[]> jsonDocument);
    public String getAuthorizeForLineApprove(Map<String,String[]> map);
    public ResponseEntity<String> updateDocumentAdvance(Map<String,String[]> jsonDocument);
    public ResponseEntity<String> advanceTransfer(Map<String,String[]> jsonDocument);
    public ResponseEntity<String> unlockAdvance(Map<String,String[]> jsonDocument);

    public ResponseEntity<String> findByDocTypeAndDocStatusCompleteAndUser(Map<String,String[]> jsonDocument);
    public ResponseEntity<String> findByDocTypeAndDocNumberAndDocStatusCompleteAndUser(Map<String,String[]> jsonDocument);
}
