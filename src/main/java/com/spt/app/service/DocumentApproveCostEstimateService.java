package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface DocumentApproveCostEstimateService {

    public ResponseEntity<String> findByDocumentApprove(String documentApprove);
    public ResponseEntity<String> saveDocumentCostEstimate(Map<String,String[]> jsonDocument);
    public ResponseEntity<String> updateDocumentCostEstimate(Map<String,String[]> jsonDocument);
    public ResponseEntity<String> deleteCostEstimate(Long id);
}
