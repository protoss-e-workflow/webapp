package com.spt.app.service;

import org.springframework.http.ResponseEntity;

public interface AdvanceOutstandingService {

    ResponseEntity<String> findAdvanceOutstanding(String venderNo,String userName);
}
