package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.Map;

/**
 * Created by korrakote on 8/28/17.
 */
public interface EmployeeService {


    public ResponseEntity<String> findByEmpCode(String empCode);
    public ResponseEntity<String> findAllEmployee();
    public ResponseEntity<String> getEmployeeForAutoComplete(String keySearch);
    public ResponseEntity<String> findByEmpUsername(String empUsername);
}
