package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface PerDiemProcessCalculateService {

    public ResponseEntity<String> processCalculatePerDiemForeign(Map<String,String[]> jsonDocument);
    public ResponseEntity<String> processCalculatePerDiemDomestic(Map<String,String[]> jsonDocument);
}
