package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface DocumentExpenseItemService {

    ResponseEntity<String> findDocumentExpenseItemByDocumentExp(String id);
    public byte[] downloadFileDocumentAttachment(String url);
    public ResponseEntity<String> findDocumentAmountSeperateByType();



    ResponseEntity<String>  findAllDocumentExceptDocStatusCCLAndREJ(String dateStart,
                                                                    String dateEnd,
                                                                    String requester,
                                                                    String docNumber,
                                                                    Integer first,
                                                                    Integer max);


    ResponseEntity<String>  findAllDocumentExceptDocStatusCCLAndREJSize(String dateStart,
                                                                        String dateEnd,
                                                                        String requester,
                                                                        String docNumber
                                                                       );




}
