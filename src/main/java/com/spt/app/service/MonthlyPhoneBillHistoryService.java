package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Map;

public interface MonthlyPhoneBillHistoryService {
	
	public ResponseEntity<String>  findByEmpCodeIgnoreCaseContainingAndMonthIgnoreCaseContainingAndYearIgnoreCaseContaining(String empCode, String month, String year);
	public ResponseEntity<String> saveMonthlyPhoneBillHistoryForExpense(Map<String,String[]> jsonDocument);
	public ResponseEntity<String> deleteMonthlyPhoneBillHistoryForExpense(Map<String,String[]> jsonDocument);
	public ResponseEntity<String> updateMonthlyPhoneBillHistoryForExpense(Map<String,String[]> jsonDocument);
}
