package com.spt.app.service;

import java.util.List;

import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

public interface EmplyeeProfileService {
	
	public ResponseEntity<String>   findEmployeeProfileByEmployeeCode(String employeeCode);
	public ResponseEntity<String>   findEmployeeProfileByKeySearch(String keySearch);
	public ResponseEntity<String>   findEmployeeProfileByKeySearchUserIn(String keySearch,String listEmplyee);
	public ResponseEntity<String>   findAutoCompleteEmployeeByKeySearch(String keySearch);
	public ResponseEntity<String>   findAutoCompleteEmployeeByKeySearchUserIn(String keySearch,String userName);
	public ResponseEntity<String>   findEmployeeProfileByCostCenter(String costCenter);
	public ResponseEntity<String>   findMaxLineByCost(String costCenter);
	public ResponseEntity<String>   findMaxLineByCostWithCom(String costCenter);
	public ResponseEntity<String>   getApproveLine(String userName);
	public ResponseEntity<String>   findByPa(String pa);
	public ResponseEntity<String>   findByPsa(String psa);
	public ResponseEntity<String>   findAdminByPapsaAndRequester(String papsa,String requester);
	public ResponseEntity<String>   findAccountByPapsaAndRequester(String papsa,String requester);
	public ResponseEntity<String>   getBankAccount(String requester);
	public String   findAdminByPapsaAndCca(String papsa,String cca);
	public String   findAccountByPapsaAndCca(String papsa,String cca);
	public String   findHrByPapsaAndCca(String papsa,String cca,String employeeStr);
	public String   findFinanceByPapsaAndCca(String papsa,String cca);
}
