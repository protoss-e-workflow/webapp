package com.spt.app.service;

import org.springframework.http.ResponseEntity;

/**
 * Created by korrakote on 8/28/17.
 */
public interface ExpenseTypeFileService {

    public ResponseEntity<String> findByAttachmentTypeCode(String code);


}
