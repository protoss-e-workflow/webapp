package com.spt.app.service;

import org.springframework.http.ResponseEntity;

import java.util.Map;

public interface RequestService {

    String createRequest(Map<String,String[]> map);
    String cancelRequest(Map<String,String[]> map);
    String approveRequest(Map<String,String[]> map);
    String approveRequestEmed(Map<String,String[]> map,String action);
    String rejectRequest(Map<String,String[]> map);
    String rejectRequestEmed(Map<String,String[]> map);
    String terminateRequest(Map<String,String[]> map);
    String resendFlow(Map<String,String[]> map);
    String approveRequestRedirect(String docNumber,String userName,String actionState);
    String rejectRequestRedirect(String docNumber,String userName,String actionState);
//    ResponseEntity<String> createRequest(Map<String,String[]> map);

    public ResponseEntity<String> findById(Long id);
    public ResponseEntity<String> findByDocumentType(String documentType);
    public ResponseEntity<String> findIncomingRequestExpense(String nextApprover,String requester,String docNumber);
    public ResponseEntity<String> findIncomingRequestApprove(String nextApprover,String requester,String docNumber);

}
