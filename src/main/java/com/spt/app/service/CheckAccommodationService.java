package com.spt.app.service;

import org.springframework.http.ResponseEntity;

public interface CheckAccommodationService {

    public ResponseEntity<String> checkAccommodationAuthorize(String docNumber,String itemId,String isIbisStd,String zoneType,String diffDays);
}
