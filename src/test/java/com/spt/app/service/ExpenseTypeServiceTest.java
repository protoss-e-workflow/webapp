package com.spt.app.service;

import com.google.common.reflect.TypeToken;
import com.google.gson.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest

public class ExpenseTypeServiceTest {
	
	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();





	
	static final Logger LOGGER = LoggerFactory.getLogger(ExpenseTypeServiceTest.class);
	
	@Autowired
	ExpenseTypeService expenseTypeService;

	@Qualifier("ExpenseTypeService")
	@Autowired
	BaseCommonService baseCommonService;

	@Qualifier("ExpenseTypeFileService")
	@Autowired
	BaseCommonService baseCommonService_File;


	@Qualifier("ExpenseTypeReferenceService")
	@Autowired
	BaseCommonService baseCommonService_Ref;

	@Qualifier("ReimburseService")
	@Autowired
	BaseCommonService reimburseService;



	@Qualifier("ExpenseTypeScreenService")
	@Autowired
	BaseCommonService baseCommonService_Screen;








	@Test
	public void findExpenseTypeByCode_CaseFound() {
		ResponseEntity<String> result = expenseTypeService.findByCode("EX0001");
		Map userMap = gson.fromJson(result.getBody(), Map.class);
		assertNotNull(userMap);
		assertEquals(userMap.get("description"),"ค่าใช้จ่ายจัดประชุม");
	}


	@Test(expected = HttpClientErrorException.class)
	public void findExpenseTypeByCode_CaseNotFound() {
		ResponseEntity<String> result = expenseTypeService.findByCode("EX00011");
//		Map userMap = gson.fromJson(result.getBody(), Map.class);


	}


	@Test
	public void findExpenseTypeByIdAndExpenseTypeReferencesCompleteReason_CaseFound() {
		ResponseEntity<String> result = expenseTypeService.findExpenseTypeByIdAndExpenseTypeReferencesCompleteReason("0002",Long.valueOf("2"));
		Map userMap = gson.fromJson(result.getBody(), Map.class);
		assertNotNull(userMap);
		assertEquals(userMap.get("code"),"EX002");
		assertEquals(userMap.get("flowType"),"F002");
	}


	@Test(expected = HttpClientErrorException.class)
	public void findExpenseTypeByIdAndExpenseTypeReferencesCompleteReason_CaseNotFoundFound() {
		ResponseEntity<String> result = expenseTypeService.findExpenseTypeByIdAndExpenseTypeReferencesCompleteReason("999900029999",Long.valueOf("1"));
//		Map userMap = gson.fromJson(result.getBody(), Map.class);


	}


	@Test
	public void findExpenseTypeByIdAndExpenseTypeFileAttachmentCode_CaseFound() {
		ResponseEntity<String> result = expenseTypeService.findExpenseTypeByIdAndExpenseTypeFileAttachmentCode("M151",Long.valueOf("2"));
		Map userMap = gson.fromJson(result.getBody(), Map.class);
		assertNotNull(userMap);
		assertEquals(userMap.get("code"),"EX002");
		assertEquals(userMap.get("flowType"),"F002");
	}


	@Test(expected = HttpClientErrorException.class)
	public void findExpenseTypeByIdAndExpenseTypeFileAttachmentCode_CaseFound_CaseNotFoundFound() {
		ResponseEntity<String> result = expenseTypeService.findExpenseTypeByIdAndExpenseTypeFileAttachmentCode("M151AABC",Long.valueOf("1"));
//		Map userMap = gson.fromJson(result.getBody(), Map.class);


	}


	@Test
	public void findExpenseTypeByIdAndReimburseRoleCode_CaseFound() {
		ResponseEntity<String> result = expenseTypeService.findExpenseTypeByIdAndReimburseRoleCode("SUPER_ADMIN",Long.valueOf("2"));
		Map userMap = gson.fromJson(result.getBody(), Map.class);
		assertNotNull(userMap);
		assertEquals(userMap.get("code"),"EX002");
		assertEquals(userMap.get("flowType"),"F002");
	}



	@Test(expected = HttpClientErrorException.class)
	public void findExpenseTypeByIdAndReimburseRoleCode_CaseFound_CaseNotFoundFound() {
		ResponseEntity<String> result = expenseTypeService.findExpenseTypeByIdAndReimburseRoleCode("SUPER_ADMIN_SS_OK",Long.valueOf("1"));
//		Map userMap = gson.fromJson(result.getBody(), Map.class);


	}


	@Test
	public void findExpenseTypeByIdAndExpenseTypeCompanyPaAndExpenseTypeCompanyPsaAndExpenseTypeCompanyGlCode_CaseFound() {
		ResponseEntity<String> result = expenseTypeService.findExpenseTypeByIdAndExpenseTypeCompanyPaAndExpenseTypeCompanyPsaAndExpenseTypeCompanyGlCode("202020","101010","303030",Long.valueOf("2"));
		Map userMap = gson.fromJson(result.getBody(), Map.class);
		assertNotNull(userMap);
		assertEquals(userMap.get("code"),"EX002");
		assertEquals(userMap.get("flowType"),"F002");
	}



	@Test(expected = HttpClientErrorException.class)
	public void findExpenseTypeByIdAndExpenseTypeCompanyPaAndExpenseTypeCompanyPsaAndExpenseTypeCompanyGlCode_CaseFound_CaseNotFoundFound() {
		ResponseEntity<String> result = expenseTypeService.findExpenseTypeByIdAndExpenseTypeCompanyPaAndExpenseTypeCompanyPsaAndExpenseTypeCompanyGlCode("123456","123456","123456",Long.valueOf("2"));
//		Map userMap = gson.fromJson(result.getBody(), Map.class);


	}


	@Test
	public void findAllExpenseType() {
		ResponseEntity<String> result = expenseTypeService.findAllExpenseType();
		Map userMap = gson.fromJson(result.getBody(), Map.class);
		assertNotNull(userMap);
		List<Map> listData = (List<Map>) userMap.get("content");

		assertTrue(listData.size()>0);
	}


	@Test
	public void findByFlowType_CaseFound() {
		ResponseEntity<String> result = expenseTypeService.findByFlowType("F002");
		Map userMap = gson.fromJson(result.getBody(), Map.class);
		assertNotNull(userMap);
		assertEquals(userMap.get("code"),"EX002");
		assertEquals(userMap.get("flowType"),"F002");
	}

	@Test(expected = HttpClientErrorException.class)	public void findByFlowType_CaseNotFound() {
		ResponseEntity<String> result = expenseTypeService.findByFlowType("FFF01002");
		Map userMap = gson.fromJson(result.getBody(), Map.class);

	}



	@Test
	public void findByCriteria_CaseFound() {

		Map<String, Object> objectMap = new HashMap<>();
		objectMap.put("queryString","&description=");



		ResponseEntity<String> result = baseCommonService.findByCriteria(objectMap);
		Map userMap = gson.fromJson(result.getBody(), Map.class);
		List<Map> listData = (List<Map>) userMap.get("content");
		assertNotNull(userMap);
		assertTrue(listData.size()>0);
//		assertEquals(userMap.get("code"),"EX002");
//		assertEquals(userMap.get("flowType"),"F002");
	}




	@Test
	public void findSize_CaseFound() {

		Map<String, Object> objectMap = new HashMap<>();
		objectMap.put("queryString","&description=");



		ResponseEntity<String> result = baseCommonService.findSize(objectMap);
		Map userMap = gson.fromJson(result.getBody(), Map.class);
		List<Map> listData = (List<Map>) userMap.get("content");
		assertNotNull(userMap);
		assertTrue(listData.size()>0);
//		assertEquals(userMap.get("code"),"EX002");
//		assertEquals(userMap.get("flowType"),"F002");
	}



	@Test
	public void loadAllData_CaseFound() {

		Map<String, Object> objectMap = new HashMap<>();
		objectMap.put("queryString","&description=");



		ResponseEntity<String> result = baseCommonService.load();
		Map userMap = gson.fromJson(result.getBody(), Map.class);
		List<Map> listData = (List<Map>) userMap.get("content");
		assertNotNull(userMap);
		assertTrue(listData.size()>0);





	}



	@Test
	public void addExpenseTypeFile() {


		Map<String, String[]> dataSend = new HashMap<>();

		String[] id = {""};
		String[] attachmentTypeCode = {"M151"};
		String[] require = {""};
		String[] expenseType = {"2"};
		String[] createdBy = {""};
		String[] createdDate = {""};
		String[] updateBy = {""};
		String[] updateDate = {""};
		String[] parentId = {"1"};
		String[] csrfToken = {"720f92e8-cd08-4db9-83a6-fe851b05198b"};

		dataSend.put("id", id);
		dataSend.put("parentId", parentId);
		dataSend.put("csrfToken", csrfToken);
		dataSend.put("attachmentTypeCode", attachmentTypeCode);
		dataSend.put("require", require);
		dataSend.put("expenseType", expenseType);
		dataSend.put("createdBy", createdBy);
		dataSend.put("createdDate", createdDate);
		dataSend.put("updateBy", updateBy);
		dataSend.put("updateDate", updateDate);






		ResponseEntity<String> result = expenseTypeService.addExpenseTypeFile(dataSend);


		Map userMap = gson.fromJson(result.getBody(), Map.class);
		assertNotNull(userMap);
		assertEquals(  "["+String.valueOf(userMap.get("attachmentTypeCode"))+"]"  , (Arrays.toString( dataSend.get("attachmentTypeCode"))).toString());




		Double deleteId_tmp = Double.valueOf(String.valueOf(userMap.get("id")));
		Long idDelete = Math.round(deleteId_tmp);
		baseCommonService_File.delete(idDelete);

	}




	@Test
	public void addExpenseTypeScreen() {


		Map<String, String[]> dataSend = new HashMap<>();

		String[] id = {""};
		String[] nameTH = {"หน้าจอ"};
		String[] nameENG = {"screen"};
		String[] type = {"String"};
		String[] structureField = {"value1"};
		String[] require = {""};
		String[] expenseType = {"2"};
		String[] createdBy = {""};
		String[] createdDate = {""};
		String[] updateBy = {""};
		String[] updateDate = {""};
		String[] parentId = {"1"};
		String[] csrfToken = {"720f92e8-cd08-4db9-83a6-fe851b05198b"};

		dataSend.put("id", id);
		dataSend.put("parentId", parentId);
		dataSend.put("csrfToken", csrfToken);
		dataSend.put("nameTH", nameTH);
		dataSend.put("nameENG", nameENG);
		dataSend.put("type", type);
		dataSend.put("structureField", structureField);
		dataSend.put("require", require);
		dataSend.put("expenseType", expenseType);
		dataSend.put("createdBy", createdBy);
		dataSend.put("createdDate", createdDate);
		dataSend.put("updateBy", updateBy);
		dataSend.put("updateDate", updateDate);






		ResponseEntity<String> result = expenseTypeService.addExpenseTypeScreen(dataSend);


		Map userMap = gson.fromJson(result.getBody(), Map.class);
		assertNotNull(userMap);
		assertEquals(  "["+String.valueOf(userMap.get("nameTH"))+"]"  , (Arrays.toString( dataSend.get("nameTH"))).toString());
		assertEquals(  "["+String.valueOf(userMap.get("nameENG"))+"]"  , (Arrays.toString( dataSend.get("nameENG"))).toString());
		assertEquals(  "["+String.valueOf(userMap.get("type"))+"]"  , (Arrays.toString( dataSend.get("type"))).toString());
		assertEquals(  "["+String.valueOf(userMap.get("structureField"))+"]"  , (Arrays.toString( dataSend.get("structureField"))).toString());



			Double deleteId_tmp = Double.valueOf(String.valueOf(userMap.get("id")));
	Long idDelete = Math.round(deleteId_tmp);
		baseCommonService_Screen.delete(idDelete);

	}


	@Test
	public void addReimburse() {


		Map<String, String[]> dataSend = new HashMap<>();

		String[] id = {""};
		String[] roleCode = {"SUPER_ADMIN"};
		String[] expenseType = {"2"};
		String[] createdBy = {""};
		String[] createdDate = {""};
		String[] updateBy = {""};
		String[] updateDate = {""};
		String[] parentId = {"1"};
		String[] csrfToken = {"720f92e8-cd08-4db9-83a6-fe851b05198b"};

		dataSend.put("id", id);
		dataSend.put("parentId", parentId);
		dataSend.put("csrfToken", csrfToken);
		dataSend.put("roleCode", roleCode);
		dataSend.put("expenseType", expenseType);
		dataSend.put("createdBy", createdBy);
		dataSend.put("createdDate", createdDate);
		dataSend.put("updateBy", updateBy);
		dataSend.put("updateDate", updateDate);






		ResponseEntity<String> result = expenseTypeService.addReimburse(dataSend);


		Map userMap = gson.fromJson(result.getBody(), Map.class);
		assertNotNull(userMap);
		assertEquals(  "["+String.valueOf(userMap.get("roleCode"))+"]"  , (Arrays.toString( dataSend.get("roleCode"))).toString());


			Double deleteId_tmp = Double.valueOf(String.valueOf(userMap.get("id")));
	Long idDelete = Math.round(deleteId_tmp);
		reimburseService.delete(idDelete);

	}


	@Test
	public void addExpenseTypeReference() {


		Map<String, String[]> dataSend = new HashMap<>();

		String[] id = {""};
		String[] approveTypeCode = {"0001"};
		String[] completeReason = {"0001"};
		String[] expenseType = {"2"};
		String[] createdBy = {""};
		String[] createdDate = {""};
		String[] updateBy = {""};
		String[] updateDate = {""};
		String[] parentId = {"1"};
		String[] csrfToken = {"720f92e8-cd08-4db9-83a6-fe851b05198b"};

		dataSend.put("id", id);
		dataSend.put("parentId", parentId);
		dataSend.put("csrfToken", csrfToken);
		dataSend.put("approveTypeCode", approveTypeCode);
		dataSend.put("completeReason", completeReason);
		dataSend.put("expenseType", expenseType);
		dataSend.put("createdBy", createdBy);
		dataSend.put("createdDate", createdDate);
		dataSend.put("updateBy", updateBy);
		dataSend.put("updateDate", updateDate);






		ResponseEntity<String> result = expenseTypeService.addExpenseTypeReference(dataSend);


		Map userMap = gson.fromJson(result.getBody(), Map.class);


		LOGGER.info(" ID ===========> {}",userMap.get("id"));
		assertNotNull(userMap);
		assertEquals(  "["+String.valueOf(userMap.get("approveTypeCode"))+"]"  , (Arrays.toString( dataSend.get("approveTypeCode"))).toString());
		assertEquals(  "["+String.valueOf(userMap.get("completeReason"))+"]"  , (Arrays.toString( dataSend.get("completeReason"))).toString());




	Double deleteId_tmp = Double.valueOf(String.valueOf(userMap.get("id")));
	Long idDelete = Math.round(deleteId_tmp);
		baseCommonService_Ref.delete(idDelete);

	}





}
