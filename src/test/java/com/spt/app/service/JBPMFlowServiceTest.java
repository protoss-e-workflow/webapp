package com.spt.app.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.spt.app.constant.ApplicationConstant;


@RunWith(SpringRunner.class)
@SpringBootTest
public class JBPMFlowServiceTest {
	
	JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).create();

	
	static final Logger LOGGER = LoggerFactory.getLogger(JBPMFlowServiceTest.class);
	
	
	@Autowired
	FlowManageService flowManageService;
	@Autowired
	JBPMFlowService jbpmFlowService;

	@Test
	public void findEmployeeProfileByEmployeeCode_ApproveTest() {
		String employeeStr = "karoons";
		String approveTypeStr = "0001";
		String expenseAmountStr = "MPG:HRAuth:0.99-HRAuth.H001;1.0";
		//===========================================================================================
		List<Map> lineApproveData = flowManageService.getAuthorizationForApproveByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName")); assertEquals("MR. BOONCHAI PIRIYASATIT",lineApproveData.get(0).get("name"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName")); assertEquals("MR. BOONCHAI PIRIYASATIT",lineApproveData.get(1).get("name"));
		//LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));
		//Ex. Data

	}



	@Test
	public void findEmployeeProfileByEmployeeCodeTest() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:Auth:0.99-Auth.F001;4000.0#MPG:Auth:0.99-Auth.F001;1000000.0";
		//===========================================================================================
		List<Map> lineApproveData = flowManageService.getAuthorizationForExpenseByEmployee(employeeStr, expenseAmountStr,approveTypeStr);
		assertEquals("Verify",lineApproveData.get(0).get("actionRoleName")); assertEquals("MR. KRISDA MONTHIENVICHIENCHAI",lineApproveData.get(0).get("name"));
		assertEquals("Approver",lineApproveData.get(1).get("actionRoleName")); assertEquals("MR. KRISDA MONTHIENVICHIENCHAI",lineApproveData.get(1).get("name"));
		assertEquals("Account",lineApproveData.get(2).get("actionRoleName")); assertEquals("Mrs. Praphaiporn Intaraaksorn",lineApproveData.get(2).get("name"));
		assertEquals("Finance",lineApproveData.get(3).get("actionRoleName")); assertEquals("Ms. Phanrapa Sangsawat",lineApproveData.get(3).get("name"));
		//LOGGER.info("==============================flow={}",gson.toJson(lineApproveData, List.class));
		//Ex. Data
		//[{"attorneyId":"202","actionRoleName":"Verify","name":"MR. KRISDA MONTHIENVICHIENCHAI","position":"Chief Executive Officer","actionRole":1},{"attorneyId":"202","actionRoleName":"Approver","name":"MR. KRISDA MONTHIENVICHIENCHAI","position":"Chief Executive Officer","actionRole":2},{"attorneyId":"209","actionRoleName":"Account","name":"Mrs. Praphaiporn Intaraaksorn","position":"","actionRole":4},{"attorneyId":"99999999","actionRoleName":"Finance","name":"Ms. Phanrapa Sangsawat","position":"Financial Officer","actionRole":5}]

	}


	@Test
	public void createDocumentFlowTest() {
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:Auth:0.99-Auth.F001;4000.0#MPG:Auth:0.99-Auth.F001;1000000.0";
		String docType = "EXP";
		String papsa = "1010-0001";
		String documentNumber = "EXP_1010_"+(new SimpleDateFormat("yyyyMMddhhmm")).format(new Date()) ;
		//===========================================================================================
		Map documentFlowData = flowManageService.createDocumentFlow(docType,employeeStr, expenseAmountStr,docType,documentNumber,approveTypeStr,papsa);
		List<Map> lineApprove = (List) documentFlowData.get("lineApprove");
		assertNotNull(documentFlowData.get("processId"));
		assertEquals("MR. KRISDA MONTHIENVICHIENCHAI",String.valueOf(lineApprove.get(0).get("name")));
		assertEquals("MR. KRISDA MONTHIENVICHIENCHAI",String.valueOf(lineApprove.get(1).get("name")));
		assertEquals("Mrs. Praphaiporn Intaraaksorn",String.valueOf(lineApprove.get(2).get("name")));
		assertEquals("Ms. Phanrapa Sangsawat",String.valueOf(lineApprove.get(3).get("name")));

	}




	@Test
	public void cancelDocumentFlowTest() {
		/* Prepare */
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:Auth:0.99-Auth.F001;4000.0#MPG:Auth:0.99-Auth.F001;1000000.0";
		String docType = "EXP";
		String papsa = "1010-0001";
		String documentNumber = "EXP_1010_"+(new SimpleDateFormat("yyyyMMddhhmm")).format(new Date()) ;
		//===========================================================================================
		Map documentFlowData = flowManageService.createDocumentFlow(docType,employeeStr, expenseAmountStr,docType,documentNumber,approveTypeStr,papsa);
		List<Map> lineApprove = (List) documentFlowData.get("lineApprove");
		String processId = (String) documentFlowData.get("processId");
		assertNotNull(processId);
		assertEquals("MR. KRISDA MONTHIENVICHIENCHAI",String.valueOf(lineApprove.get(0).get("name")));
		assertEquals("MR. KRISDA MONTHIENVICHIENCHAI",String.valueOf(lineApprove.get(1).get("name")));
		assertEquals("Mrs. Praphaiporn Intaraaksorn",String.valueOf(lineApprove.get(2).get("name")));
		assertEquals("Ms. Phanrapa Sangsawat",String.valueOf(lineApprove.get(3).get("name")));

		/* Execute */
		Map resultMapAction = flowManageService.cancelDocument(employeeStr, documentNumber);

		/* Check */
	}

	@Test
	public void approveDocumentFlowTest() {
		/* Prepare */
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:Auth:0.99-Auth.F001;4000.0#MPG:Auth:0.99-Auth.F001;1000000.0";
		String docType = "EXP";
		String papsa = "1010-0001";
		String documentNumber = "EXP_1010_"+(new SimpleDateFormat("yyyyMMddhhmm")).format(new Date()) ;
		//===========================================================================================
		Map documentFlowData = flowManageService.createDocumentFlow(docType,employeeStr, expenseAmountStr,docType,documentNumber,approveTypeStr,papsa);
		List<Map> lineApprove = (List) documentFlowData.get("lineApprove");
		String processId = (String) documentFlowData.get("processId");
		assertNotNull(processId);
		assertEquals("MR. KRISDA MONTHIENVICHIENCHAI",String.valueOf(lineApprove.get(0).get("name")));
		assertEquals("MR. KRISDA MONTHIENVICHIENCHAI",String.valueOf(lineApprove.get(1).get("name")));
		assertEquals("Mrs. Praphaiporn Intaraaksorn",String.valueOf(lineApprove.get(2).get("name")));
		assertEquals("Ms. Phanrapa Sangsawat",String.valueOf(lineApprove.get(3).get("name")));

		String documentFlow = (String) documentFlowData.get("documentFlow");

		/* Execute */
		flowManageService.approveDocument("krisdam", "VRF", documentNumber, docType, documentFlow, processId);

		/* Check */
		assertEquals("Account",jbpmFlowService.getCurrentNode(processId));
		assertEquals(ApplicationConstant.FLOW_STATUS_ACTIVE,jbpmFlowService.getActive(processId));

		/* Execute */
		flowManageService.approveDocument("praphaiporni", "ACC", documentNumber, docType, documentFlow, processId);

		/* Check */
		assertEquals("Finance",jbpmFlowService.getCurrentNode(processId));
		assertEquals(ApplicationConstant.FLOW_STATUS_ACTIVE,jbpmFlowService.getActive(processId));


		/* Execute */
		flowManageService.approveDocument("phanrapas", "FNC", documentNumber, docType, documentFlow, processId);

		/* Check */
		assertEquals("Finance",jbpmFlowService.getCurrentNode(processId));
		assertEquals(ApplicationConstant.FLOW_STATUS_INACTIVE,jbpmFlowService.getActive(processId));

	}




	@Test
	public void rejectDocumentFlowTest() {
		/* Prepare */
		String employeeStr = "karoons";
		String approveTypeStr = null;
		String expenseAmountStr = "MPG:Auth:0.99-Auth.F001;4000.0#MPG:Auth:0.99-Auth.F001;1000000.0";
		String docType = "EXP";
		String papsa = "1010-0001";
		String documentNumber = "EXP_1010_"+(new SimpleDateFormat("yyyyMMddhhmm")).format(new Date()) ;
		//===========================================================================================
		Map documentFlowData = flowManageService.createDocumentFlow(docType,employeeStr, expenseAmountStr,docType,documentNumber,approveTypeStr,papsa);
		List<Map> lineApprove = (List) documentFlowData.get("lineApprove");
		String processId = (String) documentFlowData.get("processId");
		assertNotNull(processId);
		assertEquals("MR. KRISDA MONTHIENVICHIENCHAI",String.valueOf(lineApprove.get(0).get("name")));
		assertEquals("MR. KRISDA MONTHIENVICHIENCHAI",String.valueOf(lineApprove.get(1).get("name")));
		assertEquals("Mrs. Praphaiporn Intaraaksorn",String.valueOf(lineApprove.get(2).get("name")));
		assertEquals("Ms. Phanrapa Sangsawat",String.valueOf(lineApprove.get(3).get("name")));

		String documentFlow = (String) documentFlowData.get("documentFlow");

		/* Execute */
		flowManageService.approveDocument("krisdam", "VRF", documentNumber, docType, documentFlow, processId);

		/* Check */
		assertEquals("Account",jbpmFlowService.getCurrentNode(processId));
		assertEquals(ApplicationConstant.FLOW_STATUS_ACTIVE,jbpmFlowService.getActive(processId));

		/* Execute */
		flowManageService.rejectDocument("praphaiporni", "ACC", documentNumber, docType, documentFlow, processId);

		/* Check */
		assertEquals("Account",jbpmFlowService.getCurrentNode(processId));
		assertEquals(ApplicationConstant.FLOW_STATUS_INACTIVE,jbpmFlowService.getActive(processId));




	}



}
