package com.spt.app;

import com.spt.app.spring.configuration.ApplicationInitializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Panupong  Chantaklang on 10/10/2017 .
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageBuilder {
    private static Logger LOGGER = LoggerFactory.getLogger(MessageBuilder.class);

    @Autowired
    ApplicationInitializer applicationInitializer;

    @Test
    public void genMessage(){
        LOGGER.info("-= GENERATE MESSAGE=-");
        applicationInitializer.springMessageBuilder();
        LOGGER.info("-= GENERATE COMPLETE=-");

    }


}
